/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
} from '@react-navigation/drawer';
import {createStackNavigator} from '@react-navigation/stack';
import {createSharedElementStackNavigator} from 'react-navigation-shared-element';
import HomeScreen from '../screens/HomeScreen';
import CryptoScreen from '../screens/CryptoScreen';
import CustomDrawer from '../components/CustomDrawer';
import QRScannerScreen from '../screens/QRScannerScreen';
import SupportScreen from '../screens/SupportScreen';
import TimelineScreen from '../screens/TimelineScreen';
import TimelineDetailsScreen from '../screens/TimelineDetailsScreen';
import SimplexScreen from '../screens/SimplexScreen';
import AssetScreen from '../screens/AssetScreen';
import InviteScreen from '../screens/InviteScreen';
import InviteDetailsScreen from '../screens/InviteDetailsScreen';
import UpdateHistoryScreen from '../screens/UpdateHistoryScreen';
import BlockCheckScreen from '../screens/BlockCheckScreen';
import MoneyMarketScreen from '../screens/MoneyMarketScreen';
import AdminScreen from '../screens/AdminScreen';
import ProfileSettingsScreen from '../screens/ProfileSettings/ProfileSettingsScreen';
import Configure2FAScreen from '../screens/ProfileSettings/Configure2FAScreen';
import InterestSettingScreen from '../screens/ProfileSettings/InterestSettingScreen';
import ChangePasswordScreen from '../screens/ProfileSettings/ChangePasswordScreen';
import NewWalletScreen from '../screens/NewWalletScreen';

const Drawer = createDrawerNavigator();

const CustomDrawerContent = (props) => (
  <DrawerContentScrollView {...props} horizontal scrollEnabled={false} nav>
    <CustomDrawer />
  </DrawerContentScrollView>
);

const TimelineTransitionStack = createSharedElementStackNavigator();

const TimelineTransitionNavigator = () => (
  <TimelineTransitionStack.Navigator
    initialRouteName="List"
    screenOptions={{
      headerShown: false,
      cardStyleInterpolator: ({current: {progress}}) => ({
        cardStyle: {opacity: progress},
      }),
    }}>
    <TimelineTransitionStack.Screen name="List" component={TimelineScreen} />
    <TimelineTransitionStack.Screen
      name="Details"
      component={TimelineDetailsScreen}
      sharedElements={(route, otherRoute, showing) => {
        const {item} = route.params;
        return [
          `item.${item.key}.photo`,
          `item.${item.key}.icon`,
          `item.${item.key}.name`,
          `item.${item.key}.fab`,
          'item.appBar',
          'item.breadCrumbs',
        ];
      }}
    />
  </TimelineTransitionStack.Navigator>
);

const InviteTransitionStack = createSharedElementStackNavigator();

const InviteTransitionNavigator = () => (
  <InviteTransitionStack.Navigator
    initialRouteName="List"
    screenOptions={{
      headerShown: false,
      cardStyleInterpolator: ({current: {progress}}) => ({
        cardStyle: {opacity: progress},
      }),
    }}>
    <InviteTransitionStack.Screen name="List" component={InviteScreen} />
    <InviteTransitionStack.Screen
      name="Details"
      component={InviteDetailsScreen}
      sharedElements={(route, otherRoute, showing) => {
        const {item, title} = route.params;
        return ['item.title', `item.${item.key}.item`, 'item.appBar'];
      }}
    />
  </InviteTransitionStack.Navigator>
);

const SettingsTransitionStack = createSharedElementStackNavigator();

const SettingsTransitionNavigator = () => (
  <SettingsTransitionStack.Navigator
    initialRouteName="List"
    screenOptions={{
      headerShown: false,
    }}>
    <SettingsTransitionStack.Screen
      name="List"
      component={ProfileSettingsScreen}
    />
    <SettingsTransitionStack.Screen
      name="2FASettings"
      component={Configure2FAScreen}
    />
    <SettingsTransitionStack.Screen
      name="InterestSettings"
      component={InterestSettingScreen}
    />
    <SettingsTransitionStack.Screen
      name="ChangePassword"
      component={ChangePasswordScreen}
    />
  </SettingsTransitionStack.Navigator>
);

const ChildStack = createStackNavigator();

const ChildNavigator = () => (
  <ChildStack.Navigator
    initialRouteName="Home"
    screenOptions={{headerShown: false}}>
    <ChildStack.Screen name="Home" component={HomeScreen} />
    <ChildStack.Screen name="Asset" component={AssetScreen} />
    <ChildStack.Screen name="Crypto" component={CryptoScreen} />
    <ChildStack.Screen name="Wallet" component={NewWalletScreen} />
    <ChildStack.Screen
      name="QRScanner"
      component={QRScannerScreen}
      options={{}}
    />
    <ChildStack.Screen name="Support" component={SupportScreen} />
    <ChildStack.Screen name="Simplex" component={SimplexScreen} />
    <ChildStack.Screen name="UpdateHistory" component={UpdateHistoryScreen} />
    <ChildStack.Screen name="Invite" component={InviteTransitionNavigator} />
    <ChildStack.Screen name="BlockCheck" component={BlockCheckScreen} />
    <ChildStack.Screen
      name="Timeline"
      component={TimelineTransitionNavigator}
    />
    <ChildStack.Screen name="MoneyMarket" component={MoneyMarketScreen} />
    <ChildStack.Screen name="AdminPage" component={AdminScreen} />
    <ChildStack.Screen
      name="Settings"
      component={SettingsTransitionNavigator}
    />
  </ChildStack.Navigator>
);

const AppDrawerNavigator = () => {
  return (
    <Drawer.Navigator
      initialRouteName="Home"
      drawerContent={CustomDrawerContent}
      drawerStyle={{width: 'auto'}}
      backBehavior={'history'}>
      <Drawer.Screen name="child" component={ChildNavigator} />
    </Drawer.Navigator>
  );
};

export default AppDrawerNavigator;
