import axios from 'axios';
import React, {useState, useRef, useEffect} from 'react';
import {Keyboard} from 'react-native';
import {useQuery} from 'react-query';
import {APP_CODE, GX_API_ENDPOINT} from '../configs';

/**
 * Returns if the keyboard is open / closed
 *
 * @return {bool} isOpen
 */
export function useKeyboardStatus() {
  const [isOpen, setIsOpen] = useState(false);
  const keyboardShowListener = useRef(null);
  const keyboardHideListener = useRef(null);

  useEffect(() => {
    keyboardShowListener.current = Keyboard.addListener('keyboardDidShow', () =>
      setIsOpen(true),
    );
    keyboardHideListener.current = Keyboard.addListener('keyboardDidHide', () =>
      setIsOpen(false),
    );

    return () => {
      setIsOpen(false);
      keyboardShowListener.current.remove();
      keyboardHideListener.current.remove();
    };
  }, []);

  return isOpen;
}

const getAppData = async () => {
  const resp = await axios.get(`${GX_API_ENDPOINT}/gxb/apps/get`, {
    params: {app_code: APP_CODE},
  });

  const appData = resp.data?.apps ? resp.data?.apps[0] : '';

  return appData;
};
export const useAppData = () => {
  const appData = useQuery('appData', getAppData);

  return appData;
};
