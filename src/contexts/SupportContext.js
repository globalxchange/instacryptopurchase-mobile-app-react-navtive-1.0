import React, {Component, createContext} from 'react';
import {supportTabs} from '../configs';

export const SupportContext = createContext();

export class SupportContextProvider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: supportTabs[0],
      selectedIssueType: null,
      selectedNature: null,
      selectedTxnType: null,
      requestForCall: false,
      isCallConnected: false,
      stream: null,
      shouldHangup: false,
      ticketIssueType: null,
      ticketIssueSubType: null,
      ticketMessage: '',
      ticketPhoneNumber: '',
      ticketEmailId: '',
      selectedLearnCategory: '',
      selectedImage: null,
    };
  }

  setActiveTab = (activeTab) => {
    this.setState({activeTab});
  };

  setSelectedIssueType = (selectedIssueType) => {
    this.setState({selectedIssueType});
  };

  setSelectedNature = (selectedNature) => {
    this.setState({selectedNature});
  };

  setSelectedTxnType = (selectedTxnType) => {
    this.setState({selectedTxnType});
  };

  setRequestForCall = (requestForCall) => {
    this.setState({requestForCall});
  };

  setStream = (stream) => {
    this.setState({stream});
  };

  setIsCallConnected = (isCallConnected) => {
    this.setState({isCallConnected});
  };

  setShouldHangUp = (shouldHangup) => {
    this.setState({shouldHangup});
  };

  setTicketIssueType = (ticketIssueType) => {
    this.setState({ticketIssueType});
  };

  setTicketIssueSubType = (ticketIssueSubType) => {
    this.setState({ticketIssueSubType});
  };

  setTicketMessage = (ticketMessage) => {
    this.setState({ticketMessage});
  };

  setSelectedLearnCategory = (selectedLearnCategory) => {
    this.setState({selectedLearnCategory});
  };

  setTicketPhoneNumber = (ticketPhoneNumber) => {
    this.setState({ticketPhoneNumber});
  };

  setTicketEmailId = (ticketEmailId) => {
    this.setState({ticketEmailId});
  };

  setSelectedImage = (selectedImage) => {
    this.setState({selectedImage});
  };

  clearTicketState = () => {
    this.setState({
      ticketIssueType: null,
      ticketIssueSubType: null,
      ticketMessage: '',
      ticketPhoneNumber: '',
      ticketEmailId: '',
      selectedLearnCategory: '',
      selectedImage: null,
    });
  };

  render() {
    const {children} = this.props;

    return (
      <SupportContext.Provider
        value={{
          ...this.state,
          setActiveTab: this.setActiveTab,
          setSelectedIssueType: this.setSelectedIssueType,
          setSelectedNature: this.setSelectedNature,
          setSelectedTxnType: this.setSelectedTxnType,
          setRequestForCall: this.setRequestForCall,
          setStream: this.setStream,
          setIsCallConnected: this.setIsCallConnected,
          setShouldHangUp: this.setShouldHangUp,
          setTicketIssueType: this.setTicketIssueType,
          setTicketIssueSubType: this.setTicketIssueSubType,
          setTicketMessage: this.setTicketMessage,
          setSelectedLearnCategory: this.setSelectedLearnCategory,
          setTicketPhoneNumber: this.setTicketPhoneNumber,
          setTicketEmailId: this.setTicketEmailId,
          setSelectedImage: this.setSelectedImage,
          clearTicketState: this.clearTicketState,
        }}>
        {children}
      </SupportContext.Provider>
    );
  }
}
