import React, {Component} from 'react';
import {createContext} from 'react';

const WithdrawalContext = createContext();

export class WithdrawalContextProvider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 0,
      activeWallet: '',
      vaultStep: 0,
      selectedGxVault: null,
      isCryptoFocused: true,
      cryptoInput: '',
      fiatInput: '',
      conversionRate: 1,
      isGXVaultSelectorExpanded: false,
      gxVaults: null,
      pathId: '',
    };
  }

  setStep = (step) => {
    this.setState({step});
  };
  setActiveWallet = (activeWallet) => {
    this.setState({activeWallet});
  };

  setVaultStep = (vaultStep) => {
    this.setState({vaultStep});
  };

  setActiveGxVault = (selectedGxVault) => {
    this.setState({selectedGxVault});
  };

  setIsCryptoFocused = (isCryptoFocused) => {
    this.setState({isCryptoFocused});
  };

  setCryptoInput = (cryptoInput) => {
    this.setState({cryptoInput: cryptoInput.toString()});
  };

  setFiatInput = (fiatInput) => {
    this.setState({fiatInput: fiatInput.toString()});
  };

  setIsGXVaultSelectorExpanded = (isGXVaultSelectorExpanded) => {
    this.setState({isGXVaultSelectorExpanded});
  };

  setGxVaults = (gxVaults) => {
    this.setState({gxVaults});
  };

  setPathId = (pathId) => {
    this.setState({pathId});
  };

  clearDepositState = () => {
    this.setState({
      step: 0,
      vaultStep: 0,
      selectedGxVault: null,
      isCryptoFocused: true,
      cryptoInput: '',
      fiatInput: '',
      conversionRate: 1,
      isGXVaultSelectorExpanded: false,
      pathId: '',
    });
  };

  render() {
    return (
      <WithdrawalContext.Provider
        value={{
          ...this.state,
          setStep: this.setStep,
          setActiveWallet: this.setActiveWallet,
          setVaultStep: this.setVaultStep,
          setActiveGxVault: this.setActiveGxVault,
          setIsCryptoFocused: this.setIsCryptoFocused,
          setCryptoInput: this.setCryptoInput,
          setFiatInput: this.setFiatInput,
          setIsGXVaultSelectorExpanded: this.setIsGXVaultSelectorExpanded,
          clearDepositState: this.clearDepositState,
          setGxVaults: this.setGxVaults,
          setPathId: this.setPathId,
        }}>
        {this.props.children}
      </WithdrawalContext.Provider>
    );
  }
}

export default WithdrawalContext;
