import React, {Component, createContext} from 'react';

export const DepositContext = createContext();

export class DepositContextProvider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 0,
      activeWallet: '',
      vaultStep: 0,
      selectedGxVault: null,
      isCryptoFocused: true,
      cryptoInput: '',
      fiatInput: '',
      conversionRate: 1,
      isGXVaultSelectorExpanded: false,
      gxVaults: null,
      instaDepositData: {
        selectedCountry: '',
        paymentCurrency: '',
        paymentType: '',
        selectedBanker: '',
      },
    };
  }

  setStep = (step) => {
    this.setState({step});
  };
  setActiveWallet = (activeWallet) => {
    this.setState({activeWallet});
  };

  setVaultStep = (vaultStep) => {
    this.setState({vaultStep});
  };

  setActiveGxVault = (selectedGxVault) => {
    this.setState({selectedGxVault});
  };

  setIsCryptoFocused = (isCryptoFocused) => {
    this.setState({isCryptoFocused});
  };

  setCryptoInput = (cryptoInput) => {
    this.setState({cryptoInput: cryptoInput.toString()});
  };

  setFiatInput = (fiatInput) => {
    this.setState({fiatInput: fiatInput.toString()});
  };

  setIsGXVaultSelectorExpanded = (isGXVaultSelectorExpanded) => {
    this.setState({isGXVaultSelectorExpanded});
  };

  setGxVaults = (gxVaults) => {
    this.setState({gxVaults});
  };

  setInstaDepositCountry = (selectedCountry) => {
    const {instaDepositData} = this.state;
    this.setState({instaDepositData: {...instaDepositData, selectedCountry}});
  };

  setCheckOutPaymentCurrency = (paymentCurrency) => {
    const {instaDepositData} = this.state;
    this.setState({instaDepositData: {...instaDepositData, paymentCurrency}});
  };

  setInstaDepositPaymentType = (paymentType) => {
    const {instaDepositData} = this.state;
    this.setState({instaDepositData: {...instaDepositData, paymentType}});
  };

  setInstaDepositSelectedBanker = (selectedBanker) => {
    const {instaDepositData} = this.state;
    this.setState({instaDepositData: {...instaDepositData, selectedBanker}});
  };

  clearDepositState = () => {
    this.setState({
      step: 0,
      vaultStep: 0,
      selectedGxVault: null,
      isCryptoFocused: true,
      cryptoInput: '',
      fiatInput: '',
      conversionRate: 1,
      isGXVaultSelectorExpanded: false,
    });
  };

  clearInstaDepositState = () => {
    this.setState({
      instaDepositData: {
        selectedCountry: '',
        paymentCurrency: '',
        paymentType: '',
        selectedBanker: '',
      },
    });
  };

  render() {
    return (
      <DepositContext.Provider
        value={{
          ...this.state,
          setStep: this.setStep,
          setActiveWallet: this.setActiveWallet,
          setVaultStep: this.setVaultStep,
          setActiveGxVault: this.setActiveGxVault,
          setIsCryptoFocused: this.setIsCryptoFocused,
          setCryptoInput: this.setCryptoInput,
          setFiatInput: this.setFiatInput,
          setIsGXVaultSelectorExpanded: this.setIsGXVaultSelectorExpanded,
          clearDepositState: this.clearDepositState,
          setGxVaults: this.setGxVaults,
          setInstaDepositCountry: this.setInstaDepositCountry,
          setCheckOutPaymentCurrency: this.setCheckOutPaymentCurrency,
          setInstaDepositPaymentType: this.setInstaDepositPaymentType,
          setInstaDepositSelectedBanker: this.setInstaDepositSelectedBanker,
          clearInstaDepositState: this.clearInstaDepositState,
        }}>
        {this.props.children}
      </DepositContext.Provider>
    );
  }
}

export default DepositContextProvider;
