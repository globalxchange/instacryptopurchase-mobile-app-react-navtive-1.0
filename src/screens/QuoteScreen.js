/* eslint-disable react-native/no-inline-styles */
import {useNavigation, useRoute} from '@react-navigation/native';
import Axios from 'axios';
import React, {useEffect, useRef, useState} from 'react';
import {useContext} from 'react';
import {
  BackHandler,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {WToast} from 'react-native-smart-tip';
import AppStatusBar from '../components/AppStatusBar';
import CurrencyEditView from '../components/CheckoutComponents/CurrencyEditView';
import CurrencyView from '../components/CheckoutComponents/CurrencyView';
import CheckoutEasyEntry from '../components/CheckoutEasyEntry';
import {APP_CODE, GX_API_ENDPOINT} from '../configs';
import ThemeData from '../configs/ThemeData';
import {AppContext} from '../contexts/AppContextProvider';
import {formatterHelper} from '../utils';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import {v4 as uuidv4} from 'uuid';
import CryHelper from '../utils/CryHelper';
import StartView from '../components/CheckoutComponents/StartView';
import LoadingAnimation from '../components/LoadingAnimation';
import CompleteView from '../components/CheckoutComponents/CompleteView';
import FeeAudit from '../components/CheckoutComponents/FeeAudit';
import PopupLayout from '../layouts/PopupLayout';
import QuoteConvertCurrencyPicker from '../components/QuoteConvertCurrencyPicker';
import FeeBreakdownFullView from '../components/FeeBreakdownFullView';

let axiosToken = null;

const QuoteScreen = () => {
  const navigation = useNavigation();

  const {params} = useRoute();

  const {
    updateWalletBalances,
    cryptoTableData,
    setSheetHide,
    getBlockCheckData,
  } = useContext(AppContext);

  const [path, setPath] = useState();
  const [isInstantFund, setIsInstantFund] = useState(false);
  const [fundCurrency, setFundCurrency] = useState('');
  const [fundValue, setFundValue] = useState('');
  const [minimumValue, setMinimumValue] = useState(0);
  const [showView, setShowView] = useState(false);
  const [spendValue, setSpendValue] = useState('');
  const [gettingValue, setGettingValue] = useState('');
  const [showEdit, setShowEdit] = useState();
  const [spendEdited, setSpendEdited] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [purchaseLoading, setPurchaseLoading] = useState(false);
  const [isSpendLoading, setIsSpendLoading] = useState(false);
  const [isGettingLoading, setIsGettingLoading] = useState(false);
  const [quoteResp, setQuoteResp] = useState();
  const [isCompeted, setIsCompeted] = useState(false);
  const [showStart, setShowStart] = useState(false);
  const [showFeeAudit, setShowFeeAudit] = useState(false);
  const [showFeeAuditInstant, setShowFeeAuditInstant] = useState(false);
  const [successResp, setSuccessResp] = useState('');
  const [coinPrice, setCoinPrice] = useState('');
  const [isConvertPopupOpen, setIsConvertPopupOpen] = useState(false);
  const [convertData, setConvertData] = useState();

  const spendLoaded = useRef(false);
  const getLoaded = useRef(false);

  const {
    pathData,
    sellCurrency,
    buyCurrency,
    selectedBanker,
    selectedCountry,
    paymentType,
    type,
    onClose,
  } = params;

  // console.log('Path Data', path);

  useEffect(() => {
    if (cryptoTableData && pathData) {
      pathData.forEach((pathItem) => {
        if (
          pathItem.country ===
            (selectedCountry.formData
              ? selectedCountry.formData.Name
              : selectedCountry.value) &&
          pathItem.to_currency === buyCurrency.coinSymbol &&
          pathItem.from_currency === sellCurrency.coinSymbol &&
          (pathItem.depositMethod === paymentType.code ||
            pathItem.paymentMethod === paymentType.code) &&
          pathItem.banker === selectedBanker.bankerTag
        ) {
          // console.log('Found Path', pathItem);
          if (pathItem.select_type === 'instantFund') {
            setIsInstantFund(true);
            const fund = cryptoTableData.find(
              (x) => x.coinSymbol === pathItem.deposit_currency,
            );
            setFundCurrency(fund);
            const minValue =
              pathItem.instant_fees.banker_fixed_fee +
              pathItem.instant_fees.gx_fixed_fee +
              pathItem.fund_fees.banker_fixed_fee +
              pathItem.fund_fees.gx_fixed_fee;

            getMinimumValue(minValue);
          } else {
            setIsInstantFund(false);
            getMinimumValue(pathItem.banker_fixed_fee + pathItem.gx_fixed_fee);
          }
          setPath(pathItem);
          return;
        }
      });
    }
  }, [params, cryptoTableData]);

  useEffect(() => {
    if (!getLoaded.current) {
      getApproxQuote(gettingValue, false, true);
    }
  }, [gettingValue]);

  useEffect(() => {
    if (!spendLoaded.current) {
      getApproxQuote(spendValue, true, true);
    }
  }, [spendValue]);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', onCancelPress);
    navigation.addListener('blur', onGoBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', onCancelPress);
      navigation.removeListener('blur', onGoBack);
    };
  }, []);

  const getMinimumValue = (value) => {
    Axios.get(`${GX_API_ENDPOINT}/forex/convert`, {
      params: {buy: buyCurrency.coinSymbol, from: 'USD'},
    })
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          const conversionRate =
            data[`usd_${buyCurrency.coinSymbol.toLowerCase()}`];

          setMinimumValue(conversionRate * value);
        } else {
          setMinimumValue(value);
        }
      })
      .catch((error) => {
        setMinimumValue(value);
        console.log('Conversion Rate error', error);
      })
      .finally(() => setShowView(true));
  };

  const onSpendEdit = (value) => {
    setSpendValue(value);
    setSpendEdited(true);
  };

  const onGetEdit = (value) => {
    setGettingValue(value);
    setSpendEdited(false);
  };

  const getApproxQuote = async (value, isSpending, showQuote) => {
    if (isNaN(parseFloat(value)) && !showQuote) {
      setGettingValue('');
      return WToast.show({
        data: 'Please Input A Valid value',
        position: WToast.position.TOP,
      });
    }
    if (!path || isNaN(parseFloat(value)) || parseFloat(value) <= 0) {
      return;
    }
    // if (!showQuote && isSpending && parseFloat(spendValue) < minimumValue) {
    //   return WToast.show({
    //     data: `The Minimum Value is ${formatterHelper(
    //       minimumValue,
    //       sellCurrency.coinSymbol,
    //     )}`,
    //     position: WToast.position.TOP,
    //   });
    // }

    if (axiosToken) {
      axiosToken.cancel();
      // setIsLoading(false);
    }

    if (!showQuote) {
      setIsLoading(true);
    }

    showQuote
      ? isSpending
        ? setIsGettingLoading(true)
        : setIsSpendLoading(true)
      : setPurchaseLoading(true);

    axiosToken = Axios.CancelToken.source();
    const profileId = await AsyncStorageHelper.getProfileId();
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    let postData = {
      email,
      token,
      app_code: APP_CODE,
      profile_id: profileId,
      stats: showQuote,
      identifier: uuidv4(),
      path_id: path.path_id,
    };

    if (isSpending) {
      postData = {
        ...postData,
        purchased_from: sellCurrency.coinSymbol,
        from_amount: parseFloat(value),
      };
    } else {
      postData = {
        ...postData,
        coin_purchased: buyCurrency.coinSymbol,
        purchased_amount: parseFloat(value),
      };
    }

    // console.log('Post Data', postData);

    const encryptedData = CryHelper.encryptPostData(postData);

    // setQuoteResp();

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/trade/execute`,
      {data: encryptedData},
      {
        cancelToken: axiosToken.token,
      },
    )
      .then((resp) => {
        const {data} = resp;

        console.log('Quote Resp', data);

        if (isInstantFund) {
          if (!showQuote && data?.status) {
            setSuccessResp(data);
            updateWalletBalances();
            getBlockCheckData();
            setIsCompeted(true);
            setTimeout(() => {
              setIsLoading(false);
            }, 1000);
            WToast.show({
              data: data.message,
              position: WToast.position.TOP,
            });
            setPurchaseLoading(false);
            return;
          }

          if (data?.status === false) {
            WToast.show({
              data: data.message,
              position: WToast.position.TOP,
            });
            setIsLoading(false);
            setPurchaseLoading(false);
            setIsCompeted(false);
            setShowStart(false);
            return;
          }

          const [fundQuote, instaQuote] = data;

          if (fundQuote.status && instaQuote.status) {
            setQuoteResp(data);

            const fundAmount = isSpending
              ? fundQuote.finalToAmount
              : fundQuote.finalFromAmount;

            const instantAmount = isSpending
              ? instaQuote.finalToAmount
              : instaQuote.finalFromAmount;

            setFundValue(formatterHelper(fundAmount, fundCurrency.coinSymbol));

            setCoinPrice(
              fundQuote?.final_1_to_currency_in_from_currency ||
                instaQuote?.final_1_to_currency_in_from_currency ||
                0,
            );

            isSpending
              ? (getLoaded.current = true)
              : (spendLoaded.current = true);

            isSpending
              ? setGettingValue(
                  formatterHelper(instantAmount, buyCurrency.coinSymbol),
                )
              : setSpendValue(
                  formatterHelper(instantAmount, sellCurrency.coinSymbol),
                );

            isSpending ? setIsGettingLoading(false) : setIsSpendLoading(false);
          } else {
            if (!fundQuote.status) {
              WToast.show({
                data: fundQuote.message,
                position: WToast.position.TOP,
              });
            }

            if (!instaQuote.status) {
              WToast.show({
                data: instaQuote.message,
                position: WToast.position.TOP,
              });
            }

            if (showQuote) {
              setSpendValue(formatterHelper(0, sellCurrency.coinSymbol));
              isSpending
                ? setIsGettingLoading(false)
                : setIsSpendLoading(false);
            } else {
              setIsLoading(false);
              setPurchaseLoading(false);
            }
          }
        } else {
          if (data.status) {
            if (!showQuote) {
              setSuccessResp(data);
              updateWalletBalances();
              getBlockCheckData();
              setIsCompeted(true);
              setTimeout(() => {
                setIsLoading(false);
              }, 1000);
              WToast.show({
                data: data.message,
                position: WToast.position.TOP,
              });
              setPurchaseLoading(false);
              return;
            }

            setQuoteResp(data);

            const cryptoAmount = isSpending
              ? data.finalToAmount
              : data.finalFromAmount;

            isSpending
              ? (getLoaded.current = true)
              : (spendLoaded.current = true);

            setCoinPrice(data?.final_1_to_currency_in_from_currency || 0);

            isSpending
              ? setGettingValue(
                  formatterHelper(cryptoAmount, buyCurrency.coinSymbol),
                )
              : setSpendValue(
                  formatterHelper(cryptoAmount, sellCurrency.coinSymbol),
                );

            isSpending ? setIsGettingLoading(false) : setIsSpendLoading(false);
          } else {
            if (data.message !== 'System Error') {
              WToast.show({
                data: data.message,
                position: WToast.position.TOP,
              });
            }
            if (showQuote) {
              setSpendValue(formatterHelper(0, sellCurrency.coinSymbol));
              isSpending
                ? setIsGettingLoading(false)
                : setIsSpendLoading(false);
            } else {
              setIsLoading(false);
              setPurchaseLoading(false);
            }
          }
        }
      })
      .catch((error) => {
        console.log('Error getting quote', error);
        if (!showQuote) {
          setIsLoading(false);
        }
      })
      .finally(() => {
        resetFlags();
      });
  };

  const resetFlags = () => {
    setTimeout(() => {
      getLoaded.current = false;
      spendLoaded.current = false;
    }, 200);
  };

  const buyClickHandler = async () => {
    if (purchaseLoading) {
      return;
    }

    const value = spendEdited ? spendValue : gettingValue;

    if (isNaN(parseFloat(value)) || !parseFloat(value)) {
      return WToast.show({
        data: 'Please Enter A Valid Value',
        position: WToast.position.TOP,
      });
    }

    // if (parseFloat(spendValue) < minimumValue) {
    //   return WToast.show({
    //     data: `The Minimum Value is ${formatterHelper(
    //       minimumValue,
    //       sellCurrency.coinSymbol,
    //     )}`,
    //     position: WToast.position.TOP,
    //   });
    // }

    // send the last input to trade api
    getApproxQuote(spendEdited ? spendValue : gettingValue, spendEdited, false);
  };

  const onStartPress = () => {
    if (purchaseLoading) {
      return;
    }

    const value = spendEdited ? spendValue : gettingValue;

    if (isNaN(parseFloat(value)) || !parseFloat(value)) {
      return WToast.show({
        data: 'Please Enter A Valid Value',
        position: WToast.position.TOP,
      });
    }

    setShowStart(true);
  };

  const onCancelPress = () => {
    if (showStart) {
      setShowStart(false);
    } else {
      navigation.goBack();
      setSheetHide(false);
    }
    return true;
  };

  const onGoBack = () => {
    if (Platform.OS === 'ios') {
      if (!isCompeted) {
        setSheetHide(false);
      }
    }
  };

  const onConvertItemSelected = (item) => {
    setConvertData(item);
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <View style={styles.container}>
        <AppStatusBar backgroundColor="white" barStyle="dark-content" />
        {showFeeAudit && (
          <FeeBreakdownFullView
            gettingValue={gettingValue}
            isInstantFund={isInstantFund}
            onClose={() => {
              setShowFeeAudit(false);
              setShowFeeAuditInstant(false);
            }}
            paymentCurrency={sellCurrency}
            quoteResp={quoteResp}
            selectedCrypto={buyCurrency}
            spendEdited={spendEdited}
            spendValue={spendValue}
            type={type}
            instantFundView={showFeeAuditInstant}
          />
        )}
        {isLoading ? (
          <View style={{flex: 1, justifyContent: 'center'}}>
            <LoadingAnimation />
          </View>
        ) : (
          <>
            {isCompeted || (
              <TouchableOpacity
                onPress={onCancelPress}
                style={styles.closeButton}>
                <Image
                  source={require('../assets/cancel-icon-colored.png')}
                  resizeMode="contain"
                  style={styles.closeIcon}
                />
              </TouchableOpacity>
            )}
            {showStart ? (
              isCompeted ? (
                <CompleteView
                  txnType={path?.select_type}
                  quoteResp={quoteResp}
                  sellCurrency={sellCurrency}
                  buyCurrency={buyCurrency}
                  txnId={
                    successResp?.complete_txn_id ||
                    successResp?.deposit_data?.Id ||
                    successResp?.payment_request_id
                  }
                  onClose={onClose}
                />
              ) : (
                <StartView
                  paymentType={paymentType}
                  onStart={buyClickHandler}
                />
              )
            ) : (
              <View style={styles.formContainer}>
                <View style={styles.bankerContainer}>
                  <Image
                    style={styles.bankerIcon}
                    resizeMode="contain"
                    source={{uri: selectedBanker.profilePicURL}}
                  />
                  <Text style={styles.bankerName}>
                    {selectedBanker.displayName}
                  </Text>
                </View>
                {showEdit ? (
                  <CurrencyEditView
                    coin={showEdit === 'Send' ? sellCurrency : buyCurrency}
                    value={showEdit === 'Send' ? spendValue : gettingValue}
                    title={
                      showEdit === 'Send' ? 'I Am Sending' : 'I Want To Receive'
                    }
                    onClose={() => setShowEdit()}
                    onChange={showEdit === 'Send' ? onSpendEdit : onGetEdit}
                  />
                ) : (
                  <>
                    <CurrencyView
                      coin={sellCurrency}
                      value={spendValue}
                      title="You Are Sending"
                      showFeeAudit
                      onAuditOpen={() => {
                        if (spendValue) {
                          setShowFeeAuditInstant(false);
                          setShowFeeAudit(true);
                        } else {
                          WToast.show({
                            data: 'Please Input A Value First',
                            position: WToast.position.TOP,
                          });
                        }
                      }}
                      onPress={() => setShowEdit('Send')}
                      isLoading={isSpendLoading}
                    />
                    {isInstantFund && (
                      <CurrencyView
                        coin={fundCurrency}
                        value={fundValue}
                        title="We Will Credit"
                        isLoading={isGettingLoading || isSpendLoading}
                        disabled
                      />
                    )}
                    <CurrencyView
                      coin={buyCurrency}
                      value={gettingValue}
                      title="You Are Receiving"
                      onPress={() => setShowEdit('Receive')}
                      isLoading={isGettingLoading}
                      showFeeAudit={isInstantFund}
                      onAuditOpen={() => {
                        if (spendValue) {
                          setShowFeeAuditInstant(true);
                          setShowFeeAudit(true);
                        } else {
                          WToast.show({
                            data: 'Please Input A Value First',
                            position: WToast.position.TOP,
                          });
                        }
                      }}
                    />
                    <CurrencyView
                      coin={convertData || sellCurrency}
                      value={coinPrice / (convertData?.convertValue || 1)}
                      title={`Price Of One ${buyCurrency?.coinName}`}
                      isLoading={isGettingLoading || isSpendLoading}
                      disabled
                      onCoinClick={() => setIsConvertPopupOpen(true)}
                    />
                  </>
                )}
              </View>
            )}
            {!showEdit && !isCompeted ? (
              <View style={styles.actionContainer}>
                {showStart ? (
                  <TouchableOpacity style={styles.easyEntryBtn}>
                    <Text style={styles.easyEntryText}>Tutorial</Text>
                  </TouchableOpacity>
                ) : (
                  <CheckoutEasyEntry
                    sellCurrency={sellCurrency}
                    buyCurrency={buyCurrency}
                    headerImage={require('../assets/insta-buy.png')}
                    setSellCurrencyInput={onSpendEdit}
                    disabled={
                      isInstantFund || type === 'Buy' || type === 'Sell'
                    }
                  />
                )}
                <TouchableOpacity
                  onPress={showStart ? buyClickHandler : onStartPress}
                  style={styles.startButton}>
                  <Text style={styles.startText}>
                    {showStart ? 'Transact' : 'Start'}
                  </Text>
                </TouchableOpacity>
              </View>
            ) : null}
          </>
        )}
        <QuoteConvertCurrencyPicker
          isOpen={isConvertPopupOpen}
          onClose={() => setIsConvertPopupOpen(false)}
          coinPrice={coinPrice || 0}
          sellCurrency={sellCurrency}
          buyCurrency={buyCurrency}
          onSelected={onConvertItemSelected}
        />
      </View>
    </SafeAreaView>
  );
};

export default QuoteScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 25,
    backgroundColor: 'white',
  },
  actionContainer: {
    flexDirection: 'row',
  },
  startButton: {
    backgroundColor: '#186AB4',
    height: 50,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    borderRadius: 6,
  },
  startText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
  },
  formContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  bankerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 30,
  },
  bankerIcon: {
    width: 40,
    height: 40,
    marginRight: 10,
  },
  bankerName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 23,
  },
  easyEntryBtn: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    flex: 1,
    marginRight: 15,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 6,
  },
  easyEntryText: {
    fontSize: 13,
    fontFamily: 'Montserrat-SemiBold',
    color: '#9A9A9A',
  },
  closeButton: {
    position: 'absolute',
    right: 20,
    top: 20,
    width: 30,
    height: 30,
    padding: 5,
    zIndex: 1,
  },
  closeIcon: {
    flex: 1,
    width: null,
    height: null,
  },
});
