import React, {useState} from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {SharedElement} from 'react-navigation-shared-element';
import ActionBar from '../components/ActionBar';
import BrokerSyncConfigure from '../components/BrokerSyncConfigure';
import AppMainLayout from '../layouts/AppMainLayout';
import Clipboard from '@react-native-community/clipboard';
import {useRoute} from '@react-navigation/native';
import * as WebBrowser from 'expo-web-browser';

const InviteDetailsScreen = () => {
  const {params} = useRoute();

  const [isCopied, setIsCopied] = useState(false);

  const item = params.item || '';

  const title = params.title || '';

  const onCopyHandler = () => {
    const link = item?.appLink || '';

    Clipboard.setString(link);

    setIsCopied(true);

    setTimeout(() => setIsCopied(false), 30000);
  };

  const openLink = () => {
    const link = item?.appLink || '';

    WebBrowser.openBrowserAsync(link);
  };

  return (
    <AppMainLayout disableBlockCheck>
      <SharedElement id={'item.appBar'}>
        <ActionBar />
      </SharedElement>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <SharedElement id={'item.title'}>
            <>
              <Text style={styles.header}>Your Network Is Your Net-Worth</Text>
              <Text style={styles.subHeader}>
                Pick One Of The Following Initiate Opt
              </Text>
            </>
          </SharedElement>
          <Text style={styles.title}>{title}</Text>
          <View style={styles.item}>
            <SharedElement id={`item.${item.key}.item`}>
              <View style={styles.headerContainer}>
                <Image
                  source={item.icon}
                  style={styles.image}
                  resizeMode="contain"
                />
                <View style={styles.itemHeader}>
                  <Text style={styles.preHeading}>{item.headerPre}</Text>
                  <Text style={styles.itemHeading}>{item.header}</Text>
                </View>
              </View>
            </SharedElement>
            <Text style={styles.desc}>{item.intro}</Text>
            <View style={styles.appLinkContainer}>
              <TouchableOpacity
                onPress={openLink}
                style={[styles.buttonFilled, {marginRight: 10}]}>
                <Text style={styles.buttonFilledText}>{item.buttons[0]}</Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={[styles.buttonOutlined, {opacity: 0.25}]}>
                  <Text style={styles.buttonOutlinedText}>
                    {item.buttons[1]}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            <Text style={styles.title}>Assisted Link</Text>
            <Text style={styles.desc}>{item.desc}</Text>
            <TouchableOpacity
              onPress={onCopyHandler}
              style={[styles.buttonOutlined, {marginTop: 20}]}>
              <Text style={styles.buttonOutlinedText}>
                {isCopied ? 'Copied' : 'Copy Link'}
              </Text>
            </TouchableOpacity>
            <View style={styles.additionalSharing}>
              <Text style={styles.title}>Additional Sharing Options</Text>
              <FlatList
                style={styles.list}
                horizontal
                showsHorizontalScrollIndicator={false}
                data={additionalShares}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => (
                  <View style={styles.shareItem}>
                    <Image
                      style={styles.shareIcon}
                      source={item.icon}
                      resizeMode="contain"
                    />
                  </View>
                )}
              />
            </View>
          </View>
        </View>
      </ScrollView>
      <BrokerSyncConfigure />
    </AppMainLayout>
  );
};

export default InviteDetailsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 25,
    paddingVertical: 20,
    backgroundColor: 'white',
  },
  header: {
    color: '#186AB4',
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
    fontSize: 24,
  },
  subHeader: {
    color: '#186AB4',
    fontFamily: 'Montserrat',
    textAlign: 'center',
    marginTop: 15,
    fontSize: 13,
  },
  list: {
    marginTop: 20,
  },
  title: {
    color: '#186AB4',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 25,
    marginBottom: 8,
    fontSize: 15,
  },
  item: {
    borderColor: '#EDEDED',
    borderWidth: 1,
    borderRadius: 10,
    padding: 20,
    marginTop: 10,
  },
  headerContainer: {
    flexDirection: 'row',
  },
  image: {
    width: 40,
    height: 40,
  },
  itemHeader: {
    flex: 1,
    marginLeft: 15,
  },
  preHeading: {
    color: '#186AB4',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
  },
  itemHeading: {
    color: '#186AB4',
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
  },
  desc: {
    marginTop: 15,
    color: '#186AB4',
    fontFamily: 'Montserrat',
    fontSize: 13,
  },
  appLinkContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  buttonFilled: {
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#186AB4',
    maxWidth: 150,
  },
  buttonFilledText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  buttonOutlined: {
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
    borderColor: '#186AB4',
    borderWidth: 1,
    maxWidth: 150,
  },
  buttonOutlinedText: {
    color: '#186AB4',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  additionalSharing: {
    opacity: 0.25,
  },
  shareItem: {
    marginRight: 15,
  },
  shareIcon: {
    width: 50,
    height: 50,
  },
});

const additionalShares = [
  {icon: require('../assets/share-icons/insta.png')},
  {icon: require('../assets/share-icons/youtube.png')},
  {icon: require('../assets/share-icons/twitter.png')},
  {icon: require('../assets/share-icons/snapchat.png')},
  {icon: require('../assets/share-icons/linkedin.png')},
];
