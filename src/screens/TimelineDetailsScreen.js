/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
} from 'react-native';
import AppMainLayout from '../layouts/AppMainLayout';
import AppStatusBar from '../components/AppStatusBar';
import ActionBar from '../components/ActionBar';
import {useNavigation, useRoute} from '@react-navigation/native';
import {SharedElement} from 'react-navigation-shared-element';
import * as WebBrowser from 'expo-web-browser';
import Breadcrumbs from '../components/TimeLine/Breadcrumbs';
import ThemeData from '../configs/ThemeData';
import TransactionCloud from '../components/TransactionCloud';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../configs';
import {AppContext} from '../contexts/AppContextProvider';
import LoadingAnimation from '../components/LoadingAnimation';

const TimelineDetailsScreen = () => {
  const navigation = useNavigation();
  const {params} = useRoute();

  const {getBlockCheckData} = useContext(AppContext);

  const [isTransactionCloudOpen, setIsTransactionCloudOpen] = useState(false);
  const [isActionsDisabled, setIsActionsDisabled] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const {
    item,
    tnxData,
    stepsArray,
    fromCurrency,
    toCurrency,
    paymentType,
    bankerName,
    countryData,
  } = params;

  useEffect(() => {
    const pos = stepsArray.findIndex((x) => x.key === item.key) || 0;
    const currentPos =
      stepsArray.findIndex((x) => x.key === tnxData.current_step) || 0;

    if (pos === currentPos + 1) {
      setIsActionsDisabled(false);
    } else {
      setIsActionsDisabled(true);
    }
  }, [stepsArray, item, tnxData]);

  const onCompletedClick = async (isCanceled) => {
    setIsLoading(true);
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    let postData = {
      token,
      admin_email: email,
      additional_data: {},
      id: tnxData._id,
    };

    let pos = 0;

    if (isCanceled) {
      postData = {...postData, step: stepsArray[stepsArray.length - 1].key};
      pos = stepsArray.length - 1;
    } else {
      postData = {...postData, step: item.key};
      pos = (stepsArray.findIndex((x) => x.key === item.key) || 0) + 1;
    }

    // console.log('Post Data', postData);

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/admin/update/txn/status`,
      postData,
    )
      .then(({data}) => {
        console.log('Data', data);

        if (data.status) {
          getBlockCheckData();
          navigation.replace('List', {pos});
        }
      })
      .catch((error) => {
        console.log('Error on update step', error);
      })
      .finally(() => setIsLoading(false));
  };

  // console.log('tnxData', tnxData);
  // console.log('stepsArray', stepsArray);
  // console.log('item', item);

  return (
    <AppMainLayout>
      <AppStatusBar backgroundColor="#186AB4" barStyle="light-content" />
      <SharedElement id={'item.appBar'}>
        <ActionBar />
      </SharedElement>
      <View style={{flex: 1}}>
        {isLoading && (
          <View
            style={[
              {
                ...StyleSheet.absoluteFill,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'white',
                zIndex: 1,
              },
            ]}>
            <LoadingAnimation />
          </View>
        )}

        <ScrollView
          bounces={false}
          showsVerticalScrollIndicator={false}
          style={styles.container}>
          <SharedElement id={'item.breadCrumbs'}>
            <Breadcrumbs
              fromCurrency={fromCurrency}
              paymentCurrency={toCurrency}
              paymentType={paymentType}
              selectedBanker={bankerName}
              selectedCountry={countryData}
            />
          </SharedElement>
          <SharedElement id={`item.${item.key}.photo`}>
            <Image
              source={
                item.thumbnail
                  ? {uri: item.thumbnail}
                  : require('../assets/step-placeholder.png')
              }
              resizeMode="cover"
              style={styles.coverImage}
            />
          </SharedElement>
          <View style={styles.detailsContainer}>
            <SharedElement id={`item.${item.key}.icon`}>
              <Image
                source={
                  item.icon
                    ? {uri: item.icon}
                    : require('../assets/step-icon-placeholder.png')
                }
                style={styles.bankerIcon}
              />
            </SharedElement>
            <Text style={styles.bankerName}>{item.name}</Text>
            <View style={styles.actionContainer}>
              <TouchableOpacity style={styles.actionButton}>
                <Image
                  source={require('../assets/mail-icon-colored.png')}
                  style={styles.actionIcon}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <TouchableOpacity style={styles.actionButton}>
                <Image
                  source={require('../assets/chats-io-icon-colored.png')}
                  style={styles.actionIcon}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <TouchableOpacity style={styles.actionButton}>
                <Image
                  source={require('../assets/onhold-icon-colored.png')}
                  style={styles.actionIcon}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  WebBrowser.openBrowserAsync(item.howtovideolink || '')
                }
                style={styles.actionButton}>
                <Image
                  source={require('../assets/youtube-icon-colored.png')}
                  style={styles.actionIcon}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
            <Text style={styles.bankerDetails}>{item.description || ''}</Text>
          </View>
          <View style={styles.relatedContainer}>
            <Text style={styles.relatedHeader}>Related Institution</Text>
            <View style={styles.relatedItem}>
              <View style={styles.imageContainer}>
                <Image
                  source={require('../assets/step-icon-placeholder.png')}
                  style={styles.images}
                />
              </View>
              <View style={styles.relatedDetailsContainer}>
                <Text style={styles.relatedBankerHeader}>Institution</Text>
                <Text style={styles.relatedBankerName}>Canada</Text>
                <Text style={styles.relatedLeanMore}>
                  Learn More About The Involvement Of This Institution
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.screenshotContainer}>
            <Text style={styles.relatedHeader}>Screenshots</Text>
            <FlatList
              showsHorizontalScrollIndicator={false}
              horizontal
              data={[]}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item, index}) => (
                <View style={styles.screenshotItem}>
                  <Image
                    source={{uri: item}}
                    style={styles.screenshotText}
                    resizeMode="cover"
                  />
                </View>
              )}
              ListEmptyComponent={
                <View style={styles.emptyContainer}>
                  <Text style={styles.emptyText}>No Screenshots Available</Text>
                </View>
              }
            />
          </View>
        </ScrollView>
        <View style={[styles.txnActions]}>
          <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            style={[isActionsDisabled && {opacity: 0.5}]}>
            <TouchableOpacity
              disabled={isActionsDisabled}
              onPress={() => setIsTransactionCloudOpen(true)}
              style={styles.txnActionsItem}>
              <Image
                style={styles.txnActionIcon}
                resizeMode="contain"
                source={require('../assets/transaction-cloud-icon.png')}
              />
              <Text style={styles.txnActionsText}>Upload File</Text>
            </TouchableOpacity>
            <TouchableOpacity
              disabled={isActionsDisabled}
              onPress={() => onCompletedClick(false)}
              style={styles.txnActionsItem}>
              <Text style={styles.txnActionsText}>I’ve Completed This</Text>
            </TouchableOpacity>
            {tnxData?.txn_type === 'withdraw' || (
              <TouchableOpacity
                disabled={isActionsDisabled}
                onPress={() => onCompletedClick(true)}
                style={styles.txnActionsItem}>
                <Text style={styles.txnActionsText}>Terminate</Text>
              </TouchableOpacity>
            )}
          </ScrollView>
        </View>
        {/* <FloatingButton
          onPress={navigation.goBack}
          icon={require('../assets/cancel-icon-white.png')}
        /> */}
        <TransactionCloud
          isOpen={isTransactionCloudOpen}
          setIsOpen={setIsTransactionCloudOpen}
          txnId={tnxData._id}
        />
      </View>
    </AppMainLayout>
  );
};

export default TimelineDetailsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  coverImage: {
    height: 250,
    width: '100%',
  },
  detailsContainer: {
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingBottom: 25,
  },
  bankerIcon: {
    height: 80,
    width: 80,
    marginTop: -40,
  },
  bankerName: {
    color: '#186AB4',
    fontFamily: 'Montserrat-Bold',
    fontSize: 22,
    marginBottom: 10,
    marginTop: 10,
    textAlign: 'center',
  },
  actionContainer: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  actionButton: {
    marginHorizontal: 10,
    height: 30,
    width: 30,
    padding: 5,
  },
  actionIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  bankerDetails: {
    textAlign: 'center',
    fontFamily: 'Montserrat',
    fontSize: 11,
    color: '#9A9A9A',
  },
  relatedContainer: {
    paddingHorizontal: 20,
    marginBottom: 15,
  },
  relatedHeader: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#186AB4',
    marginBottom: 5,
  },
  relatedItem: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
  },
  imageContainer: {
    height: '100%',
    width: 90,
    borderRightColor: '#EBEBEB',
    borderRightWidth: 1,
  },
  images: {
    flex: 1,
    height: null,
    width: null,
  },
  relatedDetailsContainer: {
    flex: 1,
    padding: 10,
  },
  relatedBankerHeader: {
    color: '#186AB4',
    fontFamily: 'Montserrat-Bold',
  },
  relatedBankerName: {
    color: '#186AB4',
    fontFamily: 'Montserrat',
    fontSize: 10,
    marginBottom: 10,
  },
  relatedLeanMore: {
    color: '#186AB4',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 8,
    textDecorationLine: 'underline',
  },
  screenshotContainer: {
    paddingHorizontal: 20,
    marginBottom: 15,
  },
  screenshotItem: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    height: 100,
    width: 100,
    marginRight: 5,
  },
  screenshotText: {
    flex: 1,
    height: null,
    width: null,
  },
  emptyText: {
    fontSize: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#186AB4',
    textAlign: 'center',
  },
  txnActions: {
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingBottom: 25,
    paddingTop: 10,
    paddingHorizontal: 20,
  },
  txnActionsItem: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 8,
    marginRight: 10,
  },
  txnActionIcon: {width: 20, height: 20, marginRight: 10},
  txnActionsText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 10,
    color: '#999999',
  },
});
