import React from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import {WebView} from 'react-native-webview';

const {width, height} = Dimensions.get('window');

const SimplexScreen = () => {
  return (
    <View style={styles.container}>
      <WebView source={{uri: 'https://instacryptopurchase.com/simplex'}} />
    </View>
  );
};

export default SimplexScreen;

const styles = StyleSheet.create({
  container: {
    width,
    height,
  },
});
