/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useContext} from 'react';
import {BackHandler, StyleSheet, Dimensions} from 'react-native';
import ActionBar from '../components/ActionBar';
import CryptoSwitcher from '../components/CryptoSwitcher';
import CryptoDetailsCard from '../components/CryptoDetailsCard';
import AppStatusBar from '../components/AppStatusBar';
import BuyBottomSheet from '../components/BuyBottomSheet';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from '../layouts/AppMainLayout';
import Animated from 'react-native-reanimated';
import LoadingAnimation from '../components/LoadingAnimation';
import SellBottomSheet from '../components/SellBottomSheet';
import TransactionAudit from '../components/TransactionAudit';
import DepositBottomSheet from '../components/DepositBottomSheet';
import WithdrawBottomSheet from '../components/WithdrawBottomSheet';

const {width, height} = Dimensions.get('window');

const CryptoScreen = ({route, navigation}) => {
  const {crypto} = route.params;

  const {setSelectedCrypto, isLoggedIn, setActiveRoute, pathData} = useContext(
    AppContext,
  );

  const [activeCrypto, setActiveCrypto] = useState();
  const [isLoading, setIsLoading] = useState(true);

  const [isAuditOpen, setIsAuditOpen] = useState(false);
  const [selectedAuditTxn, setSelectedAuditTxn] = useState();

  const [isBuyBottomSheetOpen, setIsBuyBottomSheetOpen] = useState(false);
  const [isSellBottomSheetOpen, setIsSellBottomSheetOpen] = useState(false);
  const [isDepositBottomSheetOpen, setIsDepositBottomSheetOpen] = useState(
    false,
  );
  const [isWithdrawBottomSheetOpen, setIsWithdrawBottomSheetOpen] = useState(
    false,
  );

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);

    navigation.addListener('focus', onScreenFocus);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);

      navigation.removeListener('focus', onScreenFocus);
    };
  }, []);

  useEffect(() => {
    if (crypto) {
      setSelectedCrypto(crypto);
      setActiveCrypto(crypto);
    }
  }, [crypto]);

  useEffect(() => {
    if (activeCrypto && activeCrypto.noOfVendor === undefined) {
      getCryptoDetails(activeCrypto);
    }
  }, [activeCrypto]);

  const buyClickHandler = () => {
    if (isLoggedIn) {
      setIsBuyBottomSheetOpen(true);
    } else {
      navigation.navigate('Login');
    }
  };

  const sellClickHandler = () => {
    if (isLoggedIn) {
      setIsSellBottomSheetOpen(true);
    } else {
      navigation.navigate('Login');
    }
  };

  const depositClickHandler = () => {
    if (isLoggedIn) {
      setIsDepositBottomSheetOpen(true);
    } else {
      navigation.navigate('Login');
    }
  };

  const withdrawClickHandler = () => {
    if (isLoggedIn) {
      setIsWithdrawBottomSheetOpen(true);
    } else {
      navigation.navigate('Login');
    }
  };

  const getCryptoDetails = (currentCrypto) => {
    setIsLoading(true);

    let noOfVendor = 0;

    pathData.forEach((pathItem) => {
      if (pathItem.to_currency === currentCrypto.coinSymbol) {
        noOfVendor += 1;
      }
    });

    const parsedCrypto = {...currentCrypto, noOfVendor};
    setActiveCrypto(parsedCrypto);
    setSelectedCrypto(parsedCrypto);
    setIsLoading(false);
  };

  const handleBack = () => {
    navigation.goBack();
    return true;
  };

  const onScreenFocus = (paylod) => {
    setActiveRoute('Home');
  };

  const changeActiveCrypto = (item) => {
    if (item.noOfVendor === undefined) {
      getCryptoDetails(item);
    }
  };

  const openTxnAudit = (txn) => {
    setSelectedAuditTxn(txn);
    setIsAuditOpen(true);
  };

  return (
    <AppMainLayout>
      <AppStatusBar backgroundColor="#186AB4" barStyle="light-content" />
      {isLoading && (
        <Animated.View style={[styles.loadingContainer]}>
          <LoadingAnimation />
        </Animated.View>
      )}
      <ActionBar />
      <CryptoSwitcher
        activeCrypto={activeCrypto}
        setActiveCrypto={changeActiveCrypto}
      />
      <CryptoDetailsCard
        activeCrypto={activeCrypto}
        buyClickHandler={buyClickHandler}
        isBottomSheetOpen={isBuyBottomSheetOpen}
        sellClickHandler={sellClickHandler}
        depositClickHandler={depositClickHandler}
        withdrawClickHandler={withdrawClickHandler}
      />
      <BuyBottomSheet
        isBottomSheetOpen={isBuyBottomSheetOpen}
        setIsBottomSheetOpen={setIsBuyBottomSheetOpen}
        activeCrypto={activeCrypto}
        openTxnAudit={openTxnAudit}
      />
      <SellBottomSheet
        isBottomSheetOpen={isSellBottomSheetOpen}
        setIsBottomSheetOpen={setIsSellBottomSheetOpen}
        activeCrypto={activeCrypto}
        openTxnAudit={openTxnAudit}
      />
      <DepositBottomSheet
        isBottomSheetOpen={isDepositBottomSheetOpen}
        setIsBottomSheetOpen={setIsDepositBottomSheetOpen}
        activeCrypto={activeCrypto}
        openTxnAudit={openTxnAudit}
      />
      <WithdrawBottomSheet
        isBottomSheetOpen={isWithdrawBottomSheetOpen}
        setIsBottomSheetOpen={setIsWithdrawBottomSheetOpen}
        activeCrypto={activeCrypto}
        openTxnAudit={openTxnAudit}
      />
      <TransactionAudit
        selectedAuditTxn={selectedAuditTxn}
        isAuditOpen={isAuditOpen}
        setIsAuditOpen={setIsAuditOpen}
      />
    </AppMainLayout>
  );
};

export default CryptoScreen;

const styles = StyleSheet.create({
  loadingContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width,
    height: height - 190,
    backgroundColor: 'white',
    zIndex: 10,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 2,
  },
});
