import React, {useEffect, useRef, useContext, useState} from 'react';
import {BackHandler, StyleSheet, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import HomeCover from '../components/HomeCover';
import HomeListSwitcher from '../components/HomeListSwitcher';
import HomeList from '../components/HomeList';
import {useRoute, useNavigation} from '@react-navigation/native';
import AppStatusBar from '../components/AppStatusBar';
import {Transitioning, Transition} from 'react-native-reanimated';
import {AppContext} from '../contexts/AppContextProvider';
import ComingSoonFragment from '../components/ComingSoonFragment';
import AppMainLayout from '../layouts/AppMainLayout';
import MethodsList from '../components/MethodsList';
import BankersList from '../components/BankersList';
import TellersList from '../components/TellersList';
import CountryList from '../components/CountryList';
import AddMethods from '../components/AddMethods';
import MainFooter from '../components/MainFooter';
import {GLOBAL_COUNTRY, listCategories} from '../configs';
import SearchLayout from '../layouts/SearchLayout';

const HomeScreen = () => {
  const {
    isCountrySelectorActive,
    setIsCountrySelectorActive,
    activeListCategory,
    setActiveRoute,
    homeSearchInput,
    filterActiveCountry,
    setActiveListCategory,
    setHomeSearchInput,
    setFilterActiveCountry,
  } = useContext(AppContext);

  const [activeFragment, setActiveFragment] = useState();
  const [searchPlaceHolderText, setSearchPlaceHolderText] = useState('');
  const [searchCountryText, setSearchCountryText] = useState('');
  const [addModalOpen, setAddModalOpen] = useState(false);
  const [isShowSearch, setIsShowSearch] = useState(false);
  const [searchList, setSearchList] = useState();

  const navigation = useNavigation();

  const route = useRoute();

  const transitionViewRef = useRef();
  const searchCallback = useRef(() => {});

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);

    navigation.addListener('focus', onScreenFocus);
    navigation.addListener('blur', onScreeBlur);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);

      navigation.removeListener('focus', onScreenFocus);
      navigation.removeListener('blur', onScreeBlur);
    };
  }, []);

  useEffect(() => {
    if (filterActiveCountry) {
      if (filterActiveCountry.value === 'Worldwide') {
        setSearchCountryText('Globally');
      } else {
        setSearchCountryText(`In ${filterActiveCountry.value}`);
      }
    }
  }, [filterActiveCountry]);

  useEffect(() => {
    if (transitionViewRef.current) {
      transitionViewRef.current.animateNextTransition();
    }

    if (isCountrySelectorActive) {
      setSearchPlaceHolderText('Search Available Countries');
    } else {
      switch (activeListCategory.title) {
        case 'Crypto':
          setSearchPlaceHolderText('Search Crypto');
          break;
        case 'Fiat':
          setSearchPlaceHolderText('Search Fiat');
          break;
        case 'Methods':
          setSearchPlaceHolderText('Search Methods');
          break;
        case 'Bankers':
          setSearchPlaceHolderText('Search Bankers');
          break;
        case 'Tellers':
          setSearchPlaceHolderText('Search Tellers');
          break;
        default:
          setSearchPlaceHolderText('Search Crypto');
      }
    }
  }, [isCountrySelectorActive]);

  useEffect(() => {
    setIsCountrySelectorActive(false);
    switch (activeListCategory.title) {
      case 'Crypto':
        setSearchPlaceHolderText('Search Crypto');
        setActiveFragment(
          <HomeList
            setSearchList={setSearchList}
            setSearchCallback={setSearchCallback}
          />,
        );
        break;
      case 'Fiat':
        setSearchPlaceHolderText('Search Fiat');
        setActiveFragment(
          <HomeList
            showFiat
            setSearchList={setSearchList}
            setSearchCallback={setSearchCallback}
          />,
        );
        break;
      case 'Methods':
        setSearchPlaceHolderText('Search Methods');
        setActiveFragment(
          <MethodsList
            setSearchList={setSearchList}
            setSearchCallback={setSearchCallback}
          />,
        );
        break;
      case 'Bankers':
        setSearchPlaceHolderText('Search Bankers');
        setActiveFragment(
          <BankersList
            setSearchList={setSearchList}
            setSearchCallback={setSearchCallback}
          />,
        );
        break;
      case 'Tellers':
        setSearchPlaceHolderText('Search Tellers');
        setActiveFragment(
          <TellersList
            setSearchList={setSearchList}
            setSearchCallback={setSearchCallback}
          />,
        );
        break;
      case 'Country':
        setSearchList();
        setSearchCallback();
        setIsCountrySelectorActive(true);
        break;
      default:
        setActiveFragment(<ComingSoonFragment key={Date.now()} />);
    }
  }, [activeListCategory]);

  const transition = (
    <Transition.Together>
      <Transition.In
        type="slide-top"
        durationMs={200}
        interpolation="easeInOut"
      />
    </Transition.Together>
  );

  const setSearchCallback = (callback) => {
    // console.log('Callback', callback);
    searchCallback.current = callback;
  };

  const handleBack = async () => {
    if (route.name === 'Home') {
      BackHandler.exitApp();
    }
    return true;
  };

  const onScreenFocus = (paylod) => {
    setActiveRoute('Home');
    setActiveListCategory(listCategories[0]);
  };

  const onScreeBlur = () => {
    setIsCountrySelectorActive(false);
    setHomeSearchInput('');
    setFilterActiveCountry(GLOBAL_COUNTRY);
    setIsShowSearch(false);
  };

  return (
    <AppMainLayout>
      <AppStatusBar backgroundColor="#186AB4" barStyle="light-content" />
      <ActionBar showSend />
      <View style={styles.container}>
        <HomeCover showHeader={isShowSearch} />
        <Transitioning.View
          style={[styles.transitionContainer]}
          ref={transitionViewRef}
          transition={transition}>
          <HomeListSwitcher
            openAddModal={() => setAddModalOpen(true)}
            isShowSearch={isShowSearch}
            openSearch={() => setIsShowSearch(true)}
            openCountry={() => setIsCountrySelectorActive(true)}
            isCountryOpen={isCountrySelectorActive}
          />
        </Transitioning.View>
        {isShowSearch && (
          <View style={styles.searchContainer}>
            <SearchLayout
              onBack={() => setIsShowSearch(false)}
              value={homeSearchInput}
              setValue={setHomeSearchInput}
              onSubmit={(_, item) =>
                searchCallback.current ? searchCallback.current(item) : null
              }
              placeholder={
                isCountrySelectorActive
                  ? searchPlaceHolderText
                  : `${searchPlaceHolderText} ${searchCountryText}`
              }
              list={searchList || []}
              showUserList
              filters={listCategories}
              setSelectedFilter={setActiveListCategory}
              selectedFilter={activeListCategory}
              dontFilter
            />
          </View>
        )}
        {isCountrySelectorActive && (homeSearchInput || isShowSearch) ? (
          <CountryList />
        ) : (
          activeFragment
        )}
        <MainFooter />
        <AddMethods
          isBottomSheetOpen={addModalOpen}
          setIsBottomSheetOpen={setAddModalOpen}
        />
      </View>
    </AppMainLayout>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {flex: 1},
  transitionContainer: {
    backgroundColor: 'white',
    paddingTop: 10,
  },
  fragmentContainer: {flex: 1},
  searchContainer: {
    ...StyleSheet.absoluteFill,
    backgroundColor: 'white',
    zIndex: 10,
  },
});
