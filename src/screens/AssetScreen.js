/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  BackHandler,
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';
import {useNavigation, useRoute} from '@react-navigation/native';
import AppMainLayout from '../layouts/AppMainLayout';
import AppStatusBar from '../components/AppStatusBar';
import ActionBar from '../components/ActionBar';
import FastImage from 'react-native-fast-image';
import AssetNavigator from '../components/AssetNavigator';
import AssetNavigationViews from '../components/AssetNavigationViews';
import CryptoSwitcher from '../components/CryptoSwitcher';
import LoadingAnimation from '../components/LoadingAnimation';
import {Transition, Transitioning} from 'react-native-reanimated';
import BuyBottomSheet from '../components/BuyBottomSheet';
import SellBottomSheet from '../components/SellBottomSheet';
import DepositBottomSheet from '../components/DepositBottomSheet';
import WithdrawBottomSheet from '../components/WithdrawBottomSheet';
import TransactionAudit from '../components/TransactionAudit';
import CryptoSwitchSearch from '../components/CryptoSwitchSearch';
import {
  getUriImage,
  getAssetData,
  usdValueFormatter,
  formatterHelper,
} from '../utils';
import ThemeData from '../configs/ThemeData';

const {width, height} = Dimensions.get('window');

const AssetScreen = () => {
  const {
    setSelectedCrypto,
    isLoggedIn,
    setActiveRoute,
    pathData,
    walletCoinData,
    getWalletCoinData,
  } = useContext(AppContext);

  useEffect(() => {
    if (!walletCoinData) {
      getWalletCoinData();
    }
  }, []);

  const {
    params: {crypto},
  } = useRoute();

  const navigation = useNavigation();

  const [activeCrypto, setActiveCrypto] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [showSwitcher, setShowSwitcher] = useState(false);

  const [isAuditOpen, setIsAuditOpen] = useState(false);
  const [selectedAuditTxn, setSelectedAuditTxn] = useState();

  const [checkoutPayload, setCheckoutPayload] = useState('');

  const [isBuyBottomSheetOpen, setIsBuyBottomSheetOpen] = useState(false);
  const [isSellBottomSheetOpen, setIsSellBottomSheetOpen] = useState(false);
  const [isDepositBottomSheetOpen, setIsDepositBottomSheetOpen] = useState(
    false,
  );
  const [isWithdrawBottomSheetOpen, setIsWithdrawBottomSheetOpen] = useState(
    false,
  );
  const [isCoinSearchOpen, setIsCoinSearchOpen] = useState(false);

  const switcherRef = useRef();

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);

    navigation.addListener('focus', onScreenFocus);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);

      navigation.removeListener('focus', onScreenFocus);
    };
  }, []);

  useEffect(() => {
    if (crypto) {
      setSelectedCrypto(crypto);
      setActiveCrypto(crypto);
    }
  }, [crypto]);

  useEffect(() => {
    if (activeCrypto && activeCrypto.noOfVendor === undefined) {
      getCryptoDetails(activeCrypto);
    }
  }, [activeCrypto]);

  const handleBack = () => {
    navigation.goBack();
    return true;
  };

  const onScreenFocus = (paylod) => {
    setActiveRoute('Home');
  };

  const buyClickHandler = () => {
    if (isLoggedIn) {
      setIsBuyBottomSheetOpen(true);
    } else {
      navigation.navigate('Login');
    }
  };

  const sellClickHandler = () => {
    if (isLoggedIn) {
      setIsSellBottomSheetOpen(true);
    } else {
      navigation.navigate('Login');
    }
  };

  const depositClickHandler = () => {
    if (isLoggedIn) {
      setIsDepositBottomSheetOpen(true);
    } else {
      navigation.navigate('Login');
    }
  };

  const withdrawClickHandler = () => {
    if (isLoggedIn) {
      setIsWithdrawBottomSheetOpen(true);
    } else {
      navigation.navigate('Login');
    }
  };

  const getCryptoDetails = (currentCrypto) => {
    setIsLoading(true);

    let noOfVendor = 0;

    pathData.forEach((pathItem) => {
      if (pathItem.to_currency === currentCrypto.coinSymbol) {
        noOfVendor += 1;
      }
    });

    const parsedCrypto = {...currentCrypto, noOfVendor};
    setActiveCrypto(parsedCrypto);
    setSelectedCrypto(parsedCrypto);
    setIsLoading(false);
  };

  const changeActiveCrypto = (item) => {
    if (item.noOfVendor === undefined) {
      getCryptoDetails(item);
    }
  };

  const showSwitcherHandler = () => {
    setShowSwitcher(!showSwitcher);
    if (switcherRef.current) {
      switcherRef.current.animateNextTransition();
    }
  };

  const openTxnAudit = (txn) => {
    setSelectedAuditTxn(txn);
    setIsAuditOpen(true);
  };

  const switcherTransition = (
    <Transition.Together>
      <Transition.Change />
    </Transition.Together>
  );

  // console.log(
  //   'getAssetData(activeCrypto?.coinSymbol, walletCoinData)',
  //   getAssetData(activeCrypto?.coinSymbol, walletCoinData),
  // );

  return (
    <AppMainLayout>
      <AppStatusBar backgroundColor="#186AB4" barStyle="light-content" />
      {isLoading && (
        <View style={[styles.loadingContainer]}>
          <LoadingAnimation />
        </View>
      )}
      <ActionBar />
      <View style={styles.appContainer}>
        <Transitioning.View
          ref={switcherRef}
          transition={switcherTransition}
          style={[
            {
              display: isCoinSearchOpen ? 'none' : 'flex',
              maxHeight: showSwitcher ? 200 : 0,
              overflow: 'hidden',
            },
          ]}>
          <CryptoSwitcher
            activeCrypto={activeCrypto}
            setActiveCrypto={changeActiveCrypto}
            isCoinSearchOpen={isCoinSearchOpen}
            setIsCoinSearchOpen={setIsCoinSearchOpen}
          />
        </Transitioning.View>
        {isCoinSearchOpen && (
          <CryptoSwitchSearch
            setActiveCrypto={setActiveCrypto}
            onClose={() => {
              setShowSwitcher(false);
              setIsCoinSearchOpen(false);
            }}
          />
        )}
        <View
          style={[
            styles.container,
            {display: isCoinSearchOpen ? 'none' : 'flex'},
          ]}>
          {activeCrypto && (
            <>
              <View style={styles.headerContainer}>
                <View style={[styles.imageContainer]}>
                  <FastImage
                    style={styles.image}
                    source={{
                      uri: getUriImage(activeCrypto.coinImage),
                    }}
                    resizeMode="contain"
                  />
                </View>
                <View style={styles.coinNameContainer}>
                  <Text numberOfLines={1} style={styles.assetName}>
                    {activeCrypto.coinName}
                  </Text>
                  <TouchableOpacity onPress={showSwitcherHandler}>
                    <Text style={styles.changeAssetBtn}>Change Asset</Text>
                  </TouchableOpacity>
                </View>
                {isLoggedIn ? (
                  <View style={styles.currencyValueContainer}>
                    <Text style={styles.walletBalance}>
                      {formatterHelper(
                        getAssetData(activeCrypto?.coinSymbol, walletCoinData)
                          ?.coinValue || 0,
                        activeCrypto?.coinSymbol,
                      )}
                    </Text>
                    <Text style={styles.walletBalanceInUsd}>
                      {usdValueFormatter.format(
                        getAssetData(activeCrypto?.coinSymbol, walletCoinData)
                          ?.coinValueUSD || 0,
                      )}
                    </Text>
                  </View>
                ) : null}
              </View>
              <AssetNavigator activeAsset={activeCrypto} />
              <AssetNavigationViews
                activeAsset={activeCrypto}
                setCheckoutPayload={setCheckoutPayload}
              />
              {checkoutPayload.selectedCountry &&
              checkoutPayload.paymentCurrency ? (
                <View style={styles.actionBtnContainer}>
                  <View
                    style={[
                      styles.buttonContainer,
                      styles.depositButtonContainer,
                    ]}>
                    <TouchableOpacity
                      style={[styles.button, styles.depositButton]}
                      onPress={buyClickHandler}>
                      <Text style={styles.btnText}>Checkout</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              ) : (
                <>
                  <View style={styles.actionBtnContainer}>
                    <View
                      style={[styles.buttonContainer, styles.buyBtnContainer]}>
                      <TouchableOpacity
                        style={[styles.button, styles.buyBtn]}
                        onPress={buyClickHandler}>
                        <Text style={styles.btnText}>Buy</Text>
                      </TouchableOpacity>
                    </View>
                    <View
                      style={[styles.buttonContainer, styles.sellBtnContainer]}>
                      <TouchableOpacity
                        style={[styles.button, styles.sellBtn]}
                        onPress={sellClickHandler}>
                        <Text style={styles.btnText}>Sell</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  {activeCrypto.asset_type === 'Fiat' && (
                    <View style={styles.actionBtnContainer}>
                      <View
                        style={[
                          styles.buttonContainer,
                          styles.depositButtonContainer,
                        ]}>
                        <TouchableOpacity
                          style={[styles.button, styles.depositButton]}
                          onPress={depositClickHandler}>
                          <Text style={styles.btnText}>Deposit</Text>
                        </TouchableOpacity>
                      </View>
                      <View
                        style={[
                          styles.buttonContainer,
                          styles.withdrawButtonContainer,
                        ]}>
                        <TouchableOpacity
                          onPress={withdrawClickHandler}
                          style={[styles.button, styles.withdrawButton]}>
                          <Text style={styles.btnText}>Withdraw</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  )}
                </>
              )}
            </>
          )}
        </View>
      </View>
      <BuyBottomSheet
        isBottomSheetOpen={isBuyBottomSheetOpen}
        setIsBottomSheetOpen={setIsBuyBottomSheetOpen}
        activeCrypto={activeCrypto}
        openTxnAudit={openTxnAudit}
        checkoutPayload={checkoutPayload}
      />
      <SellBottomSheet
        isBottomSheetOpen={isSellBottomSheetOpen}
        setIsBottomSheetOpen={setIsSellBottomSheetOpen}
        activeCrypto={activeCrypto}
        openTxnAudit={openTxnAudit}
        checkoutPayload={checkoutPayload}
      />
      <DepositBottomSheet
        isBottomSheetOpen={isDepositBottomSheetOpen}
        setIsBottomSheetOpen={setIsDepositBottomSheetOpen}
        activeCrypto={activeCrypto}
        openTxnAudit={openTxnAudit}
        checkoutPayload={checkoutPayload}
      />
      <WithdrawBottomSheet
        isBottomSheetOpen={isWithdrawBottomSheetOpen}
        setIsBottomSheetOpen={setIsWithdrawBottomSheetOpen}
        activeCrypto={activeCrypto}
        openTxnAudit={openTxnAudit}
        checkoutPayload={checkoutPayload}
      />
      <TransactionAudit
        selectedAuditTxn={selectedAuditTxn}
        isAuditOpen={isAuditOpen}
        setIsAuditOpen={setIsAuditOpen}
      />
    </AppMainLayout>
  );
};

export default AssetScreen;

const styles = StyleSheet.create({
  appContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 25,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinNameContainer: {
    paddingHorizontal: 5,
    flex: 1,
  },
  assetName: {
    fontFamily: 'Montserrat-Bold',
    color: '#464B4E',
    fontSize: 24,
  },
  changeAssetBtn: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#464B4E',
    fontSize: 11,
    textDecorationLine: 'underline',
  },
  imageContainer: {
    width: 45,
    height: 45,
    backgroundColor: 'white',
    borderRadius: 25,
    padding: 1,
  },
  image: {
    flex: 1,
    height: null,
    width: null,
  },
  currencyValueContainer: {},
  walletBalance: {
    textAlign: 'right',
    color: '#464B4E',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 24,
  },
  walletBalanceInUsd: {
    textAlign: 'right',
    color: '#464B4E',
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
  },
  actionBtnContainer: {
    flexDirection: 'row',
  },
  buttonContainer: {
    flexGrow: 1,
    width: 0,
    height: 45,
  },
  buyBtnContainer: {
    marginRight: 10,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: '100%',
  },
  buyBtn: {
    backgroundColor: '#30BC96',
  },
  btnText: {
    color: 'white',
    fontSize: 14,
    fontFamily: 'Montserrat-Bold',
  },
  checkoutText: {},
  sellBtnContainer: {},
  sellBtn: {
    backgroundColor: '#D81238',
  },
  depositButtonContainer: {
    marginRight: 10,
    marginTop: 10,
  },
  depositButton: {
    backgroundColor: '#186AB4',
  },
  withdrawButtonContainer: {
    marginTop: 10,
  },
  withdrawButton: {
    backgroundColor: '#000000',
  },
  loadingContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width,
    height: height - 190,
    backgroundColor: 'white',
    zIndex: 10,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 2,
  },
  checkoutContainer: {},
});
