import React, {useEffect, useState} from 'react';
import {
  Image,
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {SharedElement} from 'react-navigation-shared-element';
import ActionBar from '../components/ActionBar';
import BrokerSyncConfigure from '../components/BrokerSyncConfigure';
import {APP_CODE, GX_API_ENDPOINT} from '../configs';
import AppMainLayout from '../layouts/AppMainLayout';
import {useNavigation} from '@react-navigation/native';
import Axios from 'axios';

const InviteScreen = () => {
  const {navigate} = useNavigation();
  const [androidLink, setAndroidLink] = useState('');
  const [iosLink, setIosLink] = useState('');

  const onItemClick = (data, title) => {
    navigate('Details', {item: data, title});
  };

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/gxb/apps/mobile/app/links/logs/get`, {
      params: {app_code: APP_CODE},
    })
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          const logs = data.logs || [];

          // logs.sort((a, b) => a.timestamp - b.timestamp);

          const latestUpdate = logs[0];

          if (latestUpdate) {
            setAndroidLink(latestUpdate.android_app_link);
            setIosLink(latestUpdate.ios_app_link);
          }
        }
      })
      .catch((error) => {});
  }, []);

  const sections = [
    {
      title: 'Mobile First',
      data: [
        {
          key: 'iphone',
          icon: require('../assets/apple-icon.png'),
          headerPre: 'Directly To',
          header: 'Their Iphone',
          intro:
            'This will get them to download the BrokerApp onto their Iphone and register within your network from the app.',
          desc:
            'If Your Prospect Is Not Used To Testflight. Please Send Them This Link. It Will Walk Them Through Each Step Of Downloading The App.',
          appLink: iosLink,
          buttons: ['TestFlight', 'App Store'],
        },
        {
          key: 'android',
          icon: require('../assets/android-icon.png'),
          headerPre: 'Directly To',
          header: 'Their Android',
          intro:
            'This will get them to download the BrokerApp onto their Android and register within your network from the app.',
          desc:
            'If Your Prospect Is Not Used To Brain.Stream. Please Send Them This Link. It Will Walk Them Through Each Step Of Downloading The App.',
          appLink: androidLink,
          buttons: ['Brain.Stream', 'Google Play'],
        },
      ],
    },
    {
      title: 'Web',
      data: [
        {
          key: 'web',
          icon: require('../assets/desktop-icon.png'),
          headerPre: 'Browser Based',
          header: 'Desktop Signup',
          intro:
            'Get them to register within your network by completing a simple form in any internet browser from their laptop or desktop.',
          desc:
            'If Your Prospect Needs Additional Instructions, Please Send Them This Link To A Detailed Walk Through.',
          appLink: 'https://brokerapp.io/',
          buttons: ['brokerapp.io', 'App Store'],
        },
        {
          key: 'webMobile',
          icon: require('../assets/mobile-icon.png'),
          headerPre: 'Browser Based',
          header: 'Mobile Signup',
          intro:
            'Get them to register within your network by completing a simple form in any internet browser from their phone.',
          desc:
            'If Your Prospect Needs Additional Instructions, Please Send Them This Link To A Detailed Walk Through.',
          appLink: 'https://brokerapp.io/',
          buttons: ['brokerapp.io', 'App Store'],
        },
      ],
    },
  ];

  return (
    <AppMainLayout disableBlockCheck>
      <SharedElement id={'item.appBar'}>
        <ActionBar />
      </SharedElement>
      <View style={styles.container}>
        <SharedElement id={'item.title'}>
          <>
            <Text style={styles.header}>Your Network Is Your Net-Worth</Text>
            <Text style={styles.subHeader}>
              Pick One Of The Following Initiate Opt
            </Text>
          </>
        </SharedElement>
        <SectionList
          stickySectionHeadersEnabled={false}
          showsVerticalScrollIndicator={false}
          style={styles.list}
          keyExtractor={(item, index) => item + index}
          sections={sections}
          renderItem={({item, section}) => (
            <TouchableOpacity onPress={() => onItemClick(item, section.title)}>
              <View style={styles.item}>
                <SharedElement id={`item.${item.key}.item`}>
                  <View style={styles.headerContainer}>
                    <Image
                      source={item.icon}
                      style={styles.image}
                      resizeMode="contain"
                    />
                    <View style={styles.itemHeader}>
                      <Text style={styles.preHeading}>{item.headerPre}</Text>
                      <Text style={styles.itemHeading}>{item.header}</Text>
                    </View>
                  </View>
                </SharedElement>
                <Text style={styles.desc}>{item.intro}</Text>
              </View>
            </TouchableOpacity>
          )}
          renderSectionHeader={({section: {title}}) => (
            <Text style={styles.title}>{title}</Text>
          )}
        />
      </View>
      <BrokerSyncConfigure />
    </AppMainLayout>
  );
};

export default InviteScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 25,
    paddingVertical: 20,
    backgroundColor: 'white',
  },
  header: {
    color: '#186AB4',
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
    fontSize: 24,
  },
  subHeader: {
    color: '#186AB4',
    fontFamily: 'Montserrat',
    textAlign: 'center',
    marginTop: 15,
    fontSize: 13,
  },
  list: {
    marginTop: 20,
  },
  title: {
    color: '#186AB4',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 25,
    marginBottom: 8,
    fontSize: 15,
  },
  item: {
    borderColor: '#EDEDED',
    borderWidth: 1,
    borderRadius: 10,
    padding: 20,
    marginTop: 10,
  },
  headerContainer: {
    flexDirection: 'row',
  },
  image: {
    width: 40,
    height: 40,
  },
  itemHeader: {
    flex: 1,
    marginLeft: 15,
  },
  preHeading: {
    color: '#186AB4',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
  },
  itemHeading: {
    color: '#186AB4',
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
  },
  desc: {
    marginTop: 15,
    color: '#186AB4',
    fontFamily: 'Montserrat',
    fontSize: 13,
  },
});
