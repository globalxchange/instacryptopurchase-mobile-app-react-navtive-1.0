import React, {useContext} from 'react';
import {StyleSheet} from 'react-native';
import CustomNumPad from '../components/CustomNumPad';
import {SafeAreaView} from 'react-native-safe-area-context';
import BlockCheck from '../components/BlockCheck';
import {AppContext} from '../contexts/AppContextProvider';
import BlockcheckRequest from '../components/BlockcheckRequest';

const AppMainLayout = ({children, disableBlockCheck, whiteBg}) => {
  const {
    showBCHelper,
    isBlockSheetOpen,
    setIsBlockSheetOpen,
    isBlockCheckSend,
  } = useContext(AppContext);

  return (
    <SafeAreaView
      style={[styles.container, whiteBg && {backgroundColor: 'white'}]}>
      {children}
      {showBCHelper && <BlockCheck />}
      <CustomNumPad />
      <BlockcheckRequest
        isOpen={isBlockSheetOpen}
        setIsOpen={setIsBlockSheetOpen}
        isSend={isBlockCheckSend}
      />
    </SafeAreaView>
  );
};

export default AppMainLayout;

const styles = StyleSheet.create({
  container: {backgroundColor: '#186AB4', flex: 1},
});
