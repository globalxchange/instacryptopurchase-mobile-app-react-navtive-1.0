/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useState} from 'react';
import {
  Dimensions,
  Image,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {Transitioning, Transition} from 'react-native-reanimated';
import ThemeData from '../configs/ThemeData';

const {width, height} = Dimensions.get('window');

const PopupLayout = ({
  children,
  isOpen,
  onClose,
  headerImage,
  noScrollView,
  autoHeight,
  headerTitle,
  containerStyles,
  headerImageHeight,
  noHeader,
  contentContainerStyle = {},
}) => {
  const [transitionStyle, setTransitionStyle] = useState({});
  const transitionRef = useRef();

  useEffect(() => {
    if (transitionRef.current) {
      transitionRef.current.animateNextTransition();
      setTransitionStyle(containerStyles || {});
    }
  }, [containerStyles]);

  const transition = <Transition.Change interpolation="easeInOut" />;

  return (
    <Modal
      animationType="fade"
      transparent
      visible={isOpen}
      hardwareAccelerated
      statusBarTranslucent
      onDismiss={onClose}
      onRequestClose={onClose}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.overlay}
        onPress={onClose}
        onPressOut={() => {}}>
        <TouchableWithoutFeedback style={{flex: 1}}>
          <Transitioning.View
            transition={transition}
            ref={transitionRef}
            style={[
              styles.modalContainer,
              autoHeight || {height: height * 0.6},
              transitionStyle,
            ]}>
            {noHeader || (
              <View style={styles.headerContainer}>
                {headerImage ? (
                  <Image
                    style={[
                      styles.headerImage,
                      headerImageHeight && {height: headerImageHeight},
                    ]}
                    source={headerImage}
                    resizeMode="contain"
                  />
                ) : (
                  <Text
                    numberOfLines={1}
                    adjustsFontSizeToFit
                    style={styles.headerText}>
                    {headerTitle}
                  </Text>
                )}
              </View>
            )}
            {noScrollView ? (
              <View
                style={[
                  autoHeight ? {padding: 30} : {flex: 1, padding: 30},
                  {backgroundColor: 'white'},
                  contentContainerStyle,
                ]}>
                {children}
              </View>
            ) : (
              <ScrollView
                style={styles.scrollView}
                bounces={false}
                showsVerticalScrollIndicator={false}>
                <TouchableWithoutFeedback>
                  <View style={styles.viewContainer}>{children}</View>
                </TouchableWithoutFeedback>
              </ScrollView>
            )}
          </Transitioning.View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default PopupLayout;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.8)',
    flex: 1,
    justifyContent: 'center',
    // paddingVertical: height * 0.25,
  },
  modalContainer: {
    backgroundColor: 'white',
    borderRadius: 20,
    width: width * 0.9,
    marginLeft: 'auto',
    marginRight: 'auto',
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: 'white',
    maxHeight: height * 0.85,
  },
  headerContainer: {
    height: 55,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerImage: {
    height: 25,
  },
  headerText: {
    color: 'white',
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 17,
    textAlign: 'center',
    paddingHorizontal: 30,
  },
  scrollView: {
    marginVertical: 30,
  },
  viewContainer: {
    paddingHorizontal: 30,
  },
});
