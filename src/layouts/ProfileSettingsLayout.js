/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  Dimensions,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ActionBar from '../components/ActionBar';
import SettingsScreenHeader from '../components/SettingsScreenHeader';
import ThemeData from '../configs/ThemeData';
import AppMainLayout from './AppMainLayout';

const {width} = Dimensions.get('window');

const ProfileSettingsLayout = ({children, header, subHeader, breadCrumbs}) => {
  const [activeBottomTab, setActiveBottomTab] = useState(BOTTOM_TABS[1]);

  return (
    <AppMainLayout>
      <ActionBar />
      <SettingsScreenHeader
        header={header}
        subHeader={subHeader}
        breadCrumbs={breadCrumbs}
      />
      <View style={styles.container}>{children}</View>
      <View style={styles.bottomTab}>
        <FlatList
          data={BOTTOM_TABS}
          horizontal
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item) => item.title}
          renderItem={({item}) => (
            <TouchableOpacity
              key={item.title}
              disabled={item.disabled}
              onPress={() => setActiveBottomTab(item)}>
              <View
                style={[
                  styles.bottomTabItem,
                  item.disabled && {opacity: 0.4},
                  activeBottomTab?.title === item.title && {
                    borderTopWidth: 1,
                  },
                ]}>
                <Text
                  style={[
                    styles.bottomTabText,
                    activeBottomTab?.title === item.title && {
                      fontFamily: ThemeData.FONT_SEMI_BOLD,
                    },
                  ]}>
                  {item.title}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    </AppMainLayout>
  );
};

export default ProfileSettingsLayout;

const styles = StyleSheet.create({
  container: {flex: 1, paddingHorizontal: 20, backgroundColor: 'white'},
  bottomTab: {
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  bottomTabItem: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopColor: ThemeData.APP_MAIN_COLOR,
    width: width / 4,
  },
  bottomTabText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
});

const BOTTOM_TABS = [
  {title: 'XID', disabled: true},
  {title: 'Settings'},
  {title: 'KYC', disabled: true},
  {title: 'Brain', disabled: true},
];
