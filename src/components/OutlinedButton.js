import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';

const OutlinedButton = ({onPress, style, title, active}) => {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.outlinedButton, style]}>
      <Text style={[styles.outlinedBtnText, active && styles.active]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default OutlinedButton;

const styles = StyleSheet.create({
  outlinedButton: {
    borderColor: '#E9E8E8',
    borderWidth: 1,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    flex: 1,
  },
  outlinedBtnText: {
    color: '#858585',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  active: {
    fontFamily: 'Montserrat-Bold',
  },
});
