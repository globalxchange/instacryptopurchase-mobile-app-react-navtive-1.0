/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  Dimensions,
  Image,
  Keyboard,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import ThemeData from '../configs/ThemeData';
import PopupLayout from '../layouts/PopupLayout';
import {formatterHelper, getUriImage, urlValidatorRegex} from '../utils';
import {AppContext} from '../contexts/AppContextProvider';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const {width, height} = Dimensions.get('window');

const VaultSelector = ({
  label,
  placeHolder = 'Select From Vaults',
  onItemSelect,
  selectedItem,
  onNext,
}) => {
  const {top, bottom} = useSafeAreaInsets();

  const {walletCoinData} = useContext(AppContext);

  const [isPopupOpen, setIsPopupOpen] = useState(false);
  const [searchInput, setSearchInput] = useState('');
  const [isAllList, setIsAllList] = useState(false);
  const [filteredList, setFilteredList] = useState([]);
  const [isSearchFocused, setIsSearchFocused] = useState(false);

  const inputRef = useRef();

  useEffect(() => {
    if (walletCoinData) {
      const searchQuery = searchInput.trim().toLowerCase();

      const filter = [];

      walletCoinData.forEach((item) => {
        const isFound =
          item?.coinName?.toLowerCase().includes(searchQuery) ||
          item?.coinSymbol?.toLowerCase().includes(searchQuery);

        if (isFound) {
          const coinItem = {
            ...item,
            balance: formatterHelper(item.coinValue || 0, item.coinSymbol),
            image: item.coinImage,
            name: item.coinName,
          };

          if (!isAllList) {
            if (item.coinValue > 0) {
              filter.push(coinItem);
            }
          } else {
            filter.push(coinItem);
          }
        }
      });

      setFilteredList(filter);
    }
  }, [searchInput, walletCoinData, isAllList]);

  const onItemClick = (item) => {
    onItemSelect(item);
    onNext(item);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>{label}</Text>
      <TouchableOpacity onPress={() => setIsPopupOpen(!isPopupOpen)}>
        <View style={styles.dropdownContainer}>
          <FastImage
            style={styles.image}
            source={
              selectedItem
                ? urlValidatorRegex.test(selectedItem.image)
                  ? {uri: getUriImage(selectedItem.image)}
                  : selectedItem.image
                : require('../assets/coin-icon.png')
            }
            resizeMode="contain"
          />
          <Text style={styles.text}>
            {selectedItem ? selectedItem.name : placeHolder}
          </Text>
          <Image
            style={styles.dropIcon}
            source={require('../assets/dropdown-icon.png')}
            resizeMode="contain"
          />
        </View>
      </TouchableOpacity>
      <PopupLayout
        isOpen={isPopupOpen}
        onClose={() => setIsPopupOpen(false)}
        headerImage={require('../assets/wallet-picker-header.png')}
        noScrollView
        containerStyles={
          isSearchFocused && {
            height,
            width,
            maxHeight: height,
            paddingTop: top,
            paddingBottom: bottom,
            backgroundColor: 'transparent',
            borderWidth: 0,
          }
        }>
        <View style={styles.searchContainer}>
          {isSearchFocused && (
            <>
              <TouchableOpacity
                style={styles.backButton}
                onPress={() => {
                  Keyboard.dismiss();
                  inputRef.current.blur();
                  setIsSearchFocused(false);
                }}>
                <Image
                  style={styles.backIcon}
                  source={require('../assets/back-icon-colored.png')}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <View style={styles.divider} />
            </>
          )}
          <TextInput
            ref={inputRef}
            style={styles.searchInput}
            placeholder={
              isAllList ? 'Search All Vaults' : 'Search Vaults With Balances'
            }
            value={searchInput}
            onChangeText={(text) => setSearchInput(text)}
            placeholderTextColor="#788995"
            onFocus={() => setIsSearchFocused(true)}
            onBlur={() => setIsSearchFocused(false)}
          />
        </View>
        <View style={styles.listContainer}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <TouchableWithoutFeedback>
              <View>
                {filteredList.map((item) => (
                  <TouchableOpacity
                    key={item.coinSymbol}
                    onPress={() => onItemClick(item)}
                    style={styles.itemContainer}>
                    <FastImage
                      source={{uri: getUriImage(item.image)}}
                      resizeMode="contain"
                      style={styles.coinIcon}
                    />
                    <Text style={styles.coinName}>{item.coinSymbol}</Text>
                    <Text style={styles.coinBalance}>{item.balance}</Text>
                  </TouchableOpacity>
                ))}
              </View>
            </TouchableWithoutFeedback>
          </ScrollView>
        </View>
        <View style={styles.actionContainer}>
          <TouchableOpacity
            onPress={() => setIsAllList(false)}
            style={[
              [!isAllList ? styles.filledButton : styles.outlinedButton],
            ]}>
            <Text
              style={[
                !isAllList
                  ? styles.filledButtonText
                  : styles.outlinedButtonText,
              ]}>
              With Balance
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setIsAllList(true)}
            style={[isAllList ? styles.filledButton : styles.outlinedButton]}>
            <Text
              style={[
                isAllList ? styles.filledButtonText : styles.outlinedButtonText,
              ]}>
              All Vaults
            </Text>
          </TouchableOpacity>
        </View>
      </PopupLayout>
    </View>
  );
};

export default VaultSelector;

const styles = StyleSheet.create({
  container: {marginBottom: 20},
  header: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  dropdownContainer: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 15,
    alignItems: 'center',
    height: 50,
  },
  image: {
    height: 24,
    width: 24,
  },
  text: {
    flexGrow: 1,
    width: 0,
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    color: '#788995',
  },
  dropIcon: {
    width: 13,
  },
  listContainer: {
    flex: 1,
    marginTop: 20,
  },
  actionContainer: {
    flexDirection: 'row',
    marginHorizontal: -35,
    marginBottom: -35,
  },
  outlinedButton: {
    flex: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  outlinedButtonText: {
    color: '#788995',
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
  },
  filledButton: {
    flex: 1,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  filledButtonText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
  },
  searchContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
  },
  backButton: {
    height: 40,
    justifyContent: 'center',
    marginLeft: -15,
    paddingLeft: 15,
    paddingRight: 15,
  },
  backIcon: {
    width: 20,
    height: 20,
  },
  divider: {
    marginRight: 15,
    height: '100%',
    width: 1,
    backgroundColor: ThemeData.BORDER_COLOR,
  },
  searchInput: {
    height: 40,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
  },
  itemContainer: {
    flexDirection: 'row',
    paddingVertical: 20,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    alignItems: 'center',
  },
  coinIcon: {
    height: 25,
    width: 25,
  },
  coinName: {
    flex: 1,
    marginLeft: 10,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#788995',
  },
  coinBalance: {
    textAlign: 'right',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#788995',
  },
});
