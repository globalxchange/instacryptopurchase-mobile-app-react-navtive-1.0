import React, {useContext} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableWithoutFeedback,
  ScrollView,
  Dimensions,
} from 'react-native';
import {INVESTMENTS_MENUS} from '../configs';
import {AppContext} from '../contexts/AppContextProvider';

const {width} = Dimensions.get('window');

const InvestmentListSwitcher = ({openAddModal}) => {
  const {activeListCategory, setActiveListCategory} = useContext(AppContext);

  return (
    <ScrollView
      style={styles.container}
      horizontal
      showsHorizontalScrollIndicator={false}>
      {INVESTMENTS_MENUS.map((item) => (
        <TouchableWithoutFeedback
          key={item.title}
          style={styles.itemContainer}
          onPress={() => {
            if (item.title === 'Add') {
              openAddModal();
            } else {
              setActiveListCategory(item);
            }
          }}>
          <View style={styles.item}>
            <View
              style={
                activeListCategory && activeListCategory.title === item.title
                  ? styles.itemActive
                  : styles.iconContainer
              }>
              <Image
                style={styles.icon}
                source={item.icon}
                resizeMode="contain"
              />
            </View>
            <Text
              style={
                activeListCategory && activeListCategory.title === item.title
                  ? styles.titleActive
                  : styles.title
              }>
              {item.title}
            </Text>
          </View>
        </TouchableWithoutFeedback>
      ))}
    </ScrollView>
  );
};

export default InvestmentListSwitcher;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: 20,
    height: 110,
  },
  itemContainer: {paddingHorizontal: 5},
  item: {paddingVertical: 15, width: (width - 40) / 4},
  iconContainer: {
    height: 50,
    opacity: 0.5,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 2,
  },
  itemActive: {
    height: 50,
    width: 50,
    opacity: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 2,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    elevation: 2,
  },
  icon: {height: 35, width: 35},
  title: {
    textAlign: 'center',
    color: '#788995',
    marginTop: 5,
    opacity: 0.5,
    fontSize: 10,
    fontFamily: 'Montserrat',
  },
  titleActive: {
    textAlign: 'center',
    color: '#788995',
    marginTop: 5,
    fontSize: 10,
    fontFamily: 'Montserrat-SemiBold',
  },
});
