import React, {useEffect} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import ThemeData from '../../configs/ThemeData';

const OTPVerify = ({
  otpInput,
  setOtpInput,
  resendVerificationCode,
  isBlockcheck,
  onNextClick,
}) => {
  useEffect(() => {
    if (otpInput?.trim().length === 6) {
      onNextClick && onNextClick();
    }
  }, [otpInput]);

  return (
    <View style={styles.loginFromContainer}>
      <Text style={styles.loginTitle}>Register</Text>
      <Text style={styles.loginSubTitle}>
        {isBlockcheck
          ? 'Step 7: Validating Your BlockCheck Address'
          : 'Step 7: Validate Your Email Address'}
      </Text>
      <OTPInputView
        codeInputFieldStyle={styles.underlineStyleBase}
        codeInputHighlightStyle={styles.underlineStyleHighLighted}
        style={styles.otpInput}
        pinCount={6}
        // autoFocusOnLoad
        code={otpInput}
        onCodeChanged={(code) => setOtpInput(code)}
      />
      <TouchableOpacity onPress={resendVerificationCode}>
        <Text style={styles.checkText}>Resend Code</Text>
      </TouchableOpacity>
    </View>
  );
};

export default OTPVerify;

const styles = StyleSheet.create({
  loginFromContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  loginTitle: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
    fontSize: 50,
  },
  loginSubTitle: {
    color: '#999C9A',
    marginBottom: 40,
    fontFamily: 'Montserrat',
  },
  otpInput: {
    height: 60,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
  },
  underlineStyleBase: {
    borderWidth: 1,
    color: ThemeData.APP_MAIN_COLOR,
  },
  underlineStyleHighLighted: {
    borderColor: '#999C9A',
    color: ThemeData.APP_MAIN_COLOR,
  },
  checkText: {
    marginTop: 15,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
  },
});
