import React from 'react';
import {StyleSheet, View} from 'react-native';
import SkeltonItem from '../SkeltonItem';

const SkeletonCheckoutLoading = () => {
  return (
    <View style={styles.container}>
      <SkeltonItem itemHeight={35} itemWidth={120} style={styles.header} />
      <SkeltonItem
        itemHeight={10}
        itemWidth={100}
        style={styles.subHeader}
        overrideStyles={{width: '100%'}}
      />
      <View style={styles.controlContainer}>
        <SkeltonItem
          itemHeight={50}
          itemWidth={120}
          overrideStyles={{width: '100%'}}
        />
      </View>
      <SkeltonItem
        itemHeight={40}
        itemWidth={120}
        overrideStyles={{width: '100%'}}
      />
    </View>
  );
};

export default SkeletonCheckoutLoading;

const styles = StyleSheet.create({
  container: {flex: 1},
  header: {
    marginBottom: 20,
  },
  subHeader: {
    // marginBottom: 30,
  },
  controlContainer: {
    flex: 1,
    justifyContent: 'center',
  },
});
