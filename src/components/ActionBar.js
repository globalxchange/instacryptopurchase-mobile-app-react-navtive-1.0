import React, {useState, useContext} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import LivePlugins from './LivePlugins';
import {AppContext} from '../contexts/AppContextProvider';

const ActionBar = () => {
  const {isLoggedIn, isVideoFullScreen} = useContext(AppContext);

  const [isInstaTransferOpen, setIsInstaTransferOpen] = useState(false);

  const navigation = useNavigation();

  const openDrawer = () => {
    Keyboard.dismiss();
    navigation.openDrawer();
  };

  return (
    <View
      style={[
        styles.container,
        {display: isVideoFullScreen ? 'none' : 'flex'},
      ]}>
      <TouchableOpacity style={styles.toggleButton} onPress={openDrawer}>
        <Image
          style={styles.menuIcon}
          source={require('../assets/menu-icon.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <View style={styles.appIconContainer}>
        <Image
          style={styles.appIcon}
          resizeMode="contain"
          source={require('../assets/header-logo.png')}
        />
      </View>
      <TouchableOpacity
        style={styles.sendButton}
        onPress={() =>
          isLoggedIn
            ? setIsInstaTransferOpen(true)
            : navigation.navigate('Landing')
        }>
        <Image
          style={styles.sendIcon}
          source={require('../assets/send-icon-white.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <LivePlugins
        isOpen={isInstaTransferOpen}
        setIsOpen={setIsInstaTransferOpen}
      />
    </View>
  );
};

export default ActionBar;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: '#186AB4',
    height: 70,
    display: 'flex',
    flexDirection: 'row',
    position: 'relative',
  },
  toggleButton: {
    marginTop: 'auto',
    marginBottom: 'auto',
    height: '100%',
    justifyContent: 'center',
  },
  sendButton: {
    marginTop: 'auto',
    marginBottom: 'auto',
    height: '100%',
    justifyContent: 'center',
    marginLeft: 'auto',
    width: 40,
  },
  menuIcon: {marginLeft: 20},
  sendIcon: {
    marginRight: 20,
    flex: 1,
    height: null,
    width: null,
  },
  appIconContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    zIndex: -1,
  },
  appIcon: {
    height: 40,
    // width: 22,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
});
