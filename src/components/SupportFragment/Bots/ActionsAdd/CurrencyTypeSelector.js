import React from 'react';
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

const {width} = Dimensions.get('window');

const CurrencyTypeSelector = ({activeType, setActiveType}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>
        {'What Type Of Asset Do You Want To Add?'}
      </Text>
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        style={styles.scrollView}>
        {TYPES.map((item) => (
          <TouchableOpacity
            disabled={item.disabled}
            key={item.title}
            onPress={() => setActiveType(item)}>
            <View
              style={[
                styles.itemContainer,
                activeType?.title === item.title && styles.itemActive,
              ]}>
              <Image
                resizeMode="contain"
                source={item.icon}
                style={styles.itemIcon}
              />
              <Text style={styles.itemText}>{item.title}</Text>
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};

export default CurrencyTypeSelector;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    textDecorationLine: 'underline',
    fontSize: 14,
  },
  scrollView: {
    marginTop: 20,
    marginRight: -30,
  },
  itemContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#E7E7E7',
    padding: 10,
    marginRight: 20,
    width: width / 5,
    height: width / 5,
    opacity: 0.5,
  },
  itemActive: {
    borderWidth: 2,
    opacity: 1,
  },
  itemIcon: {
    width: 35,
    height: 35,
  },
  itemText: {
    textAlign: 'center',
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 10,
    fontSize: 12,
  },
});

const TYPES = [
  {
    title: 'Crypto',
    icon: require('../../../../assets/crypto-icon.png'),
  },
  {
    title: 'Fiat',
    icon: require('../../../../assets/fiat-icon.png'),
    disabled: true,
  },
];
