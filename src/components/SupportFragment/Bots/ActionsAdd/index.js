import React, {useEffect, useState} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import CoinsSelector from './CoinsSelector';
import CurrencyTypeSelector from './CurrencyTypeSelector';
import HashForm from './HashForm';
import SourceSelector from './SourceSelector';

const ActionsAdd = ({setHideSubCategory}) => {
  const [activeCurrencyType, setActiveCurrencyType] = useState('');
  const [fromSource, setFromSource] = useState('');
  const [selectedCoin, setSelectedCoin] = useState('');

  useEffect(() => {
    if (activeCurrencyType !== '' && fromSource !== '' && selectedCoin !== '') {
      setHideSubCategory(true);
    } else {
      setHideSubCategory(false);
    }
  }, [activeCurrencyType, fromSource, selectedCoin]);

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {activeCurrencyType !== '' &&
        fromSource !== '' &&
        selectedCoin !== '' ? (
          <HashForm selectedCoin={selectedCoin} />
        ) : (
          <>
            <CurrencyTypeSelector
              activeType={activeCurrencyType}
              setActiveType={setActiveCurrencyType}
            />
            {activeCurrencyType !== '' && (
              <>
                <SourceSelector
                  activeType={fromSource}
                  setActiveType={setFromSource}
                />
                {fromSource !== '' && (
                  <CoinsSelector
                    selectedCoin={selectedCoin}
                    setSelectedCoin={setSelectedCoin}
                  />
                )}
              </>
            )}
          </>
        )}
      </ScrollView>
    </View>
  );
};

export default ActionsAdd;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
