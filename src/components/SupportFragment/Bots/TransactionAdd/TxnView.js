import React, {useContext} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {AppContext} from '../../../../contexts/AppContextProvider';
import {formatterHelper, timeParserExpanded} from '../../../../utils';

const TxnView = ({selectedTxn}) => {
  const {cryptoTableData} = useContext(AppContext);

  let icon = require('../../../../assets/bitcoin-icon.png');
  let name = 'BTC';

  if (cryptoTableData) {
    const coin = cryptoTableData.find((x) => x.coinSymbol === selectedTxn.coin);
    if (coin) {
      icon = {uri: coin.coinImage};
      name = coin.coinName;
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.listItem}>
        <Image source={icon} resizeMode="contain" style={styles.itemIcon} />
        <View style={styles.textContainer}>
          <Text style={styles.itemTitle}>{`${name} Deposit`}</Text>
          <Text style={styles.itemSubTitle}>
            {timeParserExpanded(selectedTxn.timestamp)}
          </Text>
        </View>
      </View>
      <View style={styles.detailsContainer}>
        <View style={[styles.listItem, {borderWidth: 0, marginBottom: 0}]}>
          <Image source={icon} resizeMode="contain" style={styles.itemIcon} />
          <View style={styles.textContainer}>
            <Text style={styles.itemTitle}>{selectedTxn._id}</Text>
            <Text style={styles.itemSubTitle}>
              {timeParserExpanded(selectedTxn.timestamp)}
            </Text>
          </View>
        </View>
        <View style={styles.dataContainer}>
          <Text style={styles.dataLabel}>Banker</Text>
          <Text style={styles.dataValue}>{selectedTxn.banker}</Text>
        </View>
        <View style={styles.dataContainer}>
          <Text style={styles.dataLabel}>Amount</Text>
          <Text style={styles.dataValue}>
            {formatterHelper(selectedTxn.buy_amount, selectedTxn.buy_coin)}{' '}
            {selectedTxn.buy_coin}
          </Text>
        </View>
        <View style={styles.dataContainer}>
          <Text style={styles.dataLabel}>Status</Text>
          <Text style={styles.dataValue}>{selectedTxn.status}</Text>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={[
              styles.button,
              {borderRightColor: '#E7E7E7', borderRightWidth: 1},
            ]}>
            <Text style={styles.buttonText}>Dispute</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Save</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default TxnView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    borderColor: '#E7E7E7',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  itemIcon: {
    width: 35,
    height: 35,
  },
  textContainer: {
    flex: 1,
    paddingLeft: 20,
  },
  itemTitle: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
  },
  itemSubTitle: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 11,
  },
  detailsContainer: {
    borderColor: '#E7E7E7',
    borderWidth: 1,
  },
  detailsHeaderContainer: {},
  dataContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginVertical: 10,
  },
  dataLabel: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  dataValue: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderTopColor: '#E7E7E7',
    borderTopWidth: 1,
  },
  buttonText: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 12,
    textAlign: 'center',
  },
});
