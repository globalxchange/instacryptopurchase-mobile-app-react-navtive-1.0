/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {AGENCY_API_URL} from '../../../../configs';
import ArticleList from './ArticleList';
import VideosList from './VideosList';

const LeanCategoryView = ({selectedCategory}) => {
  const [videoList, setVideoList] = useState();
  const [articleList, setArticleList] = useState();
  const [type, setType] = useState('Videos');

  useEffect(() => {
    (async () => {
      Axios.get(`${AGENCY_API_URL}/video/category`, {
        params: {category: selectedCategory._id},
      })
        .then((resp) => {
          const {data} = resp;
          // console.log('Resp On ', data);

          setVideoList(data.data || []);
        })
        .catch((error) => {
          console.log('Error On', error);
        });

      Axios.get(`${AGENCY_API_URL}/article/category`, {
        params: {category: selectedCategory._id},
      })
        .then((resp) => {
          const {data} = resp;
          // console.log('Resp On ', data);

          setArticleList(data.data || []);
        })
        .catch((error) => {
          console.log('Error On', error);
        });
    })();
  }, [selectedCategory]);

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.container}>
        <View style={styles.listItem}>
          <Image resizeMode="contain" style={styles.itemIcon} />
          <View style={styles.textContainer}>
            <Text numberOfLines={1} style={styles.itemTitle}>
              {selectedCategory.title}
            </Text>
            <Text numberOfLines={1} style={styles.itemSubTitle}>
              {selectedCategory.cv}
            </Text>
          </View>
        </View>
        <Text style={styles.header}>How Do You Want To Consume Content?</Text>
        <View style={styles.typesContainer}>
          <TouchableOpacity
            onPress={() => setType('Videos')}
            style={[
              styles.type,
              {marginRight: 15},
              type === 'Videos' && {
                borderColor: '#464B4E',
              },
            ]}>
            <Image
              source={require('../../../../assets/videos-icon.png')}
              style={styles.typeIcon}
              resizeMode="contain"
            />
            <Text
              style={[
                styles.typeName,
                type === 'Videos' && {fontFamily: 'Montserrat-Bold'},
              ]}>
              Videos
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setType('Article')}
            style={[
              styles.type,
              type === 'Article' && {borderColor: '#464B4E'},
            ]}>
            <Image
              source={require('../../../../assets/article-icon.png')}
              style={styles.typeIcon}
              resizeMode="contain"
            />
            <Text
              style={[
                styles.typeName,
                type === 'Article' && {fontFamily: 'Montserrat-Bold'},
              ]}>
              Article
            </Text>
          </TouchableOpacity>
        </View>
        {type === 'Videos' ? (
          <VideosList
            selectedCategory={selectedCategory}
            videoList={videoList}
          />
        ) : (
          <ArticleList
            selectedCategory={selectedCategory}
            articleList={articleList}
          />
        )}
      </View>
    </ScrollView>
  );
};

export default LeanCategoryView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  listItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    borderColor: '#E7E7E7',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  itemIcon: {
    width: 35,
    height: 35,
  },
  textContainer: {
    flex: 1,
    paddingLeft: 20,
  },
  itemTitle: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
  },
  itemSubTitle: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 11,
  },
  header: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
    textDecorationLine: 'underline',
  },
  detailsContainer: {
    borderColor: '#E7E7E7',
    borderWidth: 1,
  },
  detailsHeaderContainer: {},
  dataContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginVertical: 10,
  },
  dataLabel: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  dataValue: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderTopColor: '#E7E7E7',
    borderTopWidth: 1,
  },
  buttonText: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 12,
    textAlign: 'center',
  },
  typesContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  type: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#E7E7E7',
    borderWidth: 1,
    justifyContent: 'center',
    height: 40,
  },
  typeIcon: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  typeName: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
});
