import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {AGENCY_API_URL, APP_CODE} from '../../../configs';
import {AppContext} from '../../../contexts/AppContextProvider';

const TopCategory = ({
  activeCategory,
  setActiveCategory,
  setActiveSubCategory,
}) => {
  const {isLoggedIn} = useContext(AppContext);

  const [learnSubMenu, setLearnSubMenu] = useState();

  useEffect(() => {
    if (isLoggedIn) {
      setActiveCategory(CATEGORIES[0]);
    } else {
      setActiveCategory(CATEGORIES[CATEGORIES.length - 1]);
    }

    return () => {};
  }, [isLoggedIn, learnSubMenu]);

  useEffect(() => {
    if (activeCategory.subMenu) {
      const firstSubCat = activeCategory.subMenu[0];

      if (firstSubCat) {
        setActiveSubCategory(firstSubCat);
      }
    }
  }, [activeCategory]);

  useEffect(() => {
    (async () => {
      Axios.get(`${AGENCY_API_URL}/publication/appcode`, {
        params: {app_code: APP_CODE},
      })
        .then((resp) => {
          const {data} = resp;
          // console.log('Resp On ', data);

          const list = data.data || [];

          const subMenu = list.map((x) => ({
            title: x.name,
            icon: {uri: x.profile_pic},
            id: x._id,
          }));

          setLearnSubMenu(subMenu);
        })
        .catch((error) => {
          console.log('Error On', error);
        });
    })();
  }, []);

  const CATEGORIES = [
    {
      title: 'Transactions',
      header: 'What Is The Type Of Transaction',
      subMenu: [
        {
          title: 'Add',
          icon: require('../../../assets/support-category-icons/add.png'),
        },
        {
          title: 'Send',
          icon: require('../../../assets/support-category-icons/send.png'),
        },
        {
          title: 'Trade',
          icon: require('../../../assets/support-category-icons/trade.png'),
        },
        {
          title: 'Invest',
          icon: require('../../../assets/support-category-icons/invest.png'),
        },
      ],
      disabled: !isLoggedIn,
    },
    {
      title: 'Actions',
      subMenu: [
        {
          title: 'Add',
          icon: require('../../../assets/support-category-icons/add.png'),
        },
        {
          title: 'Send',
          icon: require('../../../assets/support-category-icons/send.png'),
        },
        {
          title: 'Fiat Funding',
          icon: require('../../../assets/support-category-icons/trade.png'),
        },
        {
          title: 'Invest',
          icon: require('../../../assets/support-category-icons/invest.png'),
        },
      ],
      disabled: !isLoggedIn,
    },
    {
      title: 'Learn',
      header: 'What Do You Want To Learn About?',
      subMenu: learnSubMenu,
    },
  ];

  return (
    <View>
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        style={styles.container}>
        {CATEGORIES.map((item) => (
          <TouchableOpacity
            disabled={item.disabled}
            key={item.title}
            onPress={() => {
              setActiveCategory(item);
              if (item?.subMenu) {
                setActiveSubCategory(item?.subMenu[0]);
              }
            }}>
            <View
              style={[
                styles.itemContainer,
                activeCategory.title === item.title && styles.itemActive,
              ]}>
              <Text
                style={[
                  styles.itemText,
                  activeCategory.title === item.title && styles.itemTextActive,
                ]}>
                {item.title}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};

export default TopCategory;

const styles = StyleSheet.create({
  container: {
    marginRight: -30,
  },
  itemContainer: {
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderColor: '#E7E7E7',
    borderWidth: 1,
    marginRight: 10,
    minWidth: 120,
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 0.5,
  },
  itemActive: {
    opacity: 1,
  },
  itemText: {
    fontFamily: 'Montserrat',
    color: '#464B4E',
  },
  itemTextActive: {
    fontFamily: 'Montserrat-SemiBold',
  },
});
