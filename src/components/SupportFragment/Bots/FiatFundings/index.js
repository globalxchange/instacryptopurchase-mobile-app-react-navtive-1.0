import React, {useContext, useEffect, useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {AppContext} from '../../../../contexts/AppContextProvider';
import {getUriImage} from '../../../../utils';
import TransactionsList from './TransactionsList';

const FiatFunding = () => {
  const {walletCoinData} = useContext(AppContext);

  const [activeStatus, setActiveStatus] = useState();
  const [fiats, setFiats] = useState();
  const [selectedFiat, setSelectedFiat] = useState();

  useEffect(() => {
    if (walletCoinData) {
      const coins = walletCoinData.filter((x) => x.asset_type === 'Fiat');
      setFiats(coins || []);
    }
  }, [walletCoinData]);

  if (selectedFiat) {
    return <TransactionsList coin={selectedFiat} type={activeStatus} />;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>
        {activeStatus
          ? 'You Have Funding Transactions In The Following Currencies. Please Select One'
          : 'Which Type Of Funding Transactions Do You Want To See?'}
      </Text>
      <View style={styles.optionsContainer}>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {activeStatus
            ? fiats?.map((item) => (
                <TouchableOpacity
                  onPress={() => setSelectedFiat(item)}
                  key={item._id || item.coinSymbol}
                  style={styles.optionItem}>
                  <FastImage
                    source={{uri: getUriImage(item.coinImage)}}
                    resizeMode="contain"
                    style={styles.optionIcon}
                  />
                  <Text numberOfLines={1} style={styles.optionText}>
                    {item.coinName}
                  </Text>
                </TouchableOpacity>
              ))
            : STATUS_TYPES.map((item) => (
                <TouchableOpacity
                  onPress={() => setActiveStatus(item.title)}
                  key={item.title}
                  style={styles.optionItem}>
                  <Image
                    source={item.icon}
                    resizeMode="contain"
                    style={styles.optionIcon}
                  />
                  <Text numberOfLines={1} style={styles.optionText}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              ))}
        </ScrollView>
      </View>
    </View>
  );
};

export default FiatFunding;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    textDecorationLine: 'underline',
    fontSize: 14,
  },
  optionsContainer: {
    marginTop: 20,
  },
  optionItem: {
    alignItems: 'center',
    borderColor: '#E7E7E7',
    borderWidth: 1,
    width: 90,
    height: 90,
    justifyContent: 'center',
    marginRight: 20,
    padding: 5,
  },
  optionIcon: {
    width: 40,
    height: 40,
  },
  optionText: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 11,
    marginTop: 5,
  },
});

const STATUS_TYPES = [
  {
    title: 'Completed',
    icon: require('../../../../assets/green-check-round.png'),
  },
  {
    title: 'Cancelled',
    icon: require('../../../../assets/cross-circle.png'),
  },
  {
    title: 'In Process',
    icon: require('../../../../assets/pending-txn.png'),
  },
];
