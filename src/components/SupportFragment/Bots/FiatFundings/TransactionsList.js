import AsyncStorage from '@react-native-community/async-storage';
import {useNavigation} from '@react-navigation/native';
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {APP_CODE, GX_API_ENDPOINT} from '../../../../configs';
import {
  formatterHelper,
  getUriImage,
  timestampParserMini,
} from '../../../../utils';
import AsyncStorageHelper from '../../../../utils/AsyncStorageHelper';
import LoadingAnimation from '../../../LoadingAnimation';

const TransactionsList = ({coin, type}) => {
  const navigation = useNavigation();

  const [isLoading, setIsLoading] = useState(false);
  const [txnsList, setTxnsList] = useState();
  const [coinTxns, setCoinTxns] = useState();

  useEffect(() => {
    (async () => {
      if (type === 'In Process') {
        setIsLoading(true);
        const email = await AsyncStorageHelper.getLoginEmail();
        // const email = 'shorupan@gmail.com';

        Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/processing/txns/get`, {
          params: {email, app_code: APP_CODE},
        })
          .then((resp) => {
            const {data} = resp;

            setTxnsList(data?.deposits || []);
          })
          .finally(() => setIsLoading(false));
      }
    })();

    return () => {};
  }, [type]);

  useEffect(() => {
    if (txnsList) {
      const txns = txnsList.find((x) => x._id === coin.coinSymbol)?.txns || [];
      setCoinTxns(txns);
    }

    return () => {};
  }, [txnsList, coin]);

  const openTimeline = (item) => {
    navigation.navigate('Timeline', {screen: 'List', params: {data: item}});
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={styles.coinDetails}>
          <FastImage
            source={{uri: getUriImage(coin?.coinImage || null)}}
            resizeMode="contain"
            style={styles.coinImage}
          />
          <Text style={styles.coinName}>{coin?.coinName}</Text>
        </View>
        {coinTxns ? (
          <Text style={styles.noOfTxns}>{coinTxns.length} Transactions</Text>
        ) : null}
      </View>
      {isLoading ? (
        <LoadingAnimation />
      ) : (
        <View style={styles.listContainer}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={coinTxns}
            keyExtractor={(item, index) => item?._id}
            renderItem={({item}) => (
              <TouchableOpacity
                onPress={() => openTimeline(item)}
                style={styles.txnItemContainer}>
                <View style={styles.txnDetails}>
                  <View style={styles.txnValues}>
                    <Text style={styles.txnAmount}>
                      {formatterHelper(item.buy_amount, item.buy_to_deposit)}{' '}
                      {item.buy_to_deposit}
                    </Text>
                    <Text style={styles.txnDate}>
                      Initiated On {timestampParserMini(item.timestamp)}
                    </Text>
                  </View>
                  <View style={styles.statusContainer}>
                    <Text style={styles.statusName}>{item.status}</Text>
                  </View>
                </View>
                <View style={styles.bankerContainer}>
                  <Text style={styles.bankerValue}>Banker</Text>
                  <Text style={styles.bankerValue}>{item.banker}</Text>
                </View>
              </TouchableOpacity>
            )}
            ListEmptyComponent={
              <Text style={styles.emptyText}>
                No Pending Transaction Found For {coin?.coinSymbol}
              </Text>
            }
          />
        </View>
      )}
    </View>
  );
};

export default TransactionsList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderColor: '#EBEBEB',
    borderWidth: 1,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
    paddingHorizontal: 25,
    paddingVertical: 15,
  },
  coinDetails: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    width: 30,
    height: 30,
  },
  coinName: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
    marginLeft: 10,
  },
  noOfTxns: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 13,
  },
  listContainer: {
    paddingHorizontal: 15,
    paddingTop: 30,
    flex: 1,
  },
  txnItemContainer: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    marginVertical: 10,
  },
  txnDetails: {
    paddingHorizontal: 30,
    paddingVertical: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  txnValues: {},
  txnAmount: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 20,
  },
  txnDate: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 9,
  },
  statusContainer: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 8,
  },
  statusName: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  bankerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    backgroundColor: '#F1F4F6',
    paddingVertical: 12,
  },
  bankerValue: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
  },
  emptyText: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    fontSize: 16,
    paddingHorizontal: 20,
  },
});
