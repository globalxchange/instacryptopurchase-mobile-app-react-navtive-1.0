import {MTurk} from 'aws-sdk';
import React from 'react';
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

const {width} = Dimensions.get('window');

const SubCategory = ({
  activeCategory,
  activeSubCategory,
  setActiveSubCategory,
}) => {
  return (
    <View
      style={[styles.container, {marginTop: activeCategory?.header ? 20 : 0}]}>
      <Text style={styles.header}>{activeCategory?.header}</Text>
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        style={styles.scrollView}>
        {activeCategory?.subMenu?.map((item) => (
          <TouchableOpacity
            key={item.title}
            onPress={() => setActiveSubCategory(item)}>
            <View
              style={[
                styles.itemContainer,
                activeSubCategory.title === item.title && styles.itemActive,
              ]}>
              <Image
                resizeMode="contain"
                source={item.icon}
                style={styles.itemIcon}
              />
              <Text
                numberOfLines={1}
                adjustsFontSizeToFit
                minimumFontScale={0.8}
                style={styles.itemText}>
                {item.title}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};

export default SubCategory;

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  header: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    textDecorationLine: 'underline',
    fontSize: 14,
  },
  scrollView: {
    marginTop: 20,
    marginRight: -30,
  },
  itemContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#E7E7E7',
    padding: 10,
    marginRight: 20,
    width: width / 5,
    height: width / 5,
    opacity: 0.5,
  },
  itemActive: {
    borderWidth: 2,
    opacity: 1,
  },
  itemIcon: {
    width: 35,
    height: 35,
  },
  itemText: {
    textAlign: 'center',
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 10,
    fontSize: 11,
  },
});
