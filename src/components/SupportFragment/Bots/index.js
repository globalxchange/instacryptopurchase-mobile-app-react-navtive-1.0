import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Fragments from './Fragments';
import SubCategory from './SubCategory';
import TopCategory from './TopCategory';

const Bots = () => {
  const [activeCategory, setActiveCategory] = useState({title: 'Transactions'});
  const [activeSubCategory, setActiveSubCategory] = useState({title: 'Add'});
  const [hideSubCategory, setHideSubCategory] = useState(false);

  return (
    <View style={styles.container}>
      <TopCategory
        activeCategory={activeCategory}
        setActiveCategory={setActiveCategory}
        setActiveSubCategory={setActiveSubCategory}
      />
      {hideSubCategory || (
        <SubCategory
          activeCategory={activeCategory}
          activeSubCategory={activeSubCategory}
          setActiveSubCategory={setActiveSubCategory}
        />
      )}
      <Fragments
        activeCategory={activeCategory}
        activeSubCategory={activeSubCategory}
        setHideSubCategory={setHideSubCategory}
      />
    </View>
  );
};

export default Bots;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 30,
    paddingHorizontal: 30,
  },
});
