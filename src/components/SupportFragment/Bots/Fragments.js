import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionsAdd from './ActionsAdd';
import FiatFunding from './FiatFundings';
import LearnBot from './LearnBot';
import TransactionAdd from './TransactionAdd';
import TransactionSend from './TransactionSend';

const Fragments = ({activeCategory, activeSubCategory, setHideSubCategory}) => {
  let fragment = null;

  // console.log('activeCategory', activeCategory);
  // console.log('activeSubCategory', activeSubCategory);

  switch (activeCategory?.title) {
    case 'Transactions':
      switch (activeSubCategory?.title) {
        case 'Send':
          fragment = <TransactionSend />;
          break;
        case 'Add':
          fragment = <TransactionAdd />;
          break;
      }
      break;

    case 'Actions':
      switch (activeSubCategory?.title) {
        case 'Add':
          fragment = <ActionsAdd setHideSubCategory={setHideSubCategory} />;
          break;
        case 'Fiat Funding':
          fragment = <FiatFunding />;
          break;
      }
      break;

    case 'Learn':
      return <LearnBot publisher={activeSubCategory} />;
  }

  return <View style={styles.container}>{fragment}</View>;
};

export default Fragments;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 25,
  },
});
