export const issueType = [
  {
    title: 'Transactional',
    natures: [
      {
        title: "I Couldn't Make The Transaction",
        txnTypes: ['Deposit', 'Withdrawal', 'Buy', 'Sell', 'Earn'],
      },
      {
        title: 'Question About A Transaction That Happened',
      },
    ],
  },
  {title: 'Technical', natures: []},
  {title: 'Compliance', natures: []},
  {title: 'Other', natures: []},
];
