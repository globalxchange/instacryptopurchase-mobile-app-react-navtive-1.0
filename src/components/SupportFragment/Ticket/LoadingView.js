import React, {useEffect, useContext} from 'react';
import {StyleSheet, View} from 'react-native';
import LoadingAnimation from '../../LoadingAnimation';
import {SupportContext} from '../../../contexts/SupportContext';
import Axios from 'axios';
import {SUPPORT_TICKET_API} from '../../../configs';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import {WToast} from 'react-native-smart-tip';

const LoadingView = ({onComplete, onError}) => {
  const {
    ticketIssueType,
    ticketIssueSubType,
    ticketPhoneNumber,
    ticketEmailId,
    selectedImage,
    ticketMessage,
  } = useContext(SupportContext);

  const sendTicket = async () => {
    const formData = new FormData();

    const email = await AsyncStorageHelper.getLoginEmail();

    formData.append('image', selectedImage);
    formData.append('additional_text', ticketMessage);
    formData.append('contact', ticketEmailId || ticketPhoneNumber);
    formData.append('categoryID', ticketIssueType._id);
    formData.append('subcategoryID', ticketIssueSubType._id);
    formData.append('email', email);

    Axios.post(`${SUPPORT_TICKET_API}/sup_tickets`, formData).then((resp) => {
      const {data} = resp;
      console.log('Ticket Submit', data);
      if (data.status) {
        WToast.show({data: 'Ticket Submitted'});
        onComplete();
      } else {
        WToast.show({data: data.message});
        onError();
      }
    });
  };

  useEffect(() => {
    sendTicket();
  }, []);

  return (
    <View style={styles.container}>
      <LoadingAnimation />
    </View>
  );
};

export default LoadingView;

const styles = StyleSheet.create({
  container: {
    height: 240,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
