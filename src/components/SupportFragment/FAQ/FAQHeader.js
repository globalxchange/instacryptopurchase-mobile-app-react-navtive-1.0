import React from 'react';
import {StyleSheet, Text, View, TextInput, Image} from 'react-native';

const FAQHeader = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Need Some Help With Front?</Text>
      <View style={styles.searchContainer}>
        <TextInput style={styles.searchInput} placeholder="Search Something" />
        <Image source={require('../../../assets/search-icon.png')} />
      </View>
      <Text style={styles.subHeader}>Lorem ipsum, dolor sit amet</Text>
    </View>
  );
};

export default FAQHeader;

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    marginHorizontal: 20,
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 15,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 1,
  },
  header: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#000000',
    textAlign: 'center',
    marginBottom: 10,
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#F1F1F1',
    borderWidth: 2,
    backgroundColor: 'white',
    borderRadius: 10,
    paddingHorizontal: 15,
    marginBottom: 10,
  },
  searchInput: {
    flex: 1,
    fontFamily: 'Montserrat-SemiBold',
  },
  subHeader: {
    fontFamily: 'Montserrat',
    color: '#000000',
    textAlign: 'center',
    fontSize: 12,
  },
});
