import React, {useEffect, useRef, useState} from 'react';
import {
  Image,
  Keyboard,
  Modal,
  Platform,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Animated, {
  Clock,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';
import LoadingAnimation from '../LoadingAnimation';
import PasswordInput from './PasswordInput';
import UsersList from './UsersList';

const AdminMenu = ({isOpen, setIsOpen}) => {
  const {bottom} = useSafeAreaInsets();

  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const [isListOpen, setIsListOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  useEffect(() => {
    if (!isOpen) {
      resetState();
    }
  }, [isOpen]);

  const resetState = () => {
    setKeyboardHeight(0);
    setIsKeyboardOpen(false);
    setIsListOpen(false);
    setIsLoading(false);
  };

  return (
    <Modal
      animationType="slide"
      visible={isOpen}
      transparent
      hardwareAccelerated
      statusBarTranslucent
      onDismiss={() => setIsOpen(false)}
      onRequestClose={() => setIsOpen(false)}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.overlay}
        onPress={() => setIsOpen(false)}
        onPressOut={() => {}}>
        <TouchableWithoutFeedback style={{flex: 1}}>
          <Animated.View
            style={[
              styles.container,
              // {maxHeight: isExpanded ? height - 40 : height * 0.75},
              {
                paddingBottom: bottom,
                transform: [
                  {
                    translateY: interpolate(chatKeyboardAnimation.current, {
                      inputRange: [0, 1],
                      outputRange: [0, -keyboardHeight],
                    }),
                  },
                ],
              },
            ]}>
            <View style={styles.header}>
              <Image
                style={styles.headerLogo}
                source={require('../../assets/on-hold-full-icon.png')}
                resizeMode="contain"
              />
            </View>
            {isLoading ? (
              <View style={styles.loadingContainer}>
                <LoadingAnimation />
              </View>
            ) : isListOpen ? (
              <UsersList
                setIsLoading={setIsLoading}
                onClose={() => setIsOpen(false)}
              />
            ) : (
              <PasswordInput
                setIsLoading={setIsLoading}
                onClose={() => setIsOpen(false)}
              />
            )}
          </Animated.View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default AdminMenu;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {
    backgroundColor: 'white',
    paddingHorizontal: 35,
    paddingBottom: 35,
    paddingTop: 15,
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
    // paddingBottom: 20,
  },
  headerLogo: {
    height: 40,
  },
  loadingContainer: {
    paddingVertical: 50,
  },
});
