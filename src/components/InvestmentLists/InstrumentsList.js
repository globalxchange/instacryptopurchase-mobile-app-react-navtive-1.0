import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import {GX_API_ENDPOINT} from '../../configs';
import {AppContext} from '../../contexts/AppContextProvider';
import {formatterHelper} from '../../utils';
import LoadingAnimation from '../LoadingAnimation';

const InstrumentsList = () => {
  const {homeSearchInput} = useContext(AppContext);

  const [listData, setListData] = useState();
  const [filteredList, setFilteredList] = useState([]);

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/coin/investment/types/get`)
      .then(({data}) => {
        setListData(data.investments || []);
      })
      .catch((error) => {
        console.log('Error on getting offerings', error);
      });
  }, []);

  useEffect(() => {
    if (listData) {
      const queryString = homeSearchInput.toLowerCase();

      const list = listData.filter((item) =>
        item?.name?.toLowerCase().includes(queryString),
      );
      setFilteredList(list);
    }
  }, [listData, homeSearchInput]);

  return (
    <View style={styles.container}>
      {listData ? (
        <FlatList
          data={filteredList}
          keyExtractor={(item) => item._id}
          renderItem={({item}) => (
            <View style={styles.itemContainer}>
              <Image
                source={{uri: item?.icon || ''}}
                style={styles.itemIcon}
                resizeMode="cover"
              />
              <Text style={styles.itemName}>{item?.name}</Text>
              {/* <View style={styles.priceContainer}>
                <Text style={styles.itemPrice}>
                  {formatterHelper(item?.token_price || 0, item?.asset)}
                </Text>
                <Text style={styles.itemCoin}>{item?.asset}</Text>
              </View> */}
            </View>
          )}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>No Supported Fund Found</Text>
            </View>
          }
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default InstrumentsList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    paddingTop: 10,
    backgroundColor: '#f7f7f7',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  itemContainer: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
  },
  itemIcon: {
    width: 25,
    height: 25,
    borderRadius: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  itemName: {
    flex: 1,
    color: '#001D41',
    fontSize: 16,
    marginBottom: 2,
    fontFamily: 'Montserrat-SemiBold',
    marginLeft: 10,
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'baseline',
  },
  itemPrice: {
    color: '#001D41',
    opacity: 0.5,
    textAlign: 'right',
    fontFamily: 'Roboto-Bold',
    marginRight: 10,
  },
  itemCoin: {
    textAlign: 'right',
    color: '#001D41',
    opacity: 0.5,
    fontFamily: 'Roboto',
    fontSize: 10,
  },
  emptyContainer: {
    marginTop: 20,
  },
  emptyText: {
    fontFamily: 'Montserrat',
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 16,
    paddingHorizontal: 30,
  },
});
