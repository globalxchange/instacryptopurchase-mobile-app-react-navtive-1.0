import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {AppContext} from '../../contexts/AppContextProvider';
import BankersList from '../BankersList';
import FundsList from './FundsList';
import InstrumentsList from './InstrumentsList';
import OfferingList from './OfferingList';

const InvestmentLists = () => {
  const {activeListCategory} = useContext(AppContext);

  const [activeFragment, setActiveFragment] = useState(null);

  useEffect(() => {
    switch (activeListCategory.title) {
      case 'Offerings':
        setActiveFragment(<OfferingList />);
        break;
      case 'Funds':
        setActiveFragment(<FundsList />);
        break;
      case 'Instruments':
        setActiveFragment(<InstrumentsList />);
        break;
      case 'Bankers':
        setActiveFragment(<BankersList />);
        break;
      default:
        setActiveFragment(<OfferingList />);
    }
    return () => {};
  }, [activeListCategory]);

  return <View style={styles.container}>{activeFragment}</View>;
};

export default InvestmentLists;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
