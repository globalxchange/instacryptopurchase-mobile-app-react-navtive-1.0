import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {GX_API_ENDPOINT} from '../../configs';
import {AppContext} from '../../contexts/AppContextProvider';
import LoadingAnimation from '../LoadingAnimation';

const OfferingList = () => {
  const {homeSearchInput} = useContext(AppContext);

  const [listData, setListData] = useState();
  const [filteredList, setFilteredList] = useState([]);

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/coin/investment/path/get`, {
      params: {pathType: 'offering'},
    })
      .then(({data}) => {
        setListData(data.paths || []);
      })
      .catch((error) => {
        console.log('Error on getting offerings', error);
      });
  }, []);

  useEffect(() => {
    if (listData) {
      const queryString = homeSearchInput.toLowerCase();

      const list = listData.filter((item) =>
        item?.name?.toLowerCase().includes(queryString),
      );
      setFilteredList(list);
    }
  }, [listData, homeSearchInput]);

  return (
    <View style={styles.container}>
      {listData ? (
        <FlatList
          data={filteredList}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>No Supported Offerings Found</Text>
            </View>
          }
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default OfferingList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  emptyContainer: {
    marginTop: 20,
  },
  emptyText: {
    fontFamily: 'Montserrat',
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 16,
    paddingHorizontal: 30,
  },
});
