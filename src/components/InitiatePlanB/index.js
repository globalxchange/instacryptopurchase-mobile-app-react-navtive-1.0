/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useState} from 'react';
import {
  Image,
  Keyboard,
  Modal,
  Platform,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Animated, {
  Clock,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';
import Breadcrumb from './Breadcrumb';
import CompleteView from './Fragments/CompleteView';
import ExpirySelector from './Fragments/ExpirySelector';
import FromAppSelector from './Fragments/FromAppSelector';
import MessageForm from './Fragments/MessageForm';
import QuoteFrom from './Fragments/QuoteFrom';
import RecipientCurrency from './Fragments/RecipientCurrency';
import RecipientForm from './Fragments/RecipientForm';
import SendingCurrency from './Fragments/SendingCurrency';

const InitiatePlanB = ({isOpen, setIsOpen}) => {
  const {bottom} = useSafeAreaInsets();

  const [isExpanded, setIsExpanded] = useState(false);
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  const [currentStep, setCurrentStep] = useState();

  const [message, setMessage] = useState('');
  const [senderAppCode, setSenderAppCode] = useState('');
  const [senderProfileId, setSetSenderProfileId] = useState('');
  const [sendingCurrency, setSendingCurrency] = useState('');
  const [recipientCurrency, setRecipientCurrency] = useState('');
  const [recipientName, setRecipientName] = useState('');
  const [isMail, setIsMail] = useState(true);
  const [recipient, setRecipient] = useState('');
  const [recipientPhone, setRecipientPhone] = useState('');
  const [expiryDays, setExpiryDays] = useState('');

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  useEffect(() => {
    if (!isOpen) {
      resetState();
    }
  }, [isOpen]);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  const resetState = () => {
    setCurrentStep();
    setMessage('');
    setSenderAppCode('');
    setSetSenderProfileId('');
    setSendingCurrency('');
    setRecipientCurrency('');
    setRecipientName('');
    setIsMail(true);
    setRecipient('');
  };

  let activeViewContent;

  switch (currentStep) {
    case 'MessageForm':
      activeViewContent = (
        <MessageForm
          setMessage={setMessage}
          onNext={() => setCurrentStep('AppSelector')}
        />
      );
      break;
    case 'AppSelector':
      activeViewContent = (
        <FromAppSelector
          setAppCode={setSenderAppCode}
          setSenderProfileId={setSetSenderProfileId}
          onNext={() => setCurrentStep('SendingCurrency')}
        />
      );
      break;

    case 'SendingCurrency':
      activeViewContent = (
        <SendingCurrency
          senderAppCode={senderAppCode}
          senderProfileId={senderProfileId}
          sendingCurrency={sendingCurrency}
          setSendingCurrency={setSendingCurrency}
          onNext={() => setCurrentStep('RecipientCurrency')}
        />
      );
      break;

    case 'RecipientCurrency':
      activeViewContent = (
        <RecipientCurrency
          onNext={() => setCurrentStep('DestinationAddress')}
          recipientCurrency={recipientCurrency}
          setRecipientCurrency={setRecipientCurrency}
        />
      );
      break;

    case 'DestinationAddress':
      activeViewContent = (
        <RecipientForm
          currency={SendingCurrency}
          onNext={() => setCurrentStep('Expiry')}
          setEmail={setRecipient}
          setPhone={setRecipientPhone}
          setName={setRecipientName}
          setIsMail={setIsMail}
        />
      );
      break;

    case 'Expiry':
      activeViewContent = (
        <ExpirySelector
          onNext={() => setCurrentStep('Quote')}
          expiryDays={expiryDays}
          setExpiryDays={setExpiryDays}
        />
      );
      break;

    case 'Quote':
      activeViewContent = (
        <QuoteFrom
          senderAppCode={senderAppCode}
          senderProfileId={senderProfileId}
          name={recipientName}
          sendingCurrency={sendingCurrency}
          recipientCurrency={recipientCurrency}
          recipient={recipient}
          recipientPhone={recipientPhone}
          isMail={isMail}
          message={message}
          onNext={() => setCurrentStep('Completed')}
          expiryDays={expiryDays}
        />
      );
      break;

    case 'Completed':
      activeViewContent = (
        <CompleteView
          sendingCurrency={sendingCurrency}
          name={recipientName}
          isMail={isMail}
          onClose={() => setIsOpen(false)}
          isSend
        />
      );
      break;

    default:
      activeViewContent = (
        <MessageForm
          setMessage={setMessage}
          onNext={() => setCurrentStep('AppSelector')}
        />
      );
  }

  return (
    <Modal
      animationType="slide"
      visible={isOpen}
      transparent
      hardwareAccelerated
      statusBarTranslucent
      onDismiss={() => setIsOpen(false)}
      onRequestClose={() => setIsOpen(false)}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.overlay}
        onPress={() => setIsOpen(false)}
        onPressOut={() => {}}>
        <TouchableWithoutFeedback style={{flex: 1}}>
          <Animated.View
            style={[
              styles.container,
              // {maxHeight: isExpanded ? height - 40 : height * 0.75},
              {
                paddingBottom: bottom,
                transform: [
                  {
                    translateY: interpolate(chatKeyboardAnimation.current, {
                      inputRange: [0, 1],
                      outputRange: [0, -keyboardHeight],
                    }),
                  },
                ],
              },
            ]}>
            <View style={styles.fragmentContainer}>
              <Image
                source={require('../../assets/plan-b-icon-fill.png')}
                style={styles.headerImage}
                resizeMode="contain"
              />
              <View style={styles.viewContainer}>
                {currentStep !== 'Completed' && <Breadcrumb />}
                {activeViewContent}
              </View>
            </View>
          </Animated.View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default InitiatePlanB;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {
    // flex: 1,
  },
  headerImage: {
    height: 30,
    width: 170,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginVertical: 20,
  },
  viewContainer: {
    borderTopColor: '#EBEBEB',
    borderTopWidth: 1,
  },
});
