/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Animated from 'react-native-reanimated';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import LoadingAnimation from './LoadingAnimation';
import * as WebBrowser from 'expo-web-browser';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../configs';
import ThemeData from '../configs/ThemeData';

const BrokerAppInstaller = ({isOpen, setIsOpen}) => {
  const {bottom} = useSafeAreaInsets();

  const [latestUpdate, setLatestUpdate] = useState();

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/gxb/apps/mobile/app/links/logs/get`, {
      params: {app_code: 'broker_app'},
    })
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          const logs = data.logs || [];

          // console.log('Logs', logs);
          // logs.sort((a, b) => a.timestamp - b.timestamp);

          setLatestUpdate(logs[0] || '');
        }
      })
      .catch((error) => {});
  }, []);

  const onOpportunity = () => {
    WebBrowser.openBrowserAsync('https://brokerapp.io/');
  };

  const appInstallLink = (isAndroid) => {
    WebBrowser.openBrowserAsync(
      isAndroid
        ? latestUpdate.android_app_link || ''
        : latestUpdate.ios_app_link || '',
    );
  };

  return (
    <Modal
      animationType="slide"
      visible={isOpen}
      transparent
      hardwareAccelerated
      statusBarTranslucent
      onDismiss={() => setIsOpen(false)}
      onRequestClose={() => setIsOpen(false)}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.overlay}
        onPress={() => setIsOpen(false)}
        onPressOut={() => {}}>
        <TouchableWithoutFeedback style={{flex: 1}}>
          <Animated.View
            style={[
              styles.container,
              {
                // paddingBottom: bottom,
              },
            ]}>
            {latestUpdate ? (
              <View style={styles.fragmentContainer}>
                {/* <Image
                  style={styles.headerLogo}
                  source={require('../assets/broker-app-logo-dark.png')}
                  resizeMode="contain"
                /> */}
                <Text style={styles.mainHeader}>
                  Launch Your Own Crypto Brokerage
                </Text>
                <Text style={styles.headerText}>
                  By Following Us On The BrokerApp. Use The Links Below To
                  Download The App Today
                </Text>
                {/* <TouchableOpacity
                  onPress={onOpportunity}
                  style={styles.actionButton}>
                  <Image
                    source={require('../assets/direct-earning-icon.png')}
                    resizeMode="contain"
                    style={styles.actionIcon}
                  />
                  <Text style={styles.actionText}>Opportunity</Text>
                </TouchableOpacity> */}
                <TouchableOpacity
                  onPress={() => appInstallLink(false)}
                  style={styles.actionButton}>
                  <Image
                    source={require('../assets/apple-icon.png')}
                    resizeMode="contain"
                    style={styles.actionIcon}
                  />
                  <Text style={[styles.actionText]}>iOS</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => appInstallLink(true)}
                  style={styles.actionButton}>
                  <Image
                    source={require('../assets/android-icon.png')}
                    resizeMode="contain"
                    style={styles.actionIcon}
                  />
                  <Text style={[styles.actionText]}>Android</Text>
                </TouchableOpacity>
                {/* <TouchableOpacity
                  onPress={() => setIsOpen(false)}
                  style={styles.closeButton}>
                  <Text style={styles.closeText}>Close</Text>
                </TouchableOpacity> */}
              </View>
            ) : (
              <View style={styles.loadingAnimation}>
                <LoadingAnimation />
              </View>
            )}
          </Animated.View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default BrokerAppInstaller;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    backgroundColor: '#08152D',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {
    backgroundColor: 'white',
    paddingTop: 30,
    alignItems: 'center',
    paddingBottom: 100,
  },
  headerLogo: {
    height: 60,
    width: 200,
  },
  mainHeader: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 28,
    textAlign: 'center',
    paddingHorizontal: 30,
  },
  headerText: {
    textAlign: 'center',
    fontFamily: 'Montserrat',
    color: '#08152D',
    marginTop: 20,
    paddingHorizontal: 40,
    fontSize: 13,
  },
  actionButton: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EDEDED',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
    width: 200,
    justifyContent: 'center',
    marginTop: 25,
  },
  actionIcon: {
    height: 25,
    width: 25,
  },
  actionText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#555555',
    fontSize: 16,
    marginLeft: 15,
  },
  closeButton: {
    marginTop: 40,
    backgroundColor: '#186AB4',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  closeText: {
    fontFamily: 'Montserrat-Bold',
    color: 'white',
  },
  loadingAnimation: {
    height: 350,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
});
