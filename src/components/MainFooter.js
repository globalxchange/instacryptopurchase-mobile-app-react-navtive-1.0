import React, {useContext} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {AppContext} from '../contexts/AppContextProvider';
import ThemeData from '../configs/ThemeData';
import PendingTxnFooter from './PendingTxnFooter';

const MainFooter = () => {
  const {navigate} = useNavigation();

  const {isLoggedIn} = useContext(AppContext);

  return (
    <>
      {isLoggedIn ? (
        <PendingTxnFooter />
      ) : (
        <TouchableOpacity
          style={styles.footerContainer}
          onPress={() => navigate('Landing')}>
          <Text style={styles.loginText}>Click Here To Login</Text>
        </TouchableOpacity>
      )}
    </>
  );
};

export default MainFooter;

const styles = StyleSheet.create({
  footerContainer: {
    backgroundColor: '#186AB4',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 30,
    justifyContent: 'center',
  },
  footerIcon: {
    width: 120,
    height: 30,
  },
  buttonContainer: {
    borderColor: '#FFFFFF',
    borderWidth: 1,
    height: 30,
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  buttonText: {
    color: '#FFFFFF',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
  viewContainer: {
    padding: 45,
  },
  headerImage: {
    height: 40,
    width: 200,
  },
  desc: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
    marginVertical: 20,
    fontSize: 12,
  },
  note: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    marginBottom: 10,
  },
  feeItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
    marginTop: 20,
  },
  feeImage: {
    height: 25,
    width: 25,
  },
  sefAmount: {
    flex: 1,
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    color: '#788995',
  },
  offAmount: {
    fontFamily: 'Montserrat',
    color: '#788995',
  },
  goToButton: {
    backgroundColor: '#186AB4',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  gotoButtonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 15,
  },
  loginText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
    fontSize: 16,
  },
});
