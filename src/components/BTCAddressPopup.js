import axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {Image, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Clipboard from '@react-native-community/clipboard';
import {APP_CODE, GX_API_ENDPOINT} from '../configs';
import ThemeData from '../configs/ThemeData';
import {AppContext} from '../contexts/AppContextProvider';
import PopupLayout from '../layouts/PopupLayout';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import QRCode from 'react-native-qrcode-svg';
import NewTabIcon from '../assets/VectorIcons/new-tab-icon.svg';
import CopyIcon from '../assets/VectorIcons/copy-icon.svg';
import ShareIcon from '../assets/VectorIcons/share-icon.svg';
import BlochainIcon from '../assets/VectorIcons/blockchain-com-icon.svg';

const BTCAddressPopup = ({isOpen, onClose, activeWallet}) => {
  const {walletCoinData, updateWalletBalances} = useContext(AppContext);

  const [isShowAddress, setIsShowAddress] = useState(false);

  const [isCopied, setIsCopied] = useState(false);
  const [qrSize, setQrSize] = useState(0);
  const [address, setAddress] = useState('');

  useEffect(() => {
    if (walletCoinData) {
      const coin = walletCoinData.find(
        (item) => item.coinSymbol === activeWallet.coinSymbol,
      );

      // console.log('walletCoinData', walletCoinData);

      const addr = coin?.coin_address || coin?.erc20Address || '';
      setAddress(addr);
      if (
        !addr &&
        (activeWallet.coinSymbol === 'BTC' ||
          activeWallet.coinSymbol === 'TRX' ||
          activeWallet.coinSymbol === 'XRP')
      ) {
        requestForAddress(activeWallet.coinSymbol);
      }
    }
  }, [activeWallet, walletCoinData]);

  const requestForAddress = async (coin) => {
    const email = await AsyncStorageHelper.getLoginEmail();

    let postData = {email, app_code: APP_CODE};

    if (coin !== 'BTC') {
      postData = {...postData, coin};
    }

    let API =
      coin === 'BTC'
        ? `${GX_API_ENDPOINT}/coin/vault/service/coin/request/address`
        : `${GX_API_ENDPOINT}/coin/vault/service/crypto/address/request`;

    axios
      .post(API, postData)
      .then((resp) => {
        const {data} = resp;
        // console.log('Data', data);

        setAddress(data.address || '');

        updateWalletBalances();
      })
      .catch((error) => {
        console.log('Error on requesting address', error);
      });
  };

  const copyToClipboardHandler = () => {
    Clipboard.setString(address);
    setIsCopied(true);
  };

  return (
    <PopupLayout
      isOpen={isOpen}
      onClose={onClose}
      noHeader
      noScrollView
      autoHeight>
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <View style={styles.coinContainer}>
            <Image
              style={styles.cryptoLogo}
              source={{uri: activeWallet?.coinImage}}
            />
            <Text style={styles.coinName}>{activeWallet?.coinName}</Text>
          </View>
          <View style={styles.subHeader}>
            <Text style={styles.poweredText}>Vault Powered By</Text>
            <Image
              source={require('../assets/block-check-icon-blue.png')}
              style={styles.blockCheckLogo}
            />
          </View>
        </View>
        {isShowAddress ? (
          <>
            <View style={styles.addressContainer}>
              <View style={styles.qrContainer}>
                <QRCode
                  value={address || 'Empty'}
                  size={230}
                  color={address ? ThemeData.APP_MAIN_COLOR : '#C5DAEC'}
                />
              </View>
              <View style={styles.addressTextContainer}>
                <TouchableOpacity
                  style={styles.textContainer}
                  onPress={() => setIsCopied(false)}>
                  <Text
                    numberOfLines={1}
                    style={[styles.address, isCopied && styles.copiedText]}>
                    {isCopied ? 'Copied To Clipboard' : address || 'Empty'}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.copyContainer}
                  onPress={copyToClipboardHandler}>
                  <CopyIcon color={ThemeData.APP_MAIN_COLOR} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.copyContainer}
                  onPress={copyToClipboardHandler}>
                  <NewTabIcon color={ThemeData.APP_MAIN_COLOR} />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.actionContainer}>
              <TouchableOpacity
                style={styles.actionItem}
                onPress={() => setIsShowAddress(true)}>
                <ShareIcon color={ThemeData.APP_MAIN_COLOR} />
                <Text
                  style={[
                    styles.actionItemName,
                    {marginLeft: 8, fontSize: 14},
                  ]}>
                  Share
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.actionItem}
                onPress={() => setIsShowAddress(true)}>
                <BlochainIcon />
                <Text
                  style={[
                    styles.actionItemName,
                    {marginLeft: 8, fontSize: 14},
                  ]}>
                  Blockchain.com
                </Text>
              </TouchableOpacity>
            </View>
          </>
        ) : (
          <>
            <Text style={styles.desclimerText}>
              This Bitcoin Address Can Only Receive Bitcoin Using The BTC
              Blockchain.
            </Text>
            <Text style={styles.desclimerText}>
              Non-BTC Coins And Bitcoin Sent Using Non-BTC Blockchain Networks
              Will Not Be Received If Sent To This Address
            </Text>
            <View style={styles.actionContainer}>
              <TouchableOpacity
                style={styles.actionItem}
                onPress={() => setIsShowAddress(true)}>
                <Text style={styles.actionItemName}>I Understand</Text>
              </TouchableOpacity>
            </View>
          </>
        )}
      </View>
    </PopupLayout>
  );
};

export default BTCAddressPopup;

const styles = StyleSheet.create({
  container: {
    marginBottom: -30,
  },
  headerContainer: {
    alignItems: 'center',
  },
  coinContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  cryptoLogo: {
    width: 37,
    height: 37,
    resizeMode: 'contain',
  },
  coinName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 32,
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    marginLeft: 5,
  },
  subHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  poweredText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#186AB4',
    fontSize: 11,
  },
  blockCheckLogo: {
    height: 12,
    resizeMode: 'contain',
    width: 100,
  },
  desclimerText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
    textAlign: 'center',
    marginVertical: 15,
    lineHeight: 20,
    color: '#464B4E',
  },
  actionContainer: {
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    marginHorizontal: -30,
    flexDirection: 'row',
  },
  actionItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 55,
    flexDirection: 'row',
  },
  actionItemName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 15,
  },
  addressContainer: {
    alignItems: 'center',
    paddingVertical: 20,
  },
  qrContainer: {
    justifyContent: 'center',
    marginBottom: 20,
  },
  addressTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textContainer: {
    borderColor: '#EDEDED',
    borderWidth: 1,
    flexGrow: 1,
    width: 0,
    justifyContent: 'center',
    height: 40,
    paddingHorizontal: 10,
  },
  address: {
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 10,
  },
  copiedText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
  },
  copyContainer: {
    borderColor: '#EDEDED',
    borderWidth: 1,
    height: 40,
    width: 40,
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  copyIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  notSupportedView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  notSupportedText: {
    fontFamily: 'Montserrat-Bold',
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 18,
  },
});
