import OTPInputView from '@twotalltotems/react-native-otp-input';
import React, {useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import ThemeData from '../../configs/ThemeData';
import KeyboardOffset from '../KeyboardOffset';
import LoadingAnimation from '../LoadingAnimation';
import PasswordForm from './PasswordForm';

const ChangePassword = ({onBack}) => {
  const [optInput, setOptInput] = useState('');
  const [showPasswordForm, setShowPasswordForm] = useState(false);

  const onOTPConfirm = () => {
    if (!optInput.trim() || optInput.trim().length < 6) {
      return WToast.show({
        data: 'Please Enter The OTP',
        position: WToast.position.TOP,
      });
    }

    setShowPasswordForm(true);
  };

  if (showPasswordForm) {
    return (
      <PasswordForm
        otp={optInput.trim()}
        onBack={() => setShowPasswordForm(false)}
      />
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>Enter Code</Text>
      </View>
      <View style={{marginVertical: 50}}>
        <OTPInputView
          codeInputFieldStyle={styles.underlineStyleBase}
          codeInputHighlightStyle={styles.underlineStyleHighLighted}
          style={styles.otpInput}
          pinCount={6}
          // autoFocusOnLoad
          code={optInput}
          onCodeChanged={(code) => setOptInput(code)}
        />
      </View>
      <View style={styles.actionContainer}>
        <TouchableOpacity style={styles.outlinedButton} onPress={onOTPConfirm}>
          <Text style={styles.outlinedButtonText}>Confirm</Text>
        </TouchableOpacity>
      </View>
      <KeyboardOffset />
    </View>
  );
};

export default ChangePassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 18,
    color: ThemeData.APP_MAIN_COLOR,
  },
  actionContainer: {
    flexDirection: 'row',
    paddingHorizontal: 30,
  },
  outlinedButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  outlinedButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  otpInput: {
    height: 60,
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
  },
  underlineStyleBase: {
    borderWidth: 1,
    color: '#08152D',
  },
  underlineStyleHighLighted: {
    borderColor: '#999C9A',
    color: '#08152D',
  },
});
