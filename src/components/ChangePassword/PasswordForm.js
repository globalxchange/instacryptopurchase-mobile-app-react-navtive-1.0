/* eslint-disable react-native/no-inline-styles */
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {GX_AUTH_URL} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import KeyboardOffset from '../KeyboardOffset';
import LoadingAnimation from '../LoadingAnimation';
import SuccessPage from '../MFASettings/SuccessPage';

const PasswordForm = ({otp, onBack}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [lengthValidity, setLengthValidity] = useState(false);
  const [capitalValidity, setCapitalValidity] = useState(false);
  const [numberValidity, setNumberValidity] = useState(false);
  const [specialCharValidity, setSpecialCharValidity] = useState(false);
  const [confirmValidity, setConfirmValidity] = useState(false);
  const [passwordInput, setPasswordInput] = useState('');
  const [confirmInput, setConfirmInput] = useState('');
  const [showSuccess, setShowSuccess] = useState(false);

  useEffect(() => {
    const capRegex = new RegExp(/^.*[A-Z].*/);
    const numRegex = new RegExp(/^.*[0-9].*/);
    const speRegex = new RegExp(/^.*[!@#$%^&*()+=].*/);

    const password = passwordInput.trim();

    setLengthValidity(password.length >= 6);
    setCapitalValidity(capRegex.test(password));
    setNumberValidity(numRegex.test(password));
    setSpecialCharValidity(speRegex.test(password));
  }, [passwordInput]);

  useEffect(() => {
    setConfirmValidity(
      confirmInput.length >= 6 && confirmInput === passwordInput,
    );
  }, [confirmInput, passwordInput]);

  const changePassword = async () => {
    if (
      !(
        lengthValidity &&
        capitalValidity &&
        numberValidity &&
        specialCharValidity &&
        confirmValidity
      )
    ) {
      return WToast.show({
        data: 'Password Does Not Match',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();

    axios
      .post(`${GX_AUTH_URL}/gx/user/password/forgot/confirm`, {
        email,
        code: otp,
        newPassword: passwordInput,
      })
      .then(({data}) => {
        // console.log('RestPassword Data', data);

        if (data.status) {
          setShowSuccess(true);
        } else {
          WToast.show({
            data: data.message || 'Something Went Wrong',
            position: WToast.position.TOP,
          });
        }
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  if (isLoading) {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <LoadingAnimation />
      </View>
    );
  }

  if (showSuccess) {
    return (
      <SuccessPage
        header="Congratulations"
        message="Your Password Has Been Successfully Updated"
      />
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>Enter New Password</Text>
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholderTextColor={'#878788'}
          value={passwordInput}
          onChangeText={(text) => setPasswordInput(text)}
          placeholder="New Password"
          secureTextEntry
        />
        <View
          style={[
            styles.validatorSign,
            {
              backgroundColor:
                lengthValidity &&
                capitalValidity &&
                numberValidity &&
                specialCharValidity
                  ? '#08152D'
                  : '#D80027',
            },
          ]}
        />
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholderTextColor={'#878788'}
          value={confirmInput}
          onChangeText={(text) => setConfirmInput(text)}
          placeholder="Confirm Password"
          secureTextEntry
        />
        <View
          style={[
            styles.validatorSign,
            {
              backgroundColor: confirmValidity ? '#08152D' : '#D80027',
            },
          ]}
        />
      </View>
      <View style={styles.actionContainer}>
        <TouchableOpacity
          style={styles.outlinedButton}
          onPress={changePassword}>
          <Text style={styles.outlinedButtonText}>Confirm</Text>
        </TouchableOpacity>
      </View>
      <KeyboardOffset />
    </View>
  );
};

export default PasswordForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 18,
    color: ThemeData.APP_MAIN_COLOR,
  },
  inputContainer: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginTop: 25,
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  input: {
    flex: 1,
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    height: 50,
  },
  validatorSign: {
    width: 8,
    height: 8,
    borderRadius: 4,
  },
  actionContainer: {
    flexDirection: 'row',
    paddingHorizontal: 30,
    marginTop: 30,
  },
  outlinedButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  outlinedButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
});
