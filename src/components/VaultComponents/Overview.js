import React from 'react';
import {StyleSheet, View} from 'react-native';
import VaultChart from '../VaultChart';

const Overview = ({BalanceComponent, activeWallet, selectedApp}) => {
  return (
    <View style={styles.container}>
      {BalanceComponent}
      <VaultChart activeWallet={activeWallet} selectedApp={selectedApp} />
    </View>
  );
};

export default Overview;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
