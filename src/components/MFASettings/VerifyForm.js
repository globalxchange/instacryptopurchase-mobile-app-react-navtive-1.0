import OTPInputView from '@twotalltotems/react-native-otp-input';
import axios from 'axios';
import React, {useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {GX_AUTH_URL} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import KeyboardOffset from '../KeyboardOffset';
import LoadingAnimation from '../LoadingAnimation';
import SuccessPage from './SuccessPage';

const VerifyForm = ({onBack}) => {
  const [optInput, setOptInput] = useState('');
  const [noOfTries, setNoOfTries] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);

  const onVerifyHandler = async () => {
    if (!optInput.trim()) {
      return WToast.show({
        data: 'Please Input The Code Form Your Google Authenticator App',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);
    setNoOfTries(noOfTries + 1);
    const email = await AsyncStorageHelper.getLoginEmail();
    const accessToken = await AsyncStorageHelper.getAccessToken();

    const postData = {
      email,
      accessToken,
      code: optInput.trim(),
    };

    axios
      .post(`${GX_AUTH_URL}/gx/user/mfa/set/verify`, postData)
      .then(({data}) => {
        // console.log('Verify Daqta', data);

        if (data.status) {
          setIsSuccess(true);
        } else {
          WToast.show({data: data.message, position: WToast.position.TOP});
        }
        setIsSuccess(true);
      })
      .catch((error) => {
        console.log('Error on verifying pin', error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  if (isSuccess) {
    return <SuccessPage />;
  }

  return (
    <View style={styles.container}>
      {isLoading ? (
        <View style={{flex: 1, justifyContent: 'center'}}>
          <LoadingAnimation />
        </View>
      ) : (
        <>
          <View style={styles.headerContainer}>
            <Image
              resizeMode="contain"
              style={styles.authenticatorIcon}
              source={require('../../assets/authenticator-icon.png')}
            />
            <Text style={styles.headerText}>Verification</Text>
          </View>
          <Text numberOfLines={1} adjustsFontSizeToFit style={styles.subText}>
            {noOfTries > 0
              ? 'You Have Entered The Wrong Verification Code. Please Try Again'
              : 'Enter The One Time Code Form Your Google Authenticator App'}
          </Text>
          <View style={{marginVertical: 50}}>
            <OTPInputView
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              style={styles.otpInput}
              pinCount={6}
              // autoFocusOnLoad
              code={optInput}
              onCodeChanged={(code) => setOptInput(code)}
            />
          </View>
          <View style={styles.actionContainer}>
            <TouchableOpacity style={styles.outlinedButton} onPress={onBack}>
              <Text style={styles.outlinedButtonText}>Go Back</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.filledButton}
              onPress={onVerifyHandler}>
              <Text style={styles.filledButtonText}>Im Done</Text>
            </TouchableOpacity>
          </View>
          <KeyboardOffset />
        </>
      )}
    </View>
  );
};

export default VerifyForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 18,
    color: ThemeData.APP_MAIN_COLOR,
  },
  authenticatorIcon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  actionContainer: {
    flexDirection: 'row',
    paddingHorizontal: 30,
  },
  outlinedButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  outlinedButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  filledButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    marginLeft: 15,
  },
  filledButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
  },
  subText: {
    textAlign: 'center',
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 20,
  },
  otpInput: {
    height: 60,
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
  },
  underlineStyleBase: {
    borderWidth: 1,
    color: '#08152D',
  },
  underlineStyleHighLighted: {
    borderColor: '#999C9A',
    color: '#08152D',
  },
});
