import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {GX_AUTH_URL} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import ProfileSettingsItem from '../ProfileSettingsItem';
import KeyView from './KeyView';
import QRCodeView from './QRCodeView';
import VerifyForm from './VerifyForm';

const EnablingOptions = () => {
  const {navigate} = useNavigation();

  const {userEmail} = useContext(AppContext);

  const [email, setEmail] = useState('');
  const [secretCode, setSecretCode] = useState('');
  const [qrCodeValue, setQrCodeValue] = useState('');
  const [isQROpen, setIsQROpen] = useState(false);
  const [isKeyOpen, setIsKeyOpen] = useState(false);
  const [isDone, setIsDone] = useState(false);

  useEffect(() => {
    if (isKeyOpen) {
      setIsQROpen(false);
    }
  }, [isKeyOpen]);

  useEffect(() => {
    if (isQROpen) {
      setIsKeyOpen(false);
    }
  }, [isQROpen]);

  useEffect(() => {
    if (isDone) {
      setIsKeyOpen(false);
      setIsQROpen(false);
    }
  }, [isDone]);

  useEffect(() => {
    setEmail(userEmail);

    (async () => {
      const accessToken = await AsyncStorageHelper.getAccessToken();

      axios
        .post(`${GX_AUTH_URL}/gx/user/mfa/set`, {
          email: userEmail,
          accessToken,
          client_app: 'GX',
        })
        .then(({data}) => {
          // console.log('mfa data', data);
          setQrCodeValue(data?.qrcodeValue || '');
          setSecretCode(data?.SecretCode || '');
        })
        .catch((error) => {
          console.log('Error on getting mfa keys', error);
        });
    })();
  }, [userEmail]);

  if (isQROpen) {
    return (
      <QRCodeView
        onBack={() => setIsQROpen(false)}
        qrCode={qrCodeValue}
        onNext={() => setIsDone(true)}
      />
    );
  }

  if (isKeyOpen) {
    return (
      <KeyView
        onBack={() => setIsKeyOpen(false)}
        secretCode={secretCode}
        onNext={() => setIsDone(true)}
      />
    );
  }

  if (isDone) {
    return <VerifyForm onBack={() => setIsDone(false)} />;
  }

  return (
    <View style={styles.container}>
      <Text numberOfLines={1} adjustsFontSizeToFit style={styles.helpText}>
        Click Here If Its Your FIrst Time Doing This And You Need Some Help
      </Text>
      <ProfileSettingsItem
        title="Scan QR Code"
        subText="Using Google Authenticator App"
        onPress={() => setIsQROpen(true)}
      />
      <ProfileSettingsItem
        title="Use Key"
        subText="Using Google Authenticator App"
        onPress={() => setIsKeyOpen(true)}
      />
    </View>
  );
};

export default EnablingOptions;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  helpText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
    marginBottom: 20,
    textDecorationLine: 'underline',
  },
});
