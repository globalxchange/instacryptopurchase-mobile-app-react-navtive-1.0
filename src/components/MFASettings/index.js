import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ProfileSettingsItem from '../ProfileSettingsItem';
import DisableConfirmation from './DisableConfirmation';
import EnablingOptions from './EnablingOptions';

const MFASettings = ({is2FAActive}) => {
  const [isFormOpen, setIsFormOpen] = useState(false);

  return (
    <View style={styles.container}>
      {isFormOpen ? (
        is2FAActive ? (
          <DisableConfirmation goBack={() => setIsFormOpen(false)} />
        ) : (
          <EnablingOptions />
        )
      ) : (
        <ProfileSettingsItem
          title={is2FAActive ? 'Disable 2FA' : 'Enable 2FA'}
          subText="Google Authenticator"
          onPress={() => setIsFormOpen(true)}
        />
      )}
    </View>
  );
};

export default MFASettings;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
