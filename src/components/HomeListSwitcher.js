/* eslint-disable react-native/no-inline-styles */
import React, {useContext} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableWithoutFeedback,
  ScrollView,
  Dimensions,
} from 'react-native';
import {listCategories} from '../configs';
import {AppContext} from '../contexts/AppContextProvider';

const {width} = Dimensions.get('window');

const HomeListSwitcher = ({
  openAddModal,
  isShowSearch,
  openSearch,
  openCountry,
  isCountryOpen,
}) => {
  const {activeListCategory, setActiveListCategory} = useContext(AppContext);

  const itemClick = (item) => {
    if (item.title === 'Add') {
      openAddModal();
    } else {
      setActiveListCategory(item);
    }
  };

  return (
    <View style={[styles.container, isShowSearch && {marginTop: -30}]}>
      {!isShowSearch ? (
        <TouchableWithoutFeedback
          style={styles.itemContainer}
          onPress={openSearch}>
          <View style={styles.item}>
            <View style={[styles.iconContainer]}>
              <Image
                style={styles.icon}
                source={require('../assets/search-new.png')}
                resizeMode="contain"
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      ) : (
        <TouchableWithoutFeedback
          style={styles.itemContainer}
          onPress={openCountry}>
          <View style={styles.item}>
            <View
              style={[
                styles.iconContainer,
                isCountryOpen && styles.itemActive,
              ]}>
              <Image
                style={styles.icon}
                source={require('../assets/country-icon.png')}
                resizeMode="contain"
              />
            </View>
            <Text
              style={[
                styles.title,
                {
                  display: isCountryOpen ? 'flex' : 'none',
                },
              ]}>
              Country
            </Text>
            {isCountryOpen && <View style={styles.bottomArrow} />}
          </View>
        </TouchableWithoutFeedback>
      )}
      <ScrollView
        style={[styles.scrollContainer]}
        horizontal
        showsHorizontalScrollIndicator={false}>
        {listCategories.map((item) => (
          <TouchableWithoutFeedback
            key={item.title}
            style={styles.itemContainer}
            onPress={() => itemClick(item)}>
            <View style={styles.item}>
              <View style={[styles.iconContainer]}>
                <Image
                  style={[
                    styles.icon,
                    !isCountryOpen &&
                      activeListCategory?.title === item.title &&
                      styles.itemActive,
                  ]}
                  source={item.icon}
                  resizeMode="contain"
                />
              </View>
              <Text
                style={[
                  styles.title,
                  {
                    display:
                      !isCountryOpen && activeListCategory?.title === item.title
                        ? 'flex'
                        : 'none',
                  },
                ]}>
                {item.title}
              </Text>
              {activeListCategory?.title === item.title && (
                <View style={styles.bottomArrow} />
              )}
            </View>
          </TouchableWithoutFeedback>
        ))}
      </ScrollView>
    </View>
  );
};

export default HomeListSwitcher;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: 10,
  },
  scrollContainer: {
    height: 90,
  },
  itemContainer: {paddingHorizontal: 5},
  item: {
    paddingVertical: 15,
    width: (width - 40) / 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 2,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    elevation: 2,
  },
  itemActive: {
    opacity: 1,
  },
  icon: {height: 22, width: 22, opacity: 0.4},
  title: {
    textAlign: 'center',
    color: '#788995',
    marginTop: 5,
    fontSize: 10,
    fontFamily: 'Montserrat-SemiBold',
  },
  bottomArrow: {
    backgroundColor: '#f7f7f7',
    width: 16,
    height: 16,
    position: 'absolute',
    bottom: -8,
    transform: [{rotate: '45deg'}],
  },
});
