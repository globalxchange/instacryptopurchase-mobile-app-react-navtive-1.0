import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import WalletTransfer from './WalletTransfer';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import MethodSelector from './MethodSelector';
import VaultWithdraw from './VaultWithdraw';

const WalletWithdraw = ({activeWallet, isKeyBoardOpen, openTxnAudit}) => {
  const {step, clearDepositState} = useContext(WithdrawalContext);

  const [pathId, setPathId] = useState('');

  const renderComponent = () => {
    switch (step) {
      case 'CryptoWithdraw':
        return (
          <WalletTransfer
            activeWallet={activeWallet}
            isKeyBoardOpen={isKeyBoardOpen}
            openTxnAudit={openTxnAudit}
            pathId={pathId}
          />
        );
      case 'VaultWithdraw':
        return <VaultWithdraw isKeyBoardOpen={isKeyBoardOpen} />;
      default:
        return <MethodSelector setPathId={setPathId} />;
    }
  };

  useEffect(() => {
    return () => clearDepositState();
  }, []);

  return <View style={styles.container}>{renderComponent()}</View>;
};

export default WalletWithdraw;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
  },
});
