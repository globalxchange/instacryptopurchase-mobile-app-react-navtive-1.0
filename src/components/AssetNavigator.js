import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const AssetNavigator = ({activeAsset}) => {
  return (
    <View>
      <View style={styles.container}>
        <Text style={styles.item}>Countries</Text>
        <Text style={[styles.item, styles.itemDisabled]}>From Currency</Text>
      </View>
      <Text style={styles.subs}>
        These Are The Countries In Which You Can Buy/Sell {activeAsset.coinName}{' '}
        On InstaCrypto With The Average Prices In The Local Currency Note: This
        Is Just An Average Price Per Country & Does Not Imply That This Will Be
        Your Price Upon Completing Checkout
      </Text>
    </View>
  );
};

export default AssetNavigator;

const styles = StyleSheet.create({
  container: {flexDirection: 'row', marginTop: 25},
  item: {
    fontFamily: 'Montserrat-Bold',
    color: '#464B4E',
    fontSize: 20,
    marginRight: 20,
  },
  itemDisabled: {
    opacity: 0.3,
  },
  subs: {
    marginTop: 20,
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
    fontSize: 11,
  },
});
