import React, {useEffect, useState, useContext} from 'react';
import {StyleSheet, Text, View, FlatList, Dimensions} from 'react-native';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import LoadingAnimation from '../../LoadingAnimation';
import PathItem from './PathItem';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import Dialog from 'react-native-dialog';
import {WToast} from 'react-native-smart-tip';
import {AppContext} from '../../../contexts/AppContextProvider';

const {height} = Dimensions.get('window');

const DeletePath = () => {
  const {updatePaths} = useContext(AppContext);

  const [paths, setPaths] = useState();
  const [isShowDialogue, setIsShowDialogue] = useState(false);
  const [selectedPath, setSelectedPath] = useState();

  useEffect(() => {
    getPaths();
  }, []);

  const getPaths = () => {
    setPaths();
    Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/paths/get`)
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          setPaths(data.paths);
        } else {
          setPaths([]);
        }
      })
      .catch((error) => console.log('Error on getting pathData', error));
  };

  const onItemSelected = (item) => {
    setSelectedPath(item);
    setIsShowDialogue(true);
  };

  const onAlertCancel = () => {
    setSelectedPath();
    setIsShowDialogue(false);
  };

  const deleteItem = async () => {
    setIsShowDialogue(false);
    if (selectedPath) {
      const email = await AsyncStorageHelper.getLoginEmail();
      const token = await AsyncStorageHelper.getAppToken();

      const postData = {
        token,
        email,
        path_id: selectedPath.path_id,
      };

      Axios.post(
        `${GX_API_ENDPOINT}/coin/vault/service/payment/paths/delete`,
        postData,
      ).then((resp) => {
        const {data} = resp;

        WToast.show({data: data.message, position: WToast.position.TOP});
        updatePaths();
        getPaths();
        setSelectedPath();
      });
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Delete Path</Text>
      {paths ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          style={styles.pathsList}
          data={paths}
          keyExtractor={(item) => item._id}
          renderItem={({item}) => (
            <PathItem data={item} onItemSelected={onItemSelected} />
          )}
          ListEmptyComponent={
            <View style={styles.loadingContainer}>
              <Text style={styles.emptyText}>No Paths Found...</Text>
            </View>
          }
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}

      <Dialog.Container visible={isShowDialogue}>
        <Dialog.Title>Delete Path</Dialog.Title>
        <Dialog.Description>
          Do you want to delete this path? You cannot undo this action.
        </Dialog.Description>
        <Dialog.Button label="Cancel" onPress={onAlertCancel} />
        <Dialog.Button label="Delete" onPress={deleteItem} />
      </Dialog.Container>
    </View>
  );
};

export default DeletePath;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 20,
  },
  loadingContainer: {
    paddingVertical: 80,
  },
  pathsList: {
    flexGrow: 0,
    maxHeight: height * 0.65,
  },
  emptyText: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
  },
});
