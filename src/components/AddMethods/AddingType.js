import React, {useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import CustomDropDown from '../CustomDropDown';
import PrimaryButton from './PrimaryButton';

const AddingType = ({setSelectedOption}) => {
  const [selectedAddType, setSelectedAddType] = useState(dropDownData[0]);

  const onProceed = () => {
    setSelectedOption(selectedAddType.name);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Admin Controls</Text>
      <View style={styles.controls}>
        <CustomDropDown
          label="What Are You Adding?"
          items={dropDownData}
          selectedItem={selectedAddType}
          onDropDownSelect={setSelectedAddType}
        />
      </View>
      <PrimaryButton
        title={`Add ${selectedAddType.name}`}
        onPress={onProceed}
      />
    </View>
  );
};

export default AddingType;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
  controls: {
    paddingVertical: 40,
    justifyContent: 'center',
  },

  pathButton: {
    paddingVertical: 5,
  },
  pathText: {
    color: '#9A9A9A',
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
});

const dropDownData = [
  {
    name: 'Define Payment Path',
    image: require('../../assets/paths-icon.png'),
  },
  {
    name: 'Update Payment Path',
    image: require('../../assets/paths-icon.png'),
  },
  {name: 'Delete Payment Path', image: require('../../assets/paths-icon.png')},
  {name: 'Add Banker', image: require('../../assets/bank-icon.png')},
  {name: 'Edit Banker', image: require('../../assets/bank-icon.png')},
  {name: 'Delete Banker', image: require('../../assets/bank-icon.png')},
  {name: 'Add Country', image: require('../../assets/country-icon.png')},
  {name: 'Edit Country', image: require('../../assets/country-icon.png')},
  {name: 'Delete Country', image: require('../../assets/country-icon.png')},
  {name: 'Add Payment Type', image: require('../../assets/money-icon.png')},
  {name: 'Edit Payment Type', image: require('../../assets/money-icon.png')},
  {name: 'Delete Payment Type', image: require('../../assets/money-icon.png')},
];
