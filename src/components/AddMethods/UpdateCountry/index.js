import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, FlatList, Dimensions} from 'react-native';
import Axios from 'axios';
import LoadingAnimation from '../../LoadingAnimation';
import CountryItem from './CountryItem';
import UpdateForm from './UpdateForm';

const {height} = Dimensions.get('window');

const UpdateCountry = () => {
  const [countryList, setCountryList] = useState();
  const [selectedCountry, setSelectedCountry] = useState('');

  useEffect(() => {
    getCounties();
  }, []);

  const getCounties = () => {
    setCountryList();
    Axios.get(
      'https://storeapi.apimachine.com/dynamic/InstaCryptoPurchase/Countrydem?key=a8a29286-f1a1-453a-9509-20decce27a1c',
    )
      .then((resp) => {
        const {data} = resp;
        data.success ? setCountryList(data.data) : setCountryList([]);
      })
      .catch((error) => {
        console.log('Error on getting country list', error);
      });
  };

  if (selectedCountry) {
    return (
      <UpdateForm
        selectedCountry={selectedCountry}
        closeEdit={() => setSelectedCountry('')}
      />
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Select A Country</Text>
      <View style={styles.listContainer}>
        {countryList ? (
          <FlatList
            showsVerticalScrollIndicator={false}
            style={styles.flatList}
            data={countryList}
            keyExtractor={(item) => item.formData.Name}
            renderItem={({item}) => (
              <CountryItem
                data={item}
                onItemSelect={() => setSelectedCountry(item)}
              />
            )}
          />
        ) : (
          <View style={styles.loadingContainer}>
            <LoadingAnimation />
          </View>
        )}
      </View>
    </View>
  );
};

export default UpdateCountry;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 20,
  },
  listContainer: {
    paddingVertical: 20,
  },
  flatList: {
    flexGrow: 0,
    maxHeight: height * 0.65,
  },
  loadingContainer: {
    paddingVertical: 30,
  },
});
