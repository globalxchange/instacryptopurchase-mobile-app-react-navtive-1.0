import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';

const CountryItem = ({data, onItemSelect}) => {
  return (
    <TouchableOpacity onPress={onItemSelect}>
      <View style={styles.container}>
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>Flag</Text>
          <Image
            style={styles.flag}
            source={{uri: data.formData.Flag}}
            resizeMode="contain"
          />
        </View>
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>Name</Text>
          <Text style={styles.dataValue}>{data.formData.Name}</Text>
        </View>
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>Code</Text>
          <Text style={styles.dataValue}>{data.formData.CountryCode}</Text>
        </View>
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>Processor</Text>
          <Text style={styles.dataValue}>{data.formData.Processor.trim()}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default CountryItem;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
    borderColor: '#DFDDE2',
    borderWidth: 1,
    borderRadius: 4,
    marginBottom: 20,
  },
  valueGroup: {
    flexDirection: 'row',
    paddingVertical: 2,
  },
  dataKey: {
    width: '30%',
    color: '#186AB4',
    fontFamily: 'Montserrat',
    fontSize: 10,
  },
  dataValue: {
    color: '#186AB4',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    flex: 1,
  },
  deleteButton: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    height: 30,
    width: 30,
    padding: 5,
  },
  icon: {
    flex: 1,
    width: null,
    height: null,
  },
  flag: {
    width: 30,
    height: 30,
  },
});
