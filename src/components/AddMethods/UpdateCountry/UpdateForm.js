import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import {Permissions, FileSystem} from 'react-native-unimodules';
import Constants from 'expo-constants';
import {decode} from 'base64-arraybuffer';
import {S3} from 'aws-sdk';
import {WToast} from 'react-native-smart-tip';
import PrimaryButton from '../PrimaryButton';
import Axios from 'axios';
import LoadingAnimation from '../../LoadingAnimation';
import {S3_CONFIG} from '../../../configs';

const UpdateForm = ({selectedCountry, closeEdit}) => {
  const [countryName, setCountryName] = useState('');
  const [countryCode, setCountryCode] = useState('');
  const [countryFlag, setCountryFlag] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (selectedCountry) {
      setCountryName(selectedCountry.formData.Name);
      setCountryCode(selectedCountry.formData.CountryCode.toString());
      setCountryFlag(selectedCountry.formData.Flag);
    }
  }, [selectedCountry]);

  useEffect(() => {
    (async () => {
      if (Constants.platform.ios) {
        const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
          // alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const imagePickerHandler = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
        base64: true,
      });
      if (!result.cancelled) {
        setCountryFlag(result.uri);
      }
    } catch (E) {
      console.log(E);
    }
  };

  const onUpdateHandler = async () => {
    if (!countryName) {
      return WToast.show({
        data: 'Please Input A Country Name',
        position: WToast.position.TOP,
      });
    }

    if (!countryCode || isNaN(parseInt(countryCode))) {
      return WToast.show({
        data: 'Please Input A Country Code',
        position: WToast.position.TOP,
      });
    }

    let imageUrl = null;

    setIsLoading(true);

    if (!countryFlag.includes('https://')) {
      try {
        let options = {encoding: FileSystem.EncodingType.Base64};
        const fileData = await FileSystem.readAsStringAsync(
          countryFlag,
          options,
        );

        const BUCKET_NAME = 'gxnitrousdata';
        const S3Client = new S3({
          ...S3_CONFIG,
          Bucket: BUCKET_NAME,
        });

        const arrayBuffer = decode(fileData);
        const params = {
          Bucket: BUCKET_NAME,
          Key: `vaultpathsCountry/${countryName}_${countryCode}${Date.now()}.jpg`,
          Body: arrayBuffer,
          ContentType: 'image/jpeg',
          ACL: 'public-read',
        };

        try {
          await S3Client.upload(params, async (err, s3Data) => {
            if (err) {
              WToast.show({
                data: 'Error On Uploading icon',
                position: WToast.position.TOP,
              });
              console.log('Uploading Profile Pic Error', err);
            }
            if (s3Data.Location) {
              imageUrl = s3Data.Location;
            }
          });
        } catch (error) {
          console.log('Error on uploading to s3');
          WToast.show({
            data: 'Unable to upload selected file',
            position: WToast.position.TOP,
          });
          setIsLoading(false);
        }
      } catch (error) {
        console.log('​getFile -> err', error);
        WToast.show({
          data: 'Unable to load selected file',
          position: WToast.position.TOP,
        });
        setIsLoading(false);
      }
    }

    const putData = {
      Key: 'none',
      formDatabase: [
        {name: 'Name', value: countryName},
        {name: 'CountryCode', value: parseInt(countryCode)},
        {
          name: 'Flag',
          value: imageUrl || selectedCountry.Flag,
        },
        {name: 'Processor', value: 'Both'},
      ],
      productID: '5eb313d7f10390217259d884',
      productName: 'InstaCryptoPurchase',
      route: 'Countrydem',
      adminServer: 'absuifdgbifa2153$%!^fh',
    };

    Axios.put('https://appapi.apimachine.com/createDyn/DBpush', putData)
      .then((resp) => {
        const {data} = resp;
        console.log('Country Edit Resp', data);
        WToast.show({
          data: resp.data.message,
          position: WToast.position.TOP,
        });

        setIsLoading(false);
        if (data.success) {
          closeEdit();
        }
      })
      .catch((error) => {
        console.log('Error on updating country data', error);
        setIsLoading(false);
      });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Update Country</Text>
      <View style={styles.formContainer}>
        <View style={styles.inputGroup}>
          <Text style={styles.label}>Country Name</Text>
          <TextInput
            style={styles.input}
            placeholder="Ex, Canada"
            placeholderTextColor="#9A9A9A"
            value={countryName}
            onChangeText={(text) => setCountryName(text)}
          />
        </View>
        <View style={styles.inputGroup}>
          <Text style={styles.label}>Country Code</Text>
          <TextInput
            style={styles.input}
            placeholder="Ex, 204"
            placeholderTextColor="#9A9A9A"
            keyboardType="decimal-pad"
            returnKeyType="done"
            value={countryCode}
            onChangeText={(text) => setCountryCode(text)}
          />
        </View>
        <View style={styles.inputGroup}>
          <Text style={styles.label}>Country Flag Icon</Text>
          <View style={styles.flagContainer}>
            <TouchableOpacity
              style={styles.flagButton}
              onPress={imagePickerHandler}>
              <Image
                style={styles.flagIcon}
                source={{uri: countryFlag}}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <PrimaryButton title="Update Country" onPress={onUpdateHandler} />
      {isLoading && (
        <View style={[StyleSheet.absoluteFill, styles.loadingContainer]}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default UpdateForm;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
  formContainer: {
    paddingVertical: 20,
  },
  inputGroup: {},
  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    marginBottom: 20,
    height: 45,
    paddingHorizontal: 10,
    color: 'black',
    fontFamily: 'Montserrat',
  },
  flagContainer: {
    paddingVertical: 10,
    alignItems: 'center',
  },
  flagButton: {
    width: 38,
    height: 38,
  },
  flagIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  loadingContainer: {
    zIndex: 2,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
});
