import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';

const AdminLogin = ({setDevLoggedIn}) => {
  const [passwordInput, setPasswordInput] = useState('');

  const onGandAccess = () => {
    if (!passwordInput) {
      return WToast.show({
        data: 'Please Input The Secret Password',
        position: WToast.position.TOP,
      });
    }

    if (passwordInput !== 'PleaseLemmeIn') {
      return WToast.show({
        data: "Nah.. You' Not An Admin",
        position: WToast.position.TOP,
      });
    }

    setDevLoggedIn(true);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>This Function Is Reserved For Admin</Text>
      <Image
        style={styles.icon}
        source={require('../../assets/emoji-looking.png')}
        resizeMode="contain"
      />

      <View style={[styles.inputForm]}>
        <Text style={styles.inputHeader}>What Is The Secret Password?</Text>
        <TextInput
          style={styles.passwordInput}
          placeholder="*********"
          placeholderTextColor="#788995"
          secureTextEntry
          value={passwordInput}
          onChangeText={(text) => setPasswordInput(text)}
          returnKeyType="done"
        />
        <TouchableOpacity style={styles.button} onPress={onGandAccess}>
          <Text style={styles.buttonText}>Gain Access</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default AdminLogin;

const styles = StyleSheet.create({
  container: {},
  header: {
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    color: '#186AB4',
    fontSize: 22,
    marginTop: 10,
  },
  icon: {
    height: 80,
    width: 80,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginVertical: 30,
  },
  inputForm: {
    alignItems: 'center',
  },
  inputHeader: {
    color: '#186AB4',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 18,
  },
  passwordInput: {
    textAlign: 'center',
    height: 50,
    fontFamily: 'Montserrat-SemiBold',
    color: '#186AB4',
    marginVertical: 30,
    letterSpacing: 3,
    width: '100%',
    fontSize: 20,
  },
  button: {
    backgroundColor: '#186AB4',
    paddingVertical: 10,
    borderRadius: 4,
    width: 200,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
  },
});
