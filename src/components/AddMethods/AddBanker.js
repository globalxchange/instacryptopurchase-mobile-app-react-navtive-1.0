import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const AddBanker = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Add Banker</Text>
      <View style={{height: 150}} />
    </View>
  );
};

export default AddBanker;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
});
