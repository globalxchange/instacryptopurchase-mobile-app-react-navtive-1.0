import React, {useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Keyboard} from 'react-native';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import CustomDropDown from '../CustomDropDown';
import {WToast} from 'react-native-smart-tip';
import {TextInput} from 'react-native-gesture-handler';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../configs';
import LoadingAnimation from '../LoadingAnimation';

const AddPaymentType = ({closeBottomSheet}) => {
  const [selectedTxnType, setSelectedTxnType] = useState('');
  const [methodName, setMethodName] = useState('');
  const [methodCode, setMethodCode] = useState('');
  const [methodDesc, setMethodDesc] = useState('');
  const [methodFee, setMethodFee] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const onSubmit = async () => {
    Keyboard.dismiss();

    if (!selectedTxnType) {
      return WToast.show({
        data: 'Please Select The Txn Type',
        position: WToast.position.TOP,
      });
    }

    if (!methodName.trim()) {
      return WToast.show({
        data: 'Please Input Payment Type Name',
        position: WToast.position.TOP,
      });
    }

    if (!methodCode.trim()) {
      return WToast.show({
        data: 'Please Input Payment Type Code',
        position: WToast.position.TOP,
      });
    }

    if (!methodDesc.trim()) {
      return WToast.show({
        data: 'Please Input Payment Type Description',
        position: WToast.position.TOP,
      });
    }

    if (
      !methodFee ||
      isNaN(parseFloat(methodFee)) ||
      parseFloat(methodFee) < 0
    ) {
      return WToast.show({
        data: 'Please Input A Valid Fee',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      token,
      email: 'shorupan@gmail.com',
      type: selectedTxnType.name.toLowerCase(), // "deposit" / "send"
      name: methodName.trim(),
      code: methodCode.trim(), // some unique code
      description: methodDesc.trim(),
      icon: 'link',
      fee: parseFloat(methodFee), // fee in USD
      status: true, // true / false --> represents active or inactive
      other_data: {},
    };

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/payment/methods/add`,
      postData,
    )
      .then((resp) => {
        const {data} = resp;
        setIsLoading(false);
        // console.log('Adding payment type', data);

        WToast.show({data: data.message, position: WToast.position.TOP});
        closeBottomSheet();
      })
      .catch((error) => {
        setIsLoading(false);
        WToast.show({data: 'API, Error', position: WToast.position.TOP});
        console.log('Error adding payment type', error);
      });
  };

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Add Payment Type</Text>
      <View style={styles.form}>
        <CustomDropDown
          items={txnTypes}
          selectedItem={selectedTxnType}
          onDropDownSelect={setSelectedTxnType}
          label="Transaction Type"
          placeHolder="Select A Type"
        />
        <TextInput
          placeholderTextColor="#9A9A9A"
          placeholder="Payment Method Name"
          style={styles.input}
          value={methodName}
          onChangeText={(text) => setMethodName(text)}
        />
        <TextInput
          placeholderTextColor="#9A9A9A"
          placeholder="Payment Method Code"
          style={styles.input}
          onChangeText={(text) => setMethodCode(text)}
          value={methodCode}
        />
        <TextInput
          placeholderTextColor="#9A9A9A"
          placeholder="Payment Method Description"
          style={styles.input}
          onChangeText={(text) => setMethodDesc(text)}
          value={methodDesc}
        />
        <TextInput
          placeholderTextColor="#9A9A9A"
          placeholder="Payment Fee"
          style={styles.input}
          onChangeText={(text) => setMethodFee(text)}
          value={methodFee}
          keyboardType="decimal-pad"
        />
      </View>
      <TouchableOpacity style={styles.proceedBtn} onPress={onSubmit}>
        <Text style={styles.btnText}>Proceed</Text>
      </TouchableOpacity>
    </View>
  );
};

export default AddPaymentType;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 15,
  },
  form: {
    marginVertical: 15,
  },
  input: {
    height: 45,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 10,
    marginBottom: 20,
    color: 'black',
  },
  proceedBtn: {
    backgroundColor: '#186AB4',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
  },
  btnText: {
    fontSize: 16,
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  loadingContainer: {
    // flex: 1,
    paddingVertical: 50,
    justifyContent: 'center',
  },
});

const txnTypes = [
  {name: 'Deposit', image: require('../../assets/deposit-txn-icon.png')},
  {name: 'Send', image: require('../../assets/withdraw-txn-icon.png')},
];
