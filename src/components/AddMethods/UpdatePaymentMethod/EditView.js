import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import CustomDropDown from '../../CustomDropDown';
import UpdateName from './UpdateName';
import UpdateDescription from './UpdateDescription';
import UpdateFees from './UpdateFees';
import UpdateIcon from './UpdateIcon';
import UpdateOtherData from './UpdateOtherData';

const EditView = ({selectedMethod}) => {
  const [activeEdit, setActiveEdit] = useState('');

  if (activeEdit.key === 'name') {
    return (
      <UpdateName
        selectedMethod={selectedMethod}
        closeEdit={() => setActiveEdit('')}
      />
    );
  }

  if (activeEdit.key === 'description') {
    return (
      <UpdateDescription
        selectedMethod={selectedMethod}
        closeEdit={() => setActiveEdit('')}
      />
    );
  }

  if (activeEdit.key === 'fee') {
    return (
      <UpdateFees
        selectedMethod={selectedMethod}
        closeEdit={() => setActiveEdit('')}
      />
    );
  }

  if (activeEdit.key === 'icon') {
    return (
      <UpdateIcon
        selectedMethod={selectedMethod}
        closeEdit={() => setActiveEdit('')}
      />
    );
  }

  if (activeEdit.key === 'other_data') {
    return (
      <UpdateOtherData
        selectedMethod={selectedMethod}
        closeEdit={() => setActiveEdit('')}
      />
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Update Path</Text>
      <View style={styles.controlContainer}>
        <CustomDropDown
          items={options}
          label="Edit Options"
          placeHolder="Select Any Edit Option"
          onDropDownSelect={setActiveEdit}
          selectedItem={activeEdit}
        />
      </View>
    </View>
  );
};

export default EditView;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 20,
  },
  controlContainer: {
    paddingVertical: 40,
  },
});

const options = [
  {key: 'name', name: 'Edit Name'},
  {key: 'description', name: 'Edit Description'},
  {key: 'fee', name: 'Edit Fees'},
  {key: 'icon', name: 'Edit Icon'},
  {key: 'other_data', name: 'Edit Other Data'},
];
