import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import PrimaryButton from '../PrimaryButton';
import {WToast} from 'react-native-smart-tip';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT, S3_CONFIG} from '../../../configs';
import LoadingAnimation from '../../LoadingAnimation';
import * as ImagePicker from 'expo-image-picker';
import {Permissions, FileSystem} from 'react-native-unimodules';
import Constants from 'expo-constants';
import {decode} from 'base64-arraybuffer';
import {S3} from 'aws-sdk';

const UpdateIcon = ({selectedMethod, closeEdit}) => {
  const [selectedIcon, setSelectedIcon] = useState('');

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    (async () => {
      if (Constants.platform.ios) {
        const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  useEffect(() => {
    if (selectedMethod) {
      setSelectedIcon(selectedMethod.icon);
    }
  }, [selectedMethod]);

  const imagePickerHandler = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
        base64: true,
      });
      if (!result.cancelled) {
        setSelectedIcon(result.uri);
      }
    } catch (E) {
      console.log(E);
      setIsLoading(false);
    }
  };

  const updateHandler = async () => {
    const newIcon = selectedIcon;

    if (!newIcon || newIcon.includes('https://')) {
      return WToast.show({
        data: 'Please select an icon',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    let options = {encoding: FileSystem.EncodingType.Base64};
    FileSystem.readAsStringAsync(selectedIcon, options)
      .then((data) => {
        setIsLoading(true);

        const BUCKET_NAME = 'gxnitrousdata';
        const S3Client = new S3({
          ...S3_CONFIG,
          Bucket: BUCKET_NAME,
        });

        const {name, code} = selectedMethod;

        const arrayBuffer = decode(data);
        const params = {
          Bucket: BUCKET_NAME,
          Key: `vaultpathsMethods/${name}_${code}${Date.now()}.jpg`,
          Body: arrayBuffer,
          ContentType: 'image/jpeg',
          ACL: 'public-read',
        };
        S3Client.upload(params, async (err, s3Data) => {
          if (err) {
            WToast.show({
              data: 'Error On Uploading icon',
              position: WToast.position.TOP,
            });
            console.log('Uploading Profile Pic Error', err);
          }
          if (s3Data.Location) {
            setSelectedIcon(s3Data.Location);

            const iconLink = s3Data.Location;

            const email = await AsyncStorageHelper.getLoginEmail();
            const token = await AsyncStorageHelper.getAppToken();

            const postData = {
              token,
              email,
              type: selectedMethod.type,
              code: selectedMethod.code,
              field: 'icon',
              value: iconLink,
            };

            Axios.post(
              `${GX_API_ENDPOINT}/coin/vault/service/payment/methods/update`,
              postData,
            )
              .then((resp) => {
                WToast.show({
                  data: resp.data.message,
                  position: WToast.position.TOP,
                });

                setIsLoading(false);
                closeEdit();
              })
              .catch((error) => {
                setIsLoading(false);
                WToast.show({
                  data: 'Error On Updating icon',
                  position: WToast.position.TOP,
                });
              });
          }
        });
      })
      .catch((err) => {
        console.log('​getFile -> err', err);
        setIsLoading(false);
      });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Update Icon</Text>
      <View style={styles.controlContainer}>
        <Text style={styles.label}>Click to update the icon</Text>
        <View style={styles.iconContainer}>
          <TouchableOpacity
            style={styles.iconButton}
            onPress={imagePickerHandler}>
            <Image source={{uri: selectedIcon}} style={styles.icon} />
          </TouchableOpacity>
        </View>
      </View>
      <PrimaryButton title="Update Icon" onPress={updateHandler} />
      {isLoading && (
        <View style={[StyleSheet.absoluteFill, styles.loadingContainer]}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default UpdateIcon;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 30,
  },
  controlContainer: {
    paddingVertical: 30,
  },
  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },

  loadingContainer: {
    justifyContent: 'center',
    backgroundColor: 'white',
    zIndex: 2,
  },
  iconContainer: {
    alignItems: 'center',
    marginTop: 10,
  },
  iconButton: {
    height: 60,
    width: 60,
  },
  icon: {
    flex: 1,
    width: null,
    height: null,
  },
});
