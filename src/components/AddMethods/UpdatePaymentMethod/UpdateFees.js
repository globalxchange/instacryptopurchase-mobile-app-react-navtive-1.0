import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import PrimaryButton from '../PrimaryButton';
import LoadingAnimation from '../../LoadingAnimation';
import {WToast} from 'react-native-smart-tip';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';

const UpdateFees = ({selectedMethod, closeEdit}) => {
  const [feeInput, setFeeInput] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (selectedMethod) {
      setFeeInput(selectedMethod.fee.toString());
    }
  }, [selectedMethod]);

  const updateHandler = async () => {
    const newFee = parseFloat(feeInput.trim());

    if (isNaN(newFee) || newFee < 0) {
      return WToast.show({
        data: 'Please input valid fees',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      token,
      email,
      type: selectedMethod.type,
      code: selectedMethod.code,
      field: 'fee',
      value: newFee,
    };

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/payment/methods/update`,
      postData,
    )
      .then((resp) => {
        const {data} = resp;
        WToast.show({
          data: data.message,
          position: WToast.position.TOP,
        });
        setIsLoading(false);
        if (data.status) {
          closeEdit();
        }
      })
      .catch((error) => {
        setIsLoading(false);
        WToast.show({
          data: 'Error On Updating name',
          position: WToast.position.TOP,
        });
      });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Update Fees</Text>
      <View style={styles.controlContainer}>
        <Text style={styles.label}>New Payment Method Fee</Text>
        <TextInput
          style={styles.input}
          placeholder="Ex 1.5"
          value={feeInput}
          onChangeText={(text) => setFeeInput(text)}
          placeholderTextColor="#9A9A9A"
          keyboardType="decimal-pad"
          returnKeyType="done"
        />
      </View>
      <PrimaryButton title="Update Description" onPress={updateHandler} />
      {isLoading && (
        <View style={[StyleSheet.absoluteFill, styles.loadingContainer]}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default UpdateFees;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 30,
  },
  controlContainer: {
    paddingVertical: 30,
  },
  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    marginBottom: 20,
    height: 45,
    paddingHorizontal: 10,
    color: 'black',
    fontFamily: 'Montserrat',
  },
  loadingContainer: {
    justifyContent: 'center',
    backgroundColor: 'white',
    zIndex: 2,
  },
});
