import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import PrimaryButton from '../PrimaryButton';
import LoadingAnimation from '../../LoadingAnimation';
import {WToast} from 'react-native-smart-tip';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';

const UpdateDescription = ({selectedMethod, closeEdit}) => {
  const [descriptionInput, setDescriptionInput] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (selectedMethod) {
      setDescriptionInput(selectedMethod.description);
    }
  }, [selectedMethod]);

  const updateHandler = async () => {
    const newDesc = descriptionInput.trim();

    if (!newDesc) {
      return WToast.show({
        data: 'Please input a description',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      token,
      email,
      type: selectedMethod.type,
      code: selectedMethod.code,
      field: 'description',
      value: newDesc,
    };

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/payment/methods/update`,
      postData,
    )
      .then((resp) => {
        const {data} = resp;
        WToast.show({
          data: data.message,
          position: WToast.position.TOP,
        });
        setIsLoading(false);
        if (data.status) {
          closeEdit();
        }
      })
      .catch((error) => {
        setIsLoading(false);
        WToast.show({
          data: 'Error On Updating description',
          position: WToast.position.TOP,
        });
      });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Update Description</Text>
      <View style={styles.controlContainer}>
        <Text style={styles.label}>New Payment Method Description</Text>
        <TextInput
          style={styles.input}
          placeholder={`What is a ${selectedMethod.name}`}
          value={descriptionInput}
          onChangeText={(text) => setDescriptionInput(text)}
          placeholderTextColor="#9A9A9A"
          multiline
        />
      </View>
      <PrimaryButton title="Update Description" onPress={updateHandler} />
      {isLoading && (
        <View style={[StyleSheet.absoluteFill, styles.loadingContainer]}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default UpdateDescription;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 30,
  },
  controlContainer: {
    paddingVertical: 30,
  },
  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    marginBottom: 20,
    height: 120,
    paddingHorizontal: 10,
    color: 'black',
    fontFamily: 'Montserrat',
  },
  loadingContainer: {
    justifyContent: 'center',
    backgroundColor: 'white',
    zIndex: 2,
  },
});
