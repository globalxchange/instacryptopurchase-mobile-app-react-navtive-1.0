import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, Dimensions, FlatList} from 'react-native';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import MethodItem from './MethodItem';
import LoadingAnimation from '../../LoadingAnimation';
import EditView from './EditView';

const {height} = Dimensions.get('window');

const UpdatePaymentMethod = () => {
  const [paymentMethods, setPaymentMethods] = useState();
  const [selectedMethod, setSelectedMethod] = useState();

  useEffect(() => {
    getMethods();
  }, []);

  const getMethods = () => {
    setPaymentMethods();
    Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/methods/get`, {
      params: {},
    })
      .then((resp) => {
        const methodsData = resp.data;
        const methods = methodsData.status ? methodsData.methods : [];

        setPaymentMethods(methods);
      })
      .catch((error) => console.log('Error getting methods list', error));
  };

  const onItemSelected = (item) => {
    setSelectedMethod(item);
  };

  if (selectedMethod) {
    return <EditView selectedMethod={selectedMethod} />;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Delete PaymentMethod</Text>

      {paymentMethods ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          style={styles.pathsList}
          data={paymentMethods}
          keyExtractor={(item) => item._id}
          renderItem={({item}) => (
            <MethodItem data={item} onItemSelected={onItemSelected} />
          )}
          ListEmptyComponent={
            <View style={styles.loadingContainer}>
              <Text style={styles.emptyText}>No Paths Found...</Text>
            </View>
          }
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default UpdatePaymentMethod;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 20,
  },
  loadingContainer: {
    paddingVertical: 80,
  },
  pathsList: {
    flexGrow: 0,
    maxHeight: height * 0.65,
  },
  emptyText: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
  },
});
