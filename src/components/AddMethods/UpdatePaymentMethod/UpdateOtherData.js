import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, FlatList, TouchableOpacity} from 'react-native';
import MediaForm from '../AddPaymentMethod/MediaForm';
import WebsiteData from '../AddPaymentMethod/WebsiteData';
import TextForm from '../AddPaymentMethod/TextForm';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import {WToast} from 'react-native-smart-tip';
import LoadingAnimation from '../../LoadingAnimation';

const UpdateOtherData = ({selectedMethod, closeEdit}) => {
  const [selectedControl, setSelectedControl] = useState('');
  const [descriptions, setDescriptions] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setDescriptions(selectedMethod.other_data);
  }, [selectedMethod]);

  const addWebsite = (item) => {
    const websites = descriptions.websites || [];
    websites.push(item);
    setDescriptions({...descriptions, websites});
  };

  const addUploads = (item) => {
    const uploads = descriptions.uploads || [];
    uploads.push(item);
    setDescriptions({...descriptions, uploads});
  };

  const addTexts = (item) => {
    const texts = descriptions.texts || [];
    texts.push(item);
    setDescriptions({...descriptions, texts});
  };

  const renderInputForm = () => {
    switch (selectedControl.key) {
      case 'websites':
        return <WebsiteData addWebsite={addWebsite} />;
      case 'uploads':
        return (
          <MediaForm setIsLoading={setIsLoading} addUploads={addUploads} />
        );
      case 'texts':
        return <TextForm addTexts={addTexts} />;
    }
  };

  const updateHandler = async () => {
    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      token,
      email,
      type: selectedMethod.type,
      code: selectedMethod.code,
      field: 'other_data',
      value: descriptions,
    };

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/payment/methods/update`,
      postData,
    )
      .then((resp) => {
        const {data} = resp;
        WToast.show({
          data: data.message,
          position: WToast.position.TOP,
        });
        setIsLoading(false);
        if (data.status) {
          closeEdit();
        }
      })
      .catch((error) => {
        setIsLoading(false);
        WToast.show({
          data: 'Error On Updating other_data',
          position: WToast.position.TOP,
        });
      });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Add Descriptions</Text>
      <Text style={styles.paymentName}>{selectedMethod.name}</Text>
      <View style={styles.additionalDataContainer}>
        <View style={styles.dataController}>
          <Text style={styles.label}>Add Additional Data</Text>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={controllers}
            keyExtractor={(item) => item.key}
            renderItem={({item}) => (
              <TouchableOpacity
                style={[
                  styles.controlsItem,
                  selectedControl.key === item.key && styles.activeItem,
                ]}
                onPress={() => setSelectedControl(item)}>
                <Text
                  style={[
                    styles.controlText,
                    selectedControl.key === item.key &&
                      styles.activeControlText,
                  ]}>
                  {item.title}
                  {descriptions[item.key]
                    ? ` (${descriptions[item.key].length})`
                    : ''}
                </Text>
              </TouchableOpacity>
            )}
          />
          <View style={styles.currentForm}>{renderInputForm()}</View>
          <TouchableOpacity style={styles.proceedBtn} onPress={updateHandler}>
            <Text style={styles.btnText}>Continue</Text>
          </TouchableOpacity>
        </View>
      </View>
      {isLoading && (
        <View style={[StyleSheet.absoluteFill, styles.loadingContainer]}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default UpdateOtherData;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 15,
  },
  inputContainer: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 10,
    marginBottom: 20,
  },
  input: {
    height: '100%',
    color: 'black',
    fontFamily: 'Montserrat',
  },
  paymentName: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
    textAlign: 'center',
  },

  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  additionalDataContainer: {
    paddingVertical: 20,
  },
  dataController: {},
  controlsItem: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 15,
    minWidth: 100,
    paddingVertical: 4,
  },
  activeItem: {
    borderColor: '#186AB4',
  },
  controlText: {
    fontFamily: 'Montserrat',
    color: 'rgba(33, 33, 33, 0.25)',
    textAlign: 'center',
    fontSize: 13,
  },
  activeControlText: {
    color: '#186AB4',
  },
  currentForm: {
    paddingVertical: 10,
  },
  proceedBtn: {
    backgroundColor: '#186AB4',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
  },
  btnText: {
    fontSize: 16,
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  loadingContainer: {
    justifyContent: 'center',
    backgroundColor: 'white',
    zIndex: 2,
  },
});

const controllers = [
  {title: 'Website', key: 'websites'},
  {title: 'Upload', key: 'uploads'},
  {title: 'Text', key: 'texts'},
];
