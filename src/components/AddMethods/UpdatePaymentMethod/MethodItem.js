import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';

const MethodItem = ({data, onItemSelected}) => {
  return (
    <TouchableOpacity onPress={() => onItemSelected(data)}>
      <View style={styles.container}>
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>Payment ID</Text>
          <Text style={styles.dataValue}>{data._id}</Text>
        </View>
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>Payment Name</Text>
          <Text style={styles.dataValue}>{data.name}</Text>
        </View>
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>Payment Code</Text>
          <Text style={styles.dataValue}>{data.code}</Text>
        </View>
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>Payment Code</Text>
          <Text style={styles.dataValue}>{data.code}</Text>
        </View>
        {/* <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>Payment Type</Text>
          <Text style={styles.dataValue}>{data?.type?.toUpperCase()}</Text>
        </View> */}
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>Payment Fee</Text>
          <Text style={styles.dataValue}>{data.fee}%</Text>
        </View>
        <View style={styles.deleteButton}>
          <Image
            style={styles.icon}
            source={require('../../../assets/edit-icon.png')}
            resizeMode="contain"
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default MethodItem;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
    borderColor: '#DFDDE2',
    borderWidth: 1,
    borderRadius: 4,
    marginBottom: 20,
  },
  valueGroup: {
    flexDirection: 'row',
    paddingVertical: 2,
  },
  dataKey: {
    width: '30%',
    color: '#186AB4',
    fontFamily: 'Montserrat',
    fontSize: 10,
  },
  dataValue: {
    color: '#186AB4',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    flex: 1,
  },
  deleteButton: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    height: 30,
    width: 30,
    padding: 5,
  },
  icon: {
    flex: 1,
    width: null,
    height: null,
  },
});
