import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import PrimaryButton from '../PrimaryButton';
import {WToast} from 'react-native-smart-tip';
import LoadingAnimation from '../../LoadingAnimation';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';

const UpdateName = ({selectedMethod, closeEdit}) => {
  const [nameInput, setNameInput] = useState('');

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (selectedMethod) {
      setNameInput(selectedMethod.name);
    }
  }, [selectedMethod]);

  const updateHandler = async () => {
    const newName = nameInput.trim();

    if (!newName) {
      return WToast.show({
        data: 'Please input a name',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      token,
      email,
      type: selectedMethod.type,
      code: selectedMethod.code,
      field: 'name',
      value: newName,
    };

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/payment/methods/update`,
      postData,
    )
      .then((resp) => {
        const {data} = resp;
        WToast.show({
          data: data.message,
          position: WToast.position.TOP,
        });
        setIsLoading(false);
        if (data.status) {
          closeEdit();
        }
      })
      .catch((error) => {
        setIsLoading(false);
        WToast.show({
          data: 'Error On Updating name',
          position: WToast.position.TOP,
        });
      });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Update Name</Text>
      <View style={styles.controlContainer}>
        <Text style={styles.label}>New Payment Method Name</Text>
        <TextInput
          style={styles.input}
          placeholder="Ex. Bank Transfer"
          value={nameInput}
          onChangeText={(text) => setNameInput(text)}
          placeholderTextColor="#9A9A9A"
        />
      </View>
      <PrimaryButton title="Update Name" onPress={updateHandler} />
      {isLoading && (
        <View style={[StyleSheet.absoluteFill, styles.loadingContainer]}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default UpdateName;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 30,
  },
  controlContainer: {
    paddingVertical: 30,
  },
  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    marginBottom: 20,
    height: 45,
    paddingHorizontal: 10,
    color: 'black',
    fontFamily: 'Montserrat',
  },
  loadingContainer: {
    justifyContent: 'center',
    backgroundColor: 'white',
    zIndex: 2,
  },
});
