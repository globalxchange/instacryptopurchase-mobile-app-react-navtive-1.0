import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';

const StepItem = ({pos, title, posTitle, onItemEditClick}) => {
  return (
    <View style={styles.stepContainer}>
      <Text style={styles.stepPosition}>{posTitle}</Text>
      <TouchableOpacity
        style={styles.stepNameContainer}
        onPress={onItemEditClick}>
        <Image style={styles.stepIcon} />
        <Text style={styles.stepName}>{title}</Text>
        <Image
          style={styles.icon}
          source={require('../../../../assets/edit-icon.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
    </View>
  );
};

export default StepItem;

const styles = StyleSheet.create({
  stepContainer: {
    paddingBottom: 25,
  },
  stepPosition: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#9A9A9A',
    marginBottom: 8,
    textTransform: 'capitalize',
  },
  stepNameContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#DFDDE2',
    borderWidth: 1,
    borderRadius: 6,
    padding: 10,
  },
  stepIcon: {
    width: 15,
    height: 15,
    borderRadius: 7.5,
    backgroundColor: '#C4C4C4',
  },
  stepName: {
    fontFamily: 'Montserrat-SemiBold',
    flex: 1,
    paddingHorizontal: 10,
    color: '#186AB4',
    fontSize: 14,
  },
  icon: {
    height: 18,
    width: 18,
  },
});
