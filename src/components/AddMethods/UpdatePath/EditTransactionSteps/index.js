import React, {useState, useEffect, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';
import PrimaryButton from '../../PrimaryButton';
import StepItem from './StepItem';
import AddStepForm from './AddStepForm';
import NumberToWord from 'number-to-words';
import {WToast} from 'react-native-smart-tip';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../../configs';
import LoadingAnimation from '../../../LoadingAnimation';
import {AppContext} from '../../../../contexts/AppContextProvider';

const {height} = Dimensions.get('window');

const EditTransactionSteps = ({selectedPath, closeEdit, getPaths}) => {
  const {updatePaths} = useContext(AppContext);

  const [transactionSteps, setTransactionSteps] = useState();
  const [isStepFormOpen, setIsStepFormOpen] = useState(false);
  const [selectedStep, setSelectedStep] = useState();
  const [selectedPos, setSelectedPos] = useState(null);
  const [isStatusEditable, setIsStatusEditable] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (selectedPath) {
      console.log('selectedPath.total_steps', selectedPath.total_steps);
      const stepsArray = [];
      for (const key in selectedPath.total_steps) {
        const stepObj = {...selectedPath.total_steps[key], key};
        stepsArray.push(stepObj);
      }

      console.log('stepsArray', stepsArray);

      setTransactionSteps(stepsArray);
    }
  }, [selectedPath]);

  const onSubmit = () => {
    if (!transactionSteps) {
      return WToast.show({
        data: 'Please Create Transaction Steps',
        position: WToast.position.TOP,
      });
    }

    const stepsObj = {};

    transactionSteps.forEach((item) => {
      const arr = {
        clauses: item.clauses || [],
        iconLink: item.iconLink || '',
        instructions: item.instructions || [],
        links: item.links || [],
        name: item.name,
        status: item.status,
      };
      stepsObj[item.key] = arr;
    });

    setIsLoading(true);

    const postData = {
      path_id: selectedPath.path_id,
      field: 'total_steps',
      value: stepsObj,
    };

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/payment/paths/update`,
      postData,
    )
      .then((resp) => {
        const {data} = resp;
        WToast.show({
          data: data.message,
          position: WToast.position.TOP,
        });
        setIsLoading(false);
        if (data.status) {
          closeEdit();
          getPaths();
          updatePaths();
        }
      })
      .catch((error) => {
        setIsLoading(false);
        WToast.show({
          data: 'Error On Updating total_steps',
          position: WToast.position.TOP,
        });
      });
  };

  const onItemEditClick = (pos) => {
    const listLength = transactionSteps.length;

    if (pos === null || pos === undefined) {
      setIsStatusEditable(true);
      setSelectedPos(listLength - 2);
      setSelectedStep();
    } else {
      if (pos <= 0 || pos >= listLength - 2) {
        setIsStatusEditable(false);
      } else {
        setIsStatusEditable(true);
      }
      setSelectedPos(pos);
      setSelectedStep(transactionSteps[pos]);
    }
    setIsStepFormOpen(true);
  };

  const onAddNewStep = (item, pos) => {
    let itemFound = false;

    transactionSteps.forEach((x) => {
      if (item.status === x.status || item.key === x.status) {
        itemFound = true;
        return;
      }
    });

    if (itemFound) {
      onEditItem(item, pos);
    } else {
      const tempArry = transactionSteps;
      transactionSteps.splice(pos, 0, item);
      setTransactionSteps(tempArry);
    }
    setIsStepFormOpen(false);
  };

  const onEditItem = (item, pos) => {
    const tempArry = transactionSteps;

    tempArry[pos] = item;
    setTransactionSteps(tempArry);
  };

  const renderList = () => {
    if (transactionSteps) {
      const listLength = transactionSteps.length;

      const ListHead = [];
      transactionSteps.forEach((item, index) => {
        if (listLength > 3) {
          if (index >= listLength - 2) {
            return;
          }
        }

        ListHead.push(
          <StepItem
            key={`${item ? item.name : ''}${index}`}
            pos={index}
            title={item ? item.name : ''}
            posTitle={`${NumberToWord.toWordsOrdinal(index + 1)} Step`}
            onItemEditClick={() => onItemEditClick(index)}
          />,
        );
      });

      const AddStep = (
        <View key={Date.now()} style={styles.stepContainer}>
          <Text style={styles.addStepText}>Add Any Additional Steps</Text>
          <TouchableOpacity
            style={styles.addStepButton}
            onPress={() => onItemEditClick(null)}>
            <Image
              style={styles.plusIcon}
              source={require('../../../../assets/add-colored.png')}
            />
            <View style={styles.addTextContainer}>
              <Text style={styles.addText}>Add Second Step</Text>
            </View>
          </TouchableOpacity>
        </View>
      );

      let ListTail = [];

      if (listLength > 2) {
        ListTail = [
          <StepItem
            key={`${
              transactionSteps[listLength - 2]
                ? transactionSteps[listLength - 2].name
                : ''
            }${listLength - 2}`}
            pos={listLength - 2}
            title={transactionSteps[listLength - 2].name}
            posTitle={'Final Step'}
            onItemEditClick={() => onItemEditClick(listLength - 2)}
          />,
          <StepItem
            key={`${
              transactionSteps[listLength - 1]
                ? transactionSteps[listLength - 1].name
                : ''
            }${listLength - 1}`}
            pos={listLength - 1}
            title={transactionSteps[listLength - 1].name}
            posTitle={'Cancelled Step'}
            onItemEditClick={() => onItemEditClick(listLength - 1)}
          />,
        ];
      }

      return [ListHead, AddStep, ListTail];
    }
  };

  if (isStepFormOpen) {
    return (
      <AddStepForm
        onAddNewStep={onAddNewStep}
        selectedPos={selectedPos}
        selectedStep={selectedStep}
        closeForm={() => setIsStepFormOpen(false)}
        isNameDisabled={!isStatusEditable}
      />
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Transaction Structure</Text>
      <Text style={styles.bankerName}>
        {selectedPath ? selectedPath.banker : 'Banker'}
      </Text>
      <ScrollView
        bounces={false}
        style={styles.scrollView}
        showsVerticalScrollIndicator={false}>
        <TouchableWithoutFeedback>
          <View style={styles.stepsView}>{renderList()}</View>
        </TouchableWithoutFeedback>
      </ScrollView>
      <PrimaryButton title="Update Transaction Steps" onPress={onSubmit} />

      {isLoading && (
        <View
          style={[
            StyleSheet.absoluteFill,
            {justifyContent: 'center', backgroundColor: 'white', zIndex: 2},
          ]}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default EditTransactionSteps;

const styles = StyleSheet.create({
  container: {
    height: height * 0.7,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
  bankerName: {
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    color: '#9A9A9A',
    marginVertical: 5,
    fontSize: 13,
  },
  stepsView: {},
  stepContainer: {
    paddingBottom: 25,
  },
  icon: {
    height: 18,
    width: 18,
  },
  addStepText: {
    fontFamily: 'Montserrat',
    color: '#9A9A9A',
    marginBottom: 8,
  },
  plusIcon: {
    height: 25,
    width: 25,
  },
  addStepButton: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  addTextContainer: {
    backgroundColor: '#186AB4',
    flex: 1,
    justifyContent: 'center',
    borderRadius: 6,
    marginLeft: 20,
    padding: 10,
  },
  addText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
});
