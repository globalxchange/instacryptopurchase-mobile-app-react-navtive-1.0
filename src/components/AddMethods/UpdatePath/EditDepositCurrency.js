import React, {useContext, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {AppContext} from '../../../contexts/AppContextProvider';
import CustomDropDown from '../../CustomDropDown';
import PrimaryButton from '../PrimaryButton';
import {WToast} from 'react-native-smart-tip';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import LoadingAnimation from '../../LoadingAnimation';

const EditDepositCurrency = ({selectedPath, closeEdit, getPaths}) => {
  const {cryptoTableData, updatePaths} = useContext(AppContext);

  const [selectedAsset, setSelectedAsset] = useState();
  const [isLoading, setIsLoading] = useState(false);

  const options = [];

  if (cryptoTableData) {
    cryptoTableData.forEach((item) => {
      if (item.coinSymbol !== selectedPath.coin) {
        options.push({
          ...item,
          name: item.coinName,
          image: item.coinImage,
        });
      }
    });
  }

  const onEditSubmit = () => {
    if (!selectedAsset) {
      return WToast.show({
        data: 'Please select a asset',
        position: WToast.position.TOP,
      });
    }
    setIsLoading(true);

    const postData = {
      path_id: selectedPath.path_id,
      field: 'deposit_currency',
      value: selectedAsset.coinSymbol,
    };

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/payment/paths/update`,
      postData,
    )
      .then((resp) => {
        const {data} = resp;
        WToast.show({
          data: data.message,
          position: WToast.position.TOP,
        });
        setIsLoading(false);
        if (data.status) {
          closeEdit();
          getPaths();
          updatePaths();
        }
      })
      .catch((error) => {
        setIsLoading(false);
        WToast.show({
          data: 'Error On Updating deposit_currency',
          position: WToast.position.TOP,
        });
      });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Selecting From Asset</Text>
      <View style={styles.actionsContainer}>
        <CustomDropDown
          label="What Asset Is The User Allowed To Pay in?"
          items={options}
          onDropDownSelect={setSelectedAsset}
          selectedItem={selectedAsset}
          placeHolder="Select a Asset"
        />
      </View>
      <PrimaryButton title="Update Deposit Currency" onPress={onEditSubmit} />
      {isLoading && (
        <View style={[StyleSheet.absoluteFill, styles.loadingContainer]}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default EditDepositCurrency;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
  actionsContainer: {
    // flex: 1,
    justifyContent: 'center',
    paddingVertical: 50,
  },
  loadingContainer: {
    backgroundColor: 'white',
    zIndex: 2,
    justifyContent: 'center',
  },
});
