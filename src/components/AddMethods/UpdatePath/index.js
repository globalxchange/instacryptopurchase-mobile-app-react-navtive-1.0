import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, FlatList, Dimensions} from 'react-native';
import EditView from './EditView';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import LoadingAnimation from '../../LoadingAnimation';
import PathItem from './PathItem';

const {height} = Dimensions.get('window');

const UpdatePath = () => {
  const [selectedPath, setSelectedPath] = useState();
  const [paths, setPaths] = useState();

  useEffect(() => {
    getPaths();
  }, []);

  const getPaths = () => {
    setSelectedPath();
    setPaths();
    Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/paths/get`)
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          // console.log('paths', data.paths);
          setPaths(data.paths);
        } else {
          setPaths([]);
        }
      })
      .catch((error) => console.log('Error on getting pathData', error));
  };

  if (!paths) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  if (selectedPath) {
    return <EditView selectedPath={selectedPath} getPaths={getPaths} />;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Update Path</Text>
      <FlatList
        showsVerticalScrollIndicator={false}
        style={styles.pathsList}
        data={paths}
        keyExtractor={(item) => item._id}
        renderItem={({item}) => (
          <PathItem data={item} onItemSelected={setSelectedPath} />
        )}
      />
    </View>
  );
};

export default UpdatePath;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 20,
  },
  controlContainer: {
    paddingVertical: 40,
  },
  loadingContainer: {
    paddingVertical: 80,
  },
  pathsList: {
    flexGrow: 0,
    maxHeight: height * 0.65,
  },
});
