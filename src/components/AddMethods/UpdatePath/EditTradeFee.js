import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import LoadingAnimation from '../../LoadingAnimation';
import PrimaryButton from '../PrimaryButton';
import {WToast} from 'react-native-smart-tip';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';

class EditTradeFee extends Component {
  constructor(props) {
    super(props);

    this.state = {
      instaCryptoFee: '',
      bankerFee: '',
      isInstaFocused: true,
      isLoading: false,
    };
  }

  onSubmit = () => {
    const {instaCryptoFee, bankerFee} = this.state;

    const {selectedPath, closeEdit, getPaths} = this.props;

    let instaFee;
    let bankFee;

    if (instaCryptoFee === '') {
      instaFee = parseFloat(0);
    } else if (!isNaN(parseFloat(instaCryptoFee))) {
      instaFee = parseFloat(instaCryptoFee);
    } else {
      return WToast.show({
        data: 'Please input a valid InstaCrypto Fee',
        position: WToast.position.TOP,
      });
    }

    if (bankerFee === '') {
      bankFee = parseFloat(0);
    } else if (isNaN(parseFloat(bankerFee))) {
      bankFee = parseFloat(bankerFee);
    } else {
      return WToast.show({
        data: `Please input a valid ${
          selectedPath ? selectedPath.name : 'Banker'
        } Fee`,
        position: WToast.position.TOP,
      });
    }

    this.setState({isLoading: true});

    Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/payment/paths/update`, {
      path_id: selectedPath.path_id,
      field: 'gx_trade_fee',
      value: instaFee,
    })
      .then((gxResp) => {
        Axios.post(
          `${GX_API_ENDPOINT}/coin/vault/service/payment/paths/update`,
          {
            path_id: selectedPath.path_id,
            field: 'banker_trade_fee',
            value: bankFee,
          },
        )
          .then((resp) => {
            this.setState({isLoading: false});
            if (resp.data.status) {
              WToast.show({
                data: 'Trading fees updated',
                position: WToast.position.TOP,
              });
              getPaths();
              closeEdit();
            }
          })
          .catch((error) => {
            this.setState({isLoading: false});
            WToast.show({
              data: 'Error On Updating banker_trade_fee',
              position: WToast.position.TOP,
            });
          });
      })
      .catch((error) => {
        this.setState({isLoading: false});
        WToast.show({
          data: 'Error On Updating gx_trade_fee',
          position: WToast.position.TOP,
        });
      });
  };

  render() {
    const {selectedPath} = this.props;

    const {instaCryptoFee, bankerFee, isInstaFocused, isLoading} = this.state;

    if (isLoading) {
      return (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <Text style={styles.header}>Set Fixed Fee</Text>
        <View style={styles.inputForm}>
          <View style={styles.inputContainer}>
            <TextInput
              placeholder="0.00%"
              placeholderTextColor={isInstaFocused ? '#186AB4' : '#041939'}
              style={[styles.input, isInstaFocused && styles.active]}
              onFocus={() => this.setState({isInstaFocused: true})}
              keyboardType="numeric"
              returnKeyType="done"
              value={instaCryptoFee}
              onChangeText={(text) => this.setState({instaCryptoFee: text})}
            />
            <Text style={[styles.label, isInstaFocused && styles.active]}>
              InstaCrypto
            </Text>
          </View>
          <View style={styles.separator} />
          <View style={styles.inputContainer}>
            <TextInput
              placeholder="0.00%"
              placeholderTextColor={!isInstaFocused ? '#186AB4' : '#041939'}
              style={[styles.input, !isInstaFocused && styles.active]}
              onFocus={() => this.setState({isInstaFocused: false})}
              keyboardType="numeric"
              returnKeyType="done"
              value={bankerFee}
              onChangeText={(text) => this.setState({bankerFee: text})}
            />
            <Text style={[styles.label, !isInstaFocused && styles.active]}>
              {selectedPath ? selectedPath.banker : 'Banker'}
            </Text>
          </View>
        </View>
        <PrimaryButton onPress={this.onSubmit} title="Update Trade Fee" />
      </View>
    );
  }
}

export default EditTradeFee;

const styles = StyleSheet.create({
  container: {},
  header: {
    textAlign: 'center',
    color: '#186AB4',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    marginBottom: 15,
  },
  inputForm: {marginVertical: 30},
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  input: {
    color: '#041939',
    flex: 1,
    height: 45,
    fontSize: 18,
    fontFamily: 'Montserrat',
  },
  label: {
    color: '#041939',
    fontFamily: 'Montserrat',
    fontSize: 16,
  },
  active: {
    color: '#186AB4',
  },
  separator: {
    backgroundColor: '#186AB4',
    height: 1,
  },
  loadingContainer: {
    paddingVertical: 60,
  },
});
