import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import CustomDropDown from '../../CustomDropDown';
import EditDepositCurrency from './EditDepositCurrency';
import EditInstitution from './EditInstitution';
import EditTransactionType from './EditTransactionType';
import EditFixedFee from './EditFixedFee';
import EditTradeFee from './EditTradeFee';
import EditTransactionSteps from './EditTransactionSteps';

const EditView = ({selectedPath, getPaths}) => {
  const [activeEdit, setActiveEdit] = useState('');

  if (activeEdit.key === 'deposit_currency') {
    return (
      <EditDepositCurrency
        selectedPath={selectedPath}
        closeEdit={() => setActiveEdit('')}
        getPaths={getPaths}
      />
    );
  }
  if (activeEdit.key === 'institution_id') {
    return (
      <EditInstitution
        selectedPath={selectedPath}
        getPaths={getPaths}
        closeEdit={() => setActiveEdit('')}
      />
    );
  }

  if (activeEdit.key === 'select_type') {
    return (
      <EditTransactionType
        selectedPath={selectedPath}
        closeEdit={() => setActiveEdit('')}
        getPaths={getPaths}
      />
    );
  }

  if (activeEdit.key === 'fixed_fee') {
    return (
      <EditFixedFee
        selectedPath={selectedPath}
        closeEdit={() => setActiveEdit('')}
        getPaths={getPaths}
      />
    );
  }

  if (activeEdit.key === 'trade_fee') {
    return (
      <EditTradeFee
        selectedPath={selectedPath}
        closeEdit={() => setActiveEdit('')}
        getPaths={getPaths}
      />
    );
  }

  if (activeEdit.key === 'total_steps') {
    return (
      <EditTransactionSteps
        selectedPath={selectedPath}
        closeEdit={() => setActiveEdit('')}
        getPaths={getPaths}
      />
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Update Path</Text>
      <View style={styles.controlContainer}>
        <CustomDropDown
          items={options}
          label="Edit Options"
          placeHolder="Select Any Edit Option"
          onDropDownSelect={setActiveEdit}
          selectedItem={activeEdit}
        />
      </View>
    </View>
  );
};

export default EditView;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 20,
  },
  controlContainer: {
    paddingVertical: 40,
  },
});

const options = [
  // {key: 'deposit_currency', name: 'Deposit Currency'},
  {key: 'institution_id', name: 'Institution ID'},
  {key: 'select_type', name: 'Transaction Type'},
  {key: 'fixed_fee', name: 'Fixed Fees'},
  {key: 'trade_fee', name: 'Trade Fees'},
  {key: 'total_steps', name: 'Transaction Steps'},
];
