import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';

const PathItem = ({data, onItemSelected}) => {
  return (
    <TouchableOpacity onPress={() => onItemSelected(data)}>
      <View style={styles.container}>
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>Path ID</Text>
          <Text style={styles.dataValue}>{data.path_id}</Text>
        </View>
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>To Coin</Text>
          <Text style={styles.dataValue}>{data.to_currency}</Text>
        </View>
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>From Coin</Text>
          <Text style={styles.dataValue}>{data.from_currency}</Text>
        </View>
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>Country</Text>
          <Text style={styles.dataValue}>{data.country}</Text>
        </View>
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>Banker</Text>
          <Text style={styles.dataValue}>{data.banker}</Text>
        </View>
        <View style={styles.valueGroup}>
          <Text style={styles.dataKey}>Deposit Method</Text>
          <Text style={styles.dataValue}>
            {data.depositMethod || data.paymentMethod}
          </Text>
        </View>
        <View style={styles.deleteButton}>
          <Image
            style={styles.icon}
            source={require('../../../assets/edit-icon.png')}
            resizeMode="contain"
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default PathItem;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
    borderColor: '#DFDDE2',
    borderWidth: 1,
    borderRadius: 4,
    marginBottom: 20,
  },
  valueGroup: {
    flexDirection: 'row',
    paddingVertical: 2,
  },
  dataKey: {
    width: '30%',
    color: '#186AB4',
    fontFamily: 'Montserrat',
    fontSize: 10,
  },
  dataValue: {
    color: '#186AB4',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    flex: 1,
  },
  deleteButton: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    height: 30,
    width: 30,
    padding: 5,
  },
  icon: {
    flex: 1,
    width: null,
    height: null,
  },
});
