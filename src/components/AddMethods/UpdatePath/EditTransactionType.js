import React, {useState, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import PrimaryButton from '../PrimaryButton';
import {WToast} from 'react-native-smart-tip';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import LoadingAnimation from '../../LoadingAnimation';
import {AppContext} from '../../../contexts/AppContextProvider';

const EditTransactionType = ({selectedPath, closeEdit, getPaths}) => {
  const {updatePaths} = useContext(AppContext);

  const [txnType, setTxnType] = useState('');
  const [instantLink, setInstantLink] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const onSubmit = () => {
    if (!txnType) {
      return WToast.show({
        data: 'Please select a transaction type',
        position: WToast.position.TOP,
      });
    }

    if (txnType === 'Instant' && !instantLink) {
      return WToast.show({
        data: 'Please input an instant link',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const postData = {
      path_id: selectedPath.path_id,
      field: 'select_type',
      value: txnType.toLowerCase(),
    };

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/payment/paths/update`,
      postData,
    )
      .then((resp) => {
        const {data} = resp;

        Axios.post(
          `${GX_API_ENDPOINT}/coin/vault/service/payment/paths/update`,
          {
            path_id: selectedPath.path_id,
            field: 'instant_link',
            value: instantLink.toLowerCase(),
          },
        );

        WToast.show({
          data: data.message,
          position: WToast.position.TOP,
        });
        setIsLoading(false);
        if (data.status) {
          getPaths();
          updatePaths();
          closeEdit();
        }
      })
      .catch((error) => {
        setIsLoading(false);
        WToast.show({
          data: 'Error On Updating deposit_currency',
          position: WToast.position.TOP,
        });
      });
  };

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Select Type</Text>
      <View style={styles.actionsContainer}>
        <Text style={styles.label}>
          Is The Transaction Instantly Completed Or Not?
        </Text>
        <View style={styles.radioButton}>
          <TouchableOpacity
            style={[styles.radioItem, txnType === 'Instant' && styles.active]}
            onPress={() => setTxnType('Instant')}>
            <Text
              style={[
                styles.radioText,
                txnType === 'Instant' && styles.activeRadioText,
              ]}>
              Instant
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.radioItem,
              txnType === 'Fund' && styles.active,
              {marginLeft: 15},
            ]}
            onPress={() => setTxnType('Fund')}>
            <Text
              style={[
                styles.radioText,
                txnType === 'Fund' && styles.activeRadioText,
              ]}>
              Fund
            </Text>
          </TouchableOpacity>
        </View>
        {txnType === 'Instant' && (
          <View style={styles.linkInputContainer}>
            <Text style={styles.label}>
              Enter Link For Instant Transaction?
            </Text>
            <TextInput
              style={styles.input}
              placeholder="Ex. www.link.com"
              placeholderTextColor="#9A9A9A"
              value={instantLink}
              onChangeText={(text) => setInstantLink(text)}
            />
          </View>
        )}
      </View>

      <PrimaryButton title="Update Transaction Type" onPress={onSubmit} />
    </View>
  );
};

export default EditTransactionType;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
  actionsContainer: {
    // flex: 1,
    paddingVertical: 50,
    justifyContent: 'center',
  },
  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  radioButton: {
    flexDirection: 'row',
  },
  radioItem: {
    flex: 1,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
    borderRadius: 6,
  },
  active: {
    borderColor: '#186AB4',
  },
  radioText: {
    color: '#788995',
    fontFamily: 'Montserrat-SemiBold',
  },
  activeRadioText: {
    color: '#186AB4',
  },
  linkInputContainer: {
    marginTop: 20,
  },
  input: {
    color: '#041939',
    height: 45,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 10,
  },
  loadingContainer: {
    paddingVertical: 60,
  },
});
