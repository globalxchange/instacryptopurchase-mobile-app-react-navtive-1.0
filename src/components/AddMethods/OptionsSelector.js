import React, {useState} from 'react';
import DefinePath from './DefinePath';
import UpdatePath from './UpdatePath';
import DeletePath from './DeletePath';
import AddBanker from './AddBanker';
import ComingSoon from './ComingSoon';
import AddCountry from './AddCountry';
import AddPaymentMethod from './AddPaymentMethod';
import UpdatePaymentMethod from './UpdatePaymentMethod';
import DeletePaymentMethod from './DeletePaymentMethod';
import AddingType from './AddingType';

const OptionsSelector = ({setIsBottomSheetOpen}) => {
  const [selectedOption, setSelectedOption] = useState('');

  switch (selectedOption) {
    case 'Define Payment Path':
      return <DefinePath setIsBottomSheetOpen={setIsBottomSheetOpen} />;
    case 'Update Payment Path':
      return <UpdatePath />;
    case 'Delete Payment Path':
      return <DeletePath />;
    case 'Add Banker':
      return <AddBanker />;
    case 'Edit Banker':
      return <ComingSoon />;
    case 'Delete Banker':
      return <ComingSoon />;
    case 'Add Country':
      return <AddCountry closeAdd={() => setSelectedOption('')} />;
    case 'Edit Country':
      return <ComingSoon />;
    case 'Delete Country':
      return <ComingSoon />;
    case 'Add Payment Type':
      return (
        <AddPaymentMethod
          closeBottomSheet={() => setIsBottomSheetOpen(false)}
        />
      );
    case 'Edit Payment Type':
      return <UpdatePaymentMethod />;
    case 'Delete Payment Type':
      return <DeletePaymentMethod />;
    default:
      return <AddingType setSelectedOption={setSelectedOption} />;
  }
};

export default OptionsSelector;
