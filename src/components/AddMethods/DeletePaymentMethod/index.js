import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, FlatList, Dimensions} from 'react-native';
import MethodItem from './MethodItem';
import LoadingAnimation from '../../LoadingAnimation';
import Dialog from 'react-native-dialog';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import {WToast} from 'react-native-smart-tip';

const {height} = Dimensions.get('window');

const DeletePaymentMethod = () => {
  const [paymentMethods, setPaymentMethods] = useState();
  const [isShowDialogue, setIsShowDialogue] = useState(false);
  const [selectedMethod, setSelectedMethod] = useState();

  useEffect(() => {
    getMethods();
  }, []);

  const getMethods = () => {
    setPaymentMethods();
    Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/methods/get`, {
      params: {},
    })
      .then((resp) => {
        const methodsData = resp.data;
        const methods = methodsData.status ? methodsData.methods : [];

        setPaymentMethods(methods);
      })
      .catch((error) => console.log('Error getting methods list', error));
  };

  const onItemSelected = (item) => {
    setSelectedMethod(item);
    setIsShowDialogue(true);
  };

  const onAlertCancel = () => {
    setSelectedMethod();
    setIsShowDialogue(false);
  };

  const deleteItem = async () => {
    setIsShowDialogue(false);
    if (selectedMethod) {
      const email = await AsyncStorageHelper.getLoginEmail();
      const token = await AsyncStorageHelper.getAppToken();

      const postData = {
        token,
        email,
        type: selectedMethod.type,
        code: selectedMethod.code,
      };

      Axios.post(
        `${GX_API_ENDPOINT}/coin/vault/service/payment/methods/delete`,
        postData,
      ).then((resp) => {
        const {data} = resp;

        WToast.show({data: data.message, position: WToast.position.TOP});
        setSelectedMethod();

        getMethods();
      });
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Delete PaymentMethod</Text>

      {paymentMethods ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          style={styles.pathsList}
          data={paymentMethods}
          keyExtractor={(item) => item._id}
          renderItem={({item}) => (
            <MethodItem data={item} onItemSelected={onItemSelected} />
          )}
          ListEmptyComponent={
            <View style={styles.loadingContainer}>
              <Text style={styles.emptyText}>No Paths Found...</Text>
            </View>
          }
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}

      <Dialog.Container visible={isShowDialogue}>
        <Dialog.Title>Delete Path</Dialog.Title>
        <Dialog.Description>
          Do you want to delete this path? You cannot undo this action.
        </Dialog.Description>
        <Dialog.Button label="Cancel" onPress={onAlertCancel} />
        <Dialog.Button label="Delete" onPress={deleteItem} />
      </Dialog.Container>
    </View>
  );
};

export default DeletePaymentMethod;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 20,
  },
  loadingContainer: {
    paddingVertical: 80,
  },
  pathsList: {
    flexGrow: 0,
    maxHeight: height * 0.65,
  },
  emptyText: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
  },
});
