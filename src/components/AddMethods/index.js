import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
} from 'react-native';
import OptionsSelector from './OptionsSelector';
import Animated, {
  useCode,
  Clock,
  set,
  interpolate,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';
import AdminLogin from './AdminLogin';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const AddMethods = ({isBottomSheetOpen, setIsBottomSheetOpen}) => {
  const {bottom} = useSafeAreaInsets();

  const [isAdminLoggedIn, setIsAdminLoggedIn] = useState(false);
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  return (
    <Modal
      animationType="slide"
      visible={isBottomSheetOpen}
      transparent
      hardwareAccelerated
      statusBarTranslucent
      onDismiss={() => setIsBottomSheetOpen(false)}
      onRequestClose={() => setIsBottomSheetOpen(false)}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.overlay}
        onPress={() => setIsBottomSheetOpen(false)}
        onPressOut={() => {}}>
        <TouchableWithoutFeedback style={{flex: 1}}>
          <Animated.View
            style={[
              styles.container,

              {
                paddingBottom: 20 + bottom,
                transform: [
                  {
                    translateY: interpolate(chatKeyboardAnimation.current, {
                      inputRange: [0, 1],
                      outputRange: [0, -keyboardHeight],
                    }),
                  },
                ],
              },
            ]}>
            {isAdminLoggedIn ? (
              <OptionsSelector setIsBottomSheetOpen={setIsBottomSheetOpen} />
            ) : (
              <AdminLogin setDevLoggedIn={setIsAdminLoggedIn} />
            )}
          </Animated.View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default AddMethods;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    paddingHorizontal: 30,
    paddingTop: 20,
  },
  closeButton: {
    width: 24,
    height: 24,
    position: 'absolute',
    right: 15,
    top: 20,
    padding: 4,
  },
  icon: {
    flex: 1,
    width: null,
    height: null,
  },
});
