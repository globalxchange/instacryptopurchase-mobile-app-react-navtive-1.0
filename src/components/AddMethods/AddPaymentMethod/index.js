import React, {useState} from 'react';
import {StyleSheet, View, Keyboard} from 'react-native';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import {WToast} from 'react-native-smart-tip';
import Axios from 'axios';
import {GX_API_ENDPOINT, S3_CONFIG} from '../../../configs';
import LoadingAnimation from '../../LoadingAnimation';
import PaymentDetailsForm from './PaymentDetailsForm';
import PaymentDescription from './PaymentDescription';
import {FileSystem} from 'react-native-unimodules';
import {decode} from 'base64-arraybuffer';
import {S3} from 'aws-sdk';

const AddPaymentMethod = ({closeBottomSheet}) => {
  const [activeStep, setActiveStep] = useState('');

  const [selectedTxnType, setSelectedTxnType] = useState('');
  const [methodName, setMethodName] = useState('');
  const [methodCode, setMethodCode] = useState('');
  const [methodDesc, setMethodDesc] = useState('');
  const [methodFee, setMethodFee] = useState('');
  const [iconLink, setIconLink] = useState('');
  const [otherData, setOtherData] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  const createPaymentMethod = async () => {
    Keyboard.dismiss();

    if (!selectedTxnType) {
      return WToast.show({
        data: 'Please Select The Txn Type',
        position: WToast.position.TOP,
      });
    }

    if (!methodName.trim()) {
      return WToast.show({
        data: 'Please Input Payment Type Name',
        position: WToast.position.TOP,
      });
    }

    if (!methodCode.trim()) {
      return WToast.show({
        data: 'Please Input Payment Type Code',
        position: WToast.position.TOP,
      });
    }

    if (!methodDesc.trim()) {
      return WToast.show({
        data: 'Please Input Payment Type Description',
        position: WToast.position.TOP,
      });
    }

    if (
      !methodFee ||
      isNaN(parseFloat(methodFee)) ||
      parseFloat(methodFee) < 0
    ) {
      return WToast.show({
        data: 'Please Input A Valid Fee',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const token = await AsyncStorageHelper.getAppToken();
    const email = await AsyncStorageHelper.getLoginEmail();

    const fPath = iconLink;

    let options = {encoding: FileSystem.EncodingType.Base64};
    FileSystem.readAsStringAsync(fPath, options)
      .then((data) => {
        setIsLoading(true);

        const BUCKET_NAME = 'gxnitrousdata';
        const S3Client = new S3({
          ...S3_CONFIG,
          Bucket: BUCKET_NAME,
        });

        const arrayBuffer = decode(data);
        const params = {
          Bucket: BUCKET_NAME,
          Key: `vaultpathsMethods/${methodName}_${methodCode}${Date.now()}.jpg`,
          Body: arrayBuffer,
          ContentType: 'image/jpeg',
          ACL: 'public-read',
        };
        S3Client.upload(params, async (err, s3Data) => {
          if (err) {
            console.log('Uploading Profile Pic Error', err);
          }
          if (s3Data.Location) {
            const uploadLink = s3Data.Location;

            const postData = {
              token,
              email,
              type: selectedTxnType.name.toLowerCase(), // "deposit" / "send"
              name: methodName.trim(),
              code: methodCode.trim(), // some unique code
              description: methodDesc.trim(),
              icon: uploadLink,
              fee: parseFloat(methodFee), // fee in USD
              status: true, // true / false --> represents active or inactive
              other_data: otherData,
              explanation_video: 'blaah',
              explanation_video_thumbnail: 'blaah',
              explanation_video_title: 'blaah',
              color_codes: ['#FFFFFF'],
            };
            console.log('PostData', postData);

            Axios.post(
              `${GX_API_ENDPOINT}/coin/vault/service/payment/methods/add`,
              postData,
            )
              .then((resp) => {
                const {data} = resp;
                setIsLoading(false);
                console.log('Adding payment type', data);

                WToast.show({
                  data: data.message,
                  position: WToast.position.TOP,
                });
                closeBottomSheet();
              })
              .catch((error) => {
                setIsLoading(false);
                WToast.show({
                  data: 'API, Error',
                  position: WToast.position.TOP,
                });
                console.log('Error adding payment type', error);
              });
          }
        });
      })
      .catch((err) => {
        console.log('​getFile -> err', err);
        setIsLoading(false);
      });
  };

  const renderComponent = () => {
    switch (activeStep) {
      case 'PaymentDetails':
        return (
          <PaymentDetailsForm
            txnTypes={txnTypes}
            methodCode={methodCode}
            methodName={methodName}
            selectedTxnType={selectedTxnType}
            setMethodCode={setMethodCode}
            setMethodName={setMethodName}
            setSelectedTxnType={setSelectedTxnType}
            setIconLink={setIconLink}
            iconLink={iconLink}
            methodFee={methodFee}
            setMethodFee={setMethodFee}
            onNext={() => setActiveStep('PaymentDescription')}
          />
        );
      case 'PaymentDescription':
        return (
          <PaymentDescription
            methodDesc={methodDesc}
            setMethodDesc={setMethodDesc}
            methodName={methodName}
            setIsLoading={setIsLoading}
            descriptions={otherData}
            setDescriptions={setOtherData}
            onNext={createPaymentMethod}
          />
        );
      default:
        return (
          <PaymentDetailsForm
            txnTypes={txnTypes}
            methodCode={methodCode}
            methodName={methodName}
            selectedTxnType={selectedTxnType}
            setMethodCode={setMethodCode}
            setMethodName={setMethodName}
            setSelectedTxnType={setSelectedTxnType}
            setIconLink={setIconLink}
            iconLink={iconLink}
            methodFee={methodFee}
            setMethodFee={setMethodFee}
            onNext={() => setActiveStep('PaymentDescription')}
          />
        );
    }
  };

  return (
    <View style={styles.container}>
      {renderComponent()}
      {isLoading && (
        <View style={[styles.loadingContainer, StyleSheet.absoluteFill]}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default AddPaymentMethod;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
  loadingContainer: {
    // flex: 1,
    zIndex: 2,
    backgroundColor: 'white',
    paddingVertical: 50,
    justifyContent: 'center',
  },
});

const txnTypes = [
  {name: 'Deposit', image: require('../../../assets/deposit-txn-icon.png')},
  {name: 'Send', image: require('../../../assets/withdraw-txn-icon.png')},
];
