import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import WebsiteData from './WebsiteData';
import MediaForm from './MediaForm';
import TextForm from './TextForm';
import {WToast} from 'react-native-smart-tip';

const PaymentDescription = ({
  methodName,
  setIsLoading,
  descriptions,
  setDescriptions,
  setMethodDesc,
  methodDesc,
  onNext,
}) => {
  const [selectedControl, setSelectedControl] = useState('');

  const addWebsite = (item) => {
    const websites = descriptions.websites || [];
    websites.push(item);
    setDescriptions({...descriptions, websites});
  };

  const addUploads = (item) => {
    const uploads = descriptions.uploads || [];
    uploads.push(item);
    setDescriptions({...descriptions, uploads});
  };

  const addTexts = (item) => {
    const texts = descriptions.texts || [];
    texts.push(item);
    setDescriptions({...descriptions, texts});
  };

  const renderInputForm = () => {
    switch (selectedControl.key) {
      case 'websites':
        return <WebsiteData addWebsite={addWebsite} />;
      case 'uploads':
        return (
          <MediaForm setIsLoading={setIsLoading} addUploads={addUploads} />
        );
      case 'texts':
        return <TextForm addTexts={addTexts} />;
    }
  };

  const onNextClick = () => {
    if (!methodDesc) {
      return WToast.show({
        data: 'Please Input Description',
        position: WToast.position.TOP,
      });
    }
    onNext();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Add Description</Text>
      <Text style={styles.paymentName}>{methodName}</Text>
      <View style={[styles.inputContainer, {height: 120}]}>
        <TextInput
          placeholderTextColor="#9A9A9A"
          style={styles.input}
          placeholder={`What is a ${methodName}`}
          multiline
          value={methodDesc}
          onChangeText={(text) => setMethodDesc(text)}
        />
      </View>
      <View style={styles.additionalDataContainer}>
        <View style={styles.dataController}>
          <Text style={styles.label}>Add Additional Data</Text>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={controllers}
            keyExtractor={(item) => item.key}
            renderItem={({item}) => (
              <TouchableOpacity
                style={[
                  styles.controlsItem,
                  selectedControl.key === item.key && styles.activeItem,
                ]}
                onPress={() => setSelectedControl(item)}>
                <Text
                  style={[
                    styles.controlText,
                    selectedControl.key === item.key &&
                      styles.activeControlText,
                  ]}>
                  {item.title}
                  {descriptions[item.key]
                    ? ` (${descriptions[item.key].length})`
                    : ''}
                </Text>
              </TouchableOpacity>
            )}
          />
          <View style={styles.currentForm}>{renderInputForm()}</View>
          <TouchableOpacity style={styles.proceedBtn} onPress={onNextClick}>
            <Text style={styles.btnText}>Continue</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default PaymentDescription;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 15,
  },
  inputContainer: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 10,
    marginBottom: 20,
  },
  input: {
    height: '100%',
    color: 'black',
    fontFamily: 'Montserrat',
  },
  paymentName: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
    textAlign: 'center',
  },

  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  additionalDataContainer: {},
  dataController: {},
  controlsItem: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 15,
    minWidth: 100,
    paddingVertical: 4,
  },
  activeItem: {
    borderColor: '#186AB4',
  },
  controlText: {
    fontFamily: 'Montserrat',
    color: 'rgba(33, 33, 33, 0.25)',
    textAlign: 'center',
    fontSize: 13,
  },
  activeControlText: {
    color: '#186AB4',
  },
  currentForm: {
    paddingVertical: 10,
  },
  proceedBtn: {
    backgroundColor: '#186AB4',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
  },
  btnText: {
    fontSize: 16,
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
});

const controllers = [
  {title: 'Website', key: 'websites'},
  {title: 'Upload', key: 'uploads'},
  {title: 'Text', key: 'texts'},
];
