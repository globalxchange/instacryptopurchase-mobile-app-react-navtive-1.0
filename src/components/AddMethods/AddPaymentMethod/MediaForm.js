import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import {Permissions, FileSystem} from 'react-native-unimodules';
import Constants from 'expo-constants';
import {decode} from 'base64-arraybuffer';
import {S3} from 'aws-sdk';
import {WToast} from 'react-native-smart-tip';
import {S3_CONFIG} from '../../../configs';

const MediaForm = ({setIsLoading, addUploads}) => {
  const [mediaLocal, setMediaLocal] = useState();
  const [mediaName, setMediaName] = useState('');

  useEffect(() => {
    (async () => {
      if (Constants.platform.ios) {
        const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
          // alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const pickFileHandler = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
        base64: true,
      });
      if (!result.cancelled) {
        setMediaLocal(result);
      }
    } catch (E) {
      console.log(E);
    }
  };

  const addHandler = () => {
    if (!mediaName) {
      return WToast.show({
        data: 'Please input the filename',
        position: WToast.position.TOP,
      });
    }

    if (!mediaLocal) {
      return WToast.show({
        data: 'Please select a file to upload',
        position: WToast.position.TOP,
      });
    }

    const fileType = mediaLocal.uri
      .split('/')
      .slice(-1)
      .pop()
      .split('.')
      .slice(-1)
      .pop();

    setIsLoading(true);

    let options = {encoding: FileSystem.EncodingType.Base64};
    FileSystem.readAsStringAsync(mediaLocal.uri, options)
      .then((data) => {
        const BUCKET_NAME = 'gxnitrousdata';
        const S3Client = new S3({
          ...S3_CONFIG,
          Bucket: BUCKET_NAME,
        });

        const arrayBuffer = decode(data);
        const params = {
          Bucket: BUCKET_NAME,
          Key: `vaultpathsMethods/${mediaName}_${Date.now()}.${fileType}`,
          Body: arrayBuffer,
          ContentType: mediaLocal.type === 'video' ? 'video/mp4' : 'image/jpeg',
          ACL: 'public-read',
        };
        S3Client.upload(params, async (err, s3Data) => {
          if (err) {
            console.log('Uploading Profile Pic Error', err);
            WToast.show({
              data: 'Error on uploading file',
              position: WToast.position.TOP,
            });
          }
          if (s3Data.Location) {
            console.log('Video URL', s3Data.Location);
            addUploads({name: mediaName, url: s3Data.Location});
            setMediaLocal();
            setMediaName('');
          }
          setIsLoading(false);
        });
      })
      .catch((err) => {
        console.log('​getFile -> err', err);
        setIsLoading(false);
      });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Name of Media</Text>
      <View style={styles.inputForm}>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            placeholder="Ex. Instructions"
            placeholderTextColor="#9A9A9A"
            value={mediaName}
            onChangeText={(text) => setMediaName(text)}
          />
          <TouchableOpacity
            style={styles.uploadButton}
            onPress={pickFileHandler}>
            <Image
              style={styles.icon}
              resizeMode="contain"
              source={
                mediaLocal
                  ? require('../../../assets/tick.png')
                  : require('../../../assets/upload-icon.png')
              }
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={styles.addButton} onPress={addHandler}>
          <Image
            style={styles.icon}
            resizeMode="contain"
            source={require('../../../assets/add-colored.png')}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default MediaForm;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
  },
  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  inputForm: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 45,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 10,
  },
  input: {
    flex: 1,
    height: '100%',
    color: 'black',
    fontFamily: 'Montserrat',
  },
  uploadButton: {
    height: 35,
    width: 35,
    padding: 5,
  },
  icon: {
    flex: 1,
    width: null,
    height: null,
  },
  addButton: {
    marginLeft: 5,
    height: 35,
    width: 35,
    padding: 4,
  },
});
