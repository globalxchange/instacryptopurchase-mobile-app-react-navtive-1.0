/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import CustomDropDown from '../../CustomDropDown';
import * as ImagePicker from 'expo-image-picker';
import {Permissions} from 'react-native-unimodules';
import Constants from 'expo-constants';
import {WToast} from 'react-native-smart-tip';

const PaymentDetailsForm = ({
  txnTypes,
  selectedTxnType,
  setSelectedTxnType,
  methodName,
  setMethodName,
  setMethodCode,
  methodCode,
  iconLink,
  setIconLink,
  onNext,
  setMethodFee,
  methodFee,
}) => {
  const [isLoading, setIsLoading] = useState();

  useEffect(() => {
    (async () => {
      if (Constants.platform.ios) {
        const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
          // alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const imagePickerHandler = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
        base64: true,
      });
      if (!result.cancelled) {
        setIsLoading(true);

        setIconLink(result.uri);
      }
    } catch (E) {
      console.log(E);
      setIsLoading(false);
    }
  };

  const onNextClick = () => {
    if (!selectedTxnType) {
      return WToast.show({
        data: 'Please Select Transaction Type',
        position: WToast.position.TOP,
      });
    }

    if (!methodName) {
      return WToast.show({
        data: 'Please Enter Payment Method Name',
        position: WToast.position.TOP,
      });
    }

    if (!methodCode) {
      return WToast.show({
        data: 'Please Enter Payment Method Code',
        position: WToast.position.TOP,
      });
    }

    if (!iconLink) {
      return WToast.show({
        data: 'Please Enter Payment Method Icon',
        position: WToast.position.TOP,
      });
    }

    if (
      !methodFee ||
      isNaN(parseFloat(methodFee)) ||
      parseFloat(methodFee) < 0
    ) {
      return WToast.show({
        data: 'Please Input A Valid Fee',
        position: WToast.position.TOP,
      });
    }

    onNext();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Add Payment Type</Text>
      <View style={styles.form}>
        <CustomDropDown
          items={txnTypes}
          selectedItem={selectedTxnType}
          onDropDownSelect={setSelectedTxnType}
          label="Transaction Type"
          placeHolder="Select A Type"
        />
        <Text style={styles.label}>Payment Method Name</Text>
        <View style={styles.inputContainer}>
          <TextInput
            placeholderTextColor="#9A9A9A"
            placeholder="Ex Bank Transfer"
            style={[styles.input, {flex: 1}]}
            value={methodName}
            onChangeText={(text) => setMethodName(text)}
          />
          <TouchableOpacity
            style={styles.uploadButton}
            onPress={imagePickerHandler}>
            <Image
              style={styles.icon}
              source={
                iconLink
                  ? {uri: iconLink}
                  : require('../../../assets/upload-icon.png')
              }
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
        <Text style={styles.label}>Payment Method Code</Text>
        <TextInput
          placeholderTextColor="#9A9A9A"
          placeholder="Ex Bank Transfer"
          style={[styles.input, styles.inputContainer]}
          onChangeText={(text) => setMethodCode(text)}
          value={methodCode}
        />
        <Text style={styles.label}>Payment Method Fee</Text>
        <TextInput
          placeholderTextColor="#9A9A9A"
          placeholder="Ex 1.5"
          style={[styles.input, styles.inputContainer]}
          onChangeText={(text) => setMethodFee(text)}
          value={methodFee}
          keyboardType="decimal-pad"
          returnKeyType="done"
        />
      </View>
      <TouchableOpacity style={styles.proceedBtn} onPress={onNextClick}>
        <Text style={styles.btnText}>Continue</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PaymentDetailsForm;

const styles = StyleSheet.create({
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 15,
  },
  form: {
    marginVertical: 15,
  },
  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    marginBottom: 20,
  },
  input: {
    height: 45,
    paddingHorizontal: 10,
    color: 'black',
    fontFamily: 'Montserrat',
  },
  proceedBtn: {
    backgroundColor: '#186AB4',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
  },
  btnText: {
    fontSize: 16,
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  uploadButton: {
    height: 30,
    width: 30,
    padding: 5,
    marginRight: 5,
  },
  icon: {
    flex: 1,
    height: null,
    width: null,
  },
});
