import React, {useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {WToast} from 'react-native-smart-tip';
import Clipboard from '@react-native-community/clipboard';

const WebsiteData = ({addWebsite}) => {
  const [webSiteName, setWebSiteName] = useState('');
  const [websiteLink, setWebsiteLink] = useState('');

  const pasteHandler = async () => {
    const text = await Clipboard.getString();
    setWebsiteLink(text);
  };

  const addInstructionHandler = () => {
    if (!webSiteName) {
      return WToast.show({
        data: 'Please Input Website Name',
        position: WToast.position.TOP,
      });
    }

    if (!websiteLink) {
      return WToast.show({
        data: 'Please Input Website Link',
        position: WToast.position.TOP,
      });
    }

    addWebsite({name: webSiteName, link: websiteLink});

    setWebSiteName('');
    setWebsiteLink('');
  };

  return (
    <View style={styles.container}>
      <View style={styles.inputContainer}>
        <TextInput
          style={[styles.inputStyle, styles.input]}
          placeholder="Name Of Website"
          placeholderTextColor="#9A9A9A"
          value={webSiteName}
          onChangeText={(text) => setWebSiteName(text)}
        />
        <View style={styles.dummyView} />
      </View>
      <View style={styles.inputContainer}>
        <View style={styles.inputStyle}>
          <TextInput
            style={[styles.input, {flex: 1}]}
            placeholder="Ex. www.shorupan.com"
            placeholderTextColor="#9A9A9A"
            value={websiteLink}
            onChangeText={(text) => setWebsiteLink(text)}
          />
          <TouchableOpacity style={styles.pasteButton} onPress={pasteHandler}>
            <Image
              style={styles.pasteIcon}
              source={require('../../../assets/paste-icon-blue.png')}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={styles.addButton}
          onPress={addInstructionHandler}>
          <Image
            style={styles.icon}
            source={require('../../../assets/add-colored.png')}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default WebsiteData;

const styles = StyleSheet.create({
  container: {paddingVertical: 10},
  inputContainer: {
    marginBottom: 20,
    flexDirection: 'row',
  },
  inputStyle: {
    flex: 1,
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    alignItems: 'center',
  },
  input: {
    color: 'black',
    fontFamily: 'Montserrat',
    height: 40,
    paddingHorizontal: 10,
  },
  pasteButton: {
    height: 20,
    width: 20,
    padding: 3,
    marginRight: 10,
  },
  pasteIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  dummyView: {
    width: 40,
    height: 40,
  },
  addButton: {
    width: 40,
    height: 40,
    paddingVertical: 8,
    alignItems: 'flex-end',
  },
  icon: {
    width: 26,
    height: 26,
  },
});
