import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const ComingSoon = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Coming Soon...</Text>
    </View>
  );
};

export default ComingSoon;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 80,
  },
  text: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
});
