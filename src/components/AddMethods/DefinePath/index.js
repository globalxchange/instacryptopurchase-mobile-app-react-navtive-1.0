import React, {useState, useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import CountrySelector from './CountrySelector';
import AssetToSelector from './AssetToSelector';
import BankerSelector from './BankerSelector';
import PaymentMethod from './PaymentMethod';
import ReviewPath from './ReviewPath';
import SelectFromAsset from './SelectFromAsset';
import TxnTypeSelector from './TxnTypeSelector';
import SelectInstitution from './SelectInstitution';
import SetTradingFee from './SetTradingFee';
import SetFixedFee from './SetFixedFee';
import TransactionSteps from './TransactionSteps';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import Axios from 'axios';

const DefinePath = ({setIsBottomSheetOpen}) => {
  const [activeStep, setActiveStep] = useState('');

  const [selectedCountry, setSelectedCountry] = useState();
  const [selectedFromAsset, setSelectedFromAsset] = useState();
  const [selectedToAsset, setSelectedToAsset] = useState();
  const [selectedBanker, setSelectedBanker] = useState();
  const [selectedMethod, setSelectedMethod] = useState();
  const [txnType, setTxnType] = useState();
  const [selectedInstitution, setSelectedInstitution] = useState();
  const [instaTradeFee, setInstaTradeFee] = useState('');
  const [bankerTradeFee, setBankerTradeFee] = useState('');
  const [instaFixedFee, setInstaFixedFee] = useState('');
  const [bankerFixedFee, setBankerFixedFee] = useState('');
  const [bankerDetails, setBankerDetails] = useState({});
  const [instantLink, setInstantLink] = useState('');
  const [transactionSteps, setTransactionSteps] = useState(steps);
  const [failureStep, setFailureStep] = useState(3);
  const [successStep, setSuccessStep] = useState(2);

  useEffect(() => {
    if (setIsBottomSheetOpen) {
      setTransactionSteps(steps);
      getBankerData();
    }
  }, [setIsBottomSheetOpen]);

  const getBankerData = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();

    setBankerDetails({});

    Axios.get('https://teller2.apimachine.com/admin/allBankers')
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          data.data.forEach((user) => {
            if (user.email === email) {
              setBankerDetails(user);
              return;
            }
          });
        }
      })
      .catch((error) => {
        console.log('Error getting user details', error);
      });
  };

  const activeComponent = () => {
    switch (activeStep) {
      case 'SelectCountry':
        return (
          <CountrySelector
            selectedCountry={selectedCountry}
            setSelectedCountry={setSelectedCountry}
            onNext={() => setActiveStep('SelectFromAsset')}
          />
        );
      case 'SelectFromAsset':
        return (
          <SelectFromAsset
            onNextClick={() => setActiveStep('SelectToAsset')}
            onBackClick={() => setActiveStep('SelectCountry')}
            selectedAsset={selectedFromAsset}
            setSelectedAsset={setSelectedFromAsset}
          />
        );
      case 'SelectToAsset':
        return (
          <AssetToSelector
            onNextClick={() => setActiveStep('SelectMethod')}
            onBackClick={() => setActiveStep('SelectFromAsset')}
            selectedAsset={selectedToAsset}
            setSelectedAsset={setSelectedToAsset}
            selectedFromAsset={selectedFromAsset}
          />
        );
      // case 'SelectBanker':
      //   return (
      //     <BankerSelector
      //       onNextClick={() => setActiveStep('SelectMethod')}
      //       onBackPress={() => setActiveStep('SelectAsset')}
      //       selectedBanker={selectedBanker}
      //       setSelectedBanker={setSelectedBanker}
      //       bankerDetails={bankerDetails}
      //       setBankerDetails={setBankerDetails}
      //     />
      //   );
      case 'SelectMethod':
        return (
          <PaymentMethod
            onNextClick={() => setActiveStep('TxnTypeSelector')}
            onBackClick={() => setActiveStep('SelectToAsset')}
            selectedMethod={selectedMethod}
            setSelectedMethod={setSelectedMethod}
          />
        );
      case 'TxnTypeSelector':
        return (
          <TxnTypeSelector
            onBackClick={() => setActiveStep('SelectMethod')}
            onNextClick={() => setActiveStep('InstitutionSelector')}
            txnType={txnType}
            setTxnType={setTxnType}
            instantLink={instantLink}
            setInstantLink={setInstantLink}
          />
        );

      case 'InstitutionSelector':
        return (
          <SelectInstitution
            selectedInstitution={selectedInstitution}
            setSelectedInstitution={setSelectedInstitution}
            onNextClick={() => setActiveStep('SetTardeFree')}
            onBackClick={() => setActiveStep('TxnTypeSelector')}
          />
        );
      case 'SetTardeFree':
        return (
          <SetTradingFee
            bankerName={selectedBanker}
            onNextClick={() => setActiveStep('SetFixedFree')}
            onBackClick={() => setActiveStep('InstitutionSelector')}
            setInstaTradeFee={setInstaTradeFee}
            instaTradeFee={instaTradeFee}
            bankerTradeFee={bankerTradeFee}
            setBankerTradeFee={setBankerTradeFee}
          />
        );
      case 'SetFixedFree':
        return (
          <SetFixedFee
            bankerName={selectedBanker}
            onNextClick={() => setActiveStep('AddTransactionSteps')}
            onBackClick={() => setActiveStep('SetTardeFree')}
            instaFixedFee={instaFixedFee}
            bankerFixedFee={bankerFixedFee}
            setInstaFixedFee={setInstaFixedFee}
            setBankerFixedFee={setBankerFixedFee}
          />
        );

      case 'AddTransactionSteps':
        return (
          <TransactionSteps
            selectedBanker={selectedBanker}
            transactionSteps={transactionSteps}
            setTransactionSteps={setTransactionSteps}
            onNextClick={() => setActiveStep('Review')}
            onBackClick={() => setActiveStep('SetFixedFree')}
            failureStep={failureStep}
            setFailureStep={setFailureStep}
            setSuccessStep={setSuccessStep}
            successStep={successStep}
          />
        );
      case 'Review':
        return (
          <ReviewPath
            country={selectedCountry}
            fromAsset={selectedFromAsset}
            toAsset={selectedToAsset}
            banker={selectedBanker}
            method={selectedMethod}
            selectedInstitution={selectedInstitution}
            txnType={txnType}
            instaTradeFee={instaTradeFee}
            bankerTradeFee={bankerTradeFee}
            instaFixedFee={instaFixedFee}
            bankerFixedFee={bankerFixedFee}
            bankerDetails={bankerDetails}
            instantLink={instantLink}
            transactionSteps={transactionSteps}
            failureStep={failureStep}
            successStep={successStep}
            onEdit={() => setActiveStep('SelectCountry')}
            closeBottomSheet={() => setIsBottomSheetOpen(false)}
          />
        );
      default:
        return (
          <CountrySelector
            selectedCountry={selectedCountry}
            setSelectedCountry={setSelectedCountry}
            onNext={() => setActiveStep('SelectFromAsset')}
          />
        );
    }
  };

  return <View style={styles.container}>{activeComponent()}</View>;
};

export default DefinePath;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
});

const steps = [
  {
    key: 'step1',
    name: 'Initiate Transaction',
    status: 'Initiated',
  },
  {
    key: 'stepLast',
    name: 'Transaction Completed',
    status: 'Completed',
  },
  {
    key: 'stepCanceled',
    name: 'Transaction Cancelled',
    status: 'Cancelled',
  },
];
