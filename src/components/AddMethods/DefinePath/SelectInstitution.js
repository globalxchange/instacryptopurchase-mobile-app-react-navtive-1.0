import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  FlatList,
  TouchableOpacity,
  Dimensions,
  Platform,
} from 'react-native';
import Axios from 'axios';
import LoadingAnimation from '../../LoadingAnimation';
import PrimaryButton from '../PrimaryButton';
import {WToast} from 'react-native-smart-tip';

const {height} = Dimensions.get('window');

const SelectInstitution = ({
  setSelectedInstitution,
  selectedInstitution,
  onNextClick,
  onBackClick,
}) => {
  const [searchInput, setSearchInput] = useState('');
  const [instituteList, setInstituteList] = useState();
  const [filteredList, setFilteredList] = useState();

  useEffect(() => {
    Axios.get(
      'https://accountingtool.apimachine.com/getlist-of-institutes',
    ).then((resp) => {
      const {data} = resp;
      setInstituteList(data.status ? data.data : []);
    });
  }, []);

  useEffect(() => {
    if (instituteList) {
      const searchQuery = (searchInput || '').trim().toLowerCase();

      const searchList = instituteList.filter(
        (item) =>
          item.institute_name.toLowerCase().includes(searchQuery) ||
          item.short_name.toLowerCase().includes(searchQuery),
      );
      setFilteredList(searchList);
    }

    return () => {};
  }, [searchInput, instituteList]);

  const onSelectItem = (item) => {
    setSelectedInstitution(item);
    onNextClick();
  };

  const onNext = () => {
    if (!selectedInstitution) {
      return WToast.show({
        data: 'Select An Institution',
        position: WToast.position.TOP,
      });
    }
    onNextClick();
  };

  return (
    <View style={styles.container}>
      <Image
        source={require('../../../assets/inititution-icon.png')}
        style={styles.headerIcon}
        resizeMode="contain"
      />
      <Text style={styles.header}>Search Institution</Text>
      <View style={styles.searchContainer}>
        <TextInput
          style={[styles.input, styles.searchInput]}
          placeholder="Ex. Bank Of America"
          value={searchInput}
          onChangeText={(input) => setSearchInput(input)}
          returnKeyType="search"
          placeholderTextColor={'#878788'}
        />
        <View style={styles.searchIcon}>
          <Image
            source={require('../../../assets/search-icon.png')}
            style={styles.icon}
            resizeMode="contain"
          />
        </View>
      </View>
      {instituteList ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          style={styles.optionsList}
          data={filteredList}
          keyExtractor={(item) => item._id}
          renderItem={({item}) => (
            <TouchableOpacity
              style={styles.optionItem}
              onPress={() =>
                onSelectItem({
                  id: item._id,
                  name: item.institute_name,
                  shortName: item.short_name,
                  type: item.institute_type,
                })
              }>
              <View style={styles.optionDot} />
              <Text style={styles.optionText}>{item.short_name}</Text>
              <Text style={styles.optionTime}>{item.institute_type}</Text>
            </TouchableOpacity>
          )}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>No Items Found</Text>
            </View>
          }
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
      <PrimaryButton title="Next Input" onBack={onBackClick} onPress={onNext} />
    </View>
  );
};

export default SelectInstitution;

const styles = StyleSheet.create({
  container: {
    height: height * (Platform.OS === 'ios' ? 0.5 : 0.6),
  },
  headerIcon: {
    marginLeft: 'auto',
    marginRight: 'auto',
    height: 50,
    marginBottom: 20,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
  searchContainer: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  searchIcon: {
    width: 28,
    height: 28,
    padding: 8,
    marginLeft: 10,
  },
  icon: {flex: 1, width: null, height: null},
  input: {
    flex: 1,
    color: '#000000',
    fontFamily: 'Montserrat',
    fontSize: 14,
    paddingRight: 10,
    backgroundColor: 'white',
    paddingVertical: 3,
  },
  searchInput: {
    paddingLeft: 10,
  },
  optionsList: {},
  optionItem: {
    paddingVertical: 12,
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  optionDot: {
    width: 14,
    height: 14,
    borderRadius: 7,
    backgroundColor: '#FEDB41',
    marginRight: 15,
  },
  optionText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#186AB4',
    flex: 1,
  },
  optionTime: {
    fontFamily: 'Montserrat',
    color: '#186AB4',
    fontSize: 12,
    marginLeft: 15,
  },
  emptyContainer: {
    paddingVertical: 20,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 15,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingVertical: 60,
  },
});
