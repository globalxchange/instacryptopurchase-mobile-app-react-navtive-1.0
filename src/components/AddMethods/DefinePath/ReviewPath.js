import React, {useState, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import LoadingAnimation from '../../LoadingAnimation';
import Axios from 'axios';
import {GX_API_ENDPOINT, APP_CODE} from '../../../configs';
import {WToast} from 'react-native-smart-tip';
import {usdValueFormatter} from '../../../utils';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import {AppContext} from '../../../contexts/AppContextProvider';

const {height} = Dimensions.get('window');

const ReviewPath = ({
  country,
  fromAsset,
  banker,
  method,
  onEdit,
  toAsset,
  instaTradeFee,
  bankerTradeFee,
  instaFixedFee,
  bankerFixedFee,
  selectedInstitution,
  txnType,
  bankerDetails,
  instantLink,
  transactionSteps,
  closeBottomSheet,
  failureStep,
  successStep,
}) => {
  const {updatePaths} = useContext(AppContext);

  const [isLoading, setIsLoading] = useState(false);

  const createPath = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const stepsObj = {};

    transactionSteps.forEach((item, index) => {
      const arr = {
        clauses: item.clauses || [],
        iconLink: item.iconLink || '',
        instructions: item.instructions || [],
        links: item.links || [],
        name: item.name,
        status: item.status,
      };
      stepsObj[item.key] = arr;
    });

    let postData = {
      email,
      token,
      coin: toAsset.coinSymbol,
      banker: bankerDetails.bankerTag,
      country: country.formData ? country.formData.Name : country.name,
      paymentMethod: method.code,
      from_currency: fromAsset.coinSymbol,
      // banker_data: {
      //   profile_id: bankerDetails.bankerTag,
      //   app_code: APP_CODE,
      // },
      institution_id: selectedInstitution.id,
      instant_link: instantLink ? instantLink.toLowerCase() : '',
      total_steps: stepsObj,
      hedge: false,
      success_step: successStep ? `step${successStep}` : '',
      failure_step: failureStep ? `step${failureStep}` : '',
    };

    if (txnType === 'InstantFund') {
      postData = {
        ...postData,
        banker_trade_fee: bankerTradeFee,
        gx_trade_fee: instaTradeFee,
        banker_fixed_fee: bankerFixedFee,
        gx_fixed_fee: instaFixedFee,
        select_type: 'instantFund',
      };
    } else {
      postData = {
        ...postData,
        banker_trade_fee: bankerTradeFee,
        gx_trade_fee: instaTradeFee,
        banker_fixed_fee: bankerFixedFee,
        gx_fixed_fee: instaFixedFee,
        select_type: txnType.toLowerCase(),
      };
    }

    console.log('Post Data ', postData);

    setIsLoading(true);

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/payment/paths/define`,
      postData,
    )
      .then((resp) => {
        const {data} = resp;
        console.log('Create path response', data);
        WToast.show({data: data.message, position: WToast.position.TOP});

        if (data.status) {
          updatePaths();
          closeBottomSheet();
        }
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        WToast.show({
          data: 'API Error, Please try again later',
          position: WToast.position.TOP,
        });
        console.log('Error On Creating Path', error);
      });
  };

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Confirm Path Details</Text>
      <ScrollView
        bounces={false}
        style={styles.scrollView}
        showsVerticalScrollIndicator={false}>
        <TouchableWithoutFeedback>
          <View>
            <View style={styles.reviewItem}>
              <Text style={styles.title}>Selected Country</Text>
              <Text style={styles.value}>{country.name}</Text>
            </View>
            <View style={styles.reviewItem}>
              <Text style={styles.title}>Selected From Asset</Text>
              <Text style={styles.value}>{fromAsset.name}</Text>
            </View>
            <View style={styles.reviewItem}>
              <Text style={styles.title}>Selected To Asset</Text>
              <Text style={styles.value}>{toAsset.name}</Text>
            </View>
            <View style={styles.reviewItem}>
              <Text style={styles.title}>Selected Banker</Text>
              <Text style={styles.value}>{bankerDetails.bankerTag}</Text>
            </View>
            <View style={styles.reviewItem}>
              <Text style={styles.title}>Selected Payment Method</Text>
              <Text style={styles.value}>{method.name}</Text>
            </View>
            <View style={styles.reviewItem}>
              <Text style={styles.title}>InstaCrypto Trade Fee</Text>
              <Text style={styles.value}>{instaTradeFee}%</Text>
            </View>
            <View style={styles.reviewItem}>
              <Text style={styles.title}>
                {banker ? bankerDetails.bankerTag : 'Banker'} Trade Fee
              </Text>
              <Text style={styles.value}>{bankerTradeFee}%</Text>
            </View>
            <View style={styles.reviewItem}>
              <Text style={styles.title}>InstaCrypto Fixed Fee</Text>
              <Text style={styles.value}>
                {usdValueFormatter.format(instaFixedFee)}
              </Text>
            </View>
            <View style={styles.reviewItem}>
              <Text style={styles.title}>
                {banker ? bankerDetails.bankerTag : 'Banker'} Fixed Fee
              </Text>
              <Text style={styles.value}>
                {usdValueFormatter.format(bankerFixedFee)}
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
      <View style={styles.actionContainer}>
        <TouchableOpacity style={styles.actionButton} onPress={onEdit}>
          <Text style={styles.buttonText}>Edit</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.actionButton, {marginLeft: 20}]}
          onPress={createPath}>
          <Text style={styles.buttonText}>Proceed</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ReviewPath;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    height: height * 0.7,
  },
  scrollView: {
    flex: 1,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
    marginBottom: 20,
  },
  reviewItem: {
    marginVertical: 10,
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  value: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#186AB4',
    fontSize: 16,
  },
  actionContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  actionButton: {
    backgroundColor: '#186AB4',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
    paddingVertical: 14,
  },
  buttonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  loadingContainer: {
    // flex: 1,
    paddingVertical: 60,
    justifyContent: 'center',
  },
  instructionList: {
    maxHeight: 200,
  },
  instructionItem: {
    flexDirection: 'row',
    marginTop: 3,
  },
  instructionKey: {
    fontFamily: 'Montserrat',
    color: '#186AB4',
    width: '50%',
  },
  instructionValue: {
    fontFamily: 'Montserrat',
    color: '#186AB4',
  },
});
