import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import PrimaryButton from '../PrimaryButton';
import {WToast} from 'react-native-smart-tip';

const TxnTypeSelector = ({
  onNextClick,
  txnType,
  setTxnType,
  onBackClick,
  instantLink,
  setInstantLink,
}) => {
  const onNext = () => {
    if (!txnType) {
      return WToast.show({
        data: 'Please select a transaction type',
        position: WToast.position.TOP,
      });
    }

    if (txnType === 'Instant' && !instantLink) {
      return WToast.show({
        data: 'Please input an instant link',
        position: WToast.position.TOP,
      });
    }

    onNextClick();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Select Type</Text>
      <View style={styles.actionsContainer}>
        <Text style={styles.label}>
          Is The Transaction Instantly Completed Or Not?
        </Text>
        <View style={styles.radioButton}>
          <TouchableOpacity
            style={[styles.radioItem, txnType === 'Instant' && styles.active]}
            onPress={() => setTxnType('Instant')}>
            <Text
              style={[
                styles.radioText,
                txnType === 'Instant' && styles.activeRadioText,
              ]}>
              Instant
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.radioItem,
              txnType === 'Fund' && styles.active,
              {marginHorizontal: 15},
            ]}
            onPress={() => setTxnType('Fund')}>
            <Text
              style={[
                styles.radioText,
                txnType === 'Fund' && styles.activeRadioText,
              ]}>
              Fund
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.radioItem,
              txnType === 'InstantFund' && styles.active,
            ]}
            onPress={() => setTxnType('InstantFund')}>
            <Text
              style={[
                styles.radioText,
                txnType === 'InstantFund' && styles.activeRadioText,
              ]}>
              Instant Fund
            </Text>
          </TouchableOpacity>
        </View>
        {txnType === 'Instant' && (
          <View style={styles.linkInputContainer}>
            <Text style={styles.label}>
              Enter Link For Instant Transaction?
            </Text>
            <TextInput
              style={styles.input}
              placeholder="Ex. www.link.com"
              placeholderTextColor="#9A9A9A"
              value={instantLink}
              onChangeText={(text) => setInstantLink(text)}
            />
          </View>
        )}
      </View>

      <PrimaryButton title="Next Input" onPress={onNext} onBack={onBackClick} />
    </View>
  );
};

export default TxnTypeSelector;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
  actionsContainer: {
    // flex: 1,
    paddingVertical: 50,
    justifyContent: 'center',
  },
  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  radioButton: {
    flexDirection: 'row',
  },
  radioItem: {
    flex: 1,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
    borderRadius: 6,
  },
  active: {
    borderColor: '#186AB4',
  },
  radioText: {
    color: '#788995',
    fontFamily: 'Montserrat-SemiBold',
  },
  activeRadioText: {
    color: '#186AB4',
  },
  linkInputContainer: {
    marginTop: 20,
  },
  input: {
    color: '#041939',
    height: 45,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 10,
  },
});
