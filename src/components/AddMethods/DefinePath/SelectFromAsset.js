import React, {useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {AppContext} from '../../../contexts/AppContextProvider';
import {WToast} from 'react-native-smart-tip';
import CustomDropDown from '../../CustomDropDown';
import PrimaryButton from '../PrimaryButton';

const SelectFromAsset = ({
  onNextClick,
  selectedAsset,
  setSelectedAsset,
  onBackClick,
}) => {
  const {cryptoTableData} = useContext(AppContext);

  const options = cryptoTableData
    ? cryptoTableData.map((item) => ({
        ...item,
        name: item.coinName,
        image: item.coinImage,
      }))
    : [];

  const onNext = () => {
    if (!selectedAsset) {
      return WToast.show({
        data: 'Please select a asset',
        position: WToast.position.TOP,
      });
    }
    onNextClick();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Selecting From Asset</Text>
      <View style={styles.actionsContainer}>
        <CustomDropDown
          label="What Asset Is The User Allowed To Pay in?"
          items={options}
          onDropDownSelect={setSelectedAsset}
          selectedItem={selectedAsset}
          placeHolder="Select a Asset"
        />
      </View>
      <PrimaryButton title="Next Input" onPress={onNext} onBack={onBackClick} />
    </View>
  );
};

export default SelectFromAsset;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
  actionsContainer: {
    // flex: 1,
    justifyContent: 'center',
    paddingVertical: 50,
  },
});
