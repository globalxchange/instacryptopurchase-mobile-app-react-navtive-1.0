import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import PrimaryButton from '../PrimaryButton';
import {WToast} from 'react-native-smart-tip';

class SetTradingFee extends Component {
  constructor(props) {
    super(props);

    const {instaTradeFee, bankerTradeFee} = props;

    this.state = {
      instaCryptoFee: instaTradeFee ? instaTradeFee.toString() : '',
      bankerFee: bankerTradeFee ? bankerTradeFee.toString() : '',
      isInstaFocused: true,
    };
  }

  onNext = () => {
    const {instaCryptoFee, bankerFee} = this.state;
    const {
      bankerName,
      setInstaTradeFee,
      setBankerTradeFee,
      onNextClick,
    } = this.props;

    if (instaCryptoFee === '') {
      setInstaTradeFee(parseFloat(0));
    } else if (!isNaN(parseFloat(instaCryptoFee))) {
      setInstaTradeFee(parseFloat(instaCryptoFee));
    } else {
      return WToast.show({
        data: 'Please input a valid InstaCrypto Fee',
        position: WToast.position.TOP,
      });
    }

    if (bankerFee === '') {
      setBankerTradeFee(parseFloat(0));
    } else if (!isNaN(parseFloat(bankerFee))) {
      setBankerTradeFee(parseFloat(bankerFee));
    } else {
      return WToast.show({
        data: `Please input a valid ${
          bankerName ? bankerName.name : 'Banker'
        } Fee`,
        position: WToast.position.TOP,
      });
    }

    onNextClick();
  };

  render() {
    const {bankerName, onBackClick} = this.props;
    const {instaCryptoFee, bankerFee, isInstaFocused} = this.state;

    return (
      <View style={styles.container}>
        <Text style={styles.header}>Set Trading Fee</Text>
        <View style={styles.inputForm}>
          <View style={styles.inputContainer}>
            <TextInput
              placeholder="0.00%"
              placeholderTextColor={isInstaFocused ? '#186AB4' : '#041939'}
              style={[styles.input, isInstaFocused && styles.active]}
              onFocus={() => this.setState({isInstaFocused: true})}
              keyboardType="numeric"
              returnKeyType="done"
              value={instaCryptoFee}
              onChangeText={(text) => this.setState({instaCryptoFee: text})}
            />
            <Text style={[styles.label, isInstaFocused && styles.active]}>
              InstaCrypto
            </Text>
          </View>
          <View style={styles.separator} />
          <View style={styles.inputContainer}>
            <TextInput
              placeholder="0.00%"
              placeholderTextColor={!isInstaFocused ? '#186AB4' : '#041939'}
              style={[styles.input, !isInstaFocused && styles.active]}
              onFocus={() => this.setState({isInstaFocused: false})}
              keyboardType="numeric"
              returnKeyType="done"
              value={bankerFee}
              onChangeText={(text) => this.setState({bankerFee: text})}
            />
            <Text style={[styles.label, !isInstaFocused && styles.active]}>
              {bankerName ? bankerName.name : 'Banker'}
            </Text>
          </View>
        </View>
        <PrimaryButton
          title="Next Input"
          onPress={this.onNext}
          onBack={onBackClick}
        />
      </View>
    );
  }
}

export default SetTradingFee;

const styles = StyleSheet.create({
  container: {},
  header: {
    textAlign: 'center',
    color: '#186AB4',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    marginBottom: 15,
  },
  inputForm: {marginVertical: 30},
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  input: {
    color: '#041939',
    flex: 1,
    height: 45,
    fontSize: 18,
    fontFamily: 'Montserrat',
  },
  label: {
    color: '#041939',
    fontFamily: 'Montserrat',
    fontSize: 16,
  },
  active: {
    color: '#186AB4',
  },
  separator: {
    backgroundColor: '#186AB4',
    height: 1,
  },
});
