import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import CustomDropDown from '../../CustomDropDown';
import PrimaryButton from '../PrimaryButton';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import {WToast} from 'react-native-smart-tip';
import LoadingAnimation from '../../LoadingAnimation';

const PaymentMethod = ({
  onNextClick,
  selectedMethod,
  setSelectedMethod,
  onBackClick,
}) => {
  const [methods, setMethods] = useState();

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/methods/get`).then(
      (resp) => {
        const {data} = resp;

        if (data.status) {
          const items = data.methods.map((item) => ({
            ...item,
            image: item.icon,
          }));

          setMethods(items);
        }
      },
    );
    return () => {};
  }, []);

  const onNext = () => {
    if (!selectedMethod) {
      return WToast.show({
        data: 'Please select a banker',
        position: WToast.position.TOP,
      });
    }
    onNextClick();
  };

  if (!methods) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Select Payment Method</Text>
      <View style={styles.actionsContainer}>
        <CustomDropDown
          label="What Payment Method Is Used In This Path?"
          items={methods}
          onDropDownSelect={setSelectedMethod}
          selectedItem={selectedMethod}
          placeHolder="Select a Payment Method"
        />
      </View>
      <PrimaryButton title="Next Input" onPress={onNext} onBack={onBackClick} />
    </View>
  );
};

export default PaymentMethod;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
  actionsContainer: {
    // flex: 1,
    paddingVertical: 50,
    justifyContent: 'center',
  },
  loadingContainer: {
    // flex: 1,
    paddingVertical: 70,
    justifyContent: 'center',
  },
});
