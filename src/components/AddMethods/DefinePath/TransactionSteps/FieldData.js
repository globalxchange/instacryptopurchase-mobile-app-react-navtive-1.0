import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import NumberToWord from 'number-to-words';

const FieldData = ({label, buttonLabel, items, setItems}) => {
  const [isInputOpen, setIsInputOpen] = useState(false);
  const [keyInput, setKeyInput] = useState('');
  const [valueInput, setValueInput] = useState('');

  const addValueHandler = () => {
    if (keyInput ? !valueInput : valueInput) {
      return WToast.show({
        data: 'Please input both fields',
        position: WToast.position.TOP,
      });
    }

    if (keyInput && valueInput) {
      let foundIndex = -1;

      items.forEach((item, index) => {
        if (item.key === keyInput) {
          return (foundIndex = index);
        }
      });

      if (foundIndex < 0) {
        setItems([...items, {key: keyInput, value: valueInput}]);
      } else {
        const tempArray = [...items];
        tempArray[foundIndex] = {key: keyInput, value: valueInput};
        setItems(tempArray);
      }
    }

    setIsInputOpen(false);
    setKeyInput('');
    setValueInput('');
  };

  return (
    <View style={styles.container}>
      <View style={styles.buttonContainer}>
        <Text style={styles.label}>{label}</Text>
        {isInputOpen ? (
          <View style={styles.addItemContainer}>
            <View style={styles.inputContainer}>
              <View style={styles.inputView}>
                <TextInput
                  style={styles.input}
                  placeholder="Key"
                  placeholderTextColor="#9A9A9A"
                  value={keyInput}
                  onChangeText={(text) => setKeyInput(text)}
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  style={styles.input}
                  placeholder="Value"
                  placeholderTextColor="#9A9A9A"
                  value={valueInput}
                  onChangeText={(text) => setValueInput(text)}
                />
              </View>
            </View>
            <TouchableOpacity
              style={styles.addButton}
              onPress={addValueHandler}>
              <Image
                style={styles.icon}
                source={require('../../../../assets/add-colored.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        ) : (
          <>
            <TouchableOpacity
              style={styles.addStepButton}
              onPress={() => setIsInputOpen(true)}>
              <Image
                style={styles.plusIcon}
                source={require('../../../../assets/add-colored.png')}
                resizeMode="contain"
              />
              <View style={styles.addTextContainer}>
                <Text
                  style={styles.addText}>{`Add ${NumberToWord.toWordsOrdinal(
                  items.length + 1,
                )} ${buttonLabel}`}</Text>
              </View>
            </TouchableOpacity>
            <View style={styles.instructionList}>
              {items.map((item) => (
                <View key={item.key} style={styles.instructionItem}>
                  <Text style={styles.instructionKey}>{item.key}</Text>
                  <Text style={styles.instructionValue}>{item.value}</Text>
                </View>
              ))}
            </View>
          </>
        )}
      </View>
    </View>
  );
};

export default FieldData;

const styles = StyleSheet.create({
  container: {
    marginBottom: 25,
  },
  buttonContainer: {},
  label: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
    fontSize: 12,
    marginBottom: 10,
  },
  stepContainer: {
    paddingBottom: 25,
  },
  addStepText: {
    fontFamily: 'Montserrat',
    color: '#9A9A9A',
    marginBottom: 8,
  },
  plusIcon: {
    height: 25,
    width: 25,
  },
  addStepButton: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  addTextContainer: {
    backgroundColor: '#186AB4',
    flex: 1,
    justifyContent: 'center',
    borderRadius: 6,
    marginLeft: 20,
    padding: 10,
  },
  addText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 13,
    textTransform: 'capitalize',
  },
  addItemContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  inputContainer: {
    flex: 1,
  },
  inputView: {
    height: 40,
    paddingHorizontal: 10,
    borderColor: '#DFDDE2',
    borderWidth: 1,
    borderRadius: 4,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    flex: 1,
    fontFamily: 'Montserrat',
    height: '100%',
  },
  addButton: {
    width: 46,
    height: 46,
    marginBottom: 10,
    paddingVertical: 9,
    alignItems: 'flex-end',
  },
  icon: {
    width: 30,
    height: 30,
  },
  instructionList: {
    marginTop: 10,
  },
  instructionItem: {
    flexDirection: 'row',
    marginTop: 3,
  },
  instructionKey: {
    fontFamily: 'Montserrat',
    color: '#186AB4',
    width: '50%',
  },
  instructionValue: {
    fontFamily: 'Montserrat',
    color: '#186AB4',
  },
});
