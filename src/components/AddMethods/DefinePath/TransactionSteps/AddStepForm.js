import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';
import FieldData from './FieldData';
import PrimaryButton from '../../PrimaryButton';
import * as ImagePicker from 'expo-image-picker';
import {Permissions, FileSystem} from 'react-native-unimodules';
import Constants from 'expo-constants';
import {decode} from 'base64-arraybuffer';
import {WToast} from 'react-native-smart-tip';
import {S3} from 'aws-sdk';
import {S3_CONFIG} from '../../../../configs';

const {height} = Dimensions.get('window');

const AddStepForm = ({
  selectedStep,
  closeForm,
  selectedPos,
  isNameDisabled,
  onAddNewStep,
  failureStep,
  setFailureStep,
  setSuccessStep,
  successStep,
}) => {
  const [stepStatus, setStepStatus] = useState('');
  const [stepName, setStepName] = useState('');
  const [instructions, setInstructions] = useState([]);
  const [links, setLinks] = useState([]);
  const [clauses, setClauses] = useState([]);
  const [iconLink, setIconLink] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    (async () => {
      if (Constants.platform.ios) {
        const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
          // alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  useEffect(() => {
    if (selectedStep) {
      // console.log('Selected Step', selectedStep);

      setStepName(selectedStep.name || '');
      setStepStatus(selectedStep.status || '');
      setInstructions(selectedStep.instructions || []);
      setClauses(selectedStep.clauses || []);
      setLinks(selectedStep.links || []);
      setIconLink(selectedStep.iconLink || '');
    }
  }, [selectedStep]);

  const imagePickerHandler = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
        base64: true,
      });
      if (!result.cancelled) {
        setIsLoading(true);

        const fPath = result.uri;
        let options = {encoding: FileSystem.EncodingType.Base64};
        FileSystem.readAsStringAsync(fPath, options)
          .then((data) => {
            setIsLoading(true);

            const BUCKET_NAME = 'gxnitrousdata';
            const S3Client = new S3({
              ...S3_CONFIG,
              Bucket: BUCKET_NAME,
            });

            const arrayBuffer = decode(data);
            const params = {
              Bucket: BUCKET_NAME,
              Key: `vaultpathsSteps/${stepName}_${stepStatus}${Date.now()}.jpg`,
              Body: arrayBuffer,
              ContentType: 'image/jpeg',
              ACL: 'public-read',
            };
            S3Client.upload(params, async (err, s3Data) => {
              if (err) {
                console.log('Uploading Pic Error', err);
              }
              if (s3Data.Location) {
                setIconLink(s3Data.Location);
              }
            });
          })
          .catch((err) => {
            console.log('​getFile -> err', err);
            setIsLoading(false);
          });
      }
    } catch (E) {
      console.log(E);
      setIsLoading(false);
    }
  };

  const onAddStep = () => {
    if (!stepStatus) {
      return WToast.show({
        data: 'Please Input Step Status',
        position: WToast.position.TOP,
      });
    }

    if (!stepName) {
      return WToast.show({
        data: 'Please Input Step Name',
        position: WToast.position.TOP,
      });
    }

    const object = {
      key: `step${selectedPos + 1}`,
      name: stepName.trim(),
      status: stepStatus.trim(),
      links,
      instructions,
      clauses,
      iconLink,
    };

    onAddNewStep(object, selectedPos);
  };

  const onFailureStep = () => {
    setFailureStep(selectedPos + 1);

    if (successStep === selectedPos + 1) {
      setSuccessStep('');
    }
  };

  const onSuccessStep = () => {
    setSuccessStep(selectedPos + 1);

    if (failureStep === selectedPos + 1) {
      setFailureStep('');
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>
        {selectedStep ? selectedStep.name : 'Add Step Form'}
      </Text>
      <Text style={styles.stepNo}>{`Step ${
        selectedPos ? selectedPos + 1 : 1
      }`}</Text>
      <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
        <TouchableWithoutFeedback>
          <View>
            <View style={styles.formContainer}>
              <View style={styles.radioContainer}>
                <Text
                  onPress={onSuccessStep}
                  style={[
                    styles.radioItemGreen,
                    {opacity: selectedPos + 1 === successStep ? 1 : 0.5},
                  ]}>
                  Success Step
                </Text>
                <Text
                  onPress={onFailureStep}
                  style={[
                    styles.radioItemRed,
                    {opacity: selectedPos + 1 === failureStep ? 1 : 0.5},
                  ]}>
                  Failure Step
                </Text>
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.inputLabel}>
                  What You Want To Assign The Status Of The Transaction In This
                  Step ?
                </Text>
                <View
                  style={[
                    styles.inputDecoration,
                    isNameDisabled && styles.inputDisabledDecoration,
                  ]}>
                  <TextInput
                    editable={!isNameDisabled}
                    style={styles.inputField}
                    placeholder="Ex. Processing"
                    placeholderTextColor="#186AB4"
                    value={stepStatus}
                    onChangeText={(text) => setStepStatus(text)}
                  />
                </View>
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.inputLabel}>Name This Step</Text>
                <View style={[styles.inputDecoration]}>
                  <TextInput
                    style={styles.inputField}
                    placeholder="Ex. Process The Transfer"
                    placeholderTextColor="#186AB4"
                    value={stepName}
                    onChangeText={(text) => setStepName(text)}
                  />
                  <TouchableOpacity
                    style={styles.inputButton}
                    onPress={imagePickerHandler}>
                    <Image
                      style={styles.inputButtonIcon}
                      source={
                        iconLink
                          ? {uri: iconLink}
                          : require('../../../../assets/upload-icon.png')
                      }
                      resizeMode={iconLink ? 'cover' : 'contain'}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <FieldData
                label="Write Detailed Instruction(s) On The User Has to Do"
                buttonLabel="Instruction"
                items={instructions}
                setItems={setInstructions}
              />
              <FieldData
                label="Do You Want To Include Any Links?"
                buttonLabel="Link"
                items={links}
                setItems={setLinks}
              />
              <FieldData
                label="Add Any Additional Clauses"
                buttonLabel="Clauses"
                items={clauses}
                setItems={setClauses}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
      <PrimaryButton onBack={closeForm} title="Add Step" onPress={onAddStep} />
    </View>
  );
};

export default AddStepForm;

const styles = StyleSheet.create({
  container: {
    height: height * 0.7,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
  stepNo: {
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    color: '#9A9A9A',
    marginVertical: 5,
    fontSize: 13,
  },
  formContainer: {
    paddingVertical: 25,
  },
  inputContainer: {
    marginBottom: 20,
  },
  radioContainer: {
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  radioItemGreen: {
    backgroundColor: '#186AB4',
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 10,
    paddingVertical: 3,
    paddingHorizontal: 15,
    marginRight: 15,
  },
  radioItemRed: {
    backgroundColor: '#D81238',
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 10,
    paddingVertical: 3,
    paddingHorizontal: 15,
    marginRight: 15,
  },
  inputLabel: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
    fontSize: 12,
    marginBottom: 10,
  },
  inputDecoration: {
    borderColor: '#DFDDE2',
    borderWidth: 1,
    borderRadius: 6,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputDisabledDecoration: {
    borderColor: '#000000',
  },
  inputField: {
    fontFamily: 'Montserrat',
    color: '#041939',
    paddingHorizontal: 10,
    height: '100%',
    fontSize: 13,
    flex: 1,
  },
  inputFieldDisabled: {},
  inputButton: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputButtonIcon: {
    height: 25,
    width: 25,
    borderRadius: 12.5,
  },
});
