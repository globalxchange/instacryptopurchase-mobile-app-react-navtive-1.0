import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  FlatList,
} from 'react-native';
import PrimaryButton from '../PrimaryButton';
import {WToast} from 'react-native-smart-tip';
import Clipboard from '@react-native-community/clipboard';

const AddInstruction = ({
  bankerName,
  onNextClick,
  onBackClick,
  setOtherInstruction,
  otherInstruction,
}) => {
  const [keyInput, setKeyInput] = useState('');
  const [valueInput, setValueInput] = useState('');
  const [instruction, setInstruction] = useState([]);
  const [activeItem, setActiveItem] = useState('');

  useEffect(() => {
    if (otherInstruction) {
      setInstruction(otherInstruction);
    }
  }, [otherInstruction]);

  const addInstructionHandler = () => {
    if (!keyInput) {
      return WToast.show({
        data: 'Please input the key',
        position: WToast.position.TOP,
      });
    }

    if (!valueInput) {
      return WToast.show({
        data: 'Please input the value',
        position: WToast.position.TOP,
      });
    }

    let foundIndex = -1;

    instruction.forEach((item, index) => {
      if (item.key === keyInput) {
        return (foundIndex = index);
      }
    });

    if (foundIndex < 0) {
      setInstruction([...instruction, {key: keyInput, value: valueInput}]);
    } else {
      const tempArray = [...instruction];
      tempArray[foundIndex] = {key: keyInput, value: valueInput};
      setInstruction(tempArray);
    }

    setKeyInput('');
    setValueInput('');
    setActiveItem('');
  };

  const pasteHandler = async () => {
    const text = await Clipboard.getString();
    setValueInput(text);
  };

  const onListItemSelected = (item) => {
    setActiveItem(item);
    setKeyInput(item.key);
    setValueInput(item.value);
  };

  const onNext = () => {
    if (instruction.length < 1) {
      return WToast.show({
        data: 'Please add at least on instruction',
        position: WToast.position.TOP,
      });
    }

    setOtherInstruction(instruction);
    onNextClick();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Add Instructions</Text>
      <Text style={styles.bankerName}>
        {bankerName ? bankerName.name : 'Banker'}
      </Text>
      <View style={styles.formContainer}>
        <View style={styles.inputControlContainer}>
          <Text style={styles.label}>Added Instructions</Text>
          <FlatList
            horizontal
            bounces={false}
            showsHorizontalScrollIndicator={false}
            data={instruction}
            keyExtractor={(item) => item.key}
            renderItem={({item}) => (
              <TouchableOpacity
                style={[
                  styles.inputControl,
                  activeItem.key === item.key && styles.activeControl,
                ]}
                onPress={() => onListItemSelected(item)}>
                <Text
                  style={[
                    styles.controlText,
                    activeItem.key === item.key && styles.activeControlText,
                  ]}>
                  {item.key}
                </Text>
              </TouchableOpacity>
            )}
            ListEmptyComponent={
              <View style={styles.inputControl}>
                <Text style={styles.controlText}>None</Text>
              </View>
            }
          />
        </View>
        <View style={styles.addForm}>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Add Instruction</Text>
            <View style={styles.inputView}>
              <TextInput
                style={styles.input}
                placeholder="Key"
                placeholderTextColor="#9A9A9A"
                value={keyInput}
                onChangeText={(text) => setKeyInput(text)}
              />
            </View>
            <View style={styles.inputView}>
              <TextInput
                style={styles.input}
                placeholder="Value"
                placeholderTextColor="#9A9A9A"
                value={valueInput}
                onChangeText={(text) => setValueInput(text)}
              />
              <TouchableOpacity
                style={styles.pasteButton}
                onPress={pasteHandler}>
                <Image
                  style={styles.pasteIcon}
                  source={require('../../../assets/paste-icon-blue.png')}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity
            style={styles.addButton}
            onPress={addInstructionHandler}>
            <Image
              style={styles.icon}
              source={require('../../../assets/add-colored.png')}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
      </View>
      <PrimaryButton
        title="Review Payment Type"
        onPress={onNext}
        onBack={onBackClick}
      />
    </View>
  );
};

export default AddInstruction;

const styles = StyleSheet.create({
  container: {},
  header: {
    textAlign: 'center',
    color: '#186AB4',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    marginBottom: 15,
  },
  bankerName: {
    color: '#9A9A9A',
    textAlign: 'center',
    fontFamily: 'Montserrat',
    fontSize: 15,
  },
  formContainer: {
    paddingVertical: 25,
  },

  label: {
    fontFamily: 'Montserrat',
    color: '#9A9A9A',
    marginBottom: 10,
  },
  inputControlContainer: {
    alignItems: 'flex-start',
  },
  inputControl: {
    borderColor: '#DFDDE2',
    borderWidth: 1,
    borderRadius: 4,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginRight: 10,
  },
  controlText: {
    color: '#DFDDE2',
  },
  activeControl: {
    borderColor: '#186AB4',
  },
  activeControlText: {
    color: '#186AB4',
  },
  addForm: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  inputContainer: {
    flex: 1,
  },
  inputView: {
    height: 46,
    paddingHorizontal: 10,
    borderColor: '#DFDDE2',
    borderWidth: 1,
    borderRadius: 4,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    flex: 1,
    fontFamily: 'Montserrat',
    height: '100%',
  },
  addButton: {
    width: 46,
    height: 46,
    marginBottom: 10,
    paddingVertical: 9,
    alignItems: 'flex-end',
  },
  icon: {
    width: 30,
    height: 30,
  },
  pasteButton: {
    height: 30,
    width: 30,
    padding: 3,
  },
  pasteIcon: {
    flex: 1,
    width: null,
    height: null,
  },
});
