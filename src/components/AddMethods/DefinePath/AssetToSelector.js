import React, {useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import CustomDropDown from '../../CustomDropDown';
import PrimaryButton from '../PrimaryButton';
import {AppContext} from '../../../contexts/AppContextProvider';
import {WToast} from 'react-native-smart-tip';

const AssetToSelector = ({
  onNextClick,
  selectedAsset,
  setSelectedAsset,
  selectedFromAsset,
  onBackClick,
}) => {
  const {cryptoTableData} = useContext(AppContext);

  const filterCrypto = cryptoTableData
    ? cryptoTableData.filter(
        (item) => item.coinSymbol !== selectedFromAsset.coinSymbol,
      )
    : [];

  const options = filterCrypto
    ? filterCrypto.map((item) => ({
        ...item,
        name: item.coinName,
        image: item.coinImage,
      }))
    : [];

  const onNext = () => {
    if (!selectedAsset) {
      return WToast.show({
        data: 'Please select a asset',
        position: WToast.position.TOP,
      });
    }
    onNextClick();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Selecting To Asset</Text>
      <View style={styles.actionsContainer}>
        <CustomDropDown
          label="What Asset Is The User Buying?"
          items={options}
          onDropDownSelect={setSelectedAsset}
          selectedItem={selectedAsset}
          placeHolder="Select a Asset"
        />
      </View>
      <PrimaryButton title="Next Input" onPress={onNext} onBack={onBackClick} />
    </View>
  );
};

export default AssetToSelector;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
  actionsContainer: {
    // flex: 1,
    justifyContent: 'center',
    paddingVertical: 50,
  },
});
