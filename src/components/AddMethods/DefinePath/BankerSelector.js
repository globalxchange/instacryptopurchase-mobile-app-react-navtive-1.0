import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import PrimaryButton from '../PrimaryButton';
import CustomDropDown from '../../CustomDropDown';
import Axios from 'axios';
import LoadingAnimation from '../../LoadingAnimation';
import {WToast} from 'react-native-smart-tip';
import {emailValidator} from '../../../utils';
import {GX_API_ENDPOINT, APP_CODE} from '../../../configs';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';

const BankerSelector = ({
  onNextClick,
  selectedBanker,
  setSelectedBanker,
  bankerDetails,
  setBankerDetails,
  onBackPress,
}) => {
  const [bankersList, setBankersList] = useState();
  const [emailInput, setEmailInput] = useState('');
  const [showDetails, setShowDetails] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    Axios.get(
      'https://storeapi.apimachine.com/dynamic/iceprotocol/exchangeList?key=62816d7e-e570-46a2-8c9d-0c1cc97f7bf8',
    ).then((resp) => {
      const {data} = resp;

      if (data.success) {
        const bakers = data.data.map((item) => ({
          ...item,
          name: item.formData.exchangeID,
          image: item.formData.image,
        }));

        setBankersList(bakers);
      }
    });
  }, []);

  const getUser = async () => {
    if (isLoading) {
      return;
    }

    // const email = emailInput.toLowerCase().trim();
    const email = await AsyncStorageHelper.getLoginEmail();
    if (!emailValidator(email)) {
      return WToast.show({
        data: 'Please input valid email',
        position: WToast.position.TOP,
      });
    }
    setShowDetails(false);
    setBankerDetails();
    setIsLoading(true);

    Axios.get('https://teller2.apimachine.com/admin/allBankers')
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          data.data.forEach((user) => {
            if (user.email === email) {
              setBankerDetails(user);
              setShowDetails(true);
              return;
            }
          });
        }
      })
      .catch((error) => {
        console.log('Error getting user details', error);
      })
      .finally(() => setIsLoading(false));
  };

  const onNext = () => {
    if (!selectedBanker) {
      return WToast.show({
        data: 'Please select a banker',
        position: WToast.position.TOP,
      });
    }

    if (!bankerDetails) {
      return WToast.show({
        data: 'Please input valid banker email',
        position: WToast.position.TOP,
      });
    }

    onNextClick();
  };

  if (!bankersList) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Selecting Banker</Text>
      <View style={styles.actionsContainer}>
        <CustomDropDown
          label="What Banker Is Used In This Path?"
          items={bankersList}
          onDropDownSelect={setSelectedBanker}
          selectedItem={selectedBanker}
          placeHolder="Select a Banker"
        />
        <Text style={styles.label}>What Is The Email Of This Banker</Text>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            placeholderTextColor="#9A9A9A"
            placeholder="Ex. banker@mail.com"
            value={emailInput}
            onChangeText={(text) => setEmailInput(text)}
            keyboardType="email-address"
          />
          <TouchableOpacity style={styles.findButton} onPress={getUser}>
            <Text style={styles.findText}>Find</Text>
          </TouchableOpacity>
        </View>
        {isLoading && <LoadingAnimation height={50} width={50} />}
        {showDetails &&
          (bankerDetails ? (
            <View style={styles.userContainer}>
              <Image
                source={require('../../../assets/call-images.png')}
                style={styles.userAvatar}
                resizeMode="contain"
              />
              <View style={styles.namesContainer}>
                <Text style={styles.name}>
                  {bankerDetails ? bankerDetails.firstName : ''}
                </Text>
                <Text style={styles.profileId}>
                  {bankerDetails ? bankerDetails.bankerTag : ''}
                </Text>
              </View>
            </View>
          ) : (
            <Text style={styles.emptyMessage}>
              No Banker Founder With This Email
            </Text>
          ))}
      </View>
      <PrimaryButton title="Next Input" onPress={onNext} onBack={onBackPress} />
    </View>
  );
};

export default BankerSelector;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  inputContainer: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    flexDirection: 'row',
    height: 45,
    alignItems: 'center',
    paddingHorizontal: 15,
  },
  input: {
    flex: 1,
    color: '#041939',
    height: '100%',
  },
  findButton: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    paddingHorizontal: 6,
    paddingVertical: 2,
  },
  findText: {
    color: 'rgba(120, 137, 149, 0.5)',
    fontSize: 12,
  },
  userContainer: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  userAvatar: {
    width: 28,
    height: 28,
    borderRadius: 14,
  },
  namesContainer: {
    marginLeft: 10,
    flex: 1,
  },
  name: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#788995',
  },
  profileId: {
    fontFamily: 'Montserrat',
    color: '#788995',
    fontSize: 9,
  },
  emptyMessage: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#041939',
    textAlign: 'center',
    marginTop: 20,
  },
  actionsContainer: {
    // flex: 1,
    justifyContent: 'center',
    paddingVertical: 50,
  },
  loadingContainer: {
    // flex: 1,
    paddingVertical: 70,
    justifyContent: 'center',
  },
});
