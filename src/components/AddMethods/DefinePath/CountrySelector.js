import React, {useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {AppContext} from '../../../contexts/AppContextProvider';
import CustomDropDown from '../../CustomDropDown';
import PrimaryButton from '../PrimaryButton';
import {WToast} from 'react-native-smart-tip';

const CountrySelector = ({selectedCountry, setSelectedCountry, onNext}) => {
  const {countryList} = useContext(AppContext);

  const countryOptions = countryList.map((item) => ({
    ...item,
    name: item.value,
    image: item.formData.Flag,
  }));

  const onNextClick = () => {
    if (!selectedCountry) {
      return WToast.show({
        data: 'Please select a country',
        position: WToast.position.TOP,
      });
    }
    onNext();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Selecting Country</Text>
      <View style={styles.actionsContainer}>
        <CustomDropDown
          label="What Country Is Used In This Path?"
          items={countryOptions}
          onDropDownSelect={setSelectedCountry}
          selectedItem={selectedCountry}
          placeHolder="Select a country"
        />
      </View>
      <PrimaryButton title="Next Input" onPress={onNextClick} />
    </View>
  );
};

export default CountrySelector;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  header: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
  actionsContainer: {
    // flex: 1,
    paddingVertical: 50,
    justifyContent: 'center',
  },
});
