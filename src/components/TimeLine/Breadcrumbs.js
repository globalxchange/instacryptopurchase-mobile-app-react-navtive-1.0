import React from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {getUriImage} from '../../utils';

const Breadcrumbs = ({
  selectedCountry,
  fromCurrency,
  paymentCurrency,
  paymentType,
  selectedBanker,
}) => {
  return (
    <View style={styles.container}>
      <View style={[styles.itemContainer, paymentCurrency || styles.disabled]}>
        <TouchableOpacity disabled style={[styles.item]}>
          <Image
            style={styles.icon}
            source={
              fromCurrency
                ? {uri: fromCurrency.coinImage}
                : require('../../assets/default-breadcumb-icon/currency.png')
            }
            resizeMode="contain"
          />
          {/* <Text style={styles.name}>
            {paymentCurrency ? paymentCurrency.name : 'Currency'}
          </Text> */}
        </TouchableOpacity>
      </View>

      <View style={styles.itemContainer}>
        <TouchableOpacity disabled style={styles.item}>
          <FastImage
            style={styles.icon}
            source={
              selectedCountry
                ? {uri: getUriImage(selectedCountry.image)}
                : require('../../assets/default-breadcumb-icon/country.png')
            }
            resizeMode="contain"
          />
          {/* <Text style={styles.name}>
            {selectedCountry ? selectedCountry.name : 'Country'}
          </Text> */}
        </TouchableOpacity>
      </View>

      <View style={[styles.itemContainer, paymentCurrency || styles.disabled]}>
        <TouchableOpacity disabled style={[styles.item]}>
          <Image
            style={styles.icon}
            source={
              paymentCurrency
                ? {uri: paymentCurrency.image}
                : require('../../assets/default-breadcumb-icon/currency.png')
            }
            resizeMode="contain"
          />
          {/* <Text style={styles.name}>
            {paymentCurrency ? paymentCurrency.name : 'Currency'}
          </Text> */}
        </TouchableOpacity>
      </View>

      <View style={[styles.itemContainer, paymentType || styles.disabled]}>
        <TouchableOpacity disabled style={styles.item}>
          <FastImage
            style={styles.icon}
            source={
              paymentType
                ? {uri: getUriImage(paymentType.icon)}
                : require('../../assets/default-breadcumb-icon/method.png')
            }
            resizeMode="contain"
          />
          {/* <Text style={styles.name}>
            {paymentType ? paymentType.name : 'Method'}
          </Text> */}
        </TouchableOpacity>
      </View>
      <View style={[styles.itemContainer, selectedBanker || styles.disabled]}>
        <TouchableOpacity disabled style={styles.item}>
          <FastImage
            style={styles.icon}
            source={
              selectedBanker
                ? {uri: getUriImage(selectedBanker.profilePicURL)}
                : require('../../assets/default-breadcumb-icon/banker.png')
            }
            resizeMode="contain"
          />
          {/* <Text style={styles.name}>
            {selectedBanker ? selectedBanker.name : 'Banker'}
          </Text> */}
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Breadcrumbs;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: 'white',
  },

  itemContainer: {
    flex: 1,
    // backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    width: '100%',
    paddingHorizontal: 12,
    alignItems: 'center',
    justifyContent: 'center',
  },
  disabled: {
    opacity: 0.35,
  },
  forwardIcon: {
    position: 'absolute',
    left: -6,
    width: 12,
  },
  icon: {
    height: 24,
    width: 24,
    marginBottom: 3,
  },
  name: {
    color: '#788995',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 8,
    width: '100%',
    overflow: 'hidden',
    height: 10,
    textAlign: 'center',
  },
});
