import {useNavigation} from '@react-navigation/native';
import Axios from 'axios';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {View, StyleSheet, FlatList, TouchableOpacity, Text} from 'react-native';
import {SharedElement} from 'react-navigation-shared-element';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {getAssetData} from '../../utils';
import Breadcrumbs from './Breadcrumbs';
import StepItem from './StepItem';

const TimeLine = ({
  currentPath,
  currentStep,
  currentInstitution,
  stepPos,
  tnxData,
}) => {
  const {navigate} = useNavigation();

  const flatListRef = useRef();

  const {cryptoTableData} = useContext(AppContext);

  const [fromCurrency, setFromCurrency] = useState();
  const [countryData, setCountryData] = useState();
  const [toCurrency, setToCurrency] = useState();
  const [bankerName, setBankerName] = useState();
  const [paymentType, setPaymentType] = useState();

  useEffect(() => {
    if (currentPath && tnxData) {
      console.log('currentPath', currentPath);

      const from = getAssetData(currentPath?.from_currency, cryptoTableData);
      setFromCurrency(from);

      const to = getAssetData(currentPath?.to_currency, cryptoTableData);
      setToCurrency(to);

      Axios.get('https://teller2.apimachine.com/admin/allBankers')
        .then((resp) => {
          const {data} = resp;
          const bankers = data.status ? data.data : [];

          const banker = bankers.find(
            (x) => x.bankerTag === currentPath.banker,
          );
          setBankerName(banker);
        })
        .catch((error) => {});

      Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/stats/get`, {
        params: {
          paymentMethod: currentPath.paymentMethod,
          country: currentPath.country,
        },
      })
        .then(({data}) => {
          console.log('Data', data);

          const method = data.pathData?.paymentMethod[0];
          setPaymentType(method?.metadata);

          const counrty = data.pathData?.country[0];
          setCountryData(counrty?.metadata);
        })
        .catch((error) => {});
    }
  }, [currentPath, tnxData, cryptoTableData]);

  const onNextPress = () => {
    try {
      navigate('Details', {
        item: currentPath.stepsArray[stepPos],
        tnxData,
        stepsArray: currentPath.stepsArray,
      });
    } catch (error) {
      console.log('Error on next click', error);
    }
  };

  const onItemPress = (data) => {
    console.log('Clicked');
    navigate('Details', {
      item: data,
      tnxData,
      stepsArray: currentPath.stepsArray,
      fromCurrency,
      toCurrency,
      paymentType,
      bankerName,
      countryData,
    });
  };

  const scrollPos = stepPos
    ? stepPos > currentPath?.stepsArray?.length - 1
      ? currentPath?.stepsArray?.length - 1
      : stepPos
    : 0;

  return (
    <View style={styles.container}>
      <SharedElement id={'item.breadCrumbs'}>
        <Breadcrumbs
          fromCurrency={fromCurrency}
          paymentCurrency={toCurrency}
          paymentType={paymentType}
          selectedBanker={bankerName}
          selectedCountry={countryData}
        />
      </SharedElement>
      <FlatList
        ref={flatListRef}
        initialScrollIndex={scrollPos}
        onScrollToIndexFailed={() => {}}
        showsVerticalScrollIndicator={false}
        data={currentPath.stepsArray}
        keyExtractor={(item, index) => `${item._id || index}`}
        renderItem={({item}) => (
          <StepItem
            data={item}
            isCurrent={currentStep.status === item.status}
            onPress={() => onItemPress(item)}
          />
        )}
      />
      <TouchableOpacity onPress={onNextPress} style={styles.actionContainer}>
        <Text style={styles.actionText}>What To Do Next</Text>
      </TouchableOpacity>
    </View>
  );
};

export default TimeLine;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  actionContainer: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
  actionText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: 'white',
    fontSize: 18,
  },
});
