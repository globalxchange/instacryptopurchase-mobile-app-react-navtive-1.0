import React, {useState, useEffect, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  RefreshControl,
} from 'react-native';
import LoadingAnimation from './LoadingAnimation';
import Axios from 'axios';
import {TELLER_API_ENDPOINT} from '../configs';
import {AppContext} from '../contexts/AppContextProvider';
import {usdValueFormatter, formatterHelper} from '../utils';

const TellersList = () => {
  const {homeSearchInput} = useContext(AppContext);

  const [tellerList, setTellerList] = useState();
  const [filteredList, setFilteredList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (tellerList) {
      const queryString = homeSearchInput.toLowerCase();

      const list = tellerList.filter(
        (item) =>
          item.transactionAsset.toLowerCase().includes(queryString) ||
          item.cognitoName.toLowerCase().includes(queryString) ||
          item.email.toLowerCase().includes(queryString),
      );
      setFilteredList(list);
    }
  }, [homeSearchInput, tellerList]);

  useEffect(() => {
    getTellerList();
  }, []);

  const getTellerList = () => {
    setIsLoading(true);
    Axios.get(`${TELLER_API_ENDPOINT}/matchOrder`, {
      // params: {
      //   paymentMethod: 'tellerTransfer',
      //   preferredCurrency: 'any',
      //   transactionAsset: 'GXT',
      // },
    })
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          setTellerList(data.data);
          setFilteredList(data.data);
        }
      })
      .catch((error) => console.log('error Getting Live Order', error))
      .finally(() => setIsLoading(false));
  };

  return (
    <View style={styles.container}>
      {tellerList ? (
        <FlatList
          style={styles.scrollView}
          data={filteredList}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item._id}
          refreshControl={
            <RefreshControl refreshing={isLoading} onRefresh={getTellerList} />
          }
          renderItem={({item}) => (
            <View style={styles.item}>
              <Image
                style={styles.cryptoIcon}
                resizeMode="contain"
                source={{
                  uri: 'https://i.pravatar.cc/110',
                }}
              />
              <View style={styles.nameContainer}>
                <Text style={styles.cryptoName}>
                  {item.profile.cognitoName}
                </Text>
              </View>
              <View style={styles.priceContainer}>
                <Text style={styles.orderVolume}>
                  {formatterHelper(item.volumeLimitMax, item.transactionAsset)}{' '}
                  {item.transactionAsset}
                </Text>
                <Text style={styles.cryptoPrice}>
                  {usdValueFormatter.format(item.volumeLimitMax * item.price)}
                </Text>
              </View>
            </View>
          )}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>No Supported Crypto Found</Text>
            </View>
          }
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
          <Text style={styles.loadingText}>Fetching Data...</Text>
        </View>
      )}
    </View>
  );
};

export default TellersList;

const styles = StyleSheet.create({
  container: {backgroundColor: '#f7f7f7', flex: 1},
  scrollView: {flex: 1, paddingHorizontal: 10, paddingTop: 10},
  item: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
  },
  cryptoIcon: {
    width: 25,
    height: 25,
    borderRadius: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  nameContainer: {
    flexGrow: 1,
    width: 0,
    marginLeft: 10,
  },
  cryptoName: {
    color: '#001D41',
    fontSize: 16,
    marginBottom: 2,
    fontFamily: 'Montserrat-Bold',
  },
  noOfVendors: {
    color: '#001D41',
    opacity: 0.5,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  priceContainer: {
    alignItems: 'flex-end',
  },
  cryptoPrice: {
    color: '#001D41',
    marginBottom: 2,
    textAlign: 'right',
    fontFamily: 'Roboto-Bold',
  },
  orderVolume: {
    textAlign: 'right',
    color: '#001D41',
    fontFamily: 'Roboto',
    fontSize: 10,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  loadingText: {marginTop: 10, fontFamily: 'Montserrat'},
  emptyContainer: {
    marginTop: 20,
  },
  emptyText: {
    fontFamily: 'Montserrat',
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 16,
    paddingHorizontal: 30,
  },
});
