import React, {useRef, useContext, useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import CategoryMenu from './CategoryMenu';
import {Transitioning, Transition} from 'react-native-reanimated';
import FloatingButton from '../FloatingButton';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {AppContext} from '../../contexts/AppContextProvider';
import {WToast} from 'react-native-smart-tip';
import {useNavigation} from '@react-navigation/native';

const BlockCheck = () => {
  const {isLoggedIn, isBlockCheckSend, setIsBlockCheckSend} = useContext(
    AppContext,
  );
  const {navigate} = useNavigation();

  const {bottom} = useSafeAreaInsets();

  const [isOpen, setIsOpen] = useState(false);

  const overlayViewRef = useRef();
  const popupViewRef = useRef();

  const onFabPress = () => {
    if (isLoggedIn) {
      setIsOpen(!isOpen);
    } else {
      WToast.show({data: 'Please Login First', position: WToast.position.TOP});
      navigate('Landing');
    }
  };

  return (
    <>
      <Transitioning.View
        ref={overlayViewRef}
        style={[
          isOpen ? StyleSheet.absoluteFill : styles.hidden,
          styles.overlay,
        ]}>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.clickOverlay}
          onPress={() => {
            setIsOpen(false);
          }}
        />
        <Transitioning.View
          ref={popupViewRef}
          style={[
            styles.viewContainer,
            {display: isOpen ? 'flex' : 'none', bottom: bottom + 135},
          ]}>
          <CategoryMenu
            isSend={isBlockCheckSend}
            setIsSend={setIsBlockCheckSend}
            menus={menus}
            isOpen={isOpen}
          />
        </Transitioning.View>
      </Transitioning.View>
      {isLoggedIn && (
        <FloatingButton
          onPress={onFabPress}
          icon={require('../../assets/block-check-icon.png')}
        />
      )}
    </>
  );
};

export default BlockCheck;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
  },
  hidden: {
    display: 'none',
  },
  clickOverlay: {
    flex: 1,
    zIndex: 1,
  },
  viewContainer: {
    zIndex: 2,
    position: 'absolute',
    right: 30,
  },
});

const menus = ['Crypto', 'Fiat'];
