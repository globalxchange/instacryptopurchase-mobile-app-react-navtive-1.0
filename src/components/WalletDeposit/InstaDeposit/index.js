import React, {useState, useEffect, useContext} from 'react';
import {StyleSheet} from 'react-native';
import Animated from 'react-native-reanimated';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import CountryChooser from './Fragments/CountryChooser';
import FundingCurrency from './Fragments/FundingCurrency';
import FundingMethodChooser from './Fragments/FundingMethodChooser';
import BankerChooser from './Fragments/BankerChooser';
import PaymentQuote from './Fragments/PaymentQuote';
import {DepositContext} from '../../../contexts/DepositContext';

const InstaDeposit = ({activeCrypto, onClose}) => {
  const {clearInstaDepositState} = useContext(DepositContext);

  const [currentStep, setCurrentStep] = useState();
  const [pathData, setPathData] = useState();
  const [isExpanded, setIsExpanded] = useState(false);

  const getPathData = () => {
    if (activeCrypto) {
      Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/paths/get`, {
        params: {coin: activeCrypto.coinSymbol},
      }).then((resp) => {
        const {data} = resp;

        const paths = data.paths || [];

        setPathData(paths);
      });
    }
  };

  const showFundingMethod = () => {
    setCurrentStep('ChooseMethod');
  };

  const showBankerMethods = () => {
    setCurrentStep('ChooseBanker');
  };

  const showQuoteHandler = () => {
    setCurrentStep('ShowQuote');
  };
  const showFundingCurrency = () => {
    setCurrentStep('ChooseFunding');
  };

  const renderFragment = () => {
    switch (currentStep) {
      case 'ChooseCountry':
        return (
          <CountryChooser
            onNext={showFundingCurrency}
            activeCrypto={activeCrypto}
            pathData={pathData}
          />
        );
      case 'ChooseFunding':
        return (
          <FundingCurrency
            onNext={showFundingMethod}
            activeCrypto={activeCrypto}
            pathData={pathData}
          />
        );
      case 'ChooseMethod':
        return (
          <FundingMethodChooser
            onNext={showBankerMethods}
            activeCrypto={activeCrypto}
            pathData={pathData}
          />
        );
      case 'ChooseBanker':
        return (
          <BankerChooser
            onNext={showQuoteHandler}
            activeCrypto={activeCrypto}
            toggleExpansion={() => setIsExpanded(!isExpanded)}
            isExpanded={isExpanded}
            pathData={pathData}
          />
        );
      case 'ShowQuote':
        return (
          <PaymentQuote
            selectedCrypto={activeCrypto}
            pathData={pathData}
            onClose={() => {
              clearInstaDepositState();
              onClose();
            }}
          />
        );
      default:
        return (
          <CountryChooser
            onNext={showFundingCurrency}
            activeCrypto={activeCrypto}
            pathData={pathData}
          />
        );
    }
  };

  useEffect(() => {
    getPathData();
  }, [activeCrypto]);

  useEffect(() => {
    return () => {
      clearInstaDepositState();
    };
  }, []);

  return (
    <Animated.View
      style={[
        styles.container,
        // {maxHeight: isExpanded ? height - 40 : height / 2},
      ]}>
      {renderFragment()}
    </Animated.View>
  );
};

export default InstaDeposit;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    paddingHorizontal: 30,
    paddingTop: 20,
    paddingBottom: 20,
  },
});
