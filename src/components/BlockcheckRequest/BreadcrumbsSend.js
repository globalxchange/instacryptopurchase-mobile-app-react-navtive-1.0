import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {getUriImage} from '../../utils';

const BreadcrumbsSend = ({type, recipient, currency}) => {
  const onTypeClicked = () => {};

  const onRecipientClicked = () => {};

  const onCurrencyClick = () => {};

  const onTransactSelect = () => {};

  return (
    <View style={styles.container}>
      <View style={[styles.itemContainer]}>
        <TouchableOpacity
          disabled={!type}
          style={[styles.item]}
          onPress={onTypeClicked}>
          <Image
            style={styles.icon}
            source={type ? type.image : require('../../assets/gmail-icon.png')}
            resizeMode="contain"
          />
          <Text style={styles.name}>{type ? type.name : 'Send Currency'}</Text>
        </TouchableOpacity>
      </View>

      <View style={[styles.itemContainer, !currency ? styles.disabled : null]}>
        <TouchableOpacity
          disabled={!(type && recipient && currency)}
          style={styles.item}
          onPress={onCurrencyClick}>
          <Image
            style={styles.forwardIcon}
            resizeMode="contain"
            source={require('../../assets/forward-icon-grey.png')}
          />
          <FastImage
            style={styles.icon}
            source={
              currency
                ? {uri: getUriImage(currency.image)}
                : require('../../assets/default-breadcumb-icon/currency.png')
            }
            resizeMode="contain"
          />
          <Text style={styles.name}>
            {currency ? currency.coinSymbol : 'Currency'}
          </Text>
        </TouchableOpacity>
      </View>

      <View style={[styles.itemContainer, recipient || styles.disabled]}>
        <Image
          style={styles.forwardIcon}
          resizeMode="contain"
          source={require('../../assets/forward-icon-grey.png')}
        />
        <TouchableOpacity
          disabled={!(type && recipient)}
          style={styles.item}
          onPress={onRecipientClicked}>
          <FastImage
            style={styles.icon}
            source={require('../../assets/default-breadcumb-icon/recipient.png')}
            resizeMode="contain"
          />
          <Text style={styles.name}>{'Recipient'}</Text>
        </TouchableOpacity>
      </View>

      <View style={[styles.itemContainer, !currency ? styles.disabled : null]}>
        <Image
          style={styles.forwardIcon}
          resizeMode="contain"
          source={require('../../assets/forward-icon-grey.png')}
        />
        <TouchableOpacity
          disabled={!(type && recipient && currency)}
          onPress={onTransactSelect}
          style={styles.item}>
          <Image
            style={styles.icon}
            source={require('../../assets/default-breadcumb-icon/transact.png')}
            resizeMode="contain"
          />
          <Text style={styles.name}>Transact</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default BreadcrumbsSend;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 15,
  },

  itemContainer: {
    flex: 1,
    // backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    width: '100%',
    paddingHorizontal: 12,
    alignItems: 'center',
    justifyContent: 'center',
  },
  disabled: {
    opacity: 0.35,
  },
  forwardIcon: {
    position: 'absolute',
    left: -6,
    width: 12,
  },
  icon: {
    height: 24,
    width: 24,
    marginBottom: 3,
  },
  name: {
    color: '#788995',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 8,
    width: '100%',
    overflow: 'hidden',
    height: 10,
    textAlign: 'center',
  },
});
