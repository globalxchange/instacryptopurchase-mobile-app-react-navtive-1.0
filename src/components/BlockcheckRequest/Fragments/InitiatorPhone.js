import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../../../configs';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import ActionButton from '../../Connect/ActionButton';
import LoadingAnimation from '../../LoadingAnimation';
import PhoneInput from './PhoneInput';

const InitiatorPhone = ({initiatorPhone, setInitiatorPhone, onNext}) => {
  const [isCountryOpen, setIsCountryOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    (async () => {
      setIsLoading(true);

      const email = await AsyncStorageHelper.getLoginEmail();
      Axios.post(`${GX_API_ENDPOINT}/get_affiliate_data_no_logs`, {
        email,
      })
        .then((resp) => {
          const {data} = resp;
          // console.log('ProfileData', data);

          const profileData = data ? data[0] : '';

          const mobileNumber = profileData?.mobile || '';
          // console.log('mobileNumber', mobileNumber);

          if (mobileNumber.length > 6) {
            setInitiatorPhone(mobileNumber);
            onNext();
          }
        })
        .catch((error) => {})
        .finally(() => setIsLoading(false));
    })();
  }, []);

  const onNextClick = () => {
    if (!initiatorPhone) {
      return WToast.show({
        data: 'Please Input A Mobile Number',
        position: WToast.position.TOP,
      });
    }
    onNext();
  };

  return (
    <View style={styles.container}>
      {isCountryOpen || (
        <View style={styles.controlContainer}>
          <View>
            <Text style={styles.header}>Notify</Text>
            <Text style={styles.subHeader}>
              Enter Your Phone Number To Get SMS Updates On The Transaction
            </Text>
          </View>
        </View>
      )}
      <View style={{marginVertical: 40}}>
        <PhoneInput
          onChangeText={setInitiatorPhone}
          onCountryOpen={(isOpen) => setIsCountryOpen(isOpen)}
        />
      </View>
      <ActionButton text="Proceed" onPress={onNextClick} />
      {isLoading && (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default InitiatorPhone;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
  },
  header: {
    color: '#186AB4',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
    marginTop: 20,
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  loadingContainer: {
    ...StyleSheet.absoluteFill,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
});
