import React, {useEffect, useRef, useState} from 'react';
import {
  Dimensions,
  Keyboard,
  Modal,
  Platform,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import Animated, {
  Clock,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';
import BlockcheckRequestReceive from './BlockcheckRequestReceive';
import BlockcheckRequestSend from './BlockcheckRequestSend';

const {height} = Dimensions.get('window');

const BlockcheckRequest = ({isOpen, setIsOpen, isSend}) => {
  const {bottom} = useSafeAreaInsets();

  const [isExpanded, setIsExpanded] = useState(false);
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  return (
    <Modal
      animationType="slide"
      visible={isOpen}
      transparent
      hardwareAccelerated
      statusBarTranslucent
      onDismiss={() => setIsOpen(false)}
      onRequestClose={() => setIsOpen(false)}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.overlay}
        onPress={() => setIsOpen(false)}
        onPressOut={() => {}}>
        <TouchableWithoutFeedback style={{flex: 1}}>
          <Animated.View
            style={[
              styles.container,
              // {maxHeight: isExpanded ? height - 40 : height * 0.75},
              {
                paddingBottom: bottom,
                transform: [
                  {
                    translateY: interpolate(chatKeyboardAnimation.current, {
                      inputRange: [0, 1],
                      outputRange: [0, -keyboardHeight],
                    }),
                  },
                ],
              },
            ]}>
            {isSend ? (
              <BlockcheckRequestSend setIsOpen={setIsOpen} />
            ) : (
              <BlockcheckRequestReceive setIsOpen={setIsOpen} />
            )}
          </Animated.View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default BlockcheckRequest;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
});
