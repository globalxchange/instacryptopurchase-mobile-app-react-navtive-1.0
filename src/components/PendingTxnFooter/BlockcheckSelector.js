import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const BlockcheckSelector = ({
  setIsListOpen,
  depositsLength,
  withdrawalList,
  pendingSignChecks,
  pendingCashChecks,
  setListTypes,
}) => {
  const {navigate} = useNavigation();

  const openList = (type) => {
    setIsListOpen(true);
    setListTypes(type);
  };

  const ACTIONS = [
    {
      title: 'Deposits',
      count: depositsLength,
      onPress: () => openList('Deposits'),
    },
    {
      title: 'Withdrawals',
      count: withdrawalList,
      onPress: () => openList('Withdrawals'),
    },
    {
      title: 'Checks To Sign',
      count: pendingSignChecks,
      onPress: () => navigate('BlockCheck', {isCashSelected: false}),
    },
    {
      title: 'Checks To Cash',
      count: pendingCashChecks,
      onPress: () => navigate('BlockCheck', {isCashSelected: true}),
    },
  ];

  return (
    <View style={styles.container}>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        <View style={styles.iconContainer}>
          <Image
            source={require('../../assets/block-check-full-logo-white.png')}
            resizeMode="contain"
            style={styles.blockCheckIcon}
          />
        </View>
        <View style={styles.itemsContainer}>
          {ACTIONS.map((item) => (
            <TouchableOpacity
              key={item.title}
              style={styles.item}
              onPress={item.onPress}>
              <Text
                numberOfLines={1}
                adjustsFontSizeToFit
                style={styles.itemText}>
                {item.title}
              </Text>
              {item.count ? (
                <View style={styles.itemCount}>
                  <Text
                    numberOfLines={1}
                    adjustsFontSizeToFit
                    style={styles.itemCountText}>
                    {item.count}
                  </Text>
                </View>
              ) : null}
            </TouchableOpacity>
          ))}
        </View>
      </ScrollView>
    </View>
  );
};

export default BlockcheckSelector;

const styles = StyleSheet.create({
  container: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconContainer: {
    paddingLeft: 25,
    paddingRight: 25,
    width: 180,
  },
  blockCheckIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  itemsContainer: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingRight: 10,
  },
  item: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 6,
    minWidth: 90,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 12,
    paddingHorizontal: 10,
  },
  itemText: {
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 11,
  },
  itemCount: {
    backgroundColor: 'white',
    position: 'absolute',
    top: -10,
    right: -10,
    width: 20,
    height: 20,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemCountText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
});
