import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {AppContext} from '../../contexts/AppContextProvider';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import Axios from 'axios';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';
import TransactionView from './TransactionView';
import BlockcheckSelector from './BlockcheckSelector';

const PendingTxnFooter = () => {
  const {bottom} = useSafeAreaInsets();

  const {blockCheckData} = useContext(AppContext);

  const [isListOpen, setIsListOpen] = useState(false);
  const [listTypes, setListTypes] = useState();
  const [isDeposit, setIsDeposit] = useState(true);
  const [pendingDeposits, setPendingDeposits] = useState();
  const [pendingWithdraws, setPendingWithdraws] = useState();
  const [completedWithdrawals, setCompletedWithdrawals] = useState();
  const [completedDeposits, setCompletedDeposits] = useState();
  const [pendingCashChecks, setPendingCashCheck] = useState();
  const [pendingSignChecks, setPendingSignChecks] = useState();
  const [cashChecks, setCashChecks] = useState();
  const [signChecks, setSignChecks] = useState();

  const [isTxnPopupOpen, setIsTxnPopupOpen] = useState(false);
  const [selectedTnx, setSelectedTnx] = useState();
  const [isAllList, setIsAllList] = useState(false);

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();

      Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/path/deposit/txn/get`, {
        params: {app_code: APP_CODE, email},
      })
        .then(({data}) => {
          setCompletedDeposits(data?.txns || []);
        })
        .catch((error) => {
          console.log('Error getting deposit transaction', error);
        });

      Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/path/withdraw/txn/get`, {
        params: {app_code: APP_CODE, email},
      })
        .then(({data}) => {
          setCompletedWithdrawals(data?.txns || []);
        })
        .catch((error) => {
          console.log('Error getting withraw transaction', error);
        });

      Axios.get(
        `${GX_API_ENDPOINT}/coin/vault/service/blockcheck/request/get`,
        {},
      )
        .then((resp) => {
          const {data} = resp;
          console.log('BlockCheckScreen', data);

          if (data.status) {
            const list = data.bcrs || [];

            const cash = [];
            const signs = [];
            const pendingSigns = [];
            const pendingCashs = [];

            list.forEach((item) => {
              if (item.bcr_type === 'receiver') {
                if (item.status === 'pending') {
                  pendingSigns.push(item);
                }
                cash.push(item);
              } else {
                if (item.status === 'pending') {
                  pendingCashs.push(item);
                }
                signs.push(item);
              }
            });

            setPendingSignChecks(pendingSigns);
            setPendingCashCheck(pendingCashs);
            setCashChecks(cash);
            setSignChecks(signs);
          }
        })
        .catch((error) => {});
    })();
  }, []);

  useEffect(() => {
    if (!isTxnPopupOpen) {
      setSelectedTnx();
      setIsAllList(false);
    }
  }, [isTxnPopupOpen]);

  useEffect(() => {
    if (blockCheckData) {
      // console.log('blockCheckData', blockCheckData);

      setPendingDeposits([
        ...(blockCheckData.processingCryptoDeposit || []),
        ...(blockCheckData.processingFaitDeposit || []),
      ]);

      setPendingWithdraws([
        ...(blockCheckData.processingCryptoWithdraw || []),
        ...(blockCheckData.processingFaitWithdraw || []),
      ]);
    }
  }, [blockCheckData]);

  const onBackPress = () => {
    setIsListOpen(false);
  };

  let activeList;
  let allList;

  switch (listTypes) {
    case 'Deposits':
      activeList = pendingDeposits;
      if (isAllList) {
        allList = completedDeposits;
      }
      break;
    case 'Withdrawals':
      activeList = pendingWithdraws;
      if (isAllList) {
        allList = completedWithdrawals;
      }
      break;
    case 'Checks To Sign':
      activeList = pendingSignChecks;
      if (isAllList) {
        allList = signChecks;
      }
      break;
    case 'Checks To Cash':
      activeList = pendingCashChecks;
      if (isAllList) {
        allList = cashChecks;
      }
      break;
    default:
      activeList = pendingDeposits;
  }

  return (
    <View
      style={[
        styles.container,
        {marginBottom: -bottom, paddingBottom: bottom},
      ]}>
      {isListOpen ? (
        <TransactionView
          {...{
            setIsTxnPopupOpen,
            setSelectedTnx,
            isDeposit,
            isTxnPopupOpen,
            isAllList,
            selectedTnx,
            setIsAllList,
            onBackPress,
            activeList,
            allList,
            listTypes,
          }}
        />
      ) : (
        <BlockcheckSelector
          setIsListOpen={setIsListOpen}
          depositsLength={pendingDeposits?.length || 0}
          withdrawalList={pendingWithdraws?.length || 0}
          pendingSignChecks={pendingSignChecks?.length || 0}
          pendingCashChecks={pendingCashChecks?.length || 0}
          setIsDeposit={setIsDeposit}
          setListTypes={setListTypes}
        />
      )}
    </View>
  );
};

export default PendingTxnFooter;

const styles = StyleSheet.create({
  container: {
    // backgroundColor: 'white',
  },
});
