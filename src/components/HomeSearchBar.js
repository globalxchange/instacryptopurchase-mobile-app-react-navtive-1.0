import React, {useContext} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {AppContext} from '../contexts/AppContextProvider';
import {getUriImage} from '../utils';

const HomeSearchBar = ({searchPlaceholder = '', onClose}) => {
  const {
    toggleCountrySelector,
    filterActiveCountry,
    homeSearchInput,
    setHomeSearchInput,
  } = useContext(AppContext);

  return (
    <View style={styles.container}>
      <View style={styles.searchContainer}>
        <TouchableOpacity onPress={toggleCountrySelector}>
          <FastImage
            style={styles.countryIcon}
            source={{
              uri: filterActiveCountry
                ? getUriImage(filterActiveCountry.formData.Flag)
                : 'https://d22n6gp5xyztod.cloudfront.net/5eb3246af10390217259d8a7298.png',
            }}
            resizeMode="contain"
          />
        </TouchableOpacity>
        <View style={styles.separator} />
        <TextInput
          style={styles.searchInput}
          onChangeText={(text) => setHomeSearchInput(text)}
          value={homeSearchInput}
          autoFocus
          placeholder={
            searchPlaceholder.length < 30
              ? searchPlaceholder
              : searchPlaceholder.substring(0, 30)
          }
          placeholderTextColor={'#878788'}
        />
        <Image
          style={styles.searchIcon}
          source={require('../assets/search-icon.png')}
          resizeMode="contain"
        />
        <TouchableOpacity onPress={onClose} style={styles.closeButton}>
          <Image
            style={styles.closeButtonIcon}
            resizeMode="contain"
            source={require('../assets/cancel-icon.png')}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default HomeSearchBar;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f7f7f7',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  searchContainer: {
    flexDirection: 'row',
    height: 45,
    paddingHorizontal: 15,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    borderRadius: 2,
    elevation: 12,
    alignItems: 'center',
  },
  countryIcon: {
    height: 22,
    width: 22,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginRight: 15,
  },
  separator: {
    backgroundColor: '#CACACA',
    width: 1,
    marginVertical: 8,
  },
  searchInput: {
    flexGrow: 1,
    width: 0,
    height: '100%',
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    fontWeight: 'normal',
    color: 'black',
  },
  searchIcon: {
    height: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  closeButton: {
    height: 30,
    marginLeft: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  closeButtonIcon: {
    width: 15,
    height: 15,
  },
});
