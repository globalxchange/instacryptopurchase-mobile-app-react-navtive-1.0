import {useNavigation} from '@react-navigation/native';
import Axios from 'axios';
import React, {useContext, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import LoadingAnimation from '../LoadingAnimation';
import CurrencyView from './CurrencyView';

const CompleteView = ({
  quoteResp,
  sellCurrency,
  buyCurrency,
  txnType,
  txnId,
  onClose,
}) => {
  const navigation = useNavigation();

  const {walletBalances, getBlockCheckData} = useContext(AppContext);

  const [isLoading, setIsLoading] = useState(false);

  const updatedBuyBalance =
    walletBalances[`${buyCurrency.coinSymbol.toLowerCase()}_balance`] || 0;

  const sellUpdatedBalance =
    walletBalances[`${sellCurrency.coinSymbol.toLowerCase()}_balance`] || 0;

  const goHomeHandler = () => {
    onClose();
    navigation.replace('Drawer', {screen: 'child', params: {screen: 'Home'}});
  };

  const vaultNavigationHandler = () => {
    onClose();
    navigation.replace('Drawer', {
      screen: 'child',
      params: {
        screen: 'Wallet',
        params: {coin: buyCurrency?.coinSymbol},
      },
    });
  };

  const transactionNavigationHandler = async () => {
    onClose();
    setIsLoading(true);

    console.log('Id', txnId);

    const txnData = await getBlockCheckData(txnId);

    setIsLoading(false);

    if (txnData) {
      navigation.replace('Drawer', {
        screen: 'child',
        params: {
          screen: 'Timeline',
          params: {screen: 'List', params: {data: txnData, pos: 1}},
        },
      });
    }
  };

  if (isLoading) {
    return (
      <View style={styles.container}>
        <LoadingAnimation />
      </View>
    );
  }

  console.log('quoteResp', quoteResp);

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.header}>
          {txnType === 'instant' ? 'Trade Executed' : 'Successfully Initiated'}
        </Text>
      </View>
      <CurrencyView
        coin={sellCurrency}
        value={
          quoteResp?.finalFromAmount ||
          (quoteResp[0] ? quoteResp[0].finalFromAmount : 0)
        }
        title="You Sold"
        disabled
      />
      <CurrencyView
        coin={buyCurrency}
        value={
          quoteResp?.finalToAmount ||
          (quoteResp[1] ? quoteResp[1].finalToAmount : 0)
        }
        title="You Received"
        disabled
      />
      {txnType === 'instant' ? (
        <>
          <CurrencyView
            coin={sellCurrency}
            value={sellUpdatedBalance}
            title={`New ${sellCurrency.coinSymbol} Vault Balance`}
            disabled
          />
          <CurrencyView
            coin={buyCurrency}
            value={updatedBuyBalance}
            title={`New ${buyCurrency.coinSymbol} Vault Balance`}
            disabled
          />
        </>
      ) : (
        <Text style={styles.nextText}>What To Do Next?</Text>
      )}
      <View style={styles.actionContainer}>
        <TouchableOpacity
          onPress={
            txnType === 'instant' ? goHomeHandler : transactionNavigationHandler
          }
          style={styles.primaryButton}>
          <Text style={styles.primaryButtonText}>
            {txnType === 'instant' ? 'Home' : 'Start Now'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={
            txnType === 'instant' ? vaultNavigationHandler : goHomeHandler
          }
          style={styles.secondaryButton}>
          <Text
            numberOfLines={1}
            adjustsFontSizeToFit
            style={styles.secondaryButtonText}>
            {txnType === 'instant'
              ? `${buyCurrency?.coinName} Vault`
              : 'I’ll Finish It Later'}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CompleteView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  headerContainer: {
    borderBottomColor: ThemeData.APP_MAIN_COLOR,
    borderBottomWidth: 2,
    paddingBottom: 8,
    marginRight: 'auto',
  },
  header: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 28,
  },
  actionContainer: {
    flexDirection: 'row',
    marginTop: 30,
  },
  primaryButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    borderRadius: 6,
    marginRight: 15,
  },
  primaryButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
    fontSize: 13,
  },
  secondaryButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    borderRadius: 6,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  secondaryButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 13,
    paddingHorizontal: 10,
  },
  nextText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 18,
    marginTop: 40,
  },
});
