import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const StartView = ({paymentType, onStart}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>
        You Are Ready To Initiate Your Transaction
      </Text>
      <Text style={styles.subHeader}>
        You Will Be Using {paymentType.name} To Process Your Transaction. Do You
        Need A Tutorial On How To Use {paymentType.name}?
      </Text>
      <Image
        style={styles.typeImage}
        resizeMode="contain"
        source={{uri: paymentType.icon}}
      />
    </View>
  );
};

export default StartView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    textAlign: 'center',
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 24,
  },
  subHeader: {
    textAlign: 'center',
    marginTop: 20,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    lineHeight: 20,
  },
  typeImage: {
    height: 85,
    width: 85,
    marginTop: 30,
  },
});
