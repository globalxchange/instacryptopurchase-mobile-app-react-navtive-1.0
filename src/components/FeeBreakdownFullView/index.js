/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useState} from 'react';
import {
  BackHandler,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import Pie from 'react-native-pie';
import {FlatList} from 'react-native-gesture-handler';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faChevronDown, faChevronUp} from '@fortawesome/free-solid-svg-icons';
import Animated, {
  Easing,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import {timing} from 'react-native-redash';
import TotalFragment from './TotalFragment';

const FeeBreakdownFullView = ({onClose, quoteResp}) => {
  const [activeTab, setActiveTab] = useState(TABS[0]);
  const [isFragmentExpanded, setIsFragmentExpanded] = useState(false);

  const expandAnimation = useRef(new Animated.Value(1));

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', onBackPress);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    };
  }, []);

  let fragment;

  switch (activeTab?.title) {
    case 'Total':
      fragment = <TotalFragment quoteResp={quoteResp} />;
      break;
    default:
      break;
  }

  useCode(
    () =>
      isFragmentExpanded
        ? [
            set(
              expandAnimation.current,
              timing({
                duration: 300,
                from: 1,
                to: 0,
                easing: Easing.linear,
              }),
            ),
          ]
        : [
            set(
              expandAnimation.current,
              timing({
                duration: 300,
                from: 0,
                to: 1,
                easing: Easing.linear,
              }),
            ),
          ],
    [isFragmentExpanded],
  );

  const SECTIONS = [
    {
      percentage: 20,
      color: '#30BC96',
      title: 'Banker',
    },
    {
      percentage: 50,
      color: '#186AB4',
      title: 'Broker',
    },
    {
      percentage: 30,
      color: '#F7931A',
      title: 'Platform',
    },
  ];

  const onBackPress = () => {
    onClose();
    return true;
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onClose} style={styles.closeButton}>
        <Image
          source={require('../../assets/cancel-icon-colored.png')}
          resizeMode="contain"
          style={styles.closeIcon}
        />
      </TouchableOpacity>
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>Fee Breakdown</Text>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={styles.poweredBy}>Powered By </Text>
          <Image
            source={require('../../assets/exchange-fee-icon.png')}
            resizeMode="contain"
            style={styles.exchangeFeeIcon}
          />
        </View>
      </View>
      <Animated.View
        style={{
          maxHeight: interpolate(expandAnimation.current, {
            inputRange: [0, 1],
            outputRange: [0, 300],
          }),
          backgroundColor: 'white',
          overflow: 'hidden',
        }}>
        <View style={styles.chartContainer}>
          <Pie
            radius={80}
            innerRadius={70}
            sections={SECTIONS}
            dividerSize={0}
            strokeCap={'round'}
          />
          <View style={styles.chartLegendsContainer}>
            <FlatList
              contentContainerStyle={{paddingHorizontal: 20}}
              data={TABS}
              horizontal
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item) => item.title}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => setActiveTab(item)}
                  style={styles.chartLegend}>
                  <View
                    style={[styles.legendColor, {backgroundColor: item.color}]}
                  />
                  <Text style={styles.legendTitle}>{item.title}</Text>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
      </Animated.View>
      <View style={styles.tabsContainer}>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {TABS.map((item) => (
            <TouchableOpacity
              key={item.title}
              onPress={() => setActiveTab(item)}
              style={[
                styles.tabItem,
                item.title === activeTab?.title && styles.tabItemActive,
              ]}>
              <Text
                style={[
                  styles.tabItemText,
                  item.title === activeTab?.title && styles.tabItemTextActive,
                ]}>
                {item.title}
              </Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
        <TouchableOpacity
          style={styles.expandToggle}
          onPress={() => setIsFragmentExpanded(!isFragmentExpanded)}>
          <FontAwesomeIcon
            icon={isFragmentExpanded ? faChevronDown : faChevronUp}
            color={'white'}
            size={22}
          />
        </TouchableOpacity>
      </View>
      {fragment}
    </View>
  );
};

export default FeeBreakdownFullView;

const TABS = [
  {color: '#186AB4', title: 'Total'},
  {color: '#30BC96', title: 'Banker'},
  {color: '#186AB4', title: 'Broker'},
  {color: '#F7931A', title: 'Platform'},
];

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFill,
    backgroundColor: 'white',
    zIndex: 999,
  },
  closeButton: {
    width: 30,
    height: 30,
    padding: 5,
    zIndex: 1,
    marginLeft: 'auto',
    marginRight: 20,
    marginTop: 20,
  },
  closeIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  headerContainer: {
    alignItems: 'center',
  },
  headerText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 32,
  },
  poweredBy: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#182542',
    fontSize: 13,
  },
  exchangeFeeIcon: {
    width: 140,
  },
  chartContainer: {
    alignItems: 'center',
    marginTop: 20,
  },
  chartLegendsContainer: {
    width: '100%',
    marginTop: 30,
  },
  chartLegend: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    width: 120,
    marginRight: 10,
  },
  legendColor: {
    width: 5,
    height: '100%',
  },
  legendTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#464B4E',
    textAlign: 'center',
    flex: 1,
  },
  tabsContainer: {
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    marginTop: 20,
    flexDirection: 'row',
  },
  tabItem: {
    paddingVertical: 15,
    width: 120,
  },
  tabItemActive: {
    borderBottomColor: ThemeData.APP_MAIN_COLOR,
    borderBottomWidth: 2,
  },
  tabItemText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#464B4E',
    opacity: 0.3,
    textAlign: 'center',
  },
  tabItemTextActive: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    opacity: 1,
    fontSize: 15,
  },
  expandToggle: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    justifyContent: 'center',
    paddingHorizontal: 15,
  },
});
