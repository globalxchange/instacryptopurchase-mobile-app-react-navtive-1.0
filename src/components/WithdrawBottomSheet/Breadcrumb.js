import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import FastImage from 'react-native-fast-image';
import {getUriImage} from '../../utils';

const Breadcrumb = ({
  setCurrentStep,
  selectedCountry,
  paymentCurrency,
  paymentType,
  selectedBanker,
  clearForCountry,
  clearForCurrency,
  setSelectedBanker,
}) => {
  const onCountrySelect = () => {
    clearForCountry();
    setCurrentStep('ChooseCountry');
  };

  const onCurrencySelect = () => {
    clearForCurrency();
    setCurrentStep('ChooseFunding');
  };

  const onMethodSelect = () => {
    setSelectedBanker('');
    setCurrentStep('ChooseMethod');
  };

  const onBankerSelect = () => {
    setCurrentStep('ChooseBanker');
  };

  const onTransactSelect = () => {
    setCurrentStep('ShowQuote');
  };

  return (
    <View style={styles.container}>
      <View style={styles.itemContainer}>
        <TouchableOpacity style={styles.item} onPress={onCountrySelect}>
          <FastImage
            style={styles.icon}
            source={
              selectedCountry
                ? {uri: getUriImage(selectedCountry.image)}
                : require('../../assets/default-breadcumb-icon/country.png')
            }
            resizeMode="contain"
          />
          <Text style={styles.name}>
            {selectedCountry ? selectedCountry.name : 'Country'}
          </Text>
        </TouchableOpacity>
      </View>
      <View style={[styles.itemContainer, paymentType || styles.disabled]}>
        <Image
          style={styles.forwardIcon}
          resizeMode="contain"
          source={require('../../assets/forward-icon-grey.png')}
        />
        <TouchableOpacity
          disabled={!(selectedCountry && paymentCurrency)}
          style={styles.item}
          onPress={onMethodSelect}>
          <FastImage
            style={styles.icon}
            source={
              paymentType
                ? {uri: getUriImage(paymentType.image)}
                : require('../../assets/default-breadcumb-icon/method.png')
            }
            resizeMode="contain"
          />
          <Text style={styles.name}>
            {paymentType ? paymentType.name : 'Method'}
          </Text>
        </TouchableOpacity>
      </View>

      <View style={[styles.itemContainer, selectedBanker || styles.disabled]}>
        <TouchableOpacity
          disabled={!(selectedCountry && paymentCurrency && paymentType)}
          style={styles.item}
          onPress={onBankerSelect}>
          <Image
            style={styles.forwardIcon}
            resizeMode="contain"
            source={require('../../assets/forward-icon-grey.png')}
          />
          <FastImage
            style={styles.icon}
            source={
              selectedBanker
                ? {uri: getUriImage(selectedBanker.image)}
                : require('../../assets/default-breadcumb-icon/banker.png')
            }
            resizeMode="contain"
          />
          <Text style={styles.name}>
            {selectedBanker ? selectedBanker.name : 'Banker'}
          </Text>
        </TouchableOpacity>
      </View>
      <View style={[styles.itemContainer, selectedBanker || styles.disabled]}>
        <Image
          style={styles.forwardIcon}
          resizeMode="contain"
          source={require('../../assets/forward-icon-grey.png')}
        />
        <TouchableOpacity
          disabled={
            !(
              selectedCountry &&
              paymentCurrency &&
              paymentType &&
              selectedBanker
            )
          }
          onPress={onTransactSelect}
          style={styles.item}>
          <Image
            style={styles.icon}
            source={require('../../assets/default-breadcumb-icon/transact.png')}
            resizeMode="contain"
          />
          <Text style={styles.name}>Transact</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Breadcrumb;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 15,
  },

  itemContainer: {
    flex: 1,
    // backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    width: '100%',
    paddingHorizontal: 12,
    alignItems: 'center',
    justifyContent: 'center',
  },
  disabled: {
    opacity: 0.35,
  },
  forwardIcon: {
    position: 'absolute',
    left: -6,
    width: 12,
  },
  icon: {
    height: 24,
    width: 24,
    marginBottom: 3,
  },
  name: {
    color: '#788995',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 8,
    width: '100%',
    overflow: 'hidden',
    height: 10,
    textAlign: 'center',
  },
});
