/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useRef, useState, useEffect} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';
import Animated, {
  useCode,
  set,
  Clock,
  interpolate,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../utils/ReanimatedTimingHelper';
import {DepositContext} from '../contexts/DepositContext';
import {formatterHelper, usdValueFormatter} from '../utils';
import WithdrawalContext from '../contexts/WithdrawalContext';
import Axios from 'axios';
import {APP_CODE, GX_API_ENDPOINT} from '../configs';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';

const actions = ['Transactions', 'Deposit', 'Withdraw'];

const WalletView = ({
  changeFragment,
  activeFragment,
  activeWallet,
  isKeyBoardOpen,
}) => {
  const {
    isCustomNumPadOpen,
    walletShowInUsd,
    setWalletShowInUsd,
    walletBalances,
  } = useContext(AppContext);

  const {isGXVaultSelectorExpanded} = useContext(DepositContext);
  const withdrawContext = useContext(WithdrawalContext);

  const [walletBalance, setWalletBalance] = useState('...');
  const [noBalanceLoaded, setNoBalanceLoaded] = useState(false);
  const [withdrawLimit, setWithdrawLimit] = useState();

  const buttonAnimation = useRef(new Animated.Value(1));

  const expandAnimation = useRef(new Animated.Value(1));

  useEffect(() => {
    if (walletBalances) {
      let balance =
        walletBalances[`${activeWallet.coinSymbol.toLowerCase()}_balance`];

      // console.log('balance', balance);

      if (balance === undefined || balance === null) {
        (async () => {
          const profileId = await AsyncStorageHelper.getProfileId();
          Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/coins/get`, {
            app_code: APP_CODE,
            profile_id: profileId,
          })
            .then((resp) => {
              const {data} = resp;

              // console.log('CoinValue', data);

              const asset = data.coins_data.find(
                (item) => item.coinSymbol === activeWallet.coinSymbol,
              );

              // console.log('asset', asset);

              setNoBalanceLoaded(true);
              setWithdrawLimit(
                formatterHelper(
                  asset?.withdrawal_limit || 0,
                  activeWallet?.coinSymbol,
                ),
              );
              setWalletBalance(
                formatterHelper(asset?.coinValue || 0, activeWallet.coinSymbol),
              );
            })
            .catch((error) => {});
        })();
      } else {
        setNoBalanceLoaded(false);
        if (walletShowInUsd) {
          setWalletBalance(
            usdValueFormatter.format(balance * activeWallet.price.USD),
          );
        } else {
          setWalletBalance(
            `${formatterHelper(balance, activeWallet.coinSymbol)}`,
          );
        }
      }
    }
  }, [walletBalances, activeWallet, walletShowInUsd]);

  useEffect(() => {
    // console.log('activeWallet', activeWallet);
    if (activeWallet.coinSymbol === 'USD') {
      setWalletShowInUsd(true);
    } else {
      setWalletShowInUsd(false);
    }
    return () => {};
  }, [activeWallet]);

  useCode(
    () =>
      isKeyBoardOpen || isCustomNumPadOpen
        ? [
            set(
              buttonAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ]
        : [
            set(
              buttonAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ],
    [isKeyBoardOpen, isCustomNumPadOpen],
  );

  useCode(
    () =>
      isGXVaultSelectorExpanded || withdrawContext.isGXVaultSelectorExpanded
        ? [
            set(
              expandAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ]
        : [
            set(
              expandAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ],
    [isGXVaultSelectorExpanded, withdrawContext.isGXVaultSelectorExpanded],
  );

  return (
    <Animated.View
      style={[
        styles.container,
        {
          maxHeight: interpolate(expandAnimation.current, {
            inputRange: [0, 1],
            outputRange: [0, 3000],
          }),
          paddingTop: interpolate(expandAnimation.current, {
            inputRange: [0, 1],
            outputRange: [0, 30],
          }),
        },
        (isGXVaultSelectorExpanded ||
          withdrawContext.isGXVaultSelectorExpanded) && {
          backgroundColor: 'white',
        },
      ]}>
      {activeWallet ? (
        <View
          style={
            (isGXVaultSelectorExpanded ||
              withdrawContext.isGXVaultSelectorExpanded) && {opacity: 0}
          }>
          <View style={styles.nameContainer}>
            <Text style={styles.walletName}>
              {activeWallet.coinName} Wallet
            </Text>
            <View style={styles.switchContainer}>
              {activeWallet.coinSymbol !== 'USD' && (
                <TouchableOpacity
                  style={walletShowInUsd ? styles.switch : styles.switchActive}
                  onPress={() => setWalletShowInUsd(false)}>
                  <Text
                    style={
                      walletShowInUsd
                        ? styles.switchText
                        : styles.switchTextActive
                    }>
                    {activeWallet.coinSymbol}
                  </Text>
                </TouchableOpacity>
              )}
              <TouchableOpacity
                style={walletShowInUsd ? styles.switchActive : styles.switch}
                onPress={() => setWalletShowInUsd(true)}>
                <Text
                  style={
                    walletShowInUsd
                      ? styles.switchTextActive
                      : styles.switchText
                  }>
                  USD
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <Text style={styles.wallerBalance}>{walletBalance}</Text>
          {noBalanceLoaded && !isNaN(withdrawLimit) && (
            <Text
              style={
                styles.withdrawLimit
              }>{`Note: Your Withdrawal Balance is ${withdrawLimit} ${activeWallet.coinSymbol}`}</Text>
          )}
          <Animated.View
            style={[
              styles.actionsContainer,
              {
                height: interpolate(buttonAnimation.current, {
                  inputRange: [0, 1],
                  outputRange: [0, 45],
                }),
                marginTop: interpolate(buttonAnimation.current, {
                  inputRange: [0, 1],
                  outputRange: [0, 20],
                }),
              },
            ]}>
            {actions.map((item, index) => (
              <TouchableOpacity
                key={item}
                style={[
                  styles.actionsButton,
                  activeFragment === item && styles.actionsButtonActive,
                  index === actions.length - 1 && {marginRight: 0},
                ]}
                onPress={() => changeFragment(item)}>
                <Text
                  style={[
                    styles.buttonText,
                    activeFragment === item && styles.buttonTextActive,
                  ]}>
                  {item}
                </Text>
              </TouchableOpacity>
            ))}
          </Animated.View>
        </View>
      ) : null}
    </Animated.View>
  );
};

export default WalletView;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#186AB4',
    paddingHorizontal: 20,
    paddingBottom: 40,
    overflow: 'hidden',
    justifyContent: 'center',
  },
  nameContainer: {justifyContent: 'space-between', flexDirection: 'row'},
  walletName: {color: 'white', fontFamily: 'Montserrat-Bold'},
  switchContainer: {flexDirection: 'row'},
  switch: {
    backgroundColor: '#84b0da',
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
  },
  switchActive: {
    backgroundColor: 'white',
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
  },
  switchText: {
    color: '#186AB4',
    fontSize: 10,
    fontFamily: 'Montserrat',
  },
  switchTextActive: {
    color: '#186AB4',
    fontSize: 10,
    fontFamily: 'Montserrat-Bold',
  },
  wallerBalance: {
    color: 'white',
    fontFamily: 'Montserrat',
    fontSize: 35,
  },
  actionsContainer: {
    flexDirection: 'row',
    overflow: 'hidden',
  },
  actionsButton: {
    flexGrow: 1,
    width: 0,
    backgroundColor: '#186AB4',
    borderWidth: 1,
    borderColor: 'white',
    justifyContent: 'center',
    marginRight: 15,
    borderRadius: 6,
    zIndex: 1,
  },
  actionsButtonActive: {
    backgroundColor: 'white',
  },
  buttonText: {
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    fontSize: 12,
    color: 'white',
  },
  buttonTextActive: {
    color: '#186AB4',
  },
  listContainer: {backgroundColor: 'white'},
  txnItem: {},
  withdrawLimit: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    marginTop: 10,
  },
});
