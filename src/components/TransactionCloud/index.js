/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useEffect, useRef, useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import {GX_API_ENDPOINT, S3_CONFIG} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';
import * as ImagePicker from 'expo-image-picker';
import {v4 as uuidv4} from 'uuid';
import {RNS3} from 'react-native-s3-upload';
import {WModalShowToastView, WToast} from 'react-native-smart-tip';
import LoadingAnimation from '../LoadingAnimation';
import {S3} from 'aws-sdk';
import {FileSystem} from 'react-native-unimodules';
import {decode} from 'base64-arraybuffer';

const TransactionCloud = ({isOpen, setIsOpen, txnId}) => {
  const [documentName, setDocumentName] = useState('');
  const [filesList, setFilesList] = useState();
  const [pickedImage, setPickedImage] = useState('');
  const [isSuccess, setIsSuccess] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const toastRef = useRef();

  useEffect(() => {
    if (!isOpen) {
      setDocumentName('');
      setPickedImage('');
      setIsLoading(false);
      setIsSuccess(false);
    }
  }, [isOpen]);

  useEffect(() => {
    setFilesList();
    if (txnId) {
      Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/get/user/file/uploads`, {
        params: {id: txnId},
      })
        .then(({data}) => {
          // console.log('Data', data);
          if (data.status) {
            setFilesList(data.result || []);
          }
        })
        .catch((error) => {
          console.log('Error on getting transaction uploads', error);
        });
    }
  }, [txnId, isLoading]);

  const clearInputs = () => {
    setPickedImage('');
    setDocumentName('');
  };

  const imagePickerHandler = async () => {
    try {
      const options = {
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: false,
        quality: 0.9,
      };

      let result = await ImagePicker.launchImageLibraryAsync(options);

      if (!result.cancelled) {
        console.log('result', result);

        // const fPath = result.uri;

        setPickedImage(result);

        // setFileUri(fPath);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const imageUploadHandler = async () => {
    const inputText = documentName.trim();

    if (!inputText) {
      return toastRef.current({
        data: 'Please Input A Document Name',
        position: WToast.position.TOP,
      });
    }

    if (pickedImage) {
      try {
        setIsLoading(true);
        const fileObj = {
          uri: pickedImage.uri,
          name: `${uuidv4()}.jpg`,
          type: 'image/jpeg',
        };

        console.log('File', fileObj);

        const fPath = pickedImage.uri;

        const S3Client = new S3({
          ...S3_CONFIG,
          Bucket: 'gxnitrousdata',
        });
        FileSystem.readAsStringAsync(fPath, {
          encoding: FileSystem.EncodingType.Base64,
        }).then((fileBlob) => {
          const arrayBuffer = decode(fileBlob);

          const params = {
            Bucket: 'gxnitrousdata',
            Key: `deposits/${uuidv4()}.jpg`,
            Body: arrayBuffer,
            ContentType: 'image/jpeg',
            ACL: 'public-read',
          };
          S3Client.upload(params, async (err, s3Data) => {
            if (err) {
              throw new Error('Failed to upload image to S3');
            }
            if (s3Data.Location) {
              const postData = {
                file_key: s3Data.Location,
                id: txnId,
                input: inputText,
              };

              Axios.post(
                `${GX_API_ENDPOINT}/coin/fiat/update_requests`,
                postData,
              )
                .then(({data}) => {
                  // console.log('update resp', data);

                  if (data.status) {
                    setIsSuccess(true);
                    clearInputs();
                  } else {
                    toastRef.current({
                      data: data.message || 'Unable to upload the document',
                      position: WToast.position.TOP,
                    });
                  }
                })
                .finally(() => setIsLoading(false));
            }
          });
        });
      } catch (error) {
        console.log('Error on uploading file', error);
        setIsLoading(false);
      }
    }
  };

  return (
    <BottomSheetLayout
      isOpen={isOpen}
      onClose={() => setIsOpen(false)}
      reactToKeyboard>
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Image
            style={styles.headerIcon}
            resizeMode="contain"
            source={require('../../assets/sercer-icon.png')}
          />
          <Text style={styles.header}>
            {isSuccess ? 'Upload Success' : 'Transaction Cloud'}
          </Text>
        </View>
        <WModalShowToastView
          toastInstance={(toastInstance) => (toastRef.current = toastInstance)}
        />
        {isLoading ? (
          <View style={{paddingVertical: 100}}>
            <LoadingAnimation />
          </View>
        ) : (
          <View>
            {isSuccess ? (
              <Text style={styles.desc}>
                Congratulations Your File Was Uploaded To This Transaction
                Cloud.{' '}
                <Text style={styles.closeText} onPress={() => setIsOpen(false)}>
                  Click Here
                </Text>{' '}
                To Return To Transaction Feed
              </Text>
            ) : (
              <Text style={styles.desc}>
                Welcome To The Transaction Cloud. Here You Cad Upload All
                Relevant Documents Relating To This Ongoing Transaction. The
                Document Requirements Are Outlined In The Payment Path Steps.
                Additionally You May Be Requested To Provide Further
                Documentation By The Support Team.
              </Text>
            )}
            <View style={styles.documentContainer}>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                <TouchableWithoutFeedback>
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity
                      onPress={imagePickerHandler}
                      style={[
                        styles.documentItem,
                        pickedImage
                          ? {}
                          : {
                              justifyContent: 'center',
                              alignItems: 'center',
                            },
                      ]}>
                      {pickedImage ? (
                        <Image
                          source={{uri: pickedImage.uri}}
                          resizeMode="cover"
                          style={styles.filePreview}
                        />
                      ) : (
                        <Image
                          source={require('../../assets/add-icon-gray.png')}
                          resizeMode="contain"
                          style={styles.addIcon}
                        />
                      )}
                    </TouchableOpacity>
                    {filesList?.map((item) => (
                      <View style={[styles.documentItem]}>
                        <Image
                          source={{uri: item.file_key}}
                          resizeMode="cover"
                          style={styles.filePreview}
                        />
                      </View>
                    ))}
                  </View>
                </TouchableWithoutFeedback>
              </ScrollView>
            </View>
            <View style={styles.inputControls}>
              {pickedImage ? (
                <View style={styles.inputContainer}>
                  <TouchableOpacity
                    onPress={clearInputs}
                    style={styles.inputAction}>
                    <Image
                      style={styles.inputActionIcon}
                      resizeMode="contain"
                      source={require('../../assets/repeat-icon.png')}
                    />
                  </TouchableOpacity>
                  <TextInput
                    style={styles.input}
                    placeholder="Name The Document"
                    placeholderTextColor="#ACB1B6"
                    value={documentName}
                    onChangeText={(text) => setDocumentName(text)}
                  />
                  <TouchableOpacity
                    onPress={imageUploadHandler}
                    style={[
                      styles.inputAction,
                      {
                        backgroundColor: ThemeData.APP_MAIN_COLOR,
                        borderRightWidth: 0,
                      },
                    ]}>
                    <Image
                      style={styles.inputActionIcon}
                      resizeMode="contain"
                      source={require('../../assets/document-send-icon.png')}
                    />
                  </TouchableOpacity>
                </View>
              ) : null}
            </View>
          </View>
        )}
      </View>
    </BottomSheetLayout>
  );
};

export default TransactionCloud;

const styles = StyleSheet.create({
  container: {
    paddingTop: 30,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerIcon: {
    width: 40,
    height: 40,
  },
  header: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 18,
    color: ThemeData.APP_MAIN_COLOR,
  },
  desc: {
    paddingHorizontal: 30,
    marginTop: 30,
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#464B4E',
    fontSize: 12,
    lineHeight: 18,
  },
  documentContainer: {
    paddingHorizontal: 30,
    marginTop: 30,
  },
  documentItem: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    width: 90,
    height: 90,
    marginRight: 10,
  },
  addIcon: {
    width: 40,
    height: 40,
  },
  filePreview: {
    flex: 1,
    height: null,
    width: null,
  },
  inputControls: {
    height: 70,
    marginTop: 30,
  },
  inputContainer: {
    flexDirection: 'row',
    height: '100%',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  inputAction: {
    height: '100%',
    width: 70,
    justifyContent: 'center',
    alignItems: 'center',
    borderRightWidth: 1,
    borderRightColor: ThemeData.BORDER_COLOR,
  },
  inputActionIcon: {
    width: 32,
    height: 32,
  },
  input: {
    flex: 1,
    paddingHorizontal: 20,
    height: '100%',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 13,
    color: 'black',
  },
  closeText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    textDecorationLine: 'underline',
  },
});
