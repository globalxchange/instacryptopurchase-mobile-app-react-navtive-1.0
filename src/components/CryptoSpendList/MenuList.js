import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import CardTransactionsList from './CardTransactionsList';
import ProcessingTransactionsList from './ProcessingTransactionsList';

const MenuList = ({
  isMenuOpened,
  setIsAddFundOpen,
  setIsLoadSpendBalance,
  onClose,
  isListExpanded,
  setIsListExpanded,
  activeVault,
  setIsSearchOpen,
  isSearchOpen,
  openMakeSpend,
  setIsLoadPendingBalance,
  setIsLoadCardBalance,
  processingList,
  isLoadPendingBalance,
}) => {
  const [isCardsTxnsOpen, setIsCardsTxnsOpen] = useState(false);

  useEffect(() => {
    if (!isMenuOpened) {
      setIsCardsTxnsOpen(false);
    }
  }, [isMenuOpened]);

  const MENU_ITEM = [
    {
      title: 'Fund Spending Vault',
      icon: require('../../assets/default-breadcumb-icon/currency.png'),
      onClick: () => setIsAddFundOpen(true),
    },
    {
      title: 'Fund Card Balance',
      icon: require('../../assets/default-breadcumb-icon/currency.png'),
      onClick: openMakeSpend,
    },
    {
      title: 'Card Balance & Transactions',
      icon: require('../../assets/default-breadcumb-icon/currency.png'),
      onClick: () => {
        setIsCardsTxnsOpen(true);
        setIsLoadCardBalance(true);
      },
    },
    {
      title: 'Processing Balance & Transactions',
      icon: require('../../assets/default-breadcumb-icon/currency.png'),
      onClick: () => setIsLoadPendingBalance(true),
    },
  ];

  if (isCardsTxnsOpen) {
    return (
      <CardTransactionsList
        isListExpanded={isListExpanded}
        setIsListExpanded={setIsListExpanded}
        activeVault={activeVault}
        isMenuOpened={isMenuOpened}
        setIsSearchOpen={setIsSearchOpen}
        isSearchOpen={isSearchOpen}
      />
    );
  }

  if (isLoadPendingBalance) {
    return (
      <ProcessingTransactionsList
        isListExpanded={isListExpanded}
        setIsListExpanded={setIsListExpanded}
        activeVault={activeVault}
        isMenuOpened={isMenuOpened}
        setIsSearchOpen={setIsSearchOpen}
        isSearchOpen={isSearchOpen}
        transactionsList={processingList}
      />
    );
  }

  if (!isMenuOpened) {
    return null;
  }

  return (
    <View style={styles.container}>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={MENU_ITEM}
        keyExtractor={(item, index) => `${index}`}
        renderItem={({item}) => (
          <TouchableOpacity onPress={item.onClick} style={styles.itemContainer}>
            <Image
              style={styles.itemIcon}
              resizeMode="contain"
              source={item.icon}
            />
            <Text style={styles.itemTitle}>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default MenuList;

const styles = StyleSheet.create({
  container: {
    marginTop: -20,
    flex: 1,
  },
  itemContainer: {
    flexDirection: 'row',
    paddingHorizontal: 30,
    alignItems: 'center',
    paddingVertical: 20,
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
  },
  itemTitle: {
    flex: 1,
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    marginLeft: 15,
  },
  itemIcon: {
    width: 30,
    height: 30,
  },
});
