import React, {useState, useEffect, useContext} from 'react';
import {StyleSheet, Text, View, FlatList, RefreshControl} from 'react-native';
import LoadingAnimation from './LoadingAnimation';
import Axios from 'axios';
import {AppContext} from '../contexts/AppContextProvider';
import FastImage from 'react-native-fast-image';
import {getUriImage} from '../utils';
import SkeletonItemLoading from './SkeletonItemLoading';

const BankersList = ({setSearchCallback, setSearchList}) => {
  const {homeSearchInput, filterActiveCountry, pathData} = useContext(
    AppContext,
  );

  const [exchangesList, setExchangesList] = useState();
  const [unfilteredList, setUnfilteredList] = useState();
  const [filteredList, setFilteredList] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [searchCountryText, setSearchCountryText] = useState('');

  useEffect(() => {
    if (filterActiveCountry) {
      if (filterActiveCountry.value === 'Worldwide') {
        setSearchCountryText('Globally');
      } else {
        setSearchCountryText(`In ${filterActiveCountry.value}`);
      }
    }
  }, [filterActiveCountry]);

  useEffect(() => {
    if (unfilteredList) {
      const list = unfilteredList.filter((item) =>
        item.bankerTag.toLowerCase().includes(homeSearchInput.toLowerCase()),
      );

      setFilteredList(list);
    }
  }, [unfilteredList, homeSearchInput, pathData]);

  useEffect(() => {
    setSearchCallback((item) => {});
    setSearchList(unfilteredList);
    return () => {
      setSearchList();
      setSearchCallback(() => {});
    };
  }, [unfilteredList]);

  useEffect(() => {
    getBankers();
  }, []);

  useEffect(() => {
    if (exchangesList && filterActiveCountry) {
      if (filterActiveCountry.value !== 'Worldwide') {
        if (pathData && exchangesList) {
          const newList = [];

          exchangesList.forEach((item) => {
            pathData.forEach((pathItem) => {
              if (
                pathItem.banker === item.bankerTag &&
                filterActiveCountry.value === pathItem.country
              ) {
                if (!newList.includes(item)) {
                  return newList.push({
                    ...item,
                    name: item.displayName || item.bankerTag,
                    icon: item.profilePicURL,
                  });
                }
              }
            });
          });
          setUnfilteredList(newList);
        }
      } else {
        setUnfilteredList(exchangesList);
      }
    }
  }, [filterActiveCountry, exchangesList, pathData]);

  const getBankers = () => {
    setIsLoading(true);
    Axios.get('https://teller2.apimachine.com/admin/allBankers')
      .then((resp) => {
        const {data} = resp;

        // console.log('Bankers Data', data);

        const bankers = data.status ? data.data : [];

        const parsedBankers = [];

        bankers.forEach((item) => {
          let methodsList = [];
          pathData.forEach((pathItem) => {
            if (
              pathItem.banker === item.bankerTag &&
              !methodsList.includes(
                pathItem.depositMethod || pathItem.paymentMethod,
              )
            ) {
              methodsList.push(
                pathItem.depositMethod || pathItem.paymentMethod,
              );
            }
          });
          parsedBankers.push({
            ...item,
            noOfMethods: methodsList.length,
            name: item.displayName || item.bankerTag,
            icon: item.profilePicURL,
          });
        });

        setFilteredList(parsedBankers);
        setUnfilteredList(parsedBankers);
        setExchangesList(parsedBankers);
      })
      .catch((error) => {
        console.log('Error getting data on Bankerslist', error);
      })
      .finally(() => setIsLoading(false));
  };

  return (
    <View style={styles.container}>
      {filteredList ? (
        <FlatList
          style={styles.scrollView}
          data={filteredList}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item._id}
          refreshControl={
            <RefreshControl refreshing={isLoading} onRefresh={getBankers} />
          }
          renderItem={({item}) => (
            <View style={styles.item}>
              <FastImage
                style={styles.cryptoIcon}
                resizeMode="contain"
                source={{
                  uri: getUriImage(item.profilePicURL),
                }}
              />
              <View style={styles.nameContainer}>
                <Text numberOfLines={1} style={styles.cryptoName}>
                  {item.displayName || item.bankerTag}
                </Text>
              </View>
              <View style={styles.priceContainer}>
                {/* Value */}
                <Text style={styles.cryptoPrice}>
                  {item.noOfMethods} Payment Methods
                </Text>
              </View>
            </View>
          )}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>
                There Are No Bankers Who Can Conduct Transactions In{' '}
                {searchCountryText}
              </Text>
            </View>
          }
        />
      ) : (
        <SkeletonItemLoading />
      )}
    </View>
  );
};

export default BankersList;

const styles = StyleSheet.create({
  container: {backgroundColor: '#f7f7f7', flex: 1},
  scrollView: {flex: 1, paddingHorizontal: 10, paddingTop: 10},
  item: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
  },
  cryptoIcon: {
    width: 25,
    height: 25,
    borderRadius: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  nameContainer: {
    flexGrow: 1,
    width: 0,
    marginLeft: 10,
    paddingRight: 20,
  },
  cryptoName: {
    color: '#001D41',
    fontSize: 16,
    marginBottom: 2,
    fontFamily: 'Montserrat-Bold',
  },
  noOfVendors: {
    color: '#001D41',
    opacity: 0.5,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  cryptoPrice: {
    color: '#001D41',
    opacity: 0.5,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  loadingText: {marginTop: 10, fontFamily: 'Montserrat'},
  emptyContainer: {
    marginTop: 20,
  },
  emptyText: {
    fontFamily: 'Montserrat',
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 16,
    paddingHorizontal: 30,
  },
});
