/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  Dimensions,
} from 'react-native';
import {AppContext} from '../../contexts/AppContextProvider';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import Constants from 'expo-constants';
import {Permissions} from 'react-native-unimodules';
import {getUriImage, normalize, usdValueFormatter} from '../../utils';
import FastImage from 'react-native-fast-image';
import {DrawerActions, useNavigation} from '@react-navigation/native';
import BrokerAppInstaller from '../BrokerAppInstaller';
import PhotoPickerDialog from '../PhotoPickerDialog';
import axios from 'axios';
import {GX_API_ENDPOINT, S3_CONFIG} from '../../configs';
import * as ImagePicker from 'expo-image-picker';
import {FileSystem} from 'react-native-unimodules';
import {decode} from 'base64-arraybuffer';
import {S3} from 'aws-sdk';

const {width, height, scale, fontScale} = Dimensions.get('window');

const DrawerCover = ({isLoading, setIsLoading}) => {
  const {
    isLoggedIn,
    avatar,
    walletBalances,
    cryptoTableData,
    updateWalletBalances,
    profileId,
    setLoginData,
    userName,
  } = useContext(AppContext);

  const [vaultBalance, setVaultBalance] = useState(0);
  const [isInstallerOpen, setIsInstallerOpen] = useState(false);
  const [userEmail, setUserEmail] = useState('');
  const [showAlert, setShowAlert] = useState(false);

  const {dispatch} = useNavigation();

  useEffect(() => {
    (async () => {
      if (Constants.platform.ios) {
        const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
          // alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  useEffect(() => {
    if (isLoggedIn && !walletBalances && profileId) {
      updateWalletBalances();
    }
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();
      setUserEmail(email);
    })();
  }, [isLoggedIn, profileId]);

  useEffect(() => {
    // console.log('Wallet Balance', walletBalances);
    // console.log('cryptoTableData', cryptoTableData);

    if (walletBalances && cryptoTableData) {
      let totalBalance = 0;

      cryptoTableData.forEach((item) => {
        const coinBalance =
          walletBalances[`${item.coinSymbol.toLowerCase()}_balance`] *
            item.price.USD || 0;

        totalBalance += coinBalance;
      });
      setVaultBalance(totalBalance);
    }
  }, [walletBalances, cryptoTableData]);

  const inviteClickHandler = async () => {
    // Clipboard.setString(`https://globalxchange.com/${userName}`);

    // setIsCopied(true);

    // setTimeout(() => setIsCopied(false), 30000);
    dispatch(DrawerActions.closeDrawer());

    setIsInstallerOpen(true);
  };

  const changeProfilePic = async (useCamera) => {
    try {
      let result;
      const options = {
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 0.8,
      };

      if (useCamera) {
        result = await ImagePicker.launchCameraAsync(options);
      } else {
        result = await ImagePicker.launchImageLibraryAsync(options);
      }

      // console.log(result);

      setShowAlert(false);

      if (!result.cancelled) {
        setIsLoading(true);

        const BUCKET_NAME = 'gxnitrousdata';

        const S3Client = new S3({
          accessKeyId: S3_CONFIG.accessKey,
          secretAccessKey: S3_CONFIG.secretKey,
          Bucket: BUCKET_NAME,
        });

        const fPath = result.uri;

        let options = {encoding: FileSystem.EncodingType.Base64};
        FileSystem.readAsStringAsync(fPath, options)
          .then((data) => {
            const arrayBuffer = decode(data);

            const params = {
              Bucket: BUCKET_NAME,
              Key: `brandlogos/${userName}${Date.now()}.jpg`,
              Body: arrayBuffer,
              ContentType: 'image/jpeg',
              ACL: 'public-read',
            };

            S3Client.upload(params, async (err, s3Data) => {
              if (err) {
                console.log('Uploading Profile Pic Error', err);
              }

              if (s3Data.Location) {
                const email = await AsyncStorageHelper.getLoginEmail();
                const token = await AsyncStorageHelper.getAppToken();
                axios
                  .post(`${GX_API_ENDPOINT}/user/details/edit`, {
                    email,
                    field: 'profile_img',
                    value: s3Data.Location,
                    accessToken: token,
                  })
                  .then((resp) => {
                    // console.log('Profile Update Success', resp.data);
                    getUserDetails(email);
                  })
                  .catch((error) => {
                    console.log('Profile Update Failed', error);
                  })
                  .finally(() => setIsLoading(false));
              }
              // console.log('Upload Success', s3Data);
            });
          })
          .catch((err) => {
            console.log('​getFile -> err', err);
            setIsLoading(false);
          });
      }
    } catch (E) {
      console.log(E);
      setIsLoading(false);
    }
  };

  const getUserDetails = (email) => {
    axios
      .get(`${GX_API_ENDPOINT}/user/details/get`, {
        params: {email},
      })
      .then((res) => {
        const {data} = res;

        AsyncStorageHelper.setUserName(data.user.username);
        AsyncStorageHelper.setUserAvatarUrl(data.user.profile_img);
        AsyncStorageHelper.setAffId(data.user.affiliate_id);
        setLoginData(data.user.username, true, data.user.profile_img);
      })
      .catch((error) => {
        console.log('getUserDetails Error', error);
      });
  };

  return (
    <View style={styles.container}>
      <View style={[styles.profileContainer]}>
        <TouchableOpacity
          style={[styles.avatarContainer, isLoggedIn && styles.avatarBorder]}
          disabled={!isLoggedIn}
          onPress={() => setShowAlert(true)}>
          {isLoggedIn && avatar ? (
            <FastImage
              style={[styles.avatar]}
              source={{uri: getUriImage(avatar)}}
              resizeMode={'cover'}
            />
          ) : (
            <Image
              style={styles.avatar}
              source={require('../../assets/app-logo.png')}
              resizeMode={'contain'}
            />
          )}
          {isLoading && (
            <View style={styles.loadingContainer}>
              <ActivityIndicator size="large" color="#186AB4" />
            </View>
          )}
        </TouchableOpacity>
        <View style={[styles.nameContainer]}>
          <Text numberOfLines={1} style={[styles.userName]}>
            {isLoggedIn ? userName : 'InstaCrypto'}
          </Text>
          {isLoggedIn ? (
            <TouchableOpacity onPress={inviteClickHandler}>
              <Text style={styles.copyLink}>{'Invite Friends'}</Text>
            </TouchableOpacity>
          ) : (
            <Text style={styles.walletBalance}>Making Crypto Simple</Text>
          )}
        </View>
      </View>
      <BrokerAppInstaller
        isOpen={isInstallerOpen}
        setIsOpen={setIsInstallerOpen}
      />
      <PhotoPickerDialog
        isOpen={showAlert}
        setIsOpen={setShowAlert}
        callBack={(isCamera) => changeProfilePic(isCamera)}
      />
    </View>
  );
};

export default DrawerCover;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    justifyContent: 'center',
  },
  loadingContainer: {
    ...StyleSheet.absoluteFill,
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileContainer: {
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingBottom: 20,
    paddingTop: 10,
    alignItems: 'center',
  },
  avatarContainer: {
    overflow: 'hidden',
    height: 80,
    width: 80,
    borderRadius: 40,
  },
  avatarBorder: {
    borderWidth: 2,
    borderColor: '#186AB4',
  },
  avatar: {
    flex: 1,
    width: null,
    height: null,
  },
  nameContainer: {alignItems: 'center', marginTop: 10},
  userName: {
    fontSize: 22,
    color: '#186AB4',
    fontFamily: 'Montserrat-Bold',
    textTransform: 'capitalize',
  },
  walletBalance: {
    color: '#186AB4',
    fontSize: 10,
    fontFamily: 'Montserrat',
  },
  copyLink: {
    color: '#186AB4',
    fontSize: 14,
    fontFamily: 'Montserrat',
  },
  backButton: {
    width: 30,
    height: 30,
    padding: 6,
  },
  backButtonIcon: {
    flex: 1,
    height: null,
    width: null,
  },
});
