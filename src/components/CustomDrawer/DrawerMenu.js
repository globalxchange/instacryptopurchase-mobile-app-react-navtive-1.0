import React, {useContext} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {AppContext} from '../../contexts/AppContextProvider';

const DrawerMenu = ({setIsLoading}) => {
  const {activeRoute, isLoggedIn} = useContext(AppContext);

  const navigation = useNavigation();

  return (
    <View style={styles.navContainer}>
      {MENU_ITEMS.map((menu) => (
        <View key={menu.title} style={styles.navLink}>
          <TouchableOpacity
            style={styles.buttonOverlay}
            onPress={() =>
              menu.loginRequired
                ? isLoggedIn
                  ? navigation.navigate(menu.route)
                  : navigation.navigate('Landing')
                : navigation.navigate(menu.route)
            }>
            <Text
              style={[
                styles.navText,
                (activeRoute === menu.title || activeRoute === menu.route) &&
                  styles.navActive,
              ]}>
              {menu.title}
            </Text>
          </TouchableOpacity>
        </View>
      ))}
    </View>
  );
};

export default DrawerMenu;

const styles = StyleSheet.create({
  navContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
    paddingVertical: 25,
  },
  navLink: {
    height: 70,
    alignItems: 'center',
  },
  buttonOverlay: {
    flexDirection: 'row',
    flex: 1,
    paddingLeft: 30,
    paddingRight: 30,
    width: '100%',
  },
  navText: {
    flex: 1,
    marginTop: 'auto',
    marginBottom: 'auto',
    color: '#186AB4',
    fontSize: 13,
    fontFamily: 'Montserrat',
    textAlign: 'center',
  },
  navActive: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 17,
  },
});

const MENU_ITEMS = [
  {
    title: 'Feed',
    route: 'Home',
  },
  {
    title: 'Vaults',
    route: 'Wallet',
    loginRequired: true,
  },
  {
    title: 'Classrooms',
    route: '',
    // loginRequired: true,
  },
];
