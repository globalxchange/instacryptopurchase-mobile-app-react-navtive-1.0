/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  Dimensions,
  Platform,
} from 'react-native';
import DrawerCover from './DrawerCover';
import {
  useNavigation,
  DrawerActions,
  CommonActions,
} from '@react-navigation/native';
import {AppContext} from '../../contexts/AppContextProvider';
import DrawerMenu from './DrawerMenu';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {WToast} from 'react-native-smart-tip';
import FastImage from 'react-native-fast-image';
import {getUriImage} from '../../utils';
import ThemeData from '../../configs/ThemeData';

const {width, height, scale, fontScale} = Dimensions.get('window');

const CustomDrawer = () => {
  const navigation = useNavigation();
  const {bottom} = useSafeAreaInsets();

  const {
    isLoggedIn,
    removeLoginData,
    isAdminLoggedIn,
    userName,
    forceRefreshApiData,
    avatar,
  } = useContext(AppContext);

  const [isProfileMenuOpen, setIsProfileMenuOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [emailId, setEmailId] = useState('');

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();
      setEmailId(email);
    })();

    return () => {};
  }, [isLoggedIn]);

  const logoutHandler = () => {
    try {
      navigation.dispatch(DrawerActions.closeDrawer());
      navigation.navigate('Home');
      AsyncStorageHelper.deleteAllUserInfo();
      removeLoginData();
    } catch (error) {
      console.log('Error on logout', error);
    }
  };

  const loginHandler = () => {
    navigation.dispatch(DrawerActions.closeDrawer());
    navigation.navigate('Landing');
  };

  const removeAdminLogin = async () => {
    try {
      const success = await AsyncStorageHelper.removeAdminView();

      if (success) {
        forceRefreshApiData();
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{name: 'Drawer'}],
          }),
        );
      } else {
        WToast.show({
          data: 'Error on removing Admin Login',
          position: WToast.position.TOP,
        });
      }
    } catch (error) {
      WToast.show({
        data: 'Error on removing Admin Login',
        position: WToast.position.TOP,
      });
    }
  };

  return (
    <View style={[styles.container]}>
      {isProfileMenuOpen || (
        <TouchableOpacity
          onPress={() => navigation.dispatch(DrawerActions.closeDrawer())}>
          <View style={[styles.closeDrawerBtn]}>
            <Image
              style={styles.closeIcon}
              source={require('../../assets/cancel-icon-colored.png')}
              resizeMode="contain"
            />
          </View>
        </TouchableOpacity>
      )}
      <View style={[styles.viewContainer]}>
        <View style={styles.coverContainer}>
          <DrawerCover isLoading={isLoading} setIsLoading={setIsLoading} />
        </View>
        <DrawerMenu setIsLoading={setIsLoading} />
        {isAdminLoggedIn ? (
          <View style={[styles.adminView]}>
            <View style={styles.userContainer}>
              {avatar ? (
                <FastImage
                  style={styles.avatar}
                  source={{uri: getUriImage(avatar)}}
                  resizeMode={'cover'}
                />
              ) : (
                <Image
                  style={[
                    styles.avatar,
                    {width: 30, height: 30, borderRadius: 0},
                  ]}
                  source={require('../../assets/app-logo.png')}
                  resizeMode="contain"
                />
              )}
              <View style={styles.nameContainer}>
                <Text numberOfLines={1} style={styles.userName}>
                  {userName}
                </Text>
                <Text numberOfLines={1} style={styles.userMail}>
                  {emailId}
                </Text>
              </View>
            </View>
            <View style={styles.actionContainer}>
              <TouchableOpacity
                onPress={() => navigation.navigate('AdminPage')}
                style={[styles.buttonFilled, {marginRight: 15}]}>
                <Text style={styles.buttonFilledText}>Change User</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => removeAdminLogin()}
                style={styles.outlinedButton}>
                <Text style={styles.outlinedButtonText}>Leave</Text>
              </TouchableOpacity>
            </View>
          </View>
        ) : (
          !isProfileMenuOpen && (
            <TouchableOpacity
              style={[styles.loginButton, isLoggedIn && styles.logoutButton]}
              onPress={isLoggedIn ? logoutHandler : loginHandler}>
              <Text
                style={[
                  styles.buttonText,
                  isLoggedIn && styles.logoutButtonText,
                ]}>
                {isLoggedIn ? 'Logout' : 'Login'}
              </Text>
            </TouchableOpacity>
          )
        )}
        <View
          style={{
            height:
              bottom +
              (isProfileMenuOpen ? (Platform.OS === 'ios' ? 10 : 0) : 50),
          }}
        />
      </View>
    </View>
  );
};

export default CustomDrawer;

const styles = StyleSheet.create({
  container: {
    height: height - 30,
    width: width,
    paddingTop: 20,
  },
  viewContainer: {
    flex: 1,
    paddingVertical: 45 / scale,
  },
  loginButton: {
    borderColor: '#186AB4',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    paddingVertical: 10,
    marginVertical: 20,
    width: 150,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  buttonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#186AB4',
  },
  logoutButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
  },
  logoutButtonText: {
    color: 'white',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    marginTop: 10,
  },
  closeDrawerBtn: {
    marginLeft: 30,
  },
  closeIcon: {
    width: 22,
    height: 22,
  },
  coverContainer: {
    marginTop: 40,
  },
  actionIcon: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  adminView: {
    paddingHorizontal: 30,
  },
  userContainer: {
    flexDirection: 'row',
    marginBottom: 20,
    alignItems: 'center',
  },
  avatar: {
    width: 45,
    height: 45,
    borderRadius: 22.5,
  },
  nameContainer: {
    flex: 1,
    marginLeft: 10,
  },
  userName: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 18,
    textTransform: 'capitalize',
  },
  userMail: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    fontSize: 10,
  },
  actionContainer: {
    flexDirection: 'row',
  },
  buttonFilled: {
    backgroundColor: '#08152D',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    height: 40,
  },
  buttonFilledText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    textAlign: 'center',
    fontSize: 12,
  },
  outlinedButton: {
    borderColor: '#08152D',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    height: 40,
  },
  outlinedButtonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 12,
  },
});
