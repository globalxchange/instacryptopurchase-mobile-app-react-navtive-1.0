import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionButton from '../ActionButton';
import {AppContext} from '../../../contexts/AppContextProvider';
import LoadingAnimation from '../../LoadingAnimation';
import CustomDropDown from '../../CustomDropDown';
import {WToast} from 'react-native-smart-tip';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import SkeletonCheckoutLoading from '../../BuyBottomSheet/SkeletonCheckoutLoading';

const FundingMethodChooser = ({
  onNext,
  activeCrypto,
  selectedCountry,
  paymentType,
  paymentCurrency,
  setPaymentType,
}) => {
  const {totalPaymentMethods} = useContext(AppContext);

  const [availableMethods, setAvailableMethods] = useState();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (
      totalPaymentMethods &&
      activeCrypto &&
      selectedCountry &&
      paymentCurrency
    ) {
      setIsLoading(true);
      const filterMethodsList = [];

      Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/stats/get`, {
        params: {
          from_currency: activeCrypto.coinSymbol,
          country: selectedCountry.formData
            ? selectedCountry.formData.Name
            : selectedCountry.value,
          to_currency: paymentCurrency.coinSymbol,
          select_type: 'instant',
        },
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('data', resp);

          if (data.status) {
            const result = data.pathData;

            totalPaymentMethods.forEach((methodItem) => {
              result.paymentMethod.forEach((pathItem) => {
                if (pathItem._id === methodItem.code) {
                  if (!filterMethodsList.includes(methodItem)) {
                    filterMethodsList.push({
                      ...methodItem,
                      image: methodItem.icon,
                    });
                    return;
                  }
                }
              });
            });
          } else {
            WToast.show({
              data: 'Error On Querying Path Data',
              position: WToast.position.TOP,
            });
          }

          setAvailableMethods(filterMethodsList);
          setIsLoading(false);
        })
        .catch((error) => {
          WToast.show({
            data: 'Error On Querying Path Data',
            position: WToast.position.TOP,
          });
          console.log('Error On Querying Path Data', error);
        });
    }
  }, [totalPaymentMethods, activeCrypto, selectedCountry, paymentCurrency]);

  const onNextClick = () => {
    if (!paymentType) {
      return WToast.show({
        data: 'Please Select A Payment Type',
        position: WToast.position.TOP,
      });
    }
    onNext();
  };

  if (!availableMethods || isLoading) {
    return <SkeletonCheckoutLoading />;
  }

  if (availableMethods.length <= 0) {
    return (
      <View style={styles.loadingContainer}>
        <Text style={styles.notAvailableText}>
          No Payment Method Supported to Buy {activeCrypto.coinName} in{' '}
          {selectedCountry.value} with as Payment Currency
        </Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Payment Methods</Text>
      <Text style={styles.subHeader}>
        We Have Curated All The Applicable Payment Methods That You Can Use In
        {selectedCountry.value} To Sell {activeCrypto.coinName} For{' '}
        {paymentCurrency.coinName}
      </Text>
      <View style={styles.controlContainer}>
        <CustomDropDown
          placeholderIcon={require('../../../assets/default-breadcumb-icon/method.png')}
          label="Select Payment Method"
          placeHolder="Available Payment Methods"
          items={availableMethods}
          selectedItem={paymentType}
          onDropDownSelect={setPaymentType}
          numberLabel="Methods"
          onNext={onNext}
          headerIcon={require('../../../assets/insta-sell.png')}
        />
      </View>
      <ActionButton text="Proceed To Bankers" onPress={onNextClick} />
    </View>
  );
};

export default FundingMethodChooser;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  header: {
    color: '#186AB4',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 26,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  controlContainer: {
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  loadingContainer: {
    justifyContent: 'center',
    flex: 1,
  },
  notAvailableText: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
  },
});
