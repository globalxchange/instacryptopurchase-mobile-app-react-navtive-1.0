import React, {useState, useEffect, useContext} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Axios from 'axios';
import {TELLER_API_ENDPOINT, GX_API_ENDPOINT} from '../../../configs';
import LoadingAnimation from '../../LoadingAnimation';
import {AppContext} from '../../../contexts/AppContextProvider';
import CustomDropDown from '../../CustomDropDown';
import ActionButton from '../ActionButton';
import {WToast} from 'react-native-smart-tip';
import SkeletonCheckoutLoading from '../../BuyBottomSheet/SkeletonCheckoutLoading';

const BankerChooser = ({
  onNext,
  activeCrypto,
  toggleExpansion,
  isExpanded,
  setSelectedBanker,
  selectedCountry,
  paymentType,
  paymentCurrency,
  selectedBanker,
}) => {
  const {totalPaymentMethods} = useContext(AppContext);

  const [activeType, setActiveType] = useState('Bankers');
  const [totalBankerList, setTotalBankerList] = useState();
  const [bankersList, setBankersList] = useState();
  const [tellersList, setTellersList] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    Axios.get(`${TELLER_API_ENDPOINT}/matchOrder`, {
      // params: {
      //   paymentMethod: 'tellerTransfer',
      //   preferredCurrency: 'any',
      //   transactionAsset: 'GXT',
      // },
    })
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          setTellersList([]);
        }
      })
      .catch((error) => console.log('error Getting Live Order', error));

    Axios.get('https://teller2.apimachine.com/admin/allBankers').then(
      (resp) => {
        const {data} = resp;
        const bankers = data.status ? data.data : [];
        setTotalBankerList(bankers);
      },
    );
  }, []);

  useEffect(() => {
    if (
      totalPaymentMethods &&
      activeCrypto &&
      selectedCountry &&
      paymentCurrency &&
      totalBankerList
    ) {
      setIsLoading(true);
      const filterBankerList = [];

      Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/stats/get`, {
        params: {
          from_currency: activeCrypto.coinSymbol,
          country: selectedCountry.formData
            ? selectedCountry.formData.Name
            : selectedCountry.value,
          to_currency: paymentCurrency.coinSymbol,
          paymentMethod: paymentType.code,
          select_type: 'instant',
        },
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('data', resp);

          if (data.status) {
            const result = data.pathData;

            totalBankerList.forEach((bankerItem) => {
              result.banker.forEach((pathItem) => {
                if (
                  pathItem._id.toLowerCase() ===
                  bankerItem.bankerTag.toLowerCase()
                ) {
                  if (!filterBankerList.includes(bankerItem)) {
                    filterBankerList.push({
                      ...bankerItem,
                      name: bankerItem.bankerTag,
                      image: bankerItem.profilePicURL,
                    });
                    return;
                  }
                }
              });
            });
          } else {
            WToast.show({
              data: 'Error On Querying Path Data',
              position: WToast.position.TOP,
            });
          }

          setBankersList(filterBankerList);
          setIsLoading(false);
        })
        .catch((error) => {
          WToast.show({
            data: 'Error On Querying Path Data',
            position: WToast.position.TOP,
          });
          console.log('Error On Querying Path Data', error);
        });
    }
  }, [
    totalPaymentMethods,
    activeCrypto,
    selectedCountry,
    paymentCurrency,
    paymentType,
    totalBankerList,
  ]);

  const onNextClick = () => {
    if (!selectedBanker) {
      return WToast.show({
        data: 'Please Select A Banker',
        position: WToast.position.TOP,
      });
    }
    onNext();
  };

  if (isLoading) {
    return <SkeletonCheckoutLoading />;
  }

  if (activeType === 'Bankers' && !bankersList) {
    return <SkeletonCheckoutLoading />;
  }

  if (activeType === 'Tellers' && !tellersList) {
    return <SkeletonCheckoutLoading />;
  }

  if (bankersList.length <= 0) {
    return (
      <View style={styles.loadingContainer}>
        <Text style={styles.notAvailableText}>
          No Banker Supported to Buy {activeCrypto.coinName} in{' '}
          {selectedCountry.value} with {paymentCurrency.coinSymbol} as Payment
          Currency with {paymentType.name} Method
        </Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>{activeType}</Text>
      <Text style={styles.subHeader}>
        We Have Searched The Entire Crypto-Universe And Found These {activeType}{' '}
        Can Meet Your Requirements.
      </Text>
      {/* <TouchableOpacity
        style={styles.expandIconContainer}
        onPress={toggleExpansion}>
        <Image
          style={styles.expandIcon}
          source={
            isExpanded
              ? require('../../../assets/collapse-icon.png')
              : require('../../../assets/expand-icon.png')
          }
          resizeMode="contain"
        />
      </TouchableOpacity> */}
      <View style={styles.controlContainer}>
        <CustomDropDown
          placeholderIcon={require('../../../assets/default-breadcumb-icon/method.png')}
          label={activeType === 'Bankers' ? 'Select Banker' : 'Select Teller'}
          placeHolder={`Available ${activeType}`}
          items={activeType === 'Bankers' ? bankersList : tellersList}
          selectedItem={activeType === 'Bankers' ? selectedBanker : null}
          onDropDownSelect={
            activeType === 'Bankers' ? setSelectedBanker : () => null
          }
          numberLabel={activeType === 'Bankers' ? 'Bankers' : 'Tellers'}
          onNext={onNext}
          headerIcon={require('../../../assets/insta-sell.png')}
          searchPlaceHolder="Search Bankers"
        />
      </View>
      <ActionButton text="Proceed To Quote" onPress={onNextClick} />
    </View>
  );
};

export default BankerChooser;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  header: {
    color: '#186AB4',
    textAlign: 'left',
    // marginBottom: 20,
    fontSize: 26,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  switchContainer: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 10,
    marginTop: 25,
  },
  switchItem: {
    flex: 1,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
  },
  switchText: {
    color: '#186AB4',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  methodList: {},
  menuItem: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
  },
  menuItemImage: {
    width: 100,
  },
  smallMenuIcon: {
    width: 20,
    height: 20,
  },
  methodName: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#001D41',
    marginLeft: 20,
    paddingVertical: 30,
  },
  scrollView: {flex: 1, paddingHorizontal: 10, paddingTop: 10},
  item: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
  },
  cryptoIcon: {
    width: 25,
    height: 25,
    borderRadius: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  nameContainer: {
    flexGrow: 1,
    width: 0,
    marginLeft: 10,
  },
  cryptoName: {
    color: '#001D41',
    fontSize: 16,
    marginBottom: 2,
    fontFamily: 'Montserrat-Bold',
  },
  noOfVendors: {
    color: '#001D41',
    opacity: 0.5,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  priceContainer: {
    alignItems: 'center',
  },
  noOfPaymentMethod: {
    color: '#001D41',
    opacity: 0.5,
    marginBottom: 2,
    textAlign: 'right',
    fontFamily: 'Roboto-Bold',
  },
  cryptoPrice: {
    color: '#001D41',
    marginBottom: 2,
    textAlign: 'right',
    fontFamily: 'Roboto-Bold',
  },
  orderVolume: {
    textAlign: 'right',
    color: '#001D41',
    fontFamily: 'Roboto',
    fontSize: 10,
  },
  loadingText: {marginTop: 10, fontFamily: 'Montserrat'},
  emptyContainer: {
    marginTop: 20,
  },
  emptyText: {
    fontFamily: 'Montserrat-Bold',
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 16,
  },
  expandIconContainer: {
    position: 'absolute',
    top: 0,
    right: 10,
    width: 18,
    height: 18,
  },
  expandIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  loadingContainer: {
    justifyContent: 'center',
    flex: 1,
  },
  notAvailableText: {
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
  },
});
