/* eslint-disable react-native/no-inline-styles */
import React, {useContext} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {AppContext} from '../../contexts/AppContextProvider';

const AssetSelector = ({setSelectedCoin}) => {
  const {cryptoTableData} = useContext(AppContext);

  let ethData;
  let btcData;

  if (cryptoTableData) {
    ethData = cryptoTableData.find((x) => x.coinSymbol === 'ETH');
    btcData = cryptoTableData.find((x) => x.coinSymbol === 'BTC');
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Quote</Text>
      <Text style={styles.subHeader}>Which Asset Do You Want To Buy?</Text>
      <View style={styles.assetList}>
        <TouchableOpacity
          onPress={() => (ethData ? setSelectedCoin(btcData) : null)}
          style={styles.assetItem}>
          <View style={styles.imageContainer}>
            <Image
              style={styles.assetImage}
              resizeMode="contain"
              source={{uri: btcData?.image}}
            />
          </View>
          <Text style={styles.assetName}>{btcData?.name || 'Bitcoin'}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => (ethData ? setSelectedCoin(ethData) : null)}
          style={styles.assetItem}>
          <View style={styles.imageContainer}>
            <Image
              style={styles.assetImage}
              resizeMode="contain"
              source={{uri: ethData?.image}}
            />
          </View>
          <Text style={styles.assetName}>{ethData?.name || 'Ethereum'}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.assetItem}>
          <View style={[styles.imageContainer, {backgroundColor: '#186AB4'}]}>
            <Text style={{fontFamily: 'Montserrat-SemiBold', color: 'white'}}>
              More{'\n'}Coins
            </Text>
          </View>
          <Text style={[styles.assetName, {fontFamily: 'Montserrat'}]}>
            Coming Soon
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default AssetSelector;

const styles = StyleSheet.create({
  container: {
    paddingBottom: 50,
    paddingTop: 30,
  },
  header: {
    color: '#186AB4',
    textAlign: 'left',
    fontSize: 35,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
    marginTop: 20,
  },
  assetList: {
    flexDirection: 'row',
  },
  assetItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    borderColor: '#F1F4F6',
    borderWidth: 1,
    padding: 15,
    borderRadius: 5,
  },
  assetImage: {
    width: 30,
    height: 30,
  },
  assetName: {
    color: '#186AB4',
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    fontSize: 13,
    marginTop: 8,
  },
});
