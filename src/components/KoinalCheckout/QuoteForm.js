import Axios from 'axios';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {WModalShowToastView} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../../configs';
import {AppContext} from '../../contexts/AppContextProvider';
import {formatterHelper} from '../../utils';
import QuoteInput from '../BuyBottomSheet/QuoteInput';
import * as WebBrowser from 'expo-web-browser';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';

const QuoteForm = ({selectedCoin, onClose}) => {
  const {cryptoTableData, walletBalances} = useContext(AppContext);

  const [fiatInput, setFiatInput] = useState('0');
  const [estimatedCryptoValue, setEstimatedCryptoValue] = useState('0');
  const [isLoading, setIsLoading] = useState(false);
  const [errorText, setErrorText] = useState('');
  const [customerReference, setCustomerReference] = useState();
  const [purchaseLoading, setPurchaseLoading] = useState(false);
  const [walletAddress, setWalletAddress] = useState();

  const toastRef = useRef();
  const axiosTokenRef = useRef();

  useEffect(() => {
    getApproxQuote(fiatInput);
    setErrorText('');
  }, [fiatInput]);

  useEffect(() => {
    if (walletBalances?.coinAddress && selectedCoin) {
      const address =
        walletBalances.coinAddress[selectedCoin.coinSymbol]?.address || '';
      // console.log('walletBalances', address);

      setWalletAddress(address);
    }
  }, [walletBalances, selectedCoin]);

  const getApproxQuote = (value) => {
    if (parseFloat(fiatInput) !== undefined && parseFloat(fiatInput) !== null) {
      if (axiosTokenRef.current) {
        axiosTokenRef.current.cancel();
        // setIsLoading(false);
      }

      setIsLoading(true);

      setCustomerReference('');

      axiosTokenRef.current = Axios.CancelToken.source();

      const postData = {
        affiliate_id: '1',
        cryptocurrency: selectedCoin.coinSymbol,
        currency: 'USD',
        currency_amount: parseFloat(value),
      };

      console.log('Quote PostData', postData);

      Axios.post(`${GX_API_ENDPOINT}/coin/koinal/quote`, postData, {
        cancelToken: axiosTokenRef.current.token,
      })
        .then((resp) => {
          const {data} = resp;

          console.log('Quote Resp ', data);

          if (data.status) {
            const cryptoAmount = data.data.cryptocurrency_amount;
            setEstimatedCryptoValue(cryptoAmount);
            setCustomerReference(data.customer_reference);
            setErrorText('');
          } else {
            if (data.message !== 'System Error') {
              setErrorText(data.message);
            }
            setEstimatedCryptoValue(0);
          }
          setIsLoading(false);
        })
        .catch((error) => console.log('Error getting quote', error));
    }
  };

  const buyClickHandler = async () => {
    if (purchaseLoading) {
      return;
    }

    const value = parseFloat(fiatInput);

    if (value < 50 || value > 20000) {
      return setErrorText('Please input a value between 50-20000 USD');
    }
    setPurchaseLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();

    const postData = {
      email,
      // country: selectedCountry,
      gx_user: true,
      coin_address: walletAddress,
      customer_reference: customerReference,
      ip_address: '132.12.12.31',
    };

    console.log('Execute PostData', postData);

    Axios.post(`${GX_API_ENDPOINT}/coin/koinal/order`, postData)
      .then((resp) => {
        const {data} = resp;
        console.log('Execute Resp', data);
        setPurchaseLoading(false);
        if (data.status) {
          // window.open(data.redirect_url, '_blank');
          onClose();
          WebBrowser.openBrowserAsync(data.redirect_url);
        } else {
          setErrorText(data.data);
        }
      })
      .catch((error) => {
        setPurchaseLoading(false);
        console.log('createOrderRequest Error', error);
      });
  };

  let usdData;

  if (cryptoTableData) {
    usdData = cryptoTableData.find((x) => x.coinSymbol === 'USD');
  }

  return (
    <View style={styles.container}>
      <WModalShowToastView
        toastInstance={(toastInstance) => (toastRef.current = toastInstance)}
      />
      <View style={styles.headerContinuer}>
        <Text style={styles.header}>Quote</Text>
        <TouchableOpacity style={[styles.feeButton, {opacity: 0.5}]}>
          <Text style={[styles.feeText]}>Official Fee Audit</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.quoteContainer}>
        <QuoteInput
          image={{
            uri: usdData?.coinImage,
          }}
          unit={usdData?.coinSymbol || 'USD'}
          enabled
          title={'You Will Be Sending'}
          placeholder={formatterHelper('0', 'USD')}
          value={fiatInput}
          setValue={setFiatInput}
        />
        <QuoteInput
          image={{
            uri: selectedCoin.coinImage,
          }}
          unit={selectedCoin.coinSymbol}
          title={`This Is How Much ${selectedCoin.coinName} You Will Be Getting`}
          enabled={false}
          value={estimatedCryptoValue.toString()}
          placeholder={formatterHelper('0', selectedCoin.coinSymbol)}
          isLoading={isLoading}
        />
        {errorText !== '' && (
          <View>
            <Text style={styles.errorText}>{errorText}</Text>
          </View>
        )}
      </View>
      <TouchableOpacity
        style={[styles.buyBtn, isLoading && styles.disabled]}
        onPress={buyClickHandler}
        disabled={isLoading}>
        {purchaseLoading ? (
          <>
            <Text style={styles.buyBtnText}>Purchasing</Text>
            <ActivityIndicator
              style={{marginLeft: 20}}
              size="small"
              color="white"
            />
          </>
        ) : (
          <Text style={styles.buyBtnText}>
            {/* Purchase {gettingValue || 0} {selectedCrypto.coinName}s */}
            Confirm Quote
          </Text>
        )}
      </TouchableOpacity>
    </View>
  );
};

export default QuoteForm;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: 'space-between',
  },
  feeButton: {
    marginLeft: 'auto',
  },
  feeText: {
    color: '#186AB4',
    fontFamily: 'Montserrat',
    fontSize: 13,
    paddingBottom: 5,
    textDecorationLine: 'underline',
  },
  headerContinuer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'baseline',
    marginBottom: 10,
  },
  header: {
    color: '#186AB4',
    textAlign: 'left',
    fontSize: 35,
    fontFamily: 'Montserrat-Bold',
  },
  buyBtn: {
    backgroundColor: '#186AB4',
    height: 50,
    borderRadius: 6,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  disabled: {
    opacity: 0.5,
  },
  errorText: {
    color: '#FF2D55',
    textAlign: 'center',
    marginBottom: 20,
    marginTop: -10,
  },
  buyBtnText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
  },
  quoteContainer: {
    paddingVertical: 20,
  },
});
