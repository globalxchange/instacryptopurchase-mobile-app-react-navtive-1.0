import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  FlatList,
  Dimensions,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {getUriImage} from '../../utils';

const {width, height} = Dimensions.get('window');

const DetailsListItem = ({placeholder, icon, items}) => {
  const [isDropDownOpen, setIsDropDownOpen] = useState(false);

  return (
    <View>
      <TouchableOpacity
        style={styles.dropdownContainer}
        onPress={() => setIsDropDownOpen(!isDropDownOpen)}>
        <Image style={styles.image} source={icon} resizeMode="contain" />
        <Text style={styles.text}>{placeholder}</Text>
        <Image
          style={styles.dropIcon}
          source={require('../../assets/dropdown-icon.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <Modal
        animationType="fade"
        transparent
        visible={isDropDownOpen}
        hardwareAccelerated
        statusBarTranslucent
        onDismiss={() => setIsDropDownOpen(false)}
        onRequestClose={() => setIsDropDownOpen(false)}>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.overlay}
          onPress={() => setIsDropDownOpen(false)}
          onPressOut={() => {}}>
          <TouchableWithoutFeedback style={{flex: 1}}>
            <View style={styles.modalContainer}>
              <FlatList
                showsVerticalScrollIndicator={false}
                style={styles.dropDownList}
                data={items}
                keyExtractor={(item, index) => `${item.name}${index}`}
                renderItem={({item}) => (
                  <TouchableOpacity
                    style={[
                      styles.dropDownItem,
                      item.disabled && {opacity: 0.5},
                    ]}>
                    <FastImage
                      style={styles.countryImage}
                      source={{uri: getUriImage(item.image)}}
                      resizeMode="contain"
                    />
                    <Text style={styles.itemText}>{item.name}</Text>
                  </TouchableOpacity>
                )}
                ListEmptyComponent={
                  <Text style={styles.emptyText}>No Option Found...</Text>
                }
              />
            </View>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
      </Modal>
    </View>
  );
};

export default DetailsListItem;

const styles = StyleSheet.create({
  dropdownContainer: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 15,
    alignItems: 'center',
    height: 50,
    marginBottom: 15,
  },
  image: {
    height: 24,
    width: 24,
  },
  text: {
    flexGrow: 1,
    width: 0,
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
  },
  dropIcon: {
    width: 13,
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    flex: 1,
    justifyContent: 'center',
    paddingVertical: height * 0.25,
  },
  modalContainer: {
    paddingVertical: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    width: width * 0.75,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  dropDownList: {
    paddingHorizontal: 30,
  },
  dropDownItem: {flexDirection: 'row', paddingVertical: 10},
  countryImage: {height: 24, width: 24},
  itemText: {
    flex: 1,
    marginLeft: 15,
    fontFamily: 'Montserrat',
    color: '#9A9A9A',
  },
  itemSubText: {
    fontFamily: 'Roboto',
    color: '#788995',
    fontSize: 12,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#186AB4',
    textAlign: 'center',
    paddingVertical: 20,
    fontSize: 18,
  },
});
