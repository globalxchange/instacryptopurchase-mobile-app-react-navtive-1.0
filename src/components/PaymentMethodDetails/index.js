/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import Animated, {
  interpolate,
  useCode,
  set,
  Clock,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';
import DetailsListItem from './DetailsListItem';
import LoadingAnimation from '../LoadingAnimation';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../configs';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {AppContext} from '../../contexts/AppContextProvider';
import {useNavigation} from '@react-navigation/native';
import KoinalCheckout from '../KoinalCheckout';

const {height, width} = Dimensions.get('window');

const PaymentMethodDetails = ({
  isBottomSheetOpen,
  data,
  setIsBottomSheetOpen,
}) => {
  const {countryList} = useContext(AppContext);

  const {bottom} = useSafeAreaInsets();

  const {navigate} = useNavigation();

  const [isExpanded, setIsExpanded] = useState(false);
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const [listDetails, setListDetails] = useState();
  const [pathData, setPathData] = useState();

  const [isKoinalOpen, setIsKoinalOpen] = useState(false);

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );

    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  useEffect(() => {
    if (!isBottomSheetOpen) {
      setListDetails();
      setIsKeyboardOpen(false);
    }
  }, [isBottomSheetOpen]);

  useEffect(() => {
    if (data) {
      Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/stats/get`, {
        params: {paymentMethod: data.code},
      }).then((resp) => {
        const statsData = resp.data;

        // console.log('pathData', statsData);

        setPathData(statsData.pathData);

        if (data.status) {
          setListDetails({
            banker: statsData.stats.banker,
            country: statsData.stats.country,
            paymentMethod: statsData.stats.paymentMethod,
            to_currency: statsData.stats.to_currency,
            from_currency: statsData.stats.from_currency,
          });
        }
      });
    }
  }, [data, isBottomSheetOpen]);

  useEffect(() => {
    if (pathData && countryList) {
    }
  }, [pathData, countryList]);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  // console.log('Data', data);

  const availableCountryList =
    pathData?.country?.map((item) => ({
      name: item.metadata?.name,
      image: item.metadata?.image,
    })) || [];

  const fromCurrencyList =
    pathData?.to_currency?.map((item) => ({
      name: item.coin_metadata?.coinName,
      image: item.coin_metadata?.coinImage,
    })) || [];

  const availableBankerList =
    pathData?.banker?.map((item) => ({
      name: item.displayName || item._id,
      image: item.icons?.image1 || item.icons,
    })) || [];

  const toAssetsList =
    pathData?.from_currency?.map((item) => ({
      name: item.coin_metadata?.coinName,
      image: item.coin_metadata?.coinImage,
    })) || [];

  const totalAsset = [];

  toAssetsList.forEach((item) => {
    const pushItem = {
      name: item.name,
      image: item.image,
    };

    let found = false;
    totalAsset.forEach((x) => {
      if (x.name === pushItem.name) {
        return (found = true);
      }
    });
    if (!found) {
      totalAsset.push(pushItem);
    }
  });

  fromCurrencyList.forEach((item) => {
    const pushItem = {
      name: item.name,
      image: item.image,
    };

    let found = false;
    totalAsset.forEach((x) => {
      if (x.name === pushItem.name) {
        return (found = true);
      }
    });
    if (!found) {
      totalAsset.push(pushItem);
    }
  });

  const onSimplexClick = () => {
    setIsBottomSheetOpen(false);
    navigate('Simplex');
  };

  const onKoinalClick = () => {
    setIsBottomSheetOpen(false);
    setIsKoinalOpen(true);
  };

  // console.log('data', data);

  return (
    <>
      <Modal
        animationType="slide"
        visible={isBottomSheetOpen}
        transparent
        hardwareAccelerated
        statusBarTranslucent
        onDismiss={() => setIsBottomSheetOpen(false)}
        onRequestClose={() => setIsBottomSheetOpen(false)}>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.overlay}
          onPress={() => setIsBottomSheetOpen(false)}
          onPressOut={() => {}}>
          <TouchableWithoutFeedback style={{flex: 1}}>
            <Animated.View
              style={[
                styles.container,
                // {maxHeight: isExpanded ? height - 40 : height * 0.65},
                {
                  paddingBottom: bottom,
                  transform: [
                    {
                      translateY: interpolate(chatKeyboardAnimation.current, {
                        inputRange: [0, 1],
                        outputRange: [0, -keyboardHeight],
                      }),
                    },
                  ],
                },
              ]}>
              <View
                style={{
                  backgroundColor: 'white',
                  paddingHorizontal: 30,
                  paddingTop: 30,
                }}>
                {data && (
                  <>
                    <View style={styles.header}>
                      <Image
                        style={styles.methodIcon}
                        source={{uri: data.icon}}
                        resizeMode="cover"
                      />
                      <Text style={styles.methodName}>{data.name}</Text>
                    </View>
                    <Text style={styles.description}>{data.description}</Text>
                    <View style={styles.listContainer}>
                      {listDetails ? (
                        <>
                          <DetailsListItem
                            placeholder={`Available In ${listDetails.country} Countries`}
                            icon={require('../../assets/default-breadcumb-icon/country.png')}
                            items={availableCountryList}
                          />
                          <DetailsListItem
                            placeholder={`Accepting In ${listDetails.to_currency} Currencies`}
                            icon={require('../../assets/default-breadcumb-icon/country.png')}
                            items={fromCurrencyList}
                          />
                          <DetailsListItem
                            placeholder={`Supported In ${listDetails.banker} Bankers`}
                            icon={require('../../assets/default-breadcumb-icon/country.png')}
                            items={availableBankerList}
                          />
                          <DetailsListItem
                            placeholder={`Enables Access To ${
                              listDetails.to_currency +
                              listDetails.from_currency
                            } Assets`}
                            icon={require('../../assets/default-breadcumb-icon/country.png')}
                            items={totalAsset}
                          />
                        </>
                      ) : (
                        <View style={styles.loadingContainer}>
                          <LoadingAnimation />
                        </View>
                      )}
                      {(data.name === 'Credit Card' || data.code === 'CC') && (
                        <>
                          <Text style={styles.featuredHeader}>
                            Featured Vendors
                          </Text>
                          <View style={styles.featuredContainer}>
                            <ScrollView
                              showsHorizontalScrollIndicator={false}
                              horizontal>
                              <TouchableOpacity
                                onPress={onKoinalClick}
                                style={styles.featuredItem}>
                                <Image
                                  source={require('../../assets/koinal-logo.png')}
                                  style={styles.featuredItemImage}
                                  resizeMode="contain"
                                />
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={onSimplexClick}
                                style={[
                                  styles.featuredItem,
                                  {marginRight: 30},
                                ]}>
                                <Image
                                  source={require('../../assets/simplex-logo.png')}
                                  style={styles.featuredItemImage}
                                  resizeMode="contain"
                                />
                              </TouchableOpacity>
                            </ScrollView>
                          </View>
                        </>
                      )}
                    </View>
                    <TouchableOpacity style={styles.profileButton}>
                      <Text style={styles.profileButtonText}>
                        Full Insta Profile
                      </Text>
                    </TouchableOpacity>
                  </>
                )}
              </View>
            </Animated.View>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
      </Modal>
      <KoinalCheckout setIsOpen={setIsKoinalOpen} isOpen={isKoinalOpen} />
    </>
  );
};

export default PaymentMethodDetails;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: '#02C346',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  header: {
    paddingVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  methodIcon: {
    width: 40,
    height: 40,
  },
  methodName: {
    marginLeft: 15,
    flex: 1,
    fontFamily: 'Montserrat-SemiBold',
    color: '#231F20',
    fontSize: 26,
  },
  description: {
    marginTop: 10,
    fontFamily: 'Montserrat',
    fontSize: 13,
  },
  profileButton: {
    backgroundColor: '#02C346',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: -30,
    height: 55,
  },
  profileButtonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 15,
  },
  listContainer: {
    paddingTop: 35,
    paddingBottom: 20,
  },
  loadingContainer: {},
  featuredHeader: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  featuredContainer: {
    marginTop: 10,
    marginRight: -30,
  },
  featuredItem: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    width: width * 0.5,
    marginRight: 15,
  },
  featuredItemImage: {
    width: 100,
    height: 80,
  },
});
