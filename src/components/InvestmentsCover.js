/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useContext, useRef} from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';
import Animated, {
  useCode,
  Value,
  set,
  Clock,
  interpolate,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../utils/ReanimatedTimingHelper';
import FastImage from 'react-native-fast-image';
import {getUriImage} from '../utils';

const InvestmentsCover = ({searchPlaceholder, showHeader}) => {
  const {
    toggleCountrySelector,
    filterActiveCountry,
    homeSearchInput,
    setHomeSearchInput,
    cryptoTableData,
    countryList,
    totalPaymentMethods,
    pathData,
  } = useContext(AppContext);

  const [headerData, setHeaderData] = useState({});

  const headerAnimation = useRef(new Value(1));

  useEffect(() => {
    if (pathData && cryptoTableData) {
      const supportedCrypto = cryptoTableData.filter(
        (x) => x.asset_type === 'Crypto',
      );

      let numberOfCryptos = [];
      let numberOfCountries = [];
      let numberOfMethods = [];

      // supportedCrypto.forEach((cryptoItem) => {
      //   pathData.forEach((pathItem) => {
      //     if (
      //       cryptoItem.type === 'crypto' &&
      //       !numberOfCryptos.includes(cryptoItem)
      //     ) {
      //       numberOfCryptos.push(cryptoItem);
      //       return;
      //     }
      //   });
      // });
      if (countryList) {
        countryList.forEach((countryItem) => {
          pathData.forEach((pathItem) => {
            if (
              pathItem.country === countryItem.value &&
              !numberOfCountries.includes(countryItem)
            ) {
              numberOfCountries.push(countryItem);
              return;
            }
          });
        });
      }

      if (totalPaymentMethods) {
        totalPaymentMethods.forEach((paymentItem) => {
          pathData.forEach((pathItem) => {
            if (
              (pathItem.depositMethod === paymentItem.code ||
                pathItem.paymentMethod === paymentItem.code) &&
              !numberOfMethods.includes(paymentItem)
            ) {
              numberOfMethods.push(paymentItem);
              return;
            }
          });
        });
      }
      setHeaderData({
        numberOfCryptos: supportedCrypto.length,
        numberOfCountries: numberOfCountries.length,
        numberOfMethods: numberOfMethods.length,
      });
    }
  }, [cryptoTableData, pathData, countryList, totalPaymentMethods]);

  useCode(
    () =>
      showHeader
        ? [
            set(
              headerAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ]
        : [
            set(
              headerAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ],
    [showHeader],
  );

  return (
    <View style={styles.container}>
      <Animated.View
        style={{
          height: interpolate(headerAnimation.current, {
            inputRange: [0, 1],
            outputRange: [0, 80],
          }),
          overflow: 'hidden',
        }}>
        <Text style={styles.coverHeader}>{`Buy ${
          headerData.numberOfCryptos ? headerData.numberOfCryptos : ''
        } Cryptocurrencies`}</Text>
        <Text style={styles.coverSubHeader}>
          {`Using ${
            headerData.numberOfMethods !== undefined
              ? headerData.numberOfMethods
              : ''
          } Payment Methods In ${
            headerData.numberOfCountries !== undefined
              ? headerData.numberOfCountries
              : ''
          } Countries`}
        </Text>
      </Animated.View>
      <View style={styles.searchContainer}>
        <TouchableOpacity onPress={toggleCountrySelector}>
          <FastImage
            style={styles.countryIcon}
            source={{
              uri: filterActiveCountry
                ? getUriImage(filterActiveCountry.formData.Flag)
                : 'https://d22n6gp5xyztod.cloudfront.net/5eb3246af10390217259d8a7298.png',
            }}
            resizeMode="contain"
          />
        </TouchableOpacity>
        <View style={styles.separator} />
        <TextInput
          style={styles.searchInput}
          onChangeText={(text) => setHomeSearchInput(text)}
          value={homeSearchInput}
          autoFocus={false}
          placeholder={
            searchPlaceholder.length < 30
              ? searchPlaceholder
              : searchPlaceholder.substring(0, 30)
          }
          placeholderTextColor={'#878788'}
        />
        <Image
          style={styles.searchIcon}
          source={require('../assets/search-icon.png')}
          resizeMode="contain"
        />
      </View>
    </View>
  );
};

export default InvestmentsCover;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: '#fbfbfb',
  },
  coverHeader: {
    textAlign: 'center',
    marginTop: 10,
    color: '#186AB4',
    fontSize: 22,
    fontFamily: 'Montserrat-Bold',
  },
  coverSubHeader: {
    textAlign: 'center',
    marginTop: 10,
    color: '#186AB4',
    fontSize: 12,
    fontFamily: 'Montserrat-Bold',
  },
  searchContainer: {
    flexDirection: 'row',
    marginTop: 40,
    height: 45,
    paddingHorizontal: 15,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    borderRadius: 2,
    elevation: 12,
  },
  countryIcon: {
    height: 22,
    width: 22,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginRight: 15,
  },
  separator: {
    backgroundColor: '#CACACA',
    width: 1,
    marginVertical: 8,
  },
  searchInput: {
    flexGrow: 1,
    width: 0,
    marginTop: 'auto',
    marginBottom: 'auto',
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    fontWeight: 'normal',
    color: 'black',
  },
  searchIcon: {
    height: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
});
