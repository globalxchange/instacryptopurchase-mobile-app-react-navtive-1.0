import React, {useContext, useState, useEffect} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, FlatList} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';
import LoadingAnimation from './LoadingAnimation';
import FastImage from 'react-native-fast-image';
import {getUriImage} from '../utils';

const CountryList = () => {
  const {
    countryList,
    setFilterActiveCountry,
    setCheckOutCountry,
    homeSearchInput,
    pathData,
  } = useContext(AppContext);

  const [parsedList, setParsedList] = useState();
  const [filteredList, setFilteredList] = useState(countryList);

  const onCountrySelect = (country) => {
    setFilterActiveCountry(country);
    // setCheckOutCountry(country);
  };

  useEffect(() => {
    if (countryList) {
      const parsedCountries = [];

      countryList.forEach((countryItem) => {
        let bankers = [];
        pathData.forEach((pathItem) => {
          if (
            countryItem.Key === pathItem.country &&
            !bankers.includes(pathItem.banker)
          ) {
            bankers.push(pathItem.banker);
          }
        });
        parsedCountries.push({...countryItem, noOfBankers: bankers.length});
      });
      setParsedList(parsedCountries);
    }
  }, [countryList, pathData]);

  useEffect(() => {
    if (parsedList) {
      const list = parsedList.filter(
        (item) =>
          item.Key.toLowerCase().includes(homeSearchInput.toLowerCase()) ||
          item.formData.Name.toLowerCase().includes(
            homeSearchInput.toLowerCase(),
          ),
      );

      setFilteredList(list);
    }
  }, [homeSearchInput, parsedList]);

  return (
    <View style={styles.container}>
      {countryList ? (
        <FlatList
          style={styles.scrollView}
          data={filteredList}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item.Key}
          renderItem={({item}) => (
            <TouchableOpacity onPress={() => onCountrySelect(item)}>
              <View style={styles.item}>
                <FastImage
                  style={styles.cryptoIcon}
                  resizeMode="contain"
                  source={{
                    uri: getUriImage(item.formData.Flag),
                  }}
                />
                <View style={styles.nameContainer}>
                  <Text style={styles.countryName}>{item.Key}</Text>
                </View>
                <View style={styles.priceContainer}>
                  <Text style={styles.methodsNo}>
                    {item.noOfBankers !== undefined
                      ? `${item.noOfBankers} Bankers`
                      : ''}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>No Supported Country Found</Text>
            </View>
          }
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
          <Text style={styles.loadingText}>Fetching Data...</Text>
        </View>
      )}
    </View>
  );
};

export default CountryList;

const styles = StyleSheet.create({
  container: {backgroundColor: '#f7f7f7', flex: 1},
  scrollView: {flex: 1, paddingHorizontal: 10, paddingTop: 10},
  item: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
  },
  cryptoIcon: {
    width: 25,
    height: 25,
    borderRadius: 10,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  nameContainer: {
    flexGrow: 1,
    width: 0,
    marginLeft: 10,
  },
  countryName: {
    color: '#001D41',
    fontSize: 14,
    marginBottom: 2,
    fontFamily: 'Montserrat-Bold',
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  methodsNo: {
    color: '#001D41',
    opacity: 0.5,
    marginBottom: 2,
    textAlign: 'right',
    fontFamily: 'Roboto-Bold',
    marginRight: 10,
    fontSize: 12,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  loadingText: {marginTop: 10, fontFamily: 'Montserrat'},
  emptyContainer: {
    marginTop: 20,
  },
  emptyText: {
    fontFamily: 'Montserrat-Bold',
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 16,
    paddingHorizontal: 30,
  },
});
