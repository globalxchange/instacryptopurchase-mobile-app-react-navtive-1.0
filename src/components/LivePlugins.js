import {useNavigation} from '@react-navigation/native';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  Dimensions,
  Image,
  Keyboard,
  Modal,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Animated, {
  Clock,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {WToast} from 'react-native-smart-tip';
import {AppContext} from '../contexts/AppContextProvider';
import ReanimatedTimingHelper from '../utils/ReanimatedTimingHelper';
import AdminMenu from './AdminMenu';
import Connect from './Connect';
import InitiatePlanB from './InitiatePlanB';
import * as WebBrowser from 'expo-web-browser';

const {height} = Dimensions.get('window');

const LivePlugins = ({isOpen, setIsOpen}) => {
  const {navigate} = useNavigation();

  const {bottom} = useSafeAreaInsets();

  const {forceRefreshApiData} = useContext(AppContext);

  const [isExpanded, setIsExpanded] = useState(false);
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const [selectedView, setSelectedView] = useState();
  const [isPlanBOpen, setIsPlanBOpen] = useState(false);
  const [isAdminMenuOpen, setIsAdminMenuOpen] = useState(false);

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  useEffect(() => {
    if (!isOpen) {
      resetState();
      setKeyboardHeight(0);
      setIsKeyboardOpen(false);
    }
  }, [isOpen]);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  const resetState = () => {
    setSelectedView();
  };

  const onItemClick = (clickedItem) => {
    switch (clickedItem) {
      case 'BlockCheck':
        setIsOpen(false);
        navigate('BlockCheck');
        break;
      case 'UpdateHistory':
        setIsOpen(false);
        navigate('UpdateHistory');
        break;
      case 'Connect':
        setSelectedView(<Connect setIsOpen={setIsOpen} />);
        break;
      case 'Settings':
        setIsOpen(false);
        navigate('Settings');
        break;
      case 'MoneyMarkets':
        setIsOpen(false);
        navigate('MoneyMarket');
        break;
      case 'RefreshBalances':
        WToast.show({
          data: 'Refreshing App Data',
          position: WToast.position.TOP,
        });
        setIsOpen(false);
        forceRefreshApiData();
        break;
      case 'OnHold':
        setIsOpen(false);
        setIsAdminMenuOpen(true);
        break;
      case 'ChatsIo':
        setIsOpen(false);
        navigate('Support', {openUserChat: true});
        break;
      case 'CryptoCoupen':
        WebBrowser.openBrowserAsync('https://cryptocoupon.com/');
        break;
      case 'ATM':
        WebBrowser.openBrowserAsync('https://atms.app/');
        break;
    }
  };

  const openPlanB = () => {
    setIsOpen(false);
    setIsPlanBOpen(true);
  };

  return (
    <>
      <Modal
        animationType="slide"
        visible={isOpen}
        transparent
        hardwareAccelerated
        statusBarTranslucent
        onDismiss={() => setIsOpen(false)}
        onRequestClose={() => setIsOpen(false)}>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.overlay}
          onPress={() => setIsOpen(false)}
          onPressOut={() => {}}>
          <TouchableWithoutFeedback style={{flex: 1}}>
            <Animated.View
              style={[
                styles.container,
                // {maxHeight: isExpanded ? height - 40 : height * 0.75},
                {
                  paddingBottom: bottom,
                  transform: [
                    {
                      translateY: interpolate(chatKeyboardAnimation.current, {
                        inputRange: [0, 1],
                        outputRange: [0, -keyboardHeight],
                      }),
                    },
                  ],
                },
              ]}>
              <View style={styles.fragmentContainer}>
                {selectedView ? (
                  selectedView
                ) : (
                  <View style={styles.categorySelector}>
                    <Image
                      source={require('../assets/live-plugins-icon.png')}
                      style={styles.headerImage}
                      resizeMode="contain"
                    />
                    <ScrollView
                      showsVerticalScrollIndicator={false}
                      style={styles.listContainer}>
                      {items.map((item) => (
                        <TouchableOpacity
                          key={item.title}
                          disabled={item.disabled}
                          onPress={() => onItemClick(item.title)}>
                          <View
                            style={[
                              styles.listItem,
                              item.disabled && {opacity: 0.5},
                            ]}>
                            <Image
                              source={item.image}
                              resizeMode="contain"
                              style={styles.itemIcon}
                            />
                            {/* <Text style={styles.itemName}>{item.title}</Text> */}
                          </View>
                        </TouchableOpacity>
                      ))}
                    </ScrollView>
                    <TouchableOpacity
                      onPress={() => openPlanB()}
                      style={styles.action}>
                      <Image
                        resizeMode="contain"
                        style={[styles.updateNumberIcon, {height: 22}]}
                        source={require('../assets/initaite-plan-b-icon.png')}
                      />
                    </TouchableOpacity>
                  </View>
                )}
              </View>
            </Animated.View>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
      </Modal>
      <InitiatePlanB isOpen={isPlanBOpen} setIsOpen={setIsPlanBOpen} />
      <AdminMenu isOpen={isAdminMenuOpen} setIsOpen={setIsAdminMenuOpen} />
    </>
  );
};

export default LivePlugins;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: '#F1F4F6',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {
    // flex: 1,
    backgroundColor: 'white',
  },
  categorySelector: {
    // flex: 1,
    paddingHorizontal: 40,
    paddingTop: 20,
    // paddingBottom: 20,
    maxHeight: height * 0.8,
  },
  headerImage: {
    height: 50,
    width: 200,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 25,
  },
  headerText: {
    fontFamily: 'Montserrat-Bold',
    color: '#186AB4',
    textAlign: 'center',
  },
  listContainer: {
    marginBottom: 20,
  },
  listItem: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 8,
    paddingVertical: 18,
    paddingHorizontal: 25,
    marginBottom: 20,
    justifyContent: 'center',
  },
  itemIcon: {
    height: 30,
    width: 150,
  },
  itemName: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#186AB4',
    paddingHorizontal: 10,
    flex: 1,
  },
  action: {
    backgroundColor: '#F1F4F6',
    marginHorizontal: -40,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  updatesNumberContainer: {},
  updateNumberIcon: {
    height: 30,
  },
  updateNumber: {
    position: 'absolute',
    bottom: -6,
    right: -6,
    backgroundColor: '#FF5245',
    width: 22,
    height: 22,
    borderRadius: 11,
    justifyContent: 'center',
    alignItems: 'center',
  },
  updateNumberText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 13,
  },
});

const items = [
  {
    image: require('../assets/block-check-full-logo.png'),
    title: 'BlockCheck',
  },
  {image: require('../assets/connect-icon.png'), title: 'Connect'},
  {image: require('../assets/crypto-coupen-icon.png'), title: 'CryptoCoupen'},
  {image: require('../assets/atm-plugin-icon.png'), title: 'ATM'},
  {image: require('../assets/settings-plugin-icon.png'), title: 'Settings'},
  {
    image: require('../assets/money-markets-full-icon.png'),
    title: 'MoneyMarkets',
  },
  {
    image: require('../assets/app-updates-icon.png'),
    title: 'UpdateHistory',
  },
  {
    image: require('../assets/force-update-icon.png'),
    title: 'RefreshBalances',
  },
  {
    image: require('../assets/chats-io-full.png'),
    title: 'ChatsIo',
  },
  {
    image: require('../assets/on-hold-full-icon.png'),
    title: 'OnHold',
  },
];
