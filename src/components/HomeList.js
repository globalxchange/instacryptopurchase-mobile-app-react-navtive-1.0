import React, {useContext, useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  RefreshControl,
} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';
import {usdValueFormatter, percentageFormatter, getUriImage} from '../utils';
import {useNavigation} from '@react-navigation/native';
import LoadingAnimation from './LoadingAnimation';
import FastImage from 'react-native-fast-image';
import SkeletonItemLoading from './SkeletonItemLoading';

const HomeList = ({showFiat, setSearchCallback, setSearchList}) => {
  const navigation = useNavigation();

  const {
    cryptoTableData,
    homeSearchInput,
    filterActiveCountry,
    pathData,
    getCryptoData,
    getBlockCheckData,
  } = useContext(AppContext);

  const [unfilteredList, setUnfilteredList] = useState();
  const [filteredList, setFilteredList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [searchCountryText, setSearchCountryText] = useState('');

  const cryptoItemClickHandler = (crypto) => {
    navigation.navigate('Asset', {crypto: crypto});
  };

  useEffect(() => {
    if (filterActiveCountry) {
      if (filterActiveCountry.value === 'Worldwide') {
        setSearchCountryText('Globally');
      } else {
        setSearchCountryText(`In ${filterActiveCountry.value}`);
      }
    }
  }, [filterActiveCountry]);

  useEffect(() => {
    if (cryptoTableData && filterActiveCountry) {
      const list = cryptoTableData.filter(
        (item) => item.asset_type === (showFiat ? 'Fiat' : 'Crypto'),
      );

      if (!filterActiveCountry || filterActiveCountry.value !== 'Worldwide') {
        const newList = [];

        list.forEach((item) => {
          if (item.type === 'crypto') {
            return newList.push(item);
          }
          pathData.forEach((pathItem) => {
            if (
              pathItem.country === filterActiveCountry.value &&
              (pathItem.to_currency === item.coinSymbol ||
                pathItem.from_currency === item.coinSymbol) &&
              !newList.includes(item)
            ) {
              return newList.push(item);
            }
          });
        });
        setUnfilteredList(newList);
      } else {
        setUnfilteredList(list);
      }
    }
  }, [filterActiveCountry, cryptoTableData, showFiat, pathData]);

  useEffect(() => {
    setSearchCallback((item) => cryptoItemClickHandler(item));
    setSearchList(unfilteredList);
    return () => {
      setSearchList();
      setSearchCallback(() => {});
    };
  }, [unfilteredList]);

  useEffect(() => {
    if (unfilteredList) {
      const queryString = homeSearchInput.toLowerCase();

      const list = unfilteredList.filter(
        (item) =>
          item.coinName.toLowerCase().includes(queryString) ||
          item.coinSymbol.toLowerCase().includes(queryString),
      );
      setFilteredList(list);
    }
  }, [unfilteredList, homeSearchInput]);

  return (
    <View style={styles.container}>
      {!isLoading && unfilteredList ? (
        <FlatList
          style={styles.scrollView}
          data={filteredList}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item.coinName}
          refreshControl={
            <RefreshControl
              refreshing={cryptoTableData ? false : true}
              onRefresh={() => {
                getCryptoData();
                getBlockCheckData();
              }}
            />
          }
          renderItem={({item}) => (
            <TouchableOpacity onPress={() => cryptoItemClickHandler(item)}>
              <View style={styles.item}>
                <FastImage
                  style={styles.cryptoIcon}
                  resizeMode="contain"
                  source={{
                    uri: getUriImage(item.coinImage),
                  }}
                />
                <View style={styles.nameContainer}>
                  <Text style={styles.cryptoName}>{item.coinName}</Text>
                </View>
                <View style={styles.priceContainer}>
                  {/* Value */}
                  <Text style={styles.cryptoPrice}>
                    {usdValueFormatter.format(item.price.USD)}
                  </Text>
                  {/* Change in Day */}
                  <Text
                    style={
                      (item._24hrchange || 0) < 0
                        ? styles.downValue
                        : styles.upValue
                    }>
                    {`${percentageFormatter.format(item._24hrchange || 0)}%`}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>
                No Supported Crypto Found {searchCountryText}
              </Text>
            </View>
          }
        />
      ) : (
        <SkeletonItemLoading />
      )}
    </View>
  );
};

export default HomeList;

const styles = StyleSheet.create({
  container: {backgroundColor: '#f7f7f7', flex: 1},
  scrollView: {flex: 1, paddingHorizontal: 10, paddingTop: 10},
  item: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
  },
  cryptoIcon: {
    width: 25,
    height: 25,
    borderRadius: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  nameContainer: {
    flexGrow: 1,
    width: 0,
    marginLeft: 10,
  },
  cryptoName: {
    color: '#001D41',
    fontSize: 16,
    marginBottom: 2,
    fontFamily: 'Montserrat-Bold',
  },
  noOfVendors: {
    color: '#001D41',
    opacity: 0.5,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  cryptoPrice: {
    color: '#001D41',
    opacity: 0.5,
    marginBottom: 2,
    textAlign: 'right',
    fontFamily: 'Roboto-Bold',
    marginRight: 10,
  },
  upValue: {
    textAlign: 'right',
    color: '#0E9347',
    fontFamily: 'Roboto',
    fontSize: 10,
  },
  downValue: {
    textAlign: 'right',
    color: '#ca1e1e',
    fontFamily: 'Roboto',
    fontSize: 10,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  loadingText: {marginTop: 10, fontFamily: 'Montserrat'},
  emptyContainer: {
    marginTop: 20,
  },
  emptyText: {
    fontFamily: 'Montserrat',
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 16,
    paddingHorizontal: 30,
  },
});
