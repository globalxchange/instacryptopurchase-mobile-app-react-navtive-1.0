import React, {useRef} from 'react';
import {StyleSheet, Image, Dimensions} from 'react-native';
import Animated, {
  set,
  interpolate,
  Clock,
  useCode,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../utils/ReanimatedTimingHelper';

const {width} = Dimensions.get('window');

const ComingSoonFragment = () => {
  const logoAnim = useRef(new Animated.Value(0));

  const comingSoonAnimation = interpolate(logoAnim.current, {
    inputRange: [0, 1],
    outputRange: [-250, 0],
  });

  useCode(
    () => [
      set(logoAnim.current, ReanimatedTimingHelper(0, 1, new Clock(), 300)),
    ],
    [],
  );

  return (
    <Animated.View style={styles.container}>
      <Animated.View
        style={[
          styles.imageContainer,
          {transform: [{translateY: comingSoonAnimation}]},
        ]}>
        <Image
          style={styles.comingSoonImage}
          source={require('../assets/coming-soon.png')}
          resizeMode="contain"
        />
      </Animated.View>
    </Animated.View>
  );
};

export default ComingSoonFragment;

const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center', overflow: 'hidden'},
  imageContainer: {
    height: '70%',
    width: width * 0.8,
    alignItems: 'center',
  },
  comingSoonImage: {flex: 1},
});
