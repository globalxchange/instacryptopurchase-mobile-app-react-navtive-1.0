/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {AGENCY_API_URL, APP_CODE, GX_API_ENDPOINT} from '../configs';
import ThemeData from '../configs/ThemeData';
import {getUriImage} from '../utils';

const {width} = Dimensions.get('window');

const HomeAdsCarousel = () => {
  const [adsList, setAdsList] = useState();

  useEffect(() => {
    Axios.get(`${AGENCY_API_URL}/navbar/filter`, {
      params: {
        app_code: APP_CODE,
        email: 'shorupan@gmail.com',
        name: `ads${APP_CODE}`,
      },
    })
      .then(({data}) => {
        // console.log('Data', data);
        const ads = data?.data?.videos || [];

        if (ads?.length > 0) {
          setAdsList(ads);
        } else {
          Axios.get(`${AGENCY_API_URL}/navbar/filter`, {
            params: {
              app_code: 'VSA',
              email: 'shorupan@gmail.com',
              name: 'adsVSA',
            },
          }).then((resp) => {
            // console.log('fallbakck data', resp.data);
            if (resp?.data?.status) {
              setAdsList(resp?.data?.data?.videos || []);
            }
          });
        }
      })
      .catch((error) => {
        console.log('Error getting ads list', error);
      });
  }, []);

  return (
    <View style={styles.container}>
      <ScrollView
        style={{paddingHorizontal: 20}}
        horizontal
        showsHorizontalScrollIndicator={false}>
        {adsList?.map((item, index) => (
          <AdItem
            key={index}
            style={index === 3 ? {marginRight: 40} : {}}
            title={item.title || ''}
            sideImage={item.image}
            actions={[item?.attachment?.tag1, item?.attachment?.tag2]}
          />
        ))}
      </ScrollView>
    </View>
  );
};

const AdItem = ({style, title, sideImage, actions}) => {
  return (
    <View style={[styles.adItem, style]}>
      <View style={styles.textContainer}>
        <Text style={styles.adTitle}>{title}</Text>
        <View style={styles.actionContainer}>
          {actions?.map((item, index) => (
            <TouchableOpacity key={index} style={styles.action}>
              <FastImage
                style={styles.actionImage}
                resizeMode="contain"
                source={{uri: getUriImage(item?.icon)}}
              />
              <Text style={styles.actionName}>{item?.name}</Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
      <View style={styles.imageContainer}>
        <FastImage
          style={styles.sideImage}
          resizeMode="cover"
          source={{uri: getUriImage(sideImage)}}
        />
      </View>
    </View>
  );
};

export default HomeAdsCarousel;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  adItem: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    flexDirection: 'row',
    marginRight: 20,
    width: width * 0.75,
  },
  imageContainer: {
    width: 120,
  },
  sideImage: {
    flex: 1,
    width: null,
    height: null,
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingVertical: 15,
    paddingLeft: 15,
  },
  adTitle: {
    fontSize: 15,
    fontFamily: ThemeData.FONT_BOLD,
    color: '#292929',
    marginBottom: 20,
  },
  actionContainer: {
    flexDirection: 'row',
  },
  action: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 4,
    width: 80,
    justifyContent: 'space-between',
    marginRight: 5,
    paddingHorizontal: 15,
  },
  actionImage: {
    width: 12,
    height: 12,
    marginRight: 5,
  },
  actionName: {
    fontFamily: ThemeData.FONT_MEDIUM,
    fontSize: 8,
    color: '#292929',
  },
});
