import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  Image,
  Keyboard,
  Modal,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Animated, {
  Clock,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import Clipboard from '@react-native-community/clipboard';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';
import {WToast} from 'react-native-smart-tip';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {AppContext} from '../../contexts/AppContextProvider';
import LoadingAnimation from '../LoadingAnimation';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const BrokerSyncConfigure = () => {
  const {bottom} = useSafeAreaInsets();

  const {userName, setLoginData} = useContext(AppContext);

  const [isOpen, setIsOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [isEditorOpen, setIsEditorOpen] = useState(false);
  const [isCopied, setIsCopied] = useState(false);
  const [codeInput, setCodeInput] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  const code = userName;

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  useEffect(() => {
    if (!isOpen) {
      resetState();
    }
  }, [isOpen]);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  const resetState = () => {
    setIsEditorOpen(false);
    setCodeInput('');
    setIsKeyboardOpen(false);
    setIsSuccess(false);
  };

  const copyHandler = () => {
    Clipboard.setString(code);
    setIsCopied(true);
    setTimeout(() => setIsCopied(false), 20000);
  };

  const updateUserDetails = (email) => {
    Axios.get(`${GX_API_ENDPOINT}/user/details/get`, {
      params: {email},
    }).then((res) => {
      const {data} = res;

      // console.log('Data', data);
      if (data.status) {
        AsyncStorageHelper.setUserName(data.user.username);
        setLoginData(data.user.username, true, data.user.profile_img);
      }
    });
  };

  const updateCode = async () => {
    const codeTrimmed = codeInput.trim();

    if (!codeTrimmed) {
      WToast.show({data: 'Please Input A Code', position: WToast.position.TOP});
    }

    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      email,
      accessToken: token,
      field: 'username',
      value: codeTrimmed,
    };

    console.log('postData', postData);

    Axios.post(`${GX_API_ENDPOINT}/user/details/edit`, postData)
      .then((resp) => {
        const {data} = resp;

        // console.log('updateCode', data);

        if (data.status) {
          updateUserDetails(email);
          setIsSuccess(true);
          // WToast.show({
          //   data: data.message || 'BrokerSync Code Updated',
          //   position: WToast.position.TOP,
          // });
        } else {
          WToast.show({
            data: data.message || 'Error on updating BrokerSync Code',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        WToast.show({
          data: 'Error on updating BrokerSync Code',
          position: WToast.position.TOP,
        });
        console.log('Error on updating BrokerSync Code', error);
      })
      .finally(() => setIsLoading(false));
  };

  return (
    <>
      <TouchableOpacity style={styles.button} onPress={() => setIsOpen(true)}>
        <Text style={styles.buttonText}>Configure BrokerSync</Text>
      </TouchableOpacity>
      <Modal
        animationType="slide"
        visible={isOpen}
        transparent
        hardwareAccelerated
        statusBarTranslucent
        onDismiss={() => setIsOpen(false)}
        onRequestClose={() => setIsOpen(false)}>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.overlay}
          onPress={() => setIsOpen(false)}
          onPressOut={() => {}}>
          <TouchableWithoutFeedback style={{flex: 1}}>
            <Animated.View
              style={[
                styles.container,

                {
                  paddingBottom: bottom,
                  transform: [
                    {
                      translateY: interpolate(chatKeyboardAnimation.current, {
                        inputRange: [0, 1],
                        outputRange: [0, -keyboardHeight],
                      }),
                    },
                  ],
                },
              ]}>
              <View style={styles.fragmentContainer}>
                <Image
                  source={require('../../assets/broker-sync.png')}
                  style={styles.headerImage}
                  resizeMode="contain"
                />
                {isSuccess ? (
                  <View style={styles.categorySelector}>
                    <View style={styles.listContainer}>
                      <Text style={styles.successText}>
                        Congratulations Your BrokerSync Code Has Been Updated To
                      </Text>
                      <Text style={styles.newCode}>{codeInput}</Text>
                    </View>
                    <TouchableOpacity
                      style={styles.button}
                      onPress={() => setIsOpen(false)}>
                      <Text style={styles.buttonText}>Close</Text>
                    </TouchableOpacity>
                  </View>
                ) : isEditorOpen ? (
                  <View style={styles.categorySelector}>
                    {isLoading ? (
                      <View style={styles.loadingContainer}>
                        <LoadingAnimation />
                      </View>
                    ) : (
                      <View style={styles.listContainer}>
                        <Text style={styles.title}>Enter New Code</Text>
                        <View style={styles.codeEditor}>
                          <TextInput
                            placeholder="Ex. Hamburger"
                            placeholderTextColor="#878788"
                            style={styles.input}
                            value={codeInput}
                            onChangeText={(text) => setCodeInput(text)}
                            returnKeyType="done"
                          />
                          <TouchableOpacity
                            style={styles.nextButton}
                            onPress={updateCode}>
                            <Image
                              source={require('../../assets/next-forward-icon-white.png')}
                              resizeMode="contain"
                              style={styles.copyIcon}
                            />
                          </TouchableOpacity>
                        </View>
                      </View>
                    )}
                  </View>
                ) : (
                  <View style={styles.categorySelector}>
                    <View style={styles.listContainer}>
                      <Text style={styles.title}>Current BrokerSync Code</Text>
                      <View style={styles.codeContainer}>
                        <Text style={styles.code}>{code}</Text>
                        <TouchableOpacity
                          onPress={isCopied ? null : copyHandler}>
                          <Image
                            source={
                              isCopied
                                ? require('../../assets/check-icon-dark.png')
                                : require('../../assets/copy-icon.png')
                            }
                            resizeMode="contain"
                            style={styles.copyIcon}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                    <TouchableOpacity
                      style={styles.button}
                      onPress={() => setIsEditorOpen(true)}>
                      <Text style={styles.buttonText}>
                        Edit BrokerSync Code
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              </View>
            </Animated.View>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
      </Modal>
    </>
  );
};

export default BrokerSyncConfigure;

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    backgroundColor: '#186AB4',
  },
  buttonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
    paddingTop: 30,
  },
  fragmentContainer: {
    maxHeight: 350,
  },
  headerImage: {
    height: 55,
    width: 200,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  listContainer: {
    paddingHorizontal: 30,
    paddingVertical: 70,
  },
  title: {
    color: '#186AB4',
    fontFamily: 'Montserrat-Bold',
  },
  codeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#EBEBEB',
    borderRadius: 8,
    paddingHorizontal: 20,
    paddingVertical: 20,
    marginTop: 15,
  },
  code: {
    flex: 1,
    color: '#186AB4',
    fontFamily: 'Montserrat',
    fontSize: 16,
  },
  copyIcon: {
    height: 22,
    width: 22,
  },
  codeEditor: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#EBEBEB',
    borderRadius: 8,
    marginTop: 15,
    overflow: 'hidden',
  },
  input: {
    flex: 1,
    paddingHorizontal: 20,
    fontFamily: 'Montserrat',
    color: '#186AB4',
  },
  nextButton: {
    backgroundColor: '#186AB4',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    width: 60,
  },
  loadingContainer: {
    justifyContent: 'center',
    height: 230,
  },
  successText: {
    textAlign: 'center',
    fontFamily: 'Montserrat',
    color: '#186AB4',
    fontSize: 16,
  },
  newCode: {
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    color: '#186AB4',
    fontSize: 20,
    marginTop: 5,
  },
});
