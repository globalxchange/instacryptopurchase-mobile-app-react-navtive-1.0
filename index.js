/**
 * @format
 */
import './shim.js';
import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import TimeAgo from 'javascript-time-ago';
import en from 'javascript-time-ago/locale/en';

TimeAgo.addDefaultLocale(en);

AppRegistry.registerComponent(appName, () => App);
